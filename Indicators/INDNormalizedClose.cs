#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Indicators in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Indicators
{
	public class INDNormalizedClose : Indicator
	{
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"Show Normalized Distribution of Close";
				Name										= "INDNormalizedClose";
				Calculate									= Calculate.OnBarClose;
				IsOverlay									= false;
				DisplayInDataBox							= true;
				DrawOnPricePanel							= true;
				DrawHorizontalGridLines						= true;
				DrawVerticalGridLines						= true;
				PaintPriceMarkers							= true;
				ScaleJustification							= NinjaTrader.Gui.Chart.ScaleJustification.Right;
				//Disable this property if your indicator requires custom values that cumulate with each new market data event. 
				//See Help Guide for additional information.
				IsSuspendedWhileInactive					= true;
				Period					= 14;
				Border					= 1.4;
				AddLine(Brushes.DarkOliveGreen, Border, "Upper");
				AddLine(Brushes.Aqua, 0, "Zero");
				AddLine(Brushes.DarkOliveGreen, -Border, "Lower");
				AddPlot(Brushes.Black, "NormarizedClose");
			}
			else if (State == State.Configure)
			{
			}
		}

		protected override void OnBarUpdate()
		{
			//Add your custom indicator logic here.
			//Plot0.Set(NormalizedClose(Period));
			Value[0] = NormalizedClose(Period);
		}
		
		public double NormalizedClose(int term){
			double result = 0.0;
			//double constant = 40.0; //DEPENDENCY on N225M YEN currency
			double constant = 0.1; //To aboid dibide by ZERO.
			double stdDev = StdDev(Close,term)[0];
			double divider = Math.Max(stdDev,constant);
			result = (Close[0] - SMA(Close,term)[0])/divider;
			return result;
		}
		
		#region Properties
		[NinjaScriptProperty]
		[Range(1, int.MaxValue)]
		[Display(Name="Period", Order=1, GroupName="Parameters")]
		public int Period
		{ get; set; }

		[NinjaScriptProperty]
		[Range(1, double.MaxValue)]
		[Display(Name="Border", Description="Breakout Border", Order=2, GroupName="Parameters")]
		public double Border
		{ get; set; }




		[Browsable(false)]
		[XmlIgnore]
		public Series<double> NormarizedClose
		{
			get { return Values[0]; }
		}
		#endregion

	}
}

#region NinjaScript generated code. Neither change nor remove.

namespace NinjaTrader.NinjaScript.Indicators
{
	public partial class Indicator : NinjaTrader.Gui.NinjaScript.IndicatorRenderBase
	{
		private INDNormalizedClose[] cacheINDNormalizedClose;
		public INDNormalizedClose INDNormalizedClose(int period, double border)
		{
			return INDNormalizedClose(Input, period, border);
		}

		public INDNormalizedClose INDNormalizedClose(ISeries<double> input, int period, double border)
		{
			if (cacheINDNormalizedClose != null)
				for (int idx = 0; idx < cacheINDNormalizedClose.Length; idx++)
					if (cacheINDNormalizedClose[idx] != null && cacheINDNormalizedClose[idx].Period == period && cacheINDNormalizedClose[idx].Border == border && cacheINDNormalizedClose[idx].EqualsInput(input))
						return cacheINDNormalizedClose[idx];
			return CacheIndicator<INDNormalizedClose>(new INDNormalizedClose(){ Period = period, Border = border }, input, ref cacheINDNormalizedClose);
		}
	}
}

namespace NinjaTrader.NinjaScript.MarketAnalyzerColumns
{
	public partial class MarketAnalyzerColumn : MarketAnalyzerColumnBase
	{
		public Indicators.INDNormalizedClose INDNormalizedClose(int period, double border)
		{
			return indicator.INDNormalizedClose(Input, period, border);
		}

		public Indicators.INDNormalizedClose INDNormalizedClose(ISeries<double> input , int period, double border)
		{
			return indicator.INDNormalizedClose(input, period, border);
		}
	}
}

namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class Strategy : NinjaTrader.Gui.NinjaScript.StrategyRenderBase
	{
		public Indicators.INDNormalizedClose INDNormalizedClose(int period, double border)
		{
			return indicator.INDNormalizedClose(Input, period, border);
		}

		public Indicators.INDNormalizedClose INDNormalizedClose(ISeries<double> input , int period, double border)
		{
			return indicator.INDNormalizedClose(input, period, border);
		}
	}
}

#endregion
