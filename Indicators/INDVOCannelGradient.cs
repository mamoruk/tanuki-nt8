#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Indicators in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Indicators
{
	public class INDVOCannelGradient : Indicator
	{
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"Gradient of Volatility Channel .  Slope of mid value.";
				Name										= "INDVOCannelGradient";
				Calculate									= Calculate.OnBarClose;
				IsOverlay									= false;
				DisplayInDataBox							= true;
				DrawOnPricePanel							= true;
				DrawHorizontalGridLines						= true;
				DrawVerticalGridLines						= true;
				PaintPriceMarkers							= true;
				ScaleJustification							= NinjaTrader.Gui.Chart.ScaleJustification.Right;
				//Disable this property if your indicator requires custom values that cumulate with each new market data event. 
				//See Help Guide for additional information.
				IsSuspendedWhileInactive					= true;
				Period					= 1;
				//AddLine(Brushes.Black, 1, "Gradient");
				AddPlot(Brushes.Black, "Gradient");
				AddLine(Brushes.Gray, 0.0, "Zero");
			}
			else if (State == State.Configure)
			{
			}
		}

		protected override void OnBarUpdate()
		{
			//Add your custom indicator logic here.
			if(CurrentBar > Period) {
				//DEBUG Print("STARTED");
				//double diff = Bollinger(1.4,14)[0] - Bollinger(1.4,14)[Period];
				double diff = SMA(14)[0] - SMA(14)[Period];
				double gladient = diff/Period;
				Value[0] = gladient;
			}else{
				//DEBUG Print("PRE");
				Value[0] = 0.0;
			}
		}
		[NinjaScriptProperty]
		[Range(1, int.MaxValue)]
		[Display(Name="Period", Order=1, GroupName="Parameters")]
		public int Period
		{ get; set; }


	}
}

#region NinjaScript generated code. Neither change nor remove.

namespace NinjaTrader.NinjaScript.Indicators
{
	public partial class Indicator : NinjaTrader.Gui.NinjaScript.IndicatorRenderBase
	{
		private INDVOCannelGradient[] cacheINDVOCannelGradient;
		public INDVOCannelGradient INDVOCannelGradient(int period)
		{
			return INDVOCannelGradient(Input, period);
		}

		public INDVOCannelGradient INDVOCannelGradient(ISeries<double> input, int period)
		{
			if (cacheINDVOCannelGradient != null)
				for (int idx = 0; idx < cacheINDVOCannelGradient.Length; idx++)
					if (cacheINDVOCannelGradient[idx] != null && cacheINDVOCannelGradient[idx].Period == period && cacheINDVOCannelGradient[idx].EqualsInput(input))
						return cacheINDVOCannelGradient[idx];
			return CacheIndicator<INDVOCannelGradient>(new INDVOCannelGradient(){ Period = period }, input, ref cacheINDVOCannelGradient);
		}
	}
}

namespace NinjaTrader.NinjaScript.MarketAnalyzerColumns
{
	public partial class MarketAnalyzerColumn : MarketAnalyzerColumnBase
	{
		public Indicators.INDVOCannelGradient INDVOCannelGradient(int period)
		{
			return indicator.INDVOCannelGradient(Input, period);
		}

		public Indicators.INDVOCannelGradient INDVOCannelGradient(ISeries<double> input , int period)
		{
			return indicator.INDVOCannelGradient(input, period);
		}
	}
}

namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class Strategy : NinjaTrader.Gui.NinjaScript.StrategyRenderBase
	{
		public Indicators.INDVOCannelGradient INDVOCannelGradient(int period)
		{
			return indicator.INDVOCannelGradient(Input, period);
		}

		public Indicators.INDVOCannelGradient INDVOCannelGradient(ISeries<double> input , int period)
		{
			return indicator.INDVOCannelGradient(input, period);
		}
	}
}

#endregion
