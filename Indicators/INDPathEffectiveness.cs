#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Indicators in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Indicators
{
	public class INDPathEffectiveness : Indicator
	{
		//IVar
		private double windowTotal = 0.0;
		
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"Enter the description for your new custom Indicator here.";
				Name										= "INDPathEffectiveness";
				Calculate									= Calculate.OnBarClose;
				IsOverlay									= false;
				DisplayInDataBox							= true;
				DrawOnPricePanel							= true;
				DrawHorizontalGridLines						= true;
				DrawVerticalGridLines						= true;
				PaintPriceMarkers							= true;
				ScaleJustification							= NinjaTrader.Gui.Chart.ScaleJustification.Right;
				//Disable this property if your indicator requires custom values that cumulate with each new market data event. 
				//See Help Guide for additional information.
				IsSuspendedWhileInactive					= true;
				Period					= 14;
				AddPlot(Brushes.Black, "PathEfectiveness");
			}
			else if (State == State.Configure)
			{
			}
		}

		protected override void OnBarUpdate()
		{
			//Add your custom indicator logic here.
			//UPdate path length total in Period
			updateWindow(Period);
			
			//Print(string.Format("CurrentBar:{0},Period:{1}",CurrentBar,Period)); //DEBUG
			if(CurrentBar > Period) {
				//DEBUG Print("STARTED");
				Value[0] = pathEffectiveness(Period);
			}else{
				//DEBUG Print("PRE");
				Value[0] = 0.0;
			}
			//Print(string.Format("Value[0]:{0}",Value[0])); //DEBUG
			//DEBUG Print("END");
		}
		private double pathEffectiveness(int period){
			double result = 0.0;
			//int startBarInd = CurrentBar - period;
			double move = Math.Abs(Close[0] - Close[period-1]);
			//double path = pathLength(period);
			double path = windowTotal;
			double distanceMove = Math.Sqrt(Math.Pow(move,2.0) + Math.Pow((double)period,2.0));
			double distancePath = Math.Sqrt(Math.Pow(path,2.0) + Math.Pow((double)period,2.0));
			double effectiveness = distanceMove/distancePath;
			//
			return effectiveness;
		}
		private double barLength(int ago){
			//return Math.Abs(Close[ago] - Open[ago]);
			return Math.Abs(High[ago] - Low[ago]);
		}
		private double pathLength(int period){
			double total = 0.0;
			foreach(int i in Enumerable.Range(0, period)){
				//Print(string.Format("CurrentBar:{0},i:{1}",CurrentBar,i)); //DEBUG
				total += barLength(i);
			}
			return total;
		}
		private void updateWindow(int period){
			//Add cumming value
			windowTotal += barLength(0);
			//Sub leving value
			if(CurrentBar > period){
				windowTotal -= barLength(period);
			}
		}
		#region Properties
		[NinjaScriptProperty]
		[Range(1, int.MaxValue)]
		[Display(Name="Period", Description="Period", Order=1, GroupName="Parameters")]
		public int Period
		{ get; set; }
		
		[Browsable(false)]
		[XmlIgnore]
		public Series<double> PathEfectiveness
		{
			get { return Values[0]; }
		}
		#endregion

	}
}

#region NinjaScript generated code. Neither change nor remove.

namespace NinjaTrader.NinjaScript.Indicators
{
	public partial class Indicator : NinjaTrader.Gui.NinjaScript.IndicatorRenderBase
	{
		private INDPathEffectiveness[] cacheINDPathEffectiveness;
		public INDPathEffectiveness INDPathEffectiveness(int period)
		{
			return INDPathEffectiveness(Input, period);
		}

		public INDPathEffectiveness INDPathEffectiveness(ISeries<double> input, int period)
		{
			if (cacheINDPathEffectiveness != null)
				for (int idx = 0; idx < cacheINDPathEffectiveness.Length; idx++)
					if (cacheINDPathEffectiveness[idx] != null && cacheINDPathEffectiveness[idx].Period == period && cacheINDPathEffectiveness[idx].EqualsInput(input))
						return cacheINDPathEffectiveness[idx];
			return CacheIndicator<INDPathEffectiveness>(new INDPathEffectiveness(){ Period = period }, input, ref cacheINDPathEffectiveness);
		}
	}
}

namespace NinjaTrader.NinjaScript.MarketAnalyzerColumns
{
	public partial class MarketAnalyzerColumn : MarketAnalyzerColumnBase
	{
		public Indicators.INDPathEffectiveness INDPathEffectiveness(int period)
		{
			return indicator.INDPathEffectiveness(Input, period);
		}

		public Indicators.INDPathEffectiveness INDPathEffectiveness(ISeries<double> input , int period)
		{
			return indicator.INDPathEffectiveness(input, period);
		}
	}
}

namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class Strategy : NinjaTrader.Gui.NinjaScript.StrategyRenderBase
	{
		public Indicators.INDPathEffectiveness INDPathEffectiveness(int period)
		{
			return indicator.INDPathEffectiveness(Input, period);
		}

		public Indicators.INDPathEffectiveness INDPathEffectiveness(ISeries<double> input , int period)
		{
			return indicator.INDPathEffectiveness(input, period);
		}
	}
}

#endregion
