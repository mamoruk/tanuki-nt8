#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Indicators in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Indicators
{
	public class INDVolocity : Indicator
	{
		//IVar
		private double windowTotal = 0.0;
		
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"Velocity = SUM(Bodylength)/BarNumner";
				Name										= "INDVolocity";
				Calculate									= Calculate.OnBarClose;
				IsOverlay									= false;
				DisplayInDataBox							= true;
				DrawOnPricePanel							= true;
				DrawHorizontalGridLines						= true;
				DrawVerticalGridLines						= true;
				PaintPriceMarkers							= true;
				ScaleJustification							= NinjaTrader.Gui.Chart.ScaleJustification.Right;
				//Disable this property if your indicator requires custom values that cumulate with each new market data event. 
				//See Help Guide for additional information.
				IsSuspendedWhileInactive					= true;
				Period					= 3;
				AddPlot(Brushes.Black, "Velocity");
			}
			else if (State == State.Configure)
			{
			}
		}

		protected override void OnBarUpdate()
		{
			//Add your custom indicator logic here.
			if(CurrentBar > Period) {
				//DEBUG Print("STARTED");
				Value[0] = Velocity(Period);
			}else{
				//DEBUG Print("PRE");
				Value[0] = 0.0;
			}
		}
		private double Velocity(int period){
			return bodyTotal(period)/period;
		}
		private double bodyLength(int ago){
			//return Math.Abs(Close[ago] - Open[ago]);
			return Math.Abs(High[ago] - Low[ago]);
		}
		private double bodyTotal(int period){
			double total = 0.0;
			foreach(int i in Enumerable.Range(0, period)){
				//Print(string.Format("CurrentBar:{0},i:{1}",CurrentBar,i)); //DEBUG
				total += bodyLength(i);
			}
			return total;
		}
		#region Properties
		[NinjaScriptProperty]
		[Range(1, int.MaxValue)]
		[Display(Name="Period", Description="Period", Order=1, GroupName="Parameters")]
		public int Period
		{ get; set; }
		#endregion

	}
}

#region NinjaScript generated code. Neither change nor remove.

namespace NinjaTrader.NinjaScript.Indicators
{
	public partial class Indicator : NinjaTrader.Gui.NinjaScript.IndicatorRenderBase
	{
		private INDVolocity[] cacheINDVolocity;
		public INDVolocity INDVolocity(int period)
		{
			return INDVolocity(Input, period);
		}

		public INDVolocity INDVolocity(ISeries<double> input, int period)
		{
			if (cacheINDVolocity != null)
				for (int idx = 0; idx < cacheINDVolocity.Length; idx++)
					if (cacheINDVolocity[idx] != null && cacheINDVolocity[idx].Period == period && cacheINDVolocity[idx].EqualsInput(input))
						return cacheINDVolocity[idx];
			return CacheIndicator<INDVolocity>(new INDVolocity(){ Period = period }, input, ref cacheINDVolocity);
		}
	}
}

namespace NinjaTrader.NinjaScript.MarketAnalyzerColumns
{
	public partial class MarketAnalyzerColumn : MarketAnalyzerColumnBase
	{
		public Indicators.INDVolocity INDVolocity(int period)
		{
			return indicator.INDVolocity(Input, period);
		}

		public Indicators.INDVolocity INDVolocity(ISeries<double> input , int period)
		{
			return indicator.INDVolocity(input, period);
		}
	}
}

namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class Strategy : NinjaTrader.Gui.NinjaScript.StrategyRenderBase
	{
		public Indicators.INDVolocity INDVolocity(int period)
		{
			return indicator.INDVolocity(Input, period);
		}

		public Indicators.INDVolocity INDVolocity(ISeries<double> input , int period)
		{
			return indicator.INDVolocity(input, period);
		}
	}
}

#endregion
