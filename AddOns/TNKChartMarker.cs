//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Collections.Generic;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//
		// Mark Allow at candles
		//
		public void MarkAllowWithCandles(string direc, string tagBase, double offset, Brush color, CandleList cl, string valName){
			string tag, tag0 = tagBase +direc+"@";
			//double offset = TickSize;
			int ago;
			double val;
			foreach(Candle c in cl.Candles){
				tag = tag0 + c.BarNumber.ToString();
//				ago = CurrentBar - n;
				ago = NumToAgo(c.BarNumber);
				val = c.ValueByName(valName);
				switch(direc){
					case "UP":
						Draw.ArrowUp(this, tag, true, ago, val + offset, color);
						//Draw.Dot(this, tag2, true, ago, Close[ago] + offset, color);
						//Draw.Text(this, "PeakREC"+tag+CurrentBar.ToString(), CurrentBar.ToString(), ago, val + offset);
						break;
					case "DOWN":
						Draw.ArrowDown(this, tag, true, ago, val - offset, color);
						//Draw.Dot(this, tag2, true, ago, Close[ago] - offset, color);
						//Draw.Text(this, "PeakREC"+tag+CurrentBar.ToString(), CurrentBar.ToString(), ago, val - offset);
						break;
					default:
						break;
						LogError("MarkAllowAtClose",MemoNS("Illegal direc",direc));
				}
			}
		}
		//
		// Mark Allow at close
		//
		public void MarkAllowAtClose(string direc, string tagBase, double offset, Brush color, int[] barNums){
			string tag, tag0 = tagBase +direc+"@";
			//double offset = TickSize;
			int ago;
			foreach(int n in barNums){
				tag = tag0 + n.ToString();
				ago = CurrentBar - n;
				switch(direc){
					case "UP":
						Draw.ArrowUp(this, tag, true, ago, Close[ago] + offset, color);
						//Draw.Dot(this, tag2, true, ago, Close[ago] + offset, color);
						break;
					case "DOWN":
						Draw.ArrowDown(this, tag, true, ago, Close[ago] - offset, color);
						//Draw.Dot(this, tag2, true, ago, Close[ago] - offset, color);
						break;
					default:
						break;
						LogError("MarkAllowAtClose",MemoNS("Illegal direc",direc));
				}
			}
		}
		//
		// Mark regression line
		//  Input: Coefficents: a0,a1  origin of Coe: origX   Mark Range: startAgo,  endAgo
		public void MarkReglessionLine2(RegressionLine rline,string baseTag,int startBarnum, int endBarnum){
			string tag = "RegLine"+ baseTag + CurrentBar.ToString();
			double startX = (float)(startBarnum - rline.OrigBarnum);   //Orign X
			double endX   = (float)(endBarnum - rline.OrigBarnum);
			double startY = rline.ValueForX(startX);
			double endY   = rline.ValueForX(endX);		
			//Draw.Line(this, tag, true, NumToAgo(start) ,startY, NumToAgo(end) ,endY, Brushes.LimeGreen, DashStyleHelper.Dot, 2);
			Draw.Line(this, tag, true, NumToAgo(startBarnum) ,startY, NumToAgo(endBarnum) ,endY, Brushes.Gray, DashStyleHelper.DashDot, 1);
			string label = string.Format("{0:f2}:{1:f2}",rline.B,rline.R2);
			Draw.Text(this, "A+R2"+baseTag+CurrentBar.ToString(), label, NumToAgo(endBarnum), endY);
		}
		//
		// Mark regression line
		//  Input: Coefficents: a0,a1  origin of Coe: origX   Mark Range: startAgo,  endAgo
//		public void MarkReglessionLine(double[] coe, string baseTag, int origX, int start, int end){
//			string tag = "RegLine"+ baseTag + CurrentBar.ToString();
//			double startX = (float)(start - origX);
//			double endX   = (float)(end -origX);
//			double startY = LinerFunc(coe[0], coe[1], startX);
//			double endY   = LinerFunc(coe[0], coe[1], endX);			
//			//Draw.Line(this, tag, true, NumToAgo(start) ,startY, NumToAgo(end) ,endY, Brushes.LimeGreen, DashStyleHelper.Dot, 2);
//			Draw.Line(this, tag, true, NumToAgo(start) ,startY, NumToAgo(end) ,endY, Brushes.Gray, DashStyleHelper.DashDot, 1);
//		}
		//
		// Mark regression line
		//  Input: Coefficents: a0,a1  origin of Coe: origX   Mark Range: startAgo,  endAgo
		public void MarkReglessionLineAgo(double[] coe, int origX, int startAgo, int endAgo){
			string tag = "RegressionLine"+ CurrentBar.ToString();
			double startX = (float)(origX - startAgo);
			double endX   = (float)(origX - endAgo);
			double startY = LinerFunc(coe[0], coe[1], startX);
			double endY   = LinerFunc(coe[0], coe[1], endX);			
			Draw.Line(this, tag, true, startAgo,startY, endAgo,endY, Brushes.LimeGreen, DashStyleHelper.DashDot, 1);
		}
		
		//Mark Holizontal Line
//NT7	public void _NT_MarkHolizontalLine(double y, Color color ){
//			string tag = "Line"+TNKGetUniqNumber().ToString()+"@"+CurrentBar.ToString();
//			//DashStyle style = DashStyle.Dash;
//			DashStyle style = DashStyle.Dot;			
//			DrawLine(tag, false, 1, y, 0, y, color, style, 3);
//NT7	}

		public void MarkHolizontalLine(double y, Brush brush ){
			string tag = "Line"+TNKGetUniqNumber().ToString()+"@"+CurrentBar.ToString();
			//Brush myBrush = new SolidColorBrush(Color.FromRgb(56, 120, 153));
			DashStyleHelper style = DashStyleHelper.Dot;
			Draw.Line(this, tag, false, 1, y, 0, y, brush, style, 3);
		}
		
		//Mark Trend as Color
//NT7	public void _NT7_MarkTrendAsColor(string trendName){
//			Color trendColor;
//			if (trendName=="UNDFINED"){
//				trendColor = Color.Empty;
//			}else{
//				trendColor = TNKTrendColorDict[trendName];
//			};
//			BackColor = trendColor;
//NT7	}
		public void MarkTrendAsColor(string trendName){
			Brush trendBrush;
			if (trendName=="UNDFINED"){
				//NT7 trendColor = Color.Empty;
				trendBrush = null;
			}else{
				trendBrush = TNKTrendColorDict[trendName];
			};
			BackBrush = trendBrush;
		}
		
		public void MarkInitialTerm(bool inTerm){
			if(inTerm){
				BackBrush = Brushes.PaleTurquoise;
			}else{
				BackBrush = null;
			}
		}
		
		//Mark Dot
		public void MarkDotOnChart(double ypos){
			string tag = "Dot"+TNKGetUniqNumber().ToString();
			MarkDotOnChartWithTag(ypos,tag);
		}
//NT7
//		public void _NT7_MarkDotOnChartWithTag(double ypos,string pTag){
//			Color c = Color.SpringGreen;
//			MarkDotOnChartWithTagAndColor(ypos, pTag, c);
//		}
//NT7
		public void MarkDotOnChartWithTag(double ypos,string pTag){
			Brush c = Brushes.SpringGreen;
			MarkDotOnChartWithTagAndColor(ypos, pTag, c);
		}
//NT7
//		public void _NT7_MarkDotOnChartWithTagAndColor(double ypos,string pTag, Color c){
//			//DrawDot(string tag, bool autoScale, int barsAgo, double y, Color color)
//			string tag = pTag+"@"+CurrentBar.ToString();
//			DrawDot(tag,true,0, ypos, c); //2nd parameter is autoScale
//		}
//NT7
		public void MarkDotOnChartWithTagAndColor(double ypos,string pTag, Brush c){
			//DrawDot(string tag, bool autoScale, int barsAgo, double y, Brush color)
			string tag = pTag+"@"+CurrentBar.ToString();
			Draw.Dot(this, tag,true,0, ypos, c); //2nd parameter is autoScale
		}
		//Mark Label
		public void MarkLabelOnChart(string label, double ypos){
			if(ypos==0.0) return;
			//DrawText(string tag, string text, int barsAgo, double y, Color color)
			MarkLabelOnChart(label,ypos,0);
		}
//NT7		
//		public void _NT7_MarkLabelOnChart(string label, double ypos,int offset ){
//			if(ypos==0.0) return;
//			//DrawText(string tag, bool autoScale, string text, int barsAgo, double y, 
//			//         int yPixelOffset, Color textColor, Font font, StringAlignment alignment,
//			//         Color outlineColor, Color areaColor, int areaOpacity)
//			DrawText(CurrentBar.ToString()+"Label"+TNKGetUniqNumber().ToString(), true, label, 0, ypos,
//			        offset, Color.Black, new Font("Arial", 7), StringAlignment.Near,
//			         Color.Empty, Color.White, 0);
//		}
//NT7		
		public void MarkLabelOnChart(string label, double ypos,int offset ){
			if(ypos==0.0) return;
			string tag = CurrentBar.ToString()+"Label"+TNKGetUniqNumber().ToString();
			//Draw.Text(NinjaScriptBase owner, string tag, bool isAutoScale, string text, int barsAgo, double y, 
			// int yPixelOffset, Brush textBrush, SimpleFont font, TextAlignment alignment, 
			// Brush outlineBrush, Brush areaBrush, int areaOpacity)
			Draw.Text(this, tag, true, label, 0, ypos,
			        offset, Brushes.Black, new SimpleFont("Arial", 7), TextAlignment.Left,
			        null, Brushes.White, 0);
		}


		//Mark Diamond
//NT7
//		public void _NT_MarkDiamondOnChartWithTagAndColor(double ypos, string pTag, Color c){
//			if(ypos==0.0) return;
//			string tag = pTag+"@"+CurrentBar.ToString();
//			DrawDiamond(tag, true, 0, ypos, c); //2nd parameter is autoScale			
//		}
//NT7
		public void MarkDiamondOnChartWithTagAndColor(double ypos, string pTag, Brush c){
			if(ypos==0.0) return;
			string tag = pTag+"@"+CurrentBar.ToString();
			Draw.Diamond(this, tag, true, 0, ypos, c); //2nd parameter is autoScale			
		}
		public void MarkDiamondOnChartWithTag(double ypos, string pTag){
			if(ypos==0.0) return;
			MarkDiamondOnChartWithTagAndColor(ypos, pTag, Brushes.Blue);
		}		
		public void MarkDiamondOnChart(double ypos){
			if(ypos==0.0) return;
			string tag = "Diamond"+ TNKGetUniqNumber().ToString();
			MarkDiamondOnChartWithTag(ypos,tag);
		}
		
		//Count up if draw base line is same to privious draw. 
		int chartMarkPositionCount(double baseline){
//			double minimalDelta = 0.005; //% to baseline of sleshold for identify change.
//			double delta = Math.Abs(baseline - TNKChartMarkPrevioudBL)/TNKChartMarkPrevioudBL;
			double minimalDelta = TickSize;
			double delta = Math.Abs(baseline - TNKChartMarkPrevioudBL);
			if(delta < minimalDelta){
				TNKChartMarkPositionCount += 1;
			} else {
				TNKChartMarkPositionCount = 0;
			}
			TNKChartMarkPrevioudBL = baseline;
			return TNKChartMarkPositionCount;
		}
		

//		List<string>TNKDisplayConditionsOnChart = new List<string>(){
//		};
//
//		List<string>TNKHideConditionsOnChart = new List<string>(){
//		};

//		List<string>TNKOmittedConditionsOnChart = new List<string>(){
//			"RANGE:NOEVENT","UP:NOEVENT","DOWN:NOEVENT","UPRET:NOEVENT","DOWNRET:NOEVENT"
//		};
		List<string>TNKOmittedConditionsOnChart = new List<string>(){
			"RANGE:NOEVENT","UPRET:NOEVENT","DOWNRET:NOEVENT"
		};
		
		public void MarkEventNameOnChart(string conditionName){
			MarkEventNameOnChart(conditionName, conditionName, Brushes.Blue);
		}
		////HOT: ISSUE:
		public void MarkEventNameOnChart(string conditionName, string dTag, Brush dColor){
			double labelPosition = 0.0;
			double diamoundPosition = 0.0;
			double offset = 0.003;
			double baseline = 0.0;
			int offsetPixUnit = 10;
			int offsetPix = 0;
			double resistLevel  = TNKRecentPeakValue;
			double supportLevel = TNKRecentBottomValue;

			// Pass to mark Ommit events
			if (TNKOmittedConditionsOnChart.Contains(conditionName)){return;}
			// Check Display or Hide the condtionName
			if (!DoesPassWBList(conditionName,TNKDisplayConditionsOnChart,TNKHideConditionsOnChart)){return;}
			//
			string stateName = StateNameOf(conditionName);
			string stateSide = StateSide(stateName);
			string eventName = EventNameOf(conditionName);
			string eventSide = EventSide(eventName);
			string eventCategory = EventCategory(eventName);

			LogDebug("MarkEventNameOnChart()Entry",MemoNS("conditionName",conditionName),
				MemoNS("stateName",stateName),MemoNS("stateSide",stateSide),
				MemoNS("eventName",eventName),MemoNS("eventSide",eventSide),
				MemoNS("dTag",dTag),MemoNS("dColor",dColor.ToString())
			);
			string offsetSide = eventSide;
			switch(eventSide){
				case "UPSIDE":
					baseline = resistLevel; //Swing(TNKSwingPeriod).SwingHigh[0];
					switch(eventCategory){
						case "BREAK":
							break;
						case "UPDATE":
							break;
						case "NOEVENT":
							break;
						default:
							LogError("MarkEventNameOnChart()",MemoNS("Unknown eventCategory",eventCategory));
							break;
					}
					break;
				case "DOWNSIDE":
					baseline = supportLevel; //Swing(TNKSwingPeriod).SwingLow[0];
					switch(eventCategory){
						case "BREAK":
							break;
						case "UPDATE":
							break;
						case "NOEVENT":
							break;
						default:
							LogError("MarkEventNameOnChart()",MemoNS("Unknown eventCategory",eventCategory));
							break;
					}
					break;
				case "NOSIDE":
					offsetSide = stateSide; //Change offset side based on state side
					baseline = Close[0];
					switch(stateSide){
						case "UPSIDE":
							break;
						case "DOWNSIDE":
							break;
						case "NOSIDE":
							offsetSide = "UPSIDE";
							break;
						default:
							offsetSide = "UPSIDE";
							LogError("MarkEventNameOnChart()",MemoNS("Illigal stateSide",stateSide));
							break;
					}
					break;
				default:
					LogError("MarkEventNameOnChart()",MemoNS("Illigal eventSide",eventSide));
					break;
			}
			//Determin Draw position
			offsetPix = offsetPixUnit*(chartMarkPositionCount(baseline)+1);
			labelPosition = baseline;
			diamoundPosition = baseline;			
			switch(offsetSide){
				case "UPSIDE":
					break;
				case "DOWNSIDE":
					offsetPix = -offsetPix;
					break;
				case "NOSIDE":
					break;
				default:
					break;
			}

			//Draw Markers
			//MarkLabelOnChart(conditionName,pos);
			string displayEventName = DisplayEventName(conditionName);
			MarkLabelOnChart(conditionName,labelPosition,offsetPix);
			//MarkLabelOnChart(displayEventName,labelPosition,offsetPix);
			MarkDiamondOnChartWithTagAndColor(diamoundPosition, dTag, dColor);
			LogDebug("MarkEventNameOnChart()",
				MemoNS("conditionName",conditionName),
				//MemoNS("displayEventName",displayEventName),
				MemoNS("dTag",dTag),MemoNS("dColor",dColor.ToString()),
				MemoND("labelPosition",labelPosition),MemoND("diamoundPosition",diamoundPosition)
			);
		}
    }
}
