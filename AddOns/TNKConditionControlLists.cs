//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//
		// Condition Signal Dictionary
		//
		private Dictionary<string,List<string>>TNKEventHandlerNameDict=new Dictionary<string,List<string>>(){};

		//
		// Handler Execution
		//
//		List<string> Inhibit_Trend_Only = new List<string>(){
//			"RANGE:PEAKUP",
//			"RANGE:PEAKDOWN",
//			"RANGE:BOTTOMUP",
//			"RANGE:BOTTOMDOWN"
//		};
//		// Allowed Condition list to exectute handler
//		List<string> TNKEnabledConditions = new List<string>(){};		
//		// Disallowed Condition list to execute handler
//		List<string> TNKDisabledConditions = new List<string>(){};
		
		// Allowed Event list to exectute handler
		List<string> TNKEnabledEvents = new List<string>(){};		
		// Disallowed Event list to execute handler
		List<string> TNKDisabledEvents = new List<string>(){};		
		//
		// Marking on Chart
		//
		// Display Condition list on chart
		List<string>TNKDisplayConditionsOnChart = new List<string>(){};
		// Hide Condition list on chart
		List<string>TNKHideConditionsOnChart = new List<string>(){};

		//
		// Condition Catalogs
		//
		List<string>CCEmpty=new List<string>(){};
		//Define Conditions hide on chart
		List<string> CC_HideOnChart = new List<string>(){
			"RANGE:PEAKREVERSAL","RANGE:BOTTOMREVERSAL","UP:NOEVENT","DOWN:NOEVENT"
		};
		//Define Condition Action Dictionary
		// FULL Condition List
		//  Initialized in InitUtil()
		List<string> CC_All_Conditions;
		
		// Genarated from StateTransitionTable081.txt
		List<string> CC_N225M_Standard_Short = new List<string>(){
			"RANGE:PEAKUP",
			"RANGE:PEAKDOWN",
			"RANGE:BOTTOMBREAKDOWN",
			"UPRET:BOTTOMBREAKDOWN",
			"DOWN:NOEVENT",
			"DOWNRET:PEAKDOWN",
			"DOWNRET:BOTTOMBREAKDOWN"
		};
		List<string> CC_N225M_Standard_Long = new List<string>(){
			"RANGE:BOTTOMUP",
			"RANGE:BOTTOMDOWN",
			"RANGE:PEAKBREAKUP",
			"UP:NOEVENT",
			"UPRET:BOTTOMUP",
			"UPRET:PEAKBREAKUP",
			"DOWNRET:PEAKBREAKUP"
		};
		
		// Marker Condition Collection
		List<string> CC_N225M_Marker_Sell = new List<string>(){
			"RANGE:BOTTOMBREAKDOWN",
			"UPRET:BOTTOMBREAKDOWN",
			"DOWN:NOEVENT",
			"DOWNRET:BOTTOMBREAKDOWN"
		};
		List<string> CC_N225M_Marker_Buy = new List<string>(){
			"RANGE:PEAKBREAKUP",
			"UP:NOEVENT",
			"UPRET:PEAKBREAKUP",
			"DOWNRET:PEAKBREAKUP"
		};
		// Trend Direction Condition Collection
		List<string> CC_N225M_TD_BuyOnUpSellOnUnclear = new List<string>(){
			"RANGE:PEAKBREAKUP",
			"UPRET:PEAKBREAKUP",
			"DOWN:PEAKBREAKUP",
			"DOWNRET:PEAKBREAKUP"
		};
		List<string> CC_N225M_TD_SellOnDownBuyOnUnclear = new List<string>(){
			"RANGE:BOTTOMBREAKDOWN",
			"UPRET:BOTTOMBREAKDOWN",
			"DOWN:BOTTOMBREAKDOWN",
			"DOWNRET:BOTTOMBREAKDOWN",
		};
		List<string> CC_N225M_TD_BuyOnUp = new List<string>(){
			"UP:NOEVENT"
		};
		List<string> CC_N225M_TD_SellOnDown = new List<string>(){
			"DOWN:NOEVENT"
		};
		
		// Condition catelog for N225 trade trend adaprive trede system.
		List<string>CC_N225M_TP_SellInBOX_BuyInTrend = new List<String>(){
			"RANGE:PEAKBREAKUP"
		};
		List<string>CC_N225M_TP_BuyInBOX_SellInTrend = new List<String>(){
			"RANGE:BOTTOMBREAKDOWN"
		};
		List<string>CC_N225M_TP_BuyInBOX    = new List<String>(){
			"RANGE:BOTTOMDOWN","RANGE:BOTTOMUP","DOWN:BOTTOMBREAUP"
		};
		List<string>CC_N225M_TP_SellInBOX   = new List<String>(){
			"RANGE:PEAKUP", "RANGE:PEAKDOWN","UP:PEAKBREAKDOWN"
		};
		List<string>CC_N225M_TP_BuyInTrend  = new List<String>(){
			"UP:NOEVENT","UPRET:PEAKBREAKUP","UPRET:BOTTOMUP"
		};
		List<string>CC_N225M_TP_SellInTrend = new List<String>(){
			"DOWN:NOEVENT","DOWNRET:BOTTOMBREAKDOWN","DOWNRET:PEAKDOWN"
		};
		
		// Condition catelog for N225 marker system with  MarketCycle
		List<string>CC_N225M_CY_BuyOnFloor    = new List<String>(){
			"RANGE:BOTTOMDOWN","RANGE:BOTTOMUP","DOWN:BOTTOMBREAUP"
		};
		List<string>CC_N225M_CY_SellOnRoof   = new List<String>(){
			"RANGE:PEAKUP", "RANGE:PEAKDOWN","UP:PEAKBREAKDOWN"
		};
		List<string>CC_N225M_CY_SellOnRoof_BuyOnFloor = new List<String>(){
		};
		List<string>CC_N225M_CY_SellOnRoof_BuyOnFloor_ReverseInTrend = new List<String>(){
		};
		
		//
		// Event Catalogs
		//
		//Verification TWD
		List<string>EC_N225M_VERIF_TWD = new List<String>(){
			"EN_VERIF_SESSION_BIGINNING"
		};		
		//Volatility Channel Brakout 
		List<string>EC_N225M_VCBO_SimpleBuyAndSell = new List<String>(){
			"EN_VCBO_UPPERBREAKUP","EN_VCBO_LOWERBREAKDOWN"
		};
		//Initial Range BOWOBB Breakout
		List<string>EC_N225M_IRBOWOBB_SimpleBuyAndSell = new List<String>(){
			"EN_IRBOWOBB_UPPER_BREAKUP","EN_IRBOWOBB_LOWER_BREAKDOWN"
		};
		//Initial Range Breakout
		List<string>EC_N225M_IRB_SimpleBuyAndSell = new List<String>(){
			"EN_IRB_UPPER_BREAKUP","EN_IRB_LOWER_BREAKDOWN","EN_IR_UP_MOMENTUM","EN_IR_DOWN_MOMENTUM"
		};
		//Voratility SMA  Cross
		List<string>EC_N225M_VAC_SimpleBuyAndSell = new List<String>(){
			"EN_VAC_HIGH_CROSS_UP","EN_VAC_LOW_CROSS_DOWN","EN_VAC_UP_MOMENTUM","EN_VAC_DOWN_MOMENTUM"
		};
		//Normalized Breakout
		List<string>EC_N225M_NB_SimpleBuyAndSell = new List<String>(){
			"EN_NB_UPPERBREAKUP","EN_NB_LOWERBREAKDOWN","EN_NB_UPMOMENTUM", "EN_NB_DOWNMOMENTUM"
		};
		//Big Bar
		List<string>EC_N225M_BB_CounterBuyAndSell = new List<String>(){
			"EN_BB_BIGNEGATIVEBAR","EN_BB_BIGPOSITIVEBAR"
		};
		//Continuous Direction Bars
		List<string>EC_N225M_CD_SimpleBuyAndSell = new List<String>(){
			"EN_CD_CONTINUOUS_POSITIVE","EN_CD_CONTINUOUS_NEGATIVE"
		};
		//
		List<string>EC_N225M_BB_QucikRegistance = new List<String>(){
			"EN_BB_BIGNEGATIVEBAR","EN_BB_BIGPOSITIVEBAR"
		};
		//Devil's Money Colletcion
		List<string>EC_N225N_DC_Getback = new List<String>(){
			"EN_DC_UP","EN_DC_DOWN"
		};
		//
		// Set up Condtion Contorl lists 
		//	 Call from init()
		//
		public void TNKSetupConditionControl(){
			//
			//
			// Verification TWD (Trade Week of Day)
			Dictionary<string,List<string>> EHD_N225M_Verification_TWD =new Dictionary<string,List<string>>(){
				{"EHG_VERIF_TWD", EC_N225M_VERIF_TWD}  //for v3.9.16 verification  2019/05/30
			};			
			// Volatility Channel Breakout
			Dictionary<string,List<string>> EHD_N225M_VolatilityChannelBreakout =new Dictionary<string,List<string>>(){
				{"EHG_VCBO_SimpleBuyAndSell", EC_N225M_VCBO_SimpleBuyAndSell}  //for v3.9.x  2018/12/06
			};
			
			// Initial Range Breakout
			Dictionary<string,List<string>> EHD_N225M_InitialRangeBOWOBB =new Dictionary<string,List<string>>(){
				{"EHG_IRBOWOBB_SimpleBuyAndSell", EC_N225M_IRBOWOBB_SimpleBuyAndSell}  //for v3.8.0  2017/11/29
			};
			
			// Initial Range Breakout
			Dictionary<string,List<string>> EHD_N225M_InitialRangeBreakout =new Dictionary<string,List<string>>(){
				{"EHG_IRB_SimpleBuyAndSell", EC_N225M_IRB_SimpleBuyAndSell}  //for v3.5.0  2017/11/29
			};

			// Cross Volatility band and SimpleMA
			Dictionary<string,List<string>> EHD_N225M_CrossVAandSMA =new Dictionary<string,List<string>>(){
				{"EHG_VAC_VBCrossSMA", EC_N225M_VAC_SimpleBuyAndSell}  //for v3.4.0  2017/08/24
			};
			
			// Norimalized Breakout
			Dictionary<string,List<string>> EHD_N225M_NormalizedBreakout =new Dictionary<string,List<string>>(){
				{"EHG_NB_TrendSensitiveBuyAndSell", EC_N225M_NB_SimpleBuyAndSell},  //v0.1  2016/09/13
//				{"EHG_NB_SimpleBuyAndSell",         EC_N225M_NB_SimpleBuyAndSell},  //for v2.4.1
				{"EHG_BB_CounterBuyAndSell",        EC_N225M_BB_CounterBuyAndSell}, //for v3.1.0
				{"EHG_CD_SimpleBuyAndSell",			EC_N225M_CD_SimpleBuyAndSell},  //for v3.2.0
				{"EHG_DC_Getback",                  EC_N225N_DC_Getback}
			};

			//EHG version of N225 Marker base trade system with Trend Direction UP,DOWN,UNCLEAR
			Dictionary<string,List<string>>　EHD_N225M_Marker_TrendDirection =new Dictionary<string,List<string>>(){
				{"EHG_TD_BuyOnUpSellOnUnclear",    CC_N225M_TD_BuyOnUpSellOnUnclear},
				{"EHG_TD_SellOnDownBuyOnUnclear",  CC_N225M_TD_SellOnDownBuyOnUnclear},
				{"EHG_TD_BuyOnUp",                 CC_N225M_TD_BuyOnUp},
				{"EHG_TD_SellOnDown" ,             CC_N225M_TD_SellOnDown}
			};
			
			//!!!
			//Below CHD_* Dictonaries are traditional. These are going to be obsoleted.
			//!!!
			//N225 Standard scenario system
			Dictionary<string,List<string>>　CHD_N225M_STANDARD =new Dictionary<string,List<string>>(){
				{"CHG_SIMPLE_LONG",  CC_N225M_Standard_Long},
				{"CHG_SIMPLE_SHORT", CC_N225M_Standard_Short}
			};
			
			//N225 Marker base trade system
			Dictionary<string,List<string>>　CHD_N225M_Marker =new Dictionary<string,List<string>>(){
				{"CHG_SIMPLE_LONG",  CC_N225M_Marker_Buy},
				{"CHG_SIMPLE_SHORT", CC_N225M_Marker_Sell}
			};

			//N225 Maker base trade system with Trend Direction UP,DOWN,UNCLEAR
			Dictionary<string,List<string>>　CHD_N225M_Marker_TrendDirection =new Dictionary<string,List<string>>(){
				{"CHG_TD_BuyOnUpSellOnUnclear",    CC_N225M_TD_BuyOnUpSellOnUnclear},
				{"CHG_TD_SellOnDownBuyOnUnclear",  CC_N225M_TD_SellOnDownBuyOnUnclear},
				{"CHG_TD_BuyOnUp",                 CC_N225M_TD_BuyOnUp},
				{"CHG_TD_SellOnDown" ,             CC_N225M_TD_SellOnDown}
			};

//			Dictionary<string,List<string>>　CHD_N225M_Marker_LongRangeTrend =new Dictionary<string,List<string>>(){
//				{"CHG_LR_BuyOnUp", CC_N225M_Marker_Buy},
//				{"CHG_LR_SellOnDown" , CC_N225M_Marker_Sell}
//			};
			
			//N225 Marker base system with Trend Presence TREND,BOX,UNCLEAR
			Dictionary<string,List<string>>　CHD_N225M_Marker_TrendPresence =new Dictionary<string,List<string>>(){
				{"CHG_TP_BuyInBox_SellInTrend", CC_N225M_TP_BuyInBOX_SellInTrend},
				{"CHG_TP_SellInBox_BuyInTrend", CC_N225M_TP_SellInBOX_BuyInTrend},
				{"CHG_TP_BuyInBox",             CC_N225M_TP_BuyInBOX},
				{"CHG_TP_SellInBox",            CC_N225M_TP_SellInBOX},
				{"CHG_TP_BuyInTrend",           CC_N225M_TP_BuyInTrend},
				{"CHG_TP_SellInTrend",          CC_N225M_TP_SellInTrend}
			};
			
			//N225 Cycle adaptive tarade system
			Dictionary<string,List<string>>　CHD_N225M_Cycle_Adaptive =new Dictionary<string,List<string>>(){ 
				{"CHG_SellOnRoof_BuyOnFloor",               CC_All_Conditions}
			};
//			Dictionary<string,List<string>>　CHD_N225M_Marker_MarketCycle =new Dictionary<string,List<string>>(){ 
//				{"CHG_CY_BuyOnFloor",                          CC_N225M_CY_BuyOnFloor},
//				{"CHG_CY_SellOnRoof",                          CC_N225M_CY_SellOnRoof},
//				{"CHG_CY_SellOnRoof_BuyOnFloor",               CC_N225M_CY_SellOnRoof_BuyOnFloor},
//				{"CHG_CY_SellOnRoof_BuyOnFloor_RevrseInTrend", CC_N225M_CY_SellOnRoof_BuyOnFloor_ReverseInTrend}
//			};

			TNKEventHandlerNameDict = EHD_N225M_VolatilityChannelBreakout;  //Ver 3.9.16   SIGNIFICANT Release
//			TNKEventHandlerNameDict = EHD_N225M_Verification_TWD;	//Just Varidation	
//			TNKEventHandlerNameDict = EHD_N225M_InitialRangeBOWOBB;
//			TNKEventHandlerNameDict = EHD_N225M_InitialRangeBreakout;
//			TNKEventHandlerNameDict = EHD_N225M_CrossVAandSMA;
//			TNKEventHandlerNameDict = EHD_N225M_NormalizedBreakout;			
//			TNKEventHandlerNameDict = EHD_N225M_Marker_TrendDirection;
//			TNKEventHandlerNameDict = CHD_N225M_STANDARD;
//			TNKEventHandlerNameDict = CHD_N225M_Marker;
//			TNKEventHandlerNameDict = CHD_N225M_Marker_TrendDirection;
//			TNKEventHandlerNameDict = CHD_N225M_Marker_TrendPresence;
//			TNKEventHandlerNameDict = CHD_N225M_Marker_MarketCycle;
			
			//
			//TNKDisabledConditions      = CC_NKY_BadParfomance;
			TNKHideConditionsOnChart   = CC_HideOnChart;
			//TNKEnabledConditions        = CC_NKY_SingleCondition;
			//TNKDisplayConditionsOnChart = CC_NKY_SingleCondition;
			//
			// Log setups
			LogMessage("TNKSetupConditionControl()",MemoNS("TNKEnabledEvents",StringOfList(TNKEnabledEvents)));
			LogMessage("TNKSetupConditionControl()",MemoNS("TNKDisabledEvents",StringOfList(TNKDisabledEvents)));
			LogMessage("TNKSetupConditionControl()",MemoNS("TNKDisplayConditionsOnChart",StringOfList(TNKDisplayConditionsOnChart)));
			LogMessage("TNKSetupConditionControl()",MemoNS("TNKHideConditionsOnChart",StringOfList(TNKHideConditionsOnChart)));
			LogMessage("TNKSetupConditionControl()",MemoNS("TNKEventHandlerNameDict",StringOfDictionary(TNKEventHandlerNameDict)));
		}
		
		//Define StateTransition Dictionary
		// Genarated from /Users/mamoru/tanuki/NinjaTrader/MiscText/stt091.txt with sourceGen2.rb
		Dictionary<string,string> TNKStateTransitionDict =
		new Dictionary<string,string>(){
			{"RANGE:NOEVENT","RANGE"},
			{"RANGE:PEAKUP","RANGE"},
			{"RANGE:PEAKDOWN","RANGE"},
			{"RANGE:BOTTOMUP","RANGE"},
			{"RANGE:BOTTOMDOWN","RANGE"},
			{"RANGE:PEAKBREAKUP","UP"},
			{"RANGE:PEAKBREAKDOWN","RANGE"},
			{"RANGE:PEAKREVERSAL","RANGE"},
			{"RANGE:BOTTOMBREAKUP","RANGE"},
			{"RANGE:BOTTOMBREAKDOWN","DOWN"},
			{"RANGE:BOTTOMREVERSAL","RANGE"},
			{"UP:NOEVENT","UP"},
			{"UP:PEAKUP","UPRET"},
			{"UP:PEAKDOWN","UPRET"},
			{"UP:BOTTOMUP","UP"},
			{"UP:BOTTOMDOWN","RANGE"},
			{"UP:PEAKBREAKUP","UP"},
			{"UP:PEAKBREAKDOWN","RANGE"},
			{"UP:PEAKREVERSAL","RANGE"},
			{"UP:BOTTOMBREAKUP","RANGE"},
			{"UP:BOTTOMBREAKDOWN","DOWN"},
			{"UP:BOTTOMREVERSAL","UP"},
			{"UPRET:NOEVENT","UPRET"},
			{"UPRET:PEAKUP","UPRET"},
			{"UPRET:PEAKDOWN","RANGE"},
			{"UPRET:BOTTOMUP","UP"},
			{"UPRET:BOTTOMDOWN","RANGE"},
			{"UPRET:PEAKBREAKUP","UP"},
			{"UPRET:PEAKBREAKDOWN","RANGE"},
			{"UPRET:PEAKREVERSAL","RANGE"},
			{"UPRET:BOTTOMBREAKUP","RANGE"},
			{"UPRET:BOTTOMBREAKDOWN","DOWN"},
			{"UPRET:BOTTOMREVERSAL","RANGE"},
			{"DOWN:NOEVENT","DOWN"},
			{"DOWN:PEAKUP","RANGE"},
			{"DOWN:PEAKDOWN","DOWN"},
			{"DOWN:BOTTOMUP","DOWNRET"},
			{"DOWN:BOTTOMDOWN","DOWNRET"},
			{"DOWN:PEAKBREAKUP","UP"},
			{"DOWN:PEAKBREAKDOWN","RANGE"},
			{"DOWN:PEAKREVERSAL","DOWN"},
			{"DOWN:BOTTOMBREAKUP","RANGE"},
			{"DOWN:BOTTOMBREAKDOWN","DOWN"},
			{"DOWN:BOTTOMREVERSAL","RANGE"},
			{"DOWNRET:NOEVENT","DOWNRET"},
			{"DOWNRET:PEAKUP","RANGE"},
			{"DOWNRET:PEAKDOWN","DOWN"},
			{"DOWNRET:BOTTOMUP","RANGE"},
			{"DOWNRET:BOTTOMDOWN","DOWNRET"},
			{"DOWNRET:PEAKBREAKUP","UP"},
			{"DOWNRET:PEAKBREAKDOWN","RANGE"},
			{"DOWNRET:PEAKREVERSAL","RANGE"},
			{"DOWNRET:BOTTOMBREAKUP","RANGE"},
			{"DOWNRET:BOTTOMBREAKDOWN","DOWN"},
			{"DOWNRET:BOTTOMREVERSAL","RANGE"}
		};
	}
}
