//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion


// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
	//public enum TaskType {SetupLong,SetupShort,Exit,None}
	
    partial class Strategy
    {
		//
		// Define Position Specification Class
		//
		public class TradeSpec:IFDescription{
			Strategy o;
			public ObservedEvent triggerEvent{get; set;} // Event object triggerd this position
			public int priority{get; set;} // Priority of signaling  default value is 3
			public string signal{get; set;} // "BUY" or "SELL"
			public int SignalSign {get; set;} //BUY:1 SELL:-1
			// public string strategy; //Not in use
			public string tactics;  //"FOLLOW" or "COUNTER"
			public string signalName{get; set;} // ex. "SN_NB_SELL_COUNTER", "SN_NB_BUY_FOLLOW"
			public int size{get; set;} // Position Size in contract
			public int term{get; set;}    // Planed term(bar count) form entry to exit
			public string note{get; set;} // Memo for human
			//Stop and Target
			public double initialPrice{get; set;} //Assume passed close[0] from Strategy
			public double initialTargetDelta{get; set;} // Initial Tareget price
			public double initialStopDelta{get; set;}   // Initial Stop priced
			public double initialTargetPrice{get; set;} // Initial Tareget price. This is for making Target Trail before position made.
			public double initialStopPrice{get; set;}   // Initial Stop priced This is for making Stop Trail before position made.
			public Trail StopTrail{get; set;} 
			public Trail TargetTrail{get; set;} 
			//Execution related Ivar
			public DateTime createTime{get; set;} //Create time
			public bool isExecuted{get; set;} //True if executed
			public bool isTerminated{get; set;} //True if terminated
			public bool doneUpdate{get; set;}
			public int executedBarNo{get; set;} //Tanuki Execute Bar Number. Not by system.
			public int exitBarNo{get; set;} //Tanuki Exit Bar Number. Not by system.
			public Order entryOrder{get; set;}
			public double valueOnCreate{get; set;}
			public double profitDelta{get; set;}   //Profit. negative value means loss 
			public Trade latestTrade{get; set;} //Record trade in OnExecutionUpdate()@TNKSystemEvenrtHandlers.cs
			//Maximum Profit
			public double MaximumProfit{get; set;} //Maximum profit in trade
			public int MaximumProfitBar{get; set;} //Bar number of Maximum profit
			//Log store
			public LogStore Log{get; set;}
			//
			//Constructor
			// Strategy owner,
			//public TradeSpec(ObservedEvent oe, int pri, string sig,int sz, int tm, 
			public TradeSpec(Strategy owner,　ObservedEvent oe, int pri, string sig,int sz, int tm, 
				             double ip, double td, double sd ,string nt){
				o = owner;
				triggerEvent = oe;
				priority = pri;
				signal = sig;
				size = sz;
				term = tm;
				initialPrice = ip;
				initialTargetDelta = td; //if target deta is sepcified as 0 then TargetTrail will not use.
				initialStopDelta = sd;
				//Setup Signal Sign
				switch(signal){
					case "BUY":
						SignalSign = 1;
						break;
					case "SELL":
						SignalSign = -1;
						break;
					default:
						SignalSign = 0;
						o.LogError("TradeSpec()",o.MemoNS("Illegal signal",signal));
						break;
				}
				
				//Setup StopTrail
				initialStopPrice =   calculateStopPriceWithDelta();
				StopTrail =   new Trail(o,"STOP"  ,signal, initialStopPrice,   initialStopDelta,  0.0,0.0);
				
				//Setup TargetTrail
				if(initialTargetDelta != 0.0) {
					initialTargetPrice = calculateTargetPriceWithDelta();
					TargetTrail = new Trail(o,"TARGET",signal, initialTargetPrice, initialTargetDelta,0.0,0.0);
				}else{
					//TargetTrail will not use. 
					initialTargetPrice = 0.0;
					TargetTrail = null;
					o.LogDebug("TradeSpec()",o.MemoND("Default TargetTrail was not generated. initialTargetDelta",initialTargetDelta));
				}
				//
				note = nt;
				isExecuted = false;
				isTerminated = false;
			    doneUpdate = false;
				executedBarNo = -1;
				exitBarNo = -1;
				createTime = DateTime.Now;
				profitDelta = 0.0;
				entryOrder = null;
				latestTrade = null;
				//Maximum Profit
				MaximumProfit = 0.0;
				MaximumProfitBar = -1;
				//Init Log Store
				Log = new LogStore(o,"TradeStat","LogStore");
				Log.AddLog(o.MemoNS("Entry",o.TimeStamp(o.Time[0])));
				Log.AddLog(o.MemoNS("triggerEventName",triggerEvent.eventName));
				Log.AddLog(o.MemoND("initialPrice",initialPrice));
				Log.AddLog(o.MemoNI("TWD",(int)o.Time[0].DayOfWeek));
			}
			public string Description(){
				string desc = triggerEvent.Description()+" ";
				desc += priority.ToString()+" ";
				desc += signal+" ";
				desc += size.ToString()+" ";
				desc += term.ToString()+" ";
				desc += initialTargetDelta.ToString()+" ";
				desc += initialStopDelta.ToString()+" ";
				desc += initialTargetPrice.ToString()+" ";
				desc += initialStopPrice.ToString()+" ";
				desc += note;
				return desc;
			}
			public void WriteCloseLog(){
				//
				string memo = "";
				memo += o.MemoNI("executedBarNo",executedBarNo);
				memo += o.MemoNI(" exitBarNo",exitBarNo);
				memo += o.MemoNS(" signal",signal);
				memo += o.MemoND(" profitDelta",profitDelta);
				memo += o.MemoND(" MaximumProfit",MaximumProfit);
				//
				Log.AddLog(memo);
				this.Log.Write();
				return;
			}
			public string eventName{get{return triggerEvent.eventName;}}
			public string eventType{get{return triggerEvent.eventType;}}
			public double entryPrice{get{return entryOrder.AverageFillPrice;}}
			//
			double calculateTargetPriceWithDelta(){
				double price = initialPrice + initialTargetDelta * Strategy.SignalSign(signal);
				return price;
			}
			double calculateStopPriceWithDelta(){
				double price = initialPrice - initialStopDelta * Strategy.SignalSign(signal);
				return price;
			}
			//Reset StopTrail
			public Trail ResetInitialStopTrail(double val, double delta){
				initialStopPrice = val;
				initialStopDelta = delta;
				//StopTrail = new Trail(o,"STOP"  ,signal, initialStopPrice,   initialStopDelta,  0.0,0.0); //TODel
				return StopTrail;
			}
			//Reset TargetTrail
			public Trail ResetInitialTargetTrail(double val, double delta){
				initialTargetPrice = val;
				initialTargetDelta = delta;
				TargetTrail = new Trail(o,"TARGET",signal, initialTargetPrice, initialTargetDelta,0.0,0.0);
				return TargetTrail;
			}
//			//Set initialTargetDelta and TargetPrice 
//			public void setTargetDeltaAndModTargetPrice(double targetDelta){ //This function can be obsoleted
//				initialTargetDelta = targetDelta;
//				initialTargetPrice = calculateTargetPriceWithDelta();
//				TargetTrail.SetMargin(initialTargetDelta);
//			}
			//Get Exit name
			public string ExitName(){
				if(isTerminated){
					return latestTrade.Exit.Name;
				}else{
					return "";
				}
			}
			
			//
			//  Update trails to new position
			//
			public void UpdateTrails(){
				o.LogDebug("TradeSpec.UpdateTrails()",o.MemoNB("StopTrail!= null",StopTrail!= null));
				if(StopTrail   != null){StopTrail.Trace();}
				if(TargetTrail != null){TargetTrail.Trace();}
			}
			
			//Update maximum profit
			public double UpdateProfit(){
				if(o.havePosition()){
					double profit = Profit();
					if(profit > MaximumProfit){
						MaximumProfit = profit;
						MaximumProfitBar = o.CurrentBar;
					}
					o.LogDebug("UpdateProfit()",o.MemoND("MaximumProfit",MaximumProfit),o.MemoNI("MaximumProfitBar",MaximumProfitBar));
				}else{
					//Do nothing
				}
				return MaximumProfit;
			}
			public double Profit(){
				double entry = entryPrice;
				double current = o.Close[0];
				double sign = o.PositionSign();
				return (current - entry) * sign;	
			}
			// Post terminated check
			public bool IsProfitTarget(){return this.ExitName() == "Profit target" ? true:false;;}
			public bool IsStopLoss(){return  this.ExitName() == "Stop loss" ? true:false;}
			public bool IsSessionClose(){return this.ExitName() == "Exit on session close" ? true:false;}
			public bool IsLong(){return signal=="BUY" ?true:false;}
			public bool IsShort(){return signal=="SELL" ?true:false;}
			
			// Compare to another TradeSpec
			public bool IsSignalEqual(TradeSpec ts){return this.signal == ts.signal;}
			public int ExitIntervalFrom(TradeSpec ts){return this.exitBarNo - ts.exitBarNo;} // Valid on Treminated TS ONLY! Return with bar count

			
			//HOT:
			public void Execute(){
				//
				o.LogDebug("ExecuteTradeSpec()",o.MemoNS("sig",signal));
				o.LogDebug("ExecuteTradeSpec()",o.MemoNS("eventName",eventName));
				o.LogDebug("ExecuteTradeSpec()",o.MemoNI("PositionSize",size));
				o.LogDebug("ExecuteTradeSpec()",o.MemoNS("note",note));
				//
				switch(signal){
					case "BUY":
						ExecuteBuy();
					break;
					case "SELL":
						ExecuteSell();
					break;
					case "FLAT":
						ExecuteFlat();
					break;
					case "NONE":
						ExecuteNone(); //Do nothing.
					break;
					case "BREAK":
						// Flat position and break trade spec list execureion
						ExecuteBreak();
					break;
					default:
						o.LogDebug("TradeSpec.Execute()",o.MemoNS("Illigal signal",signal));
					break;
				}
				// Update tradeSpec's execution hitory
				entryOrder = o.TNKLatestOrder;
				isExecuted = true;
			}

			// High Level methods
			// Maket Series
			public void ExecuteBuy(){
				SetupBuy();
				//TBDel// o.TNKWorkingTradeSpec = this;  //Set the trade spec for TradeManagement
				o.SetWorkingTradeSpec(this); //Set the trade spec for TradeManagement
			}
			public void ExecuteSell(){
				SetupSell();
				//TBDel// o.TNKWorkingTradeSpec = this;  //Set the trade spec for TradeManagement
				o.SetWorkingTradeSpec(this); //Set the trade spec for TradeManagement
			}
			public void ExecuteFlat(){
				FlatPosition();
			}
			public void ExecuteBreak(){
				BreakTrade();
			}
			public void ExecuteNone(){}
			
			public void SetupBuy(){
				if (size != 0){
					if(StopTrail!=null){
						StopTrail.SetTrailPrice();   //Do not use TrailAlong() because position is not present here.
					}
					if(TargetTrail != null) {
						TargetTrail.SetTrailPrice(); //Do not use TrailAlong() because position is not present here.
					}
					o.TNKLatestOrder = o.EnterLong(size,eventName);
					o.LogMessage("SetupBuy()",o.MemoNI("BarsInProgress",o.BarsInProgress),o.MemoND("size",size),o.MemoND("initialStopPrice",initialStopPrice),o.MemoND("initialTargetPrice",initialTargetPrice));
				}else{
					o.LogMessage("SetupBuy()",o.MemoNI("BarsInProgress",o.BarsInProgress),o.TAGMsg("Did not take position due to calculated position size was 0"));
				}
			}
		
			public void SetupSell(){
				if (size != 0){
					if(StopTrail!=null){
						StopTrail.SetTrailPrice();   //Do not use TrailAlong() because position is not present here.
					}
					if(TargetTrail != null){
						TargetTrail.SetTrailPrice(); //Do not use TrailAlong() because position is not present here.
					}
					o.TNKLatestOrder = o.EnterShort(size,eventName);
					o.LogMessage("SetupSell()",o.MemoNI("BarsInProgress",o.BarsInProgress),o.MemoND("size",size),o.MemoND("initialStopPrice",initialStopPrice),o.MemoND("initialTargetPrice",initialTargetPrice));
				}else{
					o.LogMessage("SetupSell()",o.MemoNI("BarsInProgress",o.BarsInProgress),o.TAGMsg("Did not take position due to calculated position size was 0"));
				}
			}

			//ISSUE check if exit execute in asyncronusly.
			public void FlatPosition(){
				if(o.haveLongPosition())　 o.ExitLong();　　//Exit all posiiton
				if(o.haveShortPosition()) o.ExitShort();　//Exit all posiiton
			}

			// Flat position and break trade spec list execureion
			public void BreakTrade(){			
			}
		} 
		//===================================
		// END OF CLASS DEFINITION TradeSpec
		//===================================
		
		//
		//  Utilitis for Class TradeSpec
		//
		
		// Take No Action. 
		public TradeSpec TSG_TakeDefaultNoAction(ObservedEvent oe){
			//TNKSignalBuy();
			TradeSpec ps = MakeStandardTradeSpec(oe,"NONE"); //@PositionSizing.cs
			return ps;
		}
		
		// Take Long position. 
		public TradeSpec TSG_TakeDefaultLong(ObservedEvent oe){
			//TNKSignalBuy();
			TradeSpec ps = MakeStandardTradeSpec(oe,"BUY");
			return ps;
		}

		//Take Short position.
		public TradeSpec TSG_TakeDefaultShort(ObservedEvent oe){
			//TNKSignalSell();
			TradeSpec ps = MakeStandardTradeSpec(oe,"SELL");
			return ps;
		}
		// Take Long position. 
		public TradeSpec TSG_TakeQuickLong(ObservedEvent oe){
			//TNKSignalBuy();
			TradeSpec ps = MakeQucikTradeSpec(oe,"BUY");
			return ps;
		}

		//Take Short position.
		public TradeSpec TSG_TakeQucikShort(ObservedEvent oe){
			//TNKSignalSell();
			TradeSpec ps = MakeQucikTradeSpec(oe,"SELL");
			return ps;
		}
		
		//
		//  Make up TradeSpec with default specification.
		//    Use same logic of SetupInitialTradeSpec()
		public TradeSpec MakeStandardTradeSpec(ObservedEvent oe, string signal){
			ObservedEvent triggerEvrnt = oe;
			int priority = 3;
			//PositionSize ps = CalculatePositionSizeWithSigma();    //Old Position size method 
			PositionSize ps = CalculatePositionSizeWithVolatility();  //add at 2018/07/24 for testing.
			int size = ps.Size;
			double initialStopDelta = ps.InitialRiskDelta;
			int	term = CalculateInitialTradeTerm();
			//double initialTargetDelta   = CalculateInitialProfitTargetDelta();  //Current value is TwoSigma
			double initialTargetDelta = 0.0; //TESt to omit target //HOT
			
			string note = "MakeStandardTradeSpec()";			
			//
			LogDebug("MakeStandardTradeSpec()",
				MemoND("Close[0]",Close[0]),
				MemoNS("signal",signal),
				MemoND("SignalSign()",SignalSign(signal)),
				MemoND("initialTargetDelta",initialTargetDelta),
				MemoND("initialStopDelta",initialStopDelta)
			);
			//
			// HOT:
			TradeSpec ts = new TradeSpec(
				this,oe,priority,signal,size,term,
				Close[0],initialTargetDelta,initialStopDelta,note
			);
			return ts;
		}
		
		public TradeSpec MakeQucikTradeSpec(ObservedEvent oe, string signal){
			TradeSpec qts = MakeStandardTradeSpec(oe,signal);
			double factor = 0.5;
			//TEST:
			//return qts;
			//TEST:
			double term = (double)qts.term * factor;
			qts.term = (int)term; //Term set to 5 Bars.
			//double tpdelta = StdDev(14)[0]; // Sigma of close;
			//qts.initialTargetDelta = tpdelta;
			//qts.initialTargetPrice = CalculateTargetPriceWithDelta(signal, tpdelta);
			qts.note = "MakeQucikTradeSpec() equal to StandardSpec in this version.";
			return qts;
		}
		//
		// Trade History Procedures
		//
		public TradeSpec GetTerminatedTradeSpec(int lookback){
			int count = TNKTerminatedTradeSpecHistory.Count;
			if(count < 1) {
				return null;
			}else{
				int index = count -1 -lookback;
				return TNKTerminatedTradeSpecHistory[index];
			}
		}	
	} 
}
