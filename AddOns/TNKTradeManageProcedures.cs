//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion


// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//=================
		// Trailer Class
		//=================
		public class Trail{
			//IVARs
			public Strategy o;
			public string Type; //Stop(STOP) or Target(TARGET)
			public string Signal; // "BUY" or "SELL"
			//public double LeastDelta{get; set;}　 //Distance between "current index(price of the insturument)" and Value
			public double Margin{get; set;} //Margin between "current index(price of the insturument)" and Value
			public double ReduceWidth{get; set;} //Width for one step reduce 
			public double CreepWidth{get; set;} //Width for one step creep
			public int    ChokeFactor{get; set;} //for AdjustStopTrail2()
			//
			private double mostProfitableTrailValue=0.0; //index value of Trailer (price of the this trailer)
			private double typeSign =0.0;
			private int    wakeOn = 0; //Bar number that ends sleep


			// Constructor
			public Trail(Strategy owner, string type, string signal, double value, double margin, double reduceWidth, double creepWidth){
				o = owner;
				Type = type;
				Signal = signal;
				ReduceWidth = reduceWidth;
				CreepWidth = creepWidth;
				//TOBDel LeastDelta = margin;
				Margin = margin;
				mostProfitableTrailValue = value;
				switch(Type){
					case "STOP":
						typeSign = -1.0;
						break;
					case "TARGET":
						typeSign = 1.0;
						break;
					default:
						o.LogError("Trail",o.MemoNS("Illegal Trailer Type",Type));
						break;
				}
				//ISSUE Set mostProfitTrailValue with close[] and delta. BUT trailSign() return 0.0 in flat position. At here, position is always FLAT.
				//TBDelete ajustMostProfitableTrailValue();
				//DEBUG
				o.LogDebug("Trail()",o.MemoNS("Type",Type),o.MemoNS("Signal",Signal),o.MemoND("o.Close[0]",o.Close[0]),o.MemoND("Margin",Margin),o.MemoND("mostProfitableTrailValue",mostProfitableTrailValue));
			}
			//
			public string ClassName(){return this.GetType().Name;}
			//
			// Manupilate IVar Margin
			//
			public double SetMargin(double margin){
				margin = Math.Abs(margin);
				Margin = Math.Min(Margin,margin);
				return Margin;
			}
			
			public double ReduceMargin(double width){
				ReduceWidth = Math.Abs(width);
				SetMargin(Margin - ReduceWidth);
				o.LogDebug("Trail.reduceDelta()",o.MemoND("width",width),o.MemoND("Margin",Margin));
				return Margin ;
			}

			//
			// Ajust Trail Value
			//  Do not use TrailAlong() because position is not present here.
			//
			private double calculateTrailValue(){
				double val = o.Close[0] + Margin * trailSign(); //Do not call this fuction before position made. trailSign() return 0.0 in flat position.
				return val;
			}
			public double TrailAlong(){
				double val = calculateTrailValue();
				return TrailTo(val);
			}
			
			//
			//  Sleep for n bar
			//    Input: bar count for sleep time
			public int Sleep(int n){ //bars
				wakeOn = o.CurrentBar + n;
				return wakeOn;
			}
			public bool IsSleeping(){
				if (wakeOn==0) {
					return false;
				}else{
					return o.CurrentBar < wakeOn;
				}
			}
			//
			// Trace
			//   Call from AdjustTrail()
			//   
			public virtual void Trace(){
				//Stab in base
				o.LogError("Trail.Trace()",o.MemoNS("This method should be overridden Class",ClassName()),o.MemoNS("Type",Type));
			}
			//
			// Creep (consider it's necessity)
			//
			//public double Creep(){return mostProfitableTrailValue;}
			
			//
			// Jump the trail to break even.
			//  Return: new trail value
			//
			public double MakeBreakEven(double margin){
				double entryPrice = o.TNKRealEntryPrice;
				double price = o.Close[0];
				double sigma = o.StdDev(14)[0];
				//TBDel// double cap = o.TakeBigger(margin,0.0); //Cap shoud be positive value.
				//
				double moveFromEntry = Math.Abs(price - entryPrice);
				//TBDel// double delta = o.TakeBigger(moveFromEntry, cap); //Limit delta with cap. prevent delta become small than margin.
				double delta = o.TakeBigger(moveFromEntry, margin);
				
				SetMargin(delta);
				TrailAlong();
				
				o.LogDebug("Trail.MakeBreakEven()",o.MemoND("moveFromEntry",moveFromEntry),o.MemoND("margin",margin),o.MemoND("delta",delta));
				return mostProfitableTrailValue;
			}
			public double MakeBreakEven(){
				return MakeBreakEven(Margin);
			}
			
			//
			// Move Stop/Target to val
			//  Return: acutual set value
			//  To make onw way: Stop can move profit side/Taregt can move loss side
			//  Consider to change: Traget may move both side for flxibility.
			public double TrailTo(double val){
			// import logic of SetStopPriceOneWay(double stopPrice) ---------
				if(mostProfitableTrailValue == 0.0) {
					//This line should be executed before a position is taken.
					mostProfitableTrailValue = val;
				}else{
					string ple = o.ProfitLossEven(val, mostProfitableTrailValue);
					// Return one of "PROFIT", "LOSS", "EVEN", "UNCLEAR"
					switch(ple){
						case "PROFIT":
							if(Type == "STOP") {
								mostProfitableTrailValue = val;
							}
							break;
						case "LOSS":
							if(Type == "TARGET"){
								mostProfitableTrailValue = val;
							}
							break;
						case "EVEN":
							//no need to change.
							break;
						case "UNCLEAR":　//Any Position is not taken.
							mostProfitableTrailValue = val;
		 					//o.LogError("Trail.TrailTo()",o.MemoNS("Type",Type), o.MemoNS("Call in flat. ple",ple),o.MemoND("val",val),o.MemoND("mostProfitableTrailValue",mostProfitableTrailValue));
		  					break;
						default:
							o.LogError("Trail.TrailTo()",o.MemoNS("Type",Type), o.MemoNS("Illegal ple string",ple));
							break;
					}
				}
				o.LogDebug("Trail.TrailTo()",o.MemoNS("Type",Type),o.MemoND("val",val),o.MemoND("mostProfitableTrailValue",mostProfitableTrailValue));
				//
				SetTrailPrice();
				return mostProfitableTrailValue;
			}
			//
			//  execte SetStopPrice() or SetTargetPrice() with
			//
			public double SetTrailPrice(){
				switch(Type){
					case "STOP":
						o.SetStopPrice(mostProfitableTrailValue);
						break;
					case "TARGET":
						o.SetTargetPrice(mostProfitableTrailValue);
						break;
					default:
						o.LogError("Trail.SetTrailPrice()",o.MemoNS("Illegal Type string",Type));
						break;
				}
				return mostProfitableTrailValue;
			}
			
			private double trailSign(){
				//Trail's relative place to Close[0]. Uspside:1.0, Downside:-1.0
				//Type     STOP:-1.0  TARGET: 1.0
				//Position BUY : 1.0  SELL   :-1.0   Flat: 0.0
				//Signal   BUY : 1.0  SELL   :-1.0
				double sign = 0.0;
				if(o.PositionSign() == 0.0) { //in Flat
					sign = typeSign * Strategy.SignalSign(Signal);
				}else{
					sign = typeSign * o.PositionSign(); 
				}
				return sign;
			}
			//
			public string Description(){
				string dsc= "";
				dsc += o.MemoNS("Type",Type);
				dsc += o.MemoND("mostProfitableTrailValue",mostProfitableTrailValue);
				dsc += o.MemoND("Margin",Margin);
				dsc += o.MemoND("ReduceWidth",ReduceWidth);
				dsc += o.MemoND("CreepWidth",CreepWidth);
				return dsc;
			}
		}
		//===============================
		// End of Trail calss definition
		//===============================
		
		//=====================================
		//
		// Subclass of Trail calss definition
		//
		//=====================================
		
		//
		// Stop Trail for Volatility Channel Breakout (VCBO)
		//
		public class TrailVolatilityChannel : Trail{
			private VolatilityChannel vc {get; set;}
			// Constructor
			public TrailVolatilityChannel(Strategy owner, string type, string signal, double value, double margin, double reduceWidth, double creepWidth):
				base( owner,  type,  signal,  value,  margin,  reduceWidth,  creepWidth){
			}
			public TrailVolatilityChannel(Strategy owner, string signal, double value, double margin, VolatilityChannel channel):
				base( owner,  "STOP",  signal,  value,  margin,  0.0,  0.0){
				vc = channel;
			}
			public void SetVolatility(int period, double factor){
				 vc = new VolatilityChannel(o, period, period);
			}
			public void SetVolatility(){ //Set Default channel
				SetVolatility(TNKVolatilityChannelPeriod, TNKVolatilityChannelBreakoutBorder);
			}
			public override void Trace(){
				double stopValue = 0.0;
				//
				if(Signal=="BUY"){
					stopValue = vc.LowerValue(0);
				}else if(Signal=="SELL"){
					stopValue = vc.UpperValue(0);
				}else{
					o.LogError("TrailVolatilityChannel.Trace()",o.MemoNS("Illegal Signal",Signal));
				}
				o.LogDebug("TrailVolatilityChannel.Trace()",o.MemoND("stopValue",stopValue));
				TrailTo(stopValue);	
			}
		}
		
		//
		// Target trail for fix target
		//
		public class TrailConstantLevel : Trail{
			// Ivar
			private double targetLevel = 0.0;
			private bool isFirst = true;
			// Constructor
			public TrailConstantLevel(Strategy owner, string type, string signal, double value, double margin, double reduceWidth, double creepWidth):
				base( owner,  type,  signal,  value,  margin,  reduceWidth,  creepWidth){
			}
			public TrailConstantLevel(Strategy owner,string type, string signal, double value):
				base( owner,  type,  signal,  value,  0.0,  0.0,  0.0){
					targetLevel = value;
					isFirst = false;		
			}
			public TrailConstantLevel(Strategy owner, string signal, double value):
				base( owner,  "TARGET",  signal,  value,  0.0,  0.0,  0.0){
					targetLevel = value;
					isFirst = false;		
			}
			public override void Trace(){
				if(isFirst) {
					TrailTo(targetLevel);
					isFirst = false;
					o.LogDebug("TrailConstntLevel.Trace()",o.MemoND("targetLevel",targetLevel));
				}else{
					//Do nothing 
				}
			}
		}
		
		//=================
		//
		//  Ajust Trade
		//
		//=================
		public void AjustTrade(){
			if(TNKWorkingTradeSpec==null) {return;}
			TradeSpec tradeSpec = TNKWorkingTradeSpec;
			// Attempt Position Filter
			bool terminated = AttemptPositionFilters(tradeSpec);
			if(terminated){
				// Do nothing.
				LogDebug("AjustTrade()",MemoNB("terminated",terminated));
			}else{
				// Manage StopLoss and Target
				AdjustTraleCommon(tradeSpec);
				AdjustTrail(tradeSpec);
				//AdjustTradeWithDogRunModel(tradeSpec);
			}
		}
		//
		// Common procedure
		//   Place code for common preamble procedure for Stop and Target
		public void AdjustTraleCommon(TradeSpec ts){
		}
		//
		// Ajust trail
		//
		public void AdjustTrail(TradeSpec ts){
			ts.UpdateTrails();
		}
		public void _AdjustTrail(TradeSpec ts){
			LogDebug("AdjustTrail()",MemoNB("ts==null",ts==null));
			//Adjust Stop trail
			//Stop trail must be valid alwasys.
			if(ts.StopTrail != null){
				AdjustStopTrail2(ts);
				//AdjustStopTrail(ts);
//				if(!ts.StopTrail.IsSleeping()){
//					AdjustStopTrail(ts);
//				}else{
//					LogDebug("AdjustTrail()",TAGMsg("StopTrail is sleeping"));
//				}
			}else{
				//Strop Trail is not valid. This critical error.
				LogError("AdjustTraleCommon", TAGMsg("ts.StopTrail is null. This is crtical error."));
			}
			//Adjust Target trail
			// if TrgetTrail set null, skip to trail target.
			if(ts.TargetTrail != null) {
				if(!ts.TargetTrail.IsSleeping()){
					AdjustTargetTrail(ts);
				}else{
					LogDebug("AdjustTrail()",TAGMsg("TargetTrail is sleeping"));
				}
			}else{
				//Target may be invalid
				//Do noting
				LogDebug("AdjustTrail()",TAGMsg("TargetTrail is not set"));
			}
		}
		//
		// Adjunst Stop Trail 3
		//
		public void AdjustStopTrail3(TradeSpec ts){
			// Return if any position does not exist.
			if(!havePosition()) return;
			Trail stop = ts.StopTrail;
			//Parama
			VolatilityChannel vc = new VolatilityChannel(this, TNKVolatilityChannelPeriod, TNKVolatilityChannelBreakoutBorder);
			//
			double stopValue = 0.0;
			//
			if(PositionSign()==1.0){
				stopValue = vc.LowerValue(0);
			}else if(PositionSign()==-1.0){
				stopValue = vc.UpperValue(0);
			}else{
				LogError("AdjustStopTrail3()",MemoND("Illegal PositionSign()",PositionSign()));
			}
			stop.TrailTo(stopValue);
		}
	
		//
		// Adjunst Stop Trail 2
		//
		public void AdjustStopTrail2(TradeSpec ts){
			// Return if any position does not exist.
			if(!havePosition()) return;
			Trail stop = ts.StopTrail;
			double stopValue = 0.0;
			//Params
			int SmaPeriod = 14;
			int SigmaPeriod = 14;
			double SigmaLimit = 1.4;
			//Calculate bar count form entry
			int elapsed = CurrentBar - ts.executedBarNo;
			int planedTerm = ts.term;
			double sma = SMA(SmaPeriod)[0];
			double sigma = StdDev(SigmaPeriod)[0];
			//Check Bar position against BO channel and modifty Trail.Chokefoctor
			double checkValue = (Open[0] + Close[0])/2.0; //midpoint of body
			if(PositionSign()==1.0){
				if(checkValue >= sma + sigma * SigmaLimit){stop.ChokeFactor++;}
			}else if(PositionSign()==-1.0){
				if(checkValue <= sma - sigma * SigmaLimit){stop.ChokeFactor++;}
			}else{
				LogError("AdjustStopTrail2()",MemoND("Illegal PositionSign()",PositionSign()));
			}
			// Return if Stop trail is under sleeping
			if(stop.IsSleeping()) return;
			//
			// Update Stop
			//
			double choke=0.0, offset=0.0;
			if(elapsed == 0){
				// Entry
				//Set stop on SMA
				stopValue = sma;
				stop.TrailTo(stopValue);
			}if(elapsed <= planedTerm){
				// Run
				//Ajust trail path with offsetSigma
				choke = (double)stop.ChokeFactor/(double)planedTerm;
				offset = sigma * choke * PositionSign();
				stopValue = sma + offset;
				stop.TrailTo(stopValue);
				//LogDebug("AdjustStopTrail2()",MemoND("Run choke",choke));
			}else{
				//Out of term 
				//DO NOTHING now.
			}
			LogDebug("AdjustStopTrail2()",MemoNI("elapsed",elapsed),MemoNI("planedTerm",planedTerm),MemoND("choke",choke),MemoNI("stop.ChokeFactor",stop.ChokeFactor),MemoND("sma",sma),MemoND("offset",offset),MemoND("stopValue",stopValue));
		}
		//
		// Adjust Stop Trail
		//
		public void AdjustStopTrail(TradeSpec tradeSpec){
			// Return if any position does not exist.
			if(!havePosition()) return;
			// Return if Stop trail is under sleeping
			if(tradeSpec.StopTrail.IsSleeping()) return;
			//
			//Judge if price is in Loss zone
			// In case of Loss, Reduce Stop price. the ammount is slope of stop line.
			double price = Close[0];
			int period = 14;
			double entryPrice = TNKRealEntryPrice;
			double stopPrice=0.0;
			double stopLineSlope   = tradeSpec.initialStopDelta   / tradeSpec.term;
			string ple = ProfitLossEven(price, entryPrice);
			switch(ple){
				case "LOSS":
					//LOSS Domain
					//Use CONSTANT_SLOP in Slow move. Use RESPOND_RISK in fast move.
					string method = "CONSTANT_SLOPE";
					double verocity = move(0);
					if((Math.Sign(verocity)!=(int)PositionSign()) && (Math.Abs(verocity) > Math.Abs(stopLineSlope))) {
						// Loss speed is fast over Stop lines slope
						method = "RESPOND_RISK";
					}else{
						method = "CONSTANT_SLOPE";
					}
					LogDebug("AdjustStopTrail()",MemoNS("STOP Trails under LOSS domain with",method));
					switch(method){
						case "CONSTANT_SLOPE":
							LogDebug("AdjustStopTrail()",MemoND("stopLineSlope",stopLineSlope));
							tradeSpec.StopTrail.ReduceMargin(stopLineSlope);
							stopPrice = tradeSpec.StopTrail.TrailAlong();
							break;
						case "RESPOND_RISK":
							//Trail stoploss with loss width
							double responseFactor = 1.0;
							string posDirection = PositionDirection();
							double lossWidth = Math.Abs(Close[0] - entryPrice);
							double trailWidth = lossWidth * responseFactor;
							double calcStopPrice = tradeSpec.initialStopPrice + trailWidth * PositionSign();
							double newStopPrice;
							switch(posDirection){
								case "LONG":  //Long position
									newStopPrice = Math.Min(Close[0],calcStopPrice);
									break;
								case "SHORT": //Shprt position
									newStopPrice = Math.Max(Close[0],calcStopPrice);
									break;
								default:  //Error
									newStopPrice = calcStopPrice;
									LogError("AdjustStopTrail()",MemoNS("Illegal value of posDirection",posDirection));
									break;
							}
							LogDebug("AdjustStopTrail()",MemoND("entryPrice",entryPrice),MemoND("trailWidth",trailWidth),MemoND("newStopPrice",newStopPrice));
							stopPrice = tradeSpec.StopTrail.TrailTo(newStopPrice);
							break;
						case "STAY":
							//DO nothing
							LogDebug("AdjustStopTrail()",MemoND("Stay StopLoss at",TNKStopLossPrice));
							break;
						default:
							LogError("AdjustStopTrail()",MemoNS("Illegal Stop trail method",method));
							break;
					}
					break;
				case "PROFIT":
				case "EVEN":
					// Move Stop Price to Break Even
					string ple_sl_ep = ProfitLossEven(TNKStopLossPrice,entryPrice);
					switch(ple_sl_ep){
						case "LOSS":
						case "EVEN":
							//BreakEven Domain
							// Move StopLossPrice to near entyr price
							LogDebug("AdjustStopTrail()",TAGMsg("SLP LOSS/EVEN Move to BreakEven"));
							tradeSpec.StopTrail.MakeBreakEven();
							break;
						case "PROFIT":
							//Track Profit Domein
							// Trail close price
							// Trail delta is one sigma
							double boBorder = TNKNormalizedBreakoutBorder;
							bool   insideBOBand = IsInsideOfNBOBand(); //NT8
							// Calculate trailDelta
							double trailDelta;
							if(insideBOBand == true){ //Is inside BO band? Stop Loss Creeping if price is within Band
								//Reduce trailDelta if close is in BBand
								double creepAjustment = 2.0;　//PARAM-0:
								int barsFromBO = BardsFromRecentNBO(); //NT8
								double divider = Math.Sqrt(Math.Abs(barsFromBO))*creepAjustment;
								trailDelta = SigmaThisBar(period) / divider; //within Band
								LogDebug("AdjustStopTrail()",MemoND("barsFromBO",barsFromBO),MemoND("creepAjustment",creepAjustment),MemoND("divider",divider),MemoND("trailDelta",trailDelta));
							}else{
								trailDelta = SigmaThisBar(period); //outside of Band
							}
							//TrailStopLossWithDelta(trailDelta); //TBObsolete:
							double stopLossPrice;
							tradeSpec.StopTrail.SetMargin(trailDelta);
							stopLossPrice = tradeSpec.StopTrail.TrailAlong();
							LogDebug("AdjustStopTrail()",TAGMsg("Change_SLP_in_SLP-PROFIT"),MemoND("trailDelta",trailDelta),MemoND("StopLossPrice",stopLossPrice));
							break;
						case "UNCLEAR":
							LogError("AdjustStopTrail()",MemoNS("SLP_NoPosition-ple_sl_ep",ple_sl_ep));
							break;
						default:
							LogError("AdjustStopTrail()",MemoNS("SLP_Illigal-ple_sl_ep",ple_sl_ep));
							break;
					}
					break;
				case "UNCLEAR":
					LogError("AdjustStopTrail()",MemoNS("SLP_NoPosition-ple",ple));
					break;
				default:
					LogError("AdjustStopTrail()",MemoNS("SLP_Illigal-ple",ple));
					break;
			}
		}
		//
		// Adjust Target Trail
		//
		public void AdjustTargetTrail(TradeSpec tradeSpec){
			// Return if any position does not exist.
			if(!havePosition()) return;
			//  Define TargetLine
			double targetLineSlope = tradeSpec.initialTargetDelta / tradeSpec.term;
			//ISSUE:TargetSlope is allways plus. need to reverse sign in SELL
			//double targetPriceOnThisBar = targetLineSlope * BarsSinceEntry() + TNKRealEntryPrice;
			double targetPriceOnThisBar = TNKRealEntryPrice + (targetLineSlope * BarsSinceEntryExecution() * PositionSign());
			double price = Close[0];
			int period = 14;
			double sigma = SigmaThisBar(period);
			double entryPrice = TNKRealEntryPrice;
			double targetPrice=0.0;
			
			//Judge if price positions upper or lower than taregt line
			string ple = ProfitLossEven(price, targetPriceOnThisBar);
			LogDebug("AdjustTargetTrail()",
				MemoNS("ple",ple),
				MemoND("price",price),MemoND("targetPriceOnThisBar",targetPriceOnThisBar),
			    MemoND("tradeSpec.initialTargetDelta",tradeSpec.initialTargetDelta),
				MemoND("tradeSpec.term",tradeSpec.term),
				MemoND("targetLineSlope",targetLineSlope),
				MemoND("BarsSinceEntry()",BarsSinceEntryExecution()),
				MemoND("PositionSign()",PositionSign())
			);
			switch (ple) {
				case "LOSS":
					//Reduce Target Price Delta if price positions in LOSS zone
					//"OFFSET" Reduce ammount is slope of targetLine
					//"LINER" move on TargetLine imidiately. TargetPrice = TargetPriceOnThisbar
					//string method="OFFSET";
					//string method="LINER";
					string method = "STAY";
					switch(method){
						case "LINER": 
							double trailPriceOnThisBar = targetPriceOnThisBar; //Move on Target Line imidately.
							targetPrice = tradeSpec.TargetTrail.TrailTo(trailPriceOnThisBar);
							LogDebug("AdjustTargetTrail() LINER",MemoND("PTP LOSS trailTo trailPriceOnThisBar",trailPriceOnThisBar));
							break;
						case "OFFSET":
							tradeSpec.TargetTrail.ReduceMargin(targetLineSlope);
							targetPrice = tradeSpec.TargetTrail.TrailAlong();
							LogDebug("AdjustTargetTrail() OFFSET",MemoND("PTP LOSS reduce targetLineSlope",targetLineSlope));
							break;
						case "STAY":
							//DO NOTHING
							LogDebug("AdjustTargetTrail() STAY",MemoND("PTP LOSS reduce targetLineSlope",targetLineSlope));
							break;
						default:
							LogError("AdjustTargetTrail()",MemoNS("medhod Illigal ple value",method));
							break;
					}
					break;
				case "PROFIT":
				case "EVEN":
					//Do nothing if price positions PROFIT zone or EVEN
					LogDebug("AdjustTargetTrail()",MemoND("PTP PROFIT/EVEN Keep current PTP",targetPriceOnThisBar));
					break;
				case "UNCLEAR":
					LogError("AdjustTargetTrail()",MemoNS("PTP NoPosition ple",ple));
					break;
				default:
					LogError("AdjustTargetTrail()",MemoNS("PTP Illigal ple value",ple));
					break;
			}
		}
    }
}