//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Collections.Generic;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		/// <summary>
        /// Called on each update order event
        /// </summary>

		//=====
		// Get last Trade
		//=====	
		public int GetTradeCount(){
			return  SystemPerformance.AllTrades.Count;
		}
		//=====
		// Get past Trade
		//    Return trade past ago
		//    ago : 0 mean recent trade
		//=====
		public Trade GetPastTrade(int ago){
			Trade lastTrade = null;
			int tradeCount = SystemPerformance.AllTrades.Count;
			if(tradeCount > ago){
				lastTrade = SystemPerformance.AllTrades[tradeCount - 1 - ago];
			}else{
				lastTrade = null;
			}
			return lastTrade;
		}
		//=====
		// Get last Trade
		//=====
		public Trade GetLatestTrade(){
			return GetPastTrade(0);
		}
		//====
		// Terminate TNKWorkingTradeSpec 
		//====
		public void TerminateWorkingTradeSpec(){
			int tradeCount = SystemPerformance.AllTrades.Count;
			Trade lastTrade = SystemPerformance.AllTrades[tradeCount - 1];
			//
			TNKWorkingTradeSpec.isTerminated = true;
			TNKWorkingTradeSpec.latestTrade = lastTrade;
			TNKWorkingTradeSpec.profitDelta = (lastTrade.Exit.Price - lastTrade.Entry.Price)*SignalSign(TNKWorkingTradeSpec.signal);
			TNKWorkingTradeSpec.exitBarNo = CurrentBar;
			TNKWorkingTradeSpec.doneUpdate = true;
			//Write trade log Tag:TradeStat Facility:LogStore
			//TNKWorkingTradeSpec.Log.Write();
			TNKWorkingTradeSpec.WriteCloseLog();
			//Record to Tradespec history
			TNKTerminatedTradeSpecHistory.Add(TNKWorkingTradeSpec);
			LogDebug("OnExecutionUpdate()",
				MemoNI("Add done TradeSpec to TNKTerminatedTradeSpecHistory.Count",TNKTerminatedTradeSpecHistory.Count),
				MemoNS("TNKWorkingTradeSpec.ExitName()",TNKWorkingTradeSpec.ExitName()),
				MemoNI("TNKWorkingTradeSpec.exitBarNo",TNKWorkingTradeSpec.exitBarNo)
			);
			//TNKWorkingTradeSpec = null;
			SetWorkingTradeSpec(null);			
		}
		//=====
		// Terminate TNKWorkingTradeSpec 
		//====
		public void TerminateWorkingTradeSpec(Trade trade){
			TerminateTradeSpec(TNKWorkingTradeSpec,trade);
			SetWorkingTradeSpec(null);
		}
		//=====
		// Terminate Specified TradeSpec 
		//====
		public void TerminateTradeSpec(TradeSpec ts, Trade trade){
			int tradeCount = SystemPerformance.AllTrades.Count;
			//
			ts.isTerminated = true;
			ts.latestTrade = trade;
			ts.profitDelta = (trade.Exit.Price - trade.Entry.Price)*SignalSign(ts.signal);
			ts.exitBarNo = CurrentBar;
			ts.doneUpdate = true;
			//Write trade log Tag:TradeStat Facility:LogStore
			//ts.Log.Write();
			ts.WriteCloseLog();
			//Record to Tradespec history
			TNKTerminatedTradeSpecHistory.Add(ts);
			LogDebug("OnExecutionUpdate()",
				MemoNI("Add done TradeSpec to TNKTerminatedTradeSpecHistory.Count",TNKTerminatedTradeSpecHistory.Count),
				MemoNS("ts.ExitName()",ts.ExitName()),
				MemoNI("ts.exitBarNo",ts.exitBarNo)
			);	
		}
		//====
		//  Judge the execution is for ENTRY or EXIT or NOMATCH
		//====
		public string WhichOrderExecuted(Execution ex, Trade trade){
			string judge = "NOMATCH";
			if(ex == trade.Entry){
				judge = "ENTRY";
			}else if(ex == trade.Exit){
				judge = "EXIT";
			}else{
				judge = "NOMATCH";
			}
			LogDebug("DoesWhichOrderExecuted",MemoNS("judge",judge));
			return judge;
		}
		//====
		// Write tredelog
		//====
		public void WriteTradeLog(Trade trade){
			string entryName = trade.Entry.Name;
			string entryTime = TimeStamp(trade.Entry.Time);
			string positionType = trade.Entry.MarketPosition.ToString();
			string exitName = trade.Exit.Name;
			string exitTime = TimeStamp(trade.Exit.Time);
			int    positionSize = trade.Quantity;
			double profit = trade.ProfitCurrency * trade.Quantity;
			string tag = "TradeLog";
			string memo = string.Format("{0:s} {1:s} {2:s} {3:d} {4:f} {5:s} {6:s}",entryName,RepSpUb(exitName),positionType,positionSize,profit,entryTime,exitTime);
			//string memo =   string.Format("TradeLog {0:s} {1:s} {2:s} {3:s} {4:s}",entryName,exitName,positionType);
			LogTagAndStrings(tag,memo);			
		}
		
		//protected override void OnPositionUpdate(IPosition position){}
		//protected override void OnExecution(IExecution execution)
		protected override void OnExecutionUpdate(Execution execution, string executionId, double price, int quantity, MarketPosition marketPosition, string orderId, DateTime time){
			Order order = execution.Order;
			string orderName = order.Name;
			LogDebug("OnExecutionUpdate",MemoNS("Entry point. orderName",orderName));
			string executionFor = "";
			int tradeCount = GetTradeCount();
			Trade latestTrade = null;
			if(tradeCount >0){
				//Judge whitch odrder is going to be executed
				latestTrade = GetLatestTrade();
				executionFor = WhichOrderExecuted(execution, latestTrade);
				if(executionFor == "EXIT"){
					//It is OK to thru
				}else{
					executionFor = "ENTRY";
				}
			}else{
				executionFor = "ENTRY";
			}
			//
			// Determin whitch TradeSpec should be close
			//
			Order entryOrder = null;
			if(executionFor == "EXIT"){
				entryOrder = latestTrade.Entry.Order;
				if(TNKWorkingTradeSpec.entryOrder ==　entryOrder){
					//Close Working Trade Spec
					LogDebug("OnExecutionUpdate",MemoNS("Working TradeSpec will be terminated",executionFor));
					TerminateTradeSpec(TNKWorkingTradeSpec,latestTrade);
					WriteTradeLog(latestTrade);
					SetWorkingTradeSpec(null);
				}else if (TNKPreviousTradeSpec.entryOrder == entryOrder){
					//Close previous trade spec
					LogDebug("OnExecutionUpdate",MemoNS("DOTEN Previous TradeSpec will be terminated",executionFor));
					TerminateTradeSpec(TNKPreviousTradeSpec,latestTrade);
					WriteTradeLog(latestTrade);
				}else{
					LogError("OnExecutionUpdate",MemoNS("Executed order does not match nither working nor previous tradespec",order.ToString()));
				}
			}else{
				//This is entry execution. No need to treat tradeSpec
				// OK to thru
			}	
		}


		//protected override void OnOrderUpdate(IOrder order)
		protected override void OnOrderUpdate(Order order, double limitPrice, double stopPrice, int quantity, int filled, double averageFillPrice, OrderState orderState, DateTime time, ErrorCode error, string comment)
		{
			//if(TNKDebug) TNKLogTS(MemoNS("OnOrderUpdate():order",order.ToString()));
			if (order.OrderState==OrderState.Filled){
				TNKRealStopLossPrice = order.StopPrice;
				//NT7 TNKRealEntryPrice = order.AvgFillPrice;
				TNKRealEntryPrice = order.AverageFillPrice;
			}
			//if(TNKDebug) TNKLogTS(MemoND("OnOrderUpdate()Close[0]",Close[0]));
			//if(TNKDebug) TNKLogTS(MemoNS("OnOrderUpdate()order",order.ToString()));
			//if(TNKDebug) TNKLogTS(MemoND("OnOrderUpdate()TNKRealStopLossPrice",TNKRealStopLossrice));
			//if(TNKDebug) TNKLogTS(MemoND("OnOrderUpdate()TNKRealEntryPrice",TNKRealEntryPrice));		
		}
		
		//protected override void OnTermination()
		protected override void OnStateChange() 
		{
			if (State == State.Terminated) {
				// Clean up your resources here
				// Test
				List<EventPerformance> ranking = EventPerormanceRankingList();
				foreach( EventPerformance ep in ranking){
					LogMessage("OnTermination()",MemoNS("ep",ep.Description()));
				}
				LogMessage("OnTermination()",MemoND("TNKStats.TotalMove",TNKStats.TotalMove));
			}
		}
    }
}
