//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {

		public class PositionSize{
			public int Size {get; set;}
			public double InitialRiskDelta {get; set;}
			public double ActualRisk{get; set;}
			public PositionSize(int sz, double ird){
				Size = sz;
				InitialRiskDelta = ird;
			}
		}
		//
		//  Class Position Risk
		// 
		public class PositionRisk : IFDescription{
			Strategy owner;
			public int Size {get; set;}
			public double StopDelta {get; set;}
			public double ActualRisk {get; set;}
			public PositionRisk(Strategy o,  int sz, double ird){
				owner = o;
				Size = sz;
				StopDelta = ird;
				ActualRisk = o.CalculateActualRisk(Size, StopDelta);
			}
			public string Description(){return string.Format("{{Size:{0} StoDelta:{1}}}",Size,StopDelta);}
		}
		public double CalculateActualRisk(int size, double delta){
			return MulPointValue(delta) * size;
		}


		//
		//  Calculate Position Risk with given risk and stopDelta using full risk.
		//    Input risk: Risk amount for trade   delta: Stop Delta  maxSize:Cap of size (give limitation form clearing margin)
		//    Output PositonRisk
		public PositionRisk RiskToPositionSizeWithDeltaUsingFullRisk(double risk, double delta, int maxSize){
			double riskByDelta = DivideByPointValue(risk);
			int size = (int)Math.Floor(riskByDelta / delta);
			size = Math.Min(size, maxSize); //Cap with maxSize
			PositionRisk pr = RiskToDeltaWithPositionSize(risk, size, 10);
			return pr;
		}
		
		//
		//  Calculate Position Risk with given risk and stopDelta
		//    Input risk: Risk amount for trade   delta: Stop Delta  maxSize:Cap of size (give limitation form clearing margin)
		//    Output PositonRisk
		public PositionRisk RiskToPositionSizeWithDelta(double risk, double delta, int maxSize){
			double riskByDelta = DivideByPointValue(risk);
			int size = (int)Math.Floor(riskByDelta / delta);
			size = Math.Min(size, maxSize); //Cap with maxSize
			PositionRisk pr = new PositionRisk(this,size,delta);
			return pr;
		}
		//
		//  Calculate Positon Risk with given risk and position size
		//    Input risk: Risk amount for trade   size: Position size  minDeltaByTic: Ninimum delta by Tic
		//    Output PositonRisk
		public PositionRisk RiskToDeltaWithPositionSize(double risk, int size, int minDeltaByTic){
			double minimalDelta = minDeltaByTic * TickSize;
			double riskByDelta = DivideByPointValue(risk);
			double delta = riskByDelta/(double)size;

			PositionRisk pr;
			//When size == 1 if delta shorter than minimalDelta size shoudl be 0 (no trade)
			if(delta < minimalDelta && size <= 1){
				pr = new PositionRisk(this, 0, delta);
			}else{
				double resultDelta = Math.Max(delta, minimalDelta);
				pr = new PositionRisk(this,size,resultDelta);
			} 
			return pr;
		}
		//
		//  Calculate PositionRisk based on Risk and Stop Delta
		//    Input risk: Risk amount for trade,  StopDelta: stop delta that will take.
		//    Output PosiotnRisk
		//    v1.0 Crated in 2018/08/29
		//
		public PositionRisk CalculatePositionRiskWithStopDelta(double risk, double stopDelta){
			int maxSize = TNKPositionMaximum;  //This maximum position size should be calculated with clearing margin
			int minDeltaByTic = 10; //Ticks v3.9.21
			//int minDeltaByTic = 20; //Ticks  Test
			//double minimalDelta = minDeltaByTic * TickSize;
			//double delta = Math.Max(stopDelta, minimalDelta);			
			//PositionRisk pr = RiskToPositionSizeWithDelta(risk, stopDelta, maxSize);  //Place stop on just Volatlity line
			PositionRisk pr = RiskToPositionSizeWithDeltaUsingFullRisk(risk, stopDelta, maxSize);
			if(pr.Size > 0){
				// It is ok to take position with given stopDelta. 
			}else{
				// Risk was not sufficient to take postion with given stopDelta.
				// Assume position size 1 and calculate passible delta.
				int size = 1;
				pr = RiskToDeltaWithPositionSize(risk, size,  minDeltaByTic);
			}
			LogDebug("CalculatePositionRiskWithStopDelta()",MemoND("risk",risk),MemoNO("pr",pr));
			return pr;
		}
		
		//
		//  Calculate Posision Size based on Risk and Stoploss Delta.
		//   Position Size contains Position size and initilRiskDelta
		//
		public PositionSize CalculatePositionSizeWithSLD(double stopDelta){
			// Signicant parameter for N225M
			//PARAM RiskToTake = unitRisk * rsikFactor
			//double riskFactor = 1.0; 
			double riskFactor = 0.3;
			//
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double riskToTake = unitRisk * riskFactor;
			double unitRiskDelta = DivideByPointValue(riskToTake);
			//
			int size = (int)Math.Floor(unitRiskDelta / stopDelta);
			PositionSize ps = new PositionSize(size, stopDelta);
			return ps;
		}
		
		//
		//  Calculate Posision Size
		//   Position Size contains Position size and initilRiskDelta
		//   v1.0 Created in 2018/07/23
		//
		public PositionSize CalculatePositionSizeWithVolatility(){
			// Signicant parameter for N225M
			double volatilityFactor = 1.0; //Multiplyer for sigma
			double minimumStopDeltaByTic = 20;  //Tic  5 points for N225M
			//
			double minimumStopDeleta = minimumStopDeltaByTic * TickSize;
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double unitRiskDelta = DivideByPointValue(unitRisk);
			double volatility = Sigma() * volatilityFactor;
			//
			double stopDeleta = TakeBigger(volatility, minimumStopDeleta);
			int positionSize = (int)Math.Floor(unitRiskDelta/stopDeleta);			
			//
			PositionSize ps = new PositionSize(positionSize,stopDeleta);
			LogMessage("CalculatePositionSizeWithVolatility()",
				MemoNI("ps.size",ps.Size),
				MemoND("ps.initialRiskDelta",ps.InitialRiskDelta),
				MemoND("unitRiskDelta",unitRiskDelta),
				MemoND("volatility",volatility),
				MemoND("minimumStopDeleta",minimumStopDeleta)
			);
			return ps;
		}
		
		//
		//  Determin current Risk with sigma and two-sigama
		//    Returns case label
		// 
		public string RiskRangeBySigma(){
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double unitRiskDelta = DivideByPointValue(unitRisk);　//UnitRisk by Delta of Value.
			double sigma = Sigma();
			double rsRatio = unitRiskDelta/sigma;
			if (rsRatio >= 2.0)                  return "R2";   //2.0 >= R
			if (rsRatio < 2.0 && rsRatio >= 1.0) return "R21";  //2.0 > R >= 1.0
			if (rsRatio < 1.0 && rsRatio >= 0.5) return "R105"; //1.0 > R >= 0.5
			if (rsRatio < 0.5 && rsRatio >  0.0) return "R050"; //0.5 > R >  0.0
			if (rsRatio == 0.0) 				 return "R0";   //R = 0
			return "ERROR";
		}		
		//
		//  Calculate Posision Size
		//   Position Size contains Position size and initilRiskDelta
		// 
		public PositionSize CalculatePositionSizeWithSigma(){
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double unitRiskDelta = DivideByPointValue(unitRisk);
			double sigma = Sigma();
			double twoSigma = sigma*2.0;
			string rangeLabel = RiskRangeBySigma();
			int positionSize = 0;
			double initialRiskDelta = 0.0;
			int resultPositionSize = 0;
			double resultInitialRiskDelta = 0.0;
			
			switch(rangeLabel){
				case "R2":  //2.0 >= R
					//Limit sigma to avoid too small initialRiskDelta
					//double lowerLimitSigma = 50.0; //SIGNIFICANT PARAMETER: Yen. 50.0:10tic:5000yen DEPANDENT in N225M Test at 2016/12/29
					double marginTickCount = 10; //Tics  TicSize = 5 in N225M SIGNIFICANT PARAMETER: 10 ticks(50yen) is best performance. 2017/04/05
					double lowerLimitSigma = TickSize * marginTickCount;
					double divider = Math.Max(sigma,lowerLimitSigma);
					positionSize = (int)Math.Floor(unitRiskDelta/divider);
					LogDebug("CalculatePositionSizeWithSigma(2)",MemoNI("TNKPositionMinimum",TNKPositionMinimum),MemoNI("TNKPositionMaximum",TNKPositionMaximum),MemoNI("positionSize",positionSize),MemoND("unitRiskDelta",unitRiskDelta),MemoND("divider",divider));
					//initialRiskDelta = twoSigma; //ISSUE over risk
					initialRiskDelta = unitRiskDelta/positionSize;　//DTRT
					break;
				case "R21":  //2.0 > R >= 1.0
				case "R105": //1.0 > R >= 0.5
					positionSize = 1;
					//initialRiskDelta = twoSigma; //ISSUE over risk
					initialRiskDelta = unitRiskDelta; //DTRT
					break;
				case "R050": //0.5 > R >  0.0
					positionSize = 0;
					initialRiskDelta = 0;
					break;
				default:
					LogError("CalculatePositionSizeWithSigma()",MemoNS("rangeLabel",rangeLabel));
					break;
			}
			//Check limit
			resultPositionSize = TakeBigger( (int)positionSize,TNKPositionMinimum);
			resultPositionSize = TakeSmaller((int)positionSize,TNKPositionMaximum);
			resultInitialRiskDelta　= initialRiskDelta;
			PositionSize ps = new PositionSize(resultPositionSize,resultInitialRiskDelta);
			
			LogMessage("CalculatePositionSizeWithSigma()",
				MemoNS("rangeLabel",rangeLabel),
				MemoNI("ps.size",ps.Size),
				MemoND("ps.initialRiskDelta",ps.InitialRiskDelta),
				MemoND("unitRiskDelta",unitRiskDelta),
				MemoND("sigma",sigma),
				MemoND("twoSigma",twoSigma)
			);
			return ps;
		}
		
		public void SetInitialStopPrice(double stopPrice){
			// Calculate Stop Price
			//double stopPrice   = CalculateInitialStopLossPrice();
			// Set Stop Price
			SetStopPrice(stopPrice);

			return;
		}
		//
		//  Determin Initial Trade Term
		//    Depend Global Val: None
		//    External method: none
		public int CalculateInitialTradeTerm(){
			//int initialTradeTerm = TNKInitialTradeTerm;  //Assume 10 bar for Proof Of Concept. 
			int initialTradeTerm = TradeTermConsideringRemainsBarsInSession();
			LogDebug("CalculateInitialTradeTerm()",MemoNI("initialTradeTerm",initialTradeTerm));
			return initialTradeTerm;
		}
		//
		public int TradeTermConsideringRemainsBarsInSession(){
			int baseTradeTerm = TNKInitialTradeTerm;
			int tt = Math.Min(baseTradeTerm,RemainBarsInSession());
			tt= Math.Max(3,tt);
			return tt;
		}
		
		//
		//  Calculate Initial Profit Target Detlta
		//
		//
		public double CalculateInitialProfitTargetDelta(){
			double targetDelta = 0.0;
			double sigma = Sigma();
			double twoSigma = sigma * 2.0;
			double threeSigma = sigma * 3.0;
			
//			// Trial.  No good result. fix towSigma was better.
//			// if is in trend, use privious profit as target margin.
//			//
//			TradeSpec ltts = GetTerminatedTradeSpec(0);
//			if(ltts != null && ltts.profitDelta > 0 && CurrentBar - ltts.exitBarNo <4 ){
//				targetDelta = ltts.profitDelta;
//				LogDebug("CalculateInitialProfitTargetDelta()",MemoND("targetDelta",targetDelta));
//			}else{
//				targetDelta = twoSigma;;
//			}
			
			targetDelta = twoSigma;
			
			//
			double capTargetDelta = 10000; //NOCAP
			//Select value
//			targetDelta = Math.Max(targetDelta, twoSigma);　//v2.2.13
//			targetDelta = Math.Max(twoSigma,swingRange);
//			targetDelta = Math.Max(sigma,swingRange);
//			targetDelta = twoSigma;
//			targetDelta = swingRange > 0.0 ? swingRange : twoSigma;
			
			//Cap value
			targetDelta = Math.Min(targetDelta,capTargetDelta);　//Cap target delta

			LogDebug("CalculateInitialProfitTargetDelta()",
				MemoND("targetDelta",targetDelta),MemoNB("Capped!",targetDelta==capTargetDelta),
				MemoND("TNKSwingRange",TNKSwingRange),MemoND("twoSigma",twoSigma),MemoND("capTargetDelta",capTargetDelta)
			);
			return targetDelta;
		}
		
		public double CalculateInitialRiskByDelta(){
			//double atrIR = Math.Abs(ATR(5)[0]　*　TNKVolatilityRiskFactor);
			double factor = 2.0; //HOT
			double sigmaIR = StdDev(14)[0]*factor;
			double initialRisk = sigmaIR;
			return initialRisk;
		}
		
		public double CalculateInitialRiskParContract(){
			//Ammount(Sougaku) of Initial Risk par contract
			double initialRisk = CalculateInitialRiskByDelta();
			double initialRiskParContract = MulPointValue(initialRisk); 
			return initialRiskParContract;
		}

		public double AccountSize(){
			double accountSize = 0.0;
			if (IsInReal()) {
				//accountSize = Account.Get(AccountItem.BuyingPower, Currency.JapaneseYen); // 0.0 in historical bars.
				accountSize = Account.Get(AccountItem.CashValue, Currency.JapaneseYen); // 0.0 in historical bars.				
				//accountSize = 750000; //Just for test.
			}else{
				accountSize = TNKBackTestAccountSize;
			}
			LogDebug("AccountSize()",MemoNB("IsInReal()",IsInReal()),MemoND("accountSize",accountSize));
			return accountSize;
		}
		
		public double CalcUnitRisk(int tradeUnit){
			double accountSize = AccountSize();
			double unitRisk = accountSize/tradeUnit;
			LogDebug("CalcUnitRisk()", MemoND("accountSize",accountSize),MemoND("unitRisk",unitRisk));
			return unitRisk;			
		}

    }
}