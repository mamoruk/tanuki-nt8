//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Collections.Generic;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//
		//  Execute on Start of OnBarUpdate
		//
		public void UpdatePreamble(){
			// Update IVars
			// BO borders
			double sigma = StdDev(TNKNBOPeriod)[0];
			TNKBOMidLevel  = SMA(TNKNBOPeriod)[0];
			TNKBOUpLevel   = TNKBOMidLevel + TNKNBOBorder*sigma;
			TNKBOLowLevel = TNKBOMidLevel - TNKNBOBorder*sigma;
			//
			if(Bars.IsFirstBarOfSession){
				UpdateOnNewSessionStart();
			}
			UpdateCurrentTradeSpec();
		}
		
		//
		//  Execute on End of OnBarUpdate
		//
		public void UpdatePost(){
			//Update TradeSpecs in HistoryList
			UpdateMandatory();
			UpdateTradeSpec();
			UpdateEventPerformanceRank();
			UpdateStats();
		}

		public class Stats{
			public double TotalMove{get; set;}
		}
		public class EventPerformance{
			public string eventName{get; set;}
			public double totalDelta{get; set;}
			public int count{get; set;}
			public int  rank{get; set;}
			public int rankPrevious{get; set;}
			public double averageDelta{
				get{ return this.count == 0? 0:this.totalDelta/this.count;}
				set{}
			}
			//Constructor
			public EventPerformance(string en){
				eventName = en;
				totalDelta = 0.0;
				count = 0;
				rank = 0;
				rankPrevious = 0;
			}
			//Description
			public string Description(){
				string desc = eventName + " " + count.ToString() + " " + totalDelta.ToString()+ " " + averageDelta.ToString() ;
				return desc;
			}
		}

		//
		// Update on new session start
		//
		public void UpdateOnNewSessionStart(){
			TNKFirstBarIndexOfCurrentSession = CurrentBar;
			LogDebug("UpdatePreamble()",MemoNI("TNKFirstBarIndexOfCurrentSession",TNKFirstBarIndexOfCurrentSession));
			TNKInSessionDOTENSignalCount = 0;
			LogDebug("UpdatePreamble()",MemoNI("TNKInSessionDOTENSignalCount reset to",TNKInSessionDOTENSignalCount));
			return;
		}
		//
		// Update Current TradeSpec
		//
		public void UpdateCurrentTradeSpec(){
			if(TNKWorkingTradeSpec != null) {
				TNKWorkingTradeSpec.UpdateProfit();
			}
			return;	
		}
		
		//
		// Update for mandatory tasks
		//  DO NOT REMOVE
		public void UpdateMandatory(){
			RecordVars();
		}
		
		//
		//  Record Vars
		//
		public void RecordVars(){
			//Record　TNKSwingRange
			if(TNKSwingRange<0.0) return;
			AddToHistoryIfValueChanged(TNKSwingRangeHistory, TNKSwingRange);
		}
		// Add value to hitory if value chaged over chageThreshold (>0.0)
		public void AddToHistoryIfValueChanged(List<double> history, double val, double changeThreshold){
			int count = history.Count;
			if (count == 0){
				history.Add(val);
			}else{
				double last = history[count-1];
				last = last == 0.0 ? 1.0 : last;
				double change = Math.Abs((val - last)/last);
				if (change >= changeThreshold){
					//Add new record
					history.Add(val);
					LogDebug("AddToHistoryIfValueChanged()",MemoNI("count",count),MemoND("change",change),MemoND("last",last),MemoND("val",val));
				}else{
					//Do nothing. 
				}
			}
		}
		public void AddToHistoryIfValueChanged(List<double> history, double val){
			double changeThreshold = 0.1; //10%
			AddToHistoryIfValueChanged(history,val,changeThreshold);
		}
		//
		// Update Statistics
		//
		public void UpdateStats(){
			TNKStats.TotalMove += Math.Abs(Move(0));
		}
		
		//
		// Update Trade performance by Trigger Events
		//
		public void UpdateEventPerformanceRank(){
			//MakeList of doneUpdate TradeSpecs from TNKTradeSpecHistory
//			List<TradeSpec> doneUpdateList = TNKTradeSpecHistory.FindAll(
//				ts => (ts.doneUpdate)
//			);
			List<TradeSpec> doneUpdateList = TNKTradeSpecHistory.FindAll(
				ts => (ts.doneUpdate) && (ts.exitBarNo >TNKEventPerformanceLastProcessNo)
			);
			//list<TradeSpec> doneUpdateList = TNKTradeSpecHistory.FindAll(ts => ts.isExecuted && ts.doneUpdate);
			
//			if(TNKTradeSpecHistory.Count>0){
//			TradeSpec last = TNKTradeSpecHistory[TNKTradeSpecHistory.Count-1];
//			LogDebug("UpdateEventPerformanceRank()",MemoNI("TNKTradeSpecHistory.count",TNKTradeSpecHistory.Count));
//			LogDebug("UpdateEventPerformanceRank()",MemoNI("TNKEventPerformanceLastProcessNo",TNKEventPerformanceLastProcessNo));
//			LogDebug("UpdateEventPerformanceRank()",MemoNB("last.doneUpdate",last.doneUpdate),MemoNI("last.executedBarNo",last.executedBarNo));
//			LogDebug("UpdateEventPerformanceRank()",MemoNI("doneUpdateList.count",doneUpdateList.Count));
//			}
			
			//Sum up valueDelta by event in TNKEventPerformanceRank
			EventPerformance ep;
			foreach(TradeSpec ts in doneUpdateList){
				try{
					ep = TNKEventPerformanceRank[ts.eventName];
					LogDebug("UpdateEventPerformanceRank()",MemoNS("Found ep",ep.Description()));
				}catch (KeyNotFoundException){
					ep = new EventPerformance(ts.eventName);
					TNKEventPerformanceRank.Add(ts.eventName,ep);
					LogDebug("UpdateEventPerformanceRank()",MemoNS("New ep",ep.Description()));
				}
				ep.totalDelta += ts.profitDelta;
				ep.count++;
				TNKEventPerformanceLastProcessNo = ts.exitBarNo;
			}
			//LogDebug("UpdateEventPerformanceRank()",MemoNI("TNKEventPerformanceLastProcessNo",TNKEventPerformanceLastProcessNo));
			// Set worst event name to TNKWorstPerformanceEventContinuous
			List<EventPerformance>epList = EventPerormanceRankingList();
			if(epList.Count>0 && epList[0].averageDelta <0.0){
				TNKWorstPerformanceEventContinuous = epList[0].eventName;
			}else{
				TNKWorstPerformanceEventContinuous="";
			}
			// if in historical data, update also TNKWorstPerformanceEventHistorical
			if(IsInHistorical()) {
				TNKWorstPerformanceEventHistorical = TNKWorstPerformanceEventContinuous;
			}
		}
		
		public List<EventPerformance>EventPerormanceRankingList(){
//			List<KeyValuePair<string,EventPerformance>> eps = new List<KeyValuePair<string,EventPerformance>>(TNKEventPerformanceRank);
			var eps = TNKEventPerformanceRank.Values;
			List<EventPerformance> epList = new List<EventPerformance>(eps);
			epList.Sort(
				delegate(EventPerformance a,EventPerformance b){
					return a.averageDelta.CompareTo(b.averageDelta);
					//return a.totalDelta.CompareTo(b.totalDelta);
				}
			);
			return epList;
		}
		//
		//　Update Trade Spec
		//　　profitDelta field for trade perfomance evealuation
		//
		public void UpdateTradeSpec(){
			//Pickup tradeSpec needs to update
			List<TradeSpec> tsList = TNKTradeSpecHistory.FindAll(ts => ts.doneUpdate==false);
			//LogDebug("UpdateTradeSpec()",MemoNI("tsList.Count",tsList.Count));
			//Upadte
			foreach(TradeSpec ts in tsList){
				//update profit field
				string signal = ts.signal;
				if(ts.isExecuted){
					//Check for exit trade
					//NT7 int tradeCount  = Performance.AllTrades.Count;
					int tradeCount  = SystemPerformance.AllTrades.Count;
					if(tradeCount >0){
						//NT7 Trade lastTrade = Performance.AllTrades[tradeCount - 1];
						Trade lastTrade = SystemPerformance.AllTrades[tradeCount - 1];
						if(lastTrade.Entry.Order == ts.entryOrder){
							//Trade completed
							ts.profitDelta = (lastTrade.Exit.Price - lastTrade.Entry.Price)*SignalSign(signal);
							ts.exitBarNo = CurrentBar;
							ts.doneUpdate = true;
							LogDebug("UpdateTradeSpec() Executed",MemoNS("ts.eventName",ts.eventName),MemoNS("lastTrade.Entry.Name",lastTrade.Entry.Name),MemoND("ts.profitDelta",ts.profitDelta));
						} else {
							//Trade do not complete
							// Do nothing
						}
					}else{
						//No Signal is executed yet
						// Do nothing
					}
				}else{
					//Caluculate simulated profit
					if(CurrentBar > ts.executedBarNo + ts.term){
						LogDebug("UpdateTradeSpec()",MemoNI("CurrentBar",CurrentBar),MemoNI("ts.executedBarNo",ts.executedBarNo));
						ts.profitDelta = (Close[0] - Close[CurrentBar - ts.executedBarNo])*SignalSign(signal);
						ts.exitBarNo = CurrentBar;
						ts.doneUpdate = true;
						LogDebug("UpdateTradeSpec() Simulated",MemoNS("ts.eventName",ts.eventName),MemoND("ts.profitDelta",ts.profitDelta));
					}else{
						//Term do not elapse.
						//Do nothing.
					}
				}
			}
		}
    }
}