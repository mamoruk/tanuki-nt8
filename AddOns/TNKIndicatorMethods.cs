//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Gui.Chart;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all indicators and is required. Do not change it.
//namespace NinjaTrader.Indicator
namespace NinjaTrader.NinjaScript.Indicators
{
    /// <summary>
    /// This file holds all user defined indicator methods. (TRIAL)
    /// </summary>
    partial class Indicator
    {
		public double EffciencyRatio(int period)
		{
			double diff = Close[0] - Close[period-1];
			diff = Math.Abs(diff);
			double vola=0.0;
			for (int i=0; i<period; i++){
				vola = vola + Math.Abs(Close[i]-Close[i+1]);
			}
			return vola==0 ? 0 : diff/vola; 
		}
    }
}
