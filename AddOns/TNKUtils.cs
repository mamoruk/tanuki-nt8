//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Runtime.CompilerServices;
//using System.Diagnostics;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection; 
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
	// Global Enums;
	public enum TrendType {Up,UpRetracement,Down,DownRetracement,Range,Undefined,NotSpecified}
	public enum StateType {Up,UpRetracement,Down,DownRetracement,Range,Undefined,NotSpecified}
//	public enum EventType {
//		PeakUp,PeakDown,BottomUp,BottomDown,
//		PeakBreakUp,PeakBreakDown,BottomBreakUp,BottomBreakDown,
//		PeakReversal,BottomReversal,
//		PeakCorerelation,NoEvent
//	}
	public enum TaskType  {SetupLong,SetupShort,Exit,None}
	public enum SignalType {Buy,Sell,Reverse,Exit,None}
	
	partial class Strategy
    {
		//
		// Inisitalize some Ivars MUST call from Initial() of main thread.
		//
		public void InitUtil(){
			TNKTradeUnitRatio = 1.0/TNKTradeUnit;
			InitializeEventNameList();
			LogDebug("InitUtil()",MemoNS("TNKEventNameList",StringOfList(TNKEventNameList)));
		}
		
		//Instance Variables
		public string TNKVersionNumber = "0.0";
		public string TNKVersionDescription = "Default Description";
		public string TNKVersionBTPerformance;
		public bool   TNKDebug=true;
		//Candle pool
		public CandlePool TNKCandlePool = null;
		
		//Sesssion related vars
		public int TNKFirstBarIndexOfCurrentSession = 0;
		public int TNKInSessionDOTENSignalCount = 0;
		
		//First flag for Log Header
		public bool TNKDidLogHeader=false;
		//Log file name
		public static string TNKLogFileName = null;
		public static string TNKLogFolderName = "";
		public static bool   TNKDoesLogFile = false;
		//public string TNKLogFolderName = @"C:\Users\apple\Desktop\";
		//public string TNKLogFolderName = @"\\psf\Dropbox\Tanuki\Logs\";
		// Swing base trend detection
		public int TNKSwingPeriod = 3;
		// Market Condition Marking 
		public string TNKStateNameCurrent  = "RANGE";
		public string TNKStateNamePrivious = "UNDEFINED";
		public string TNKCurrentEventName  = "NOEVENT";
		public string TNKPreviousEvenName = "NOEVENT";	
		// Peak - Bootm 
		public double TNKRecentPeakValue = 0.0;
		public double TNKPreviousPeakValue = 0.0;
		public double TNKRecentBottomValue = 0.0;
		public double TNKPreviousBottomValue = 0.0;
		public double TNKRecentPeakBreakUpValue = 0.0;
		public double TNKRecentBottomBreakDownValue = 0.0;
		public double TNKThresholdReversalRatio = 1.0; //TEMPRAL 100% is sreshold of revese event. 100% is better than 50%.Fibonacchi Ratio?
		public double TNKSwingRange = -1.0;
		private List<double> TNKSwingRangeHistory = new List<double>();
		
		// Observed Event
//		public List<string>    TNKObservedEventNameList; //OBDELETE Var?
		public  List<string>        TNKMCObservedEventNameList;
		private List<ObservedEvent> TNKObservedEventList= new List<ObservedEvent>();
		private List<ObservedEvent> TNKEventHistory= new List<ObservedEvent>();
		// TradeSpec History
		private List<TradeSpec> TNKTradeSpecHistory = new List<TradeSpec>();
		private List<TradeSpec> TNKTerminatedTradeSpecHistory =new List<TradeSpec>();
		// TradePerformance
		private Dictionary<string,EventPerformance> TNKEventPerformanceRank = new Dictionary<string,EventPerformance>();
		private int TNKEventPerformanceLastProcessNo = -1;
  		private string TNKWorstPerformanceEventHistorical="";  //Worst Performace event name Decide with Hitorical date
		private string TNKWorstPerformanceEventContinuous=""; //Worst Performace event name Modify continuously
		// Var for Observers
		//   Initial Range Breakout (IRB)
		double TNKIRHigh = 0.0;
		double TNKIRLow = 0.0;
		bool   TNKIRValid = false;
		//		
		// Event Ovserve magins
		public double TNKMarginPeakBreakUp = 0.0;
		public double TNKMarginPeakBreakDown = 0.0;
		public double TNKMarginBottomBreakUp = 0.0;
		public double TNKMarginBottomBreakDown = 0.0;
		public double TNKMarginPeakUp = 0.0;
		public double TNKMarginPeakDown = 0.0;
		public double TNKMarginBottomUp = 0.0;
		public double TNKMarginBottomDown = 0.0;
		//
		static public double TNKNormalizedBreakoutBorder = 1.4;
		static public int    TNKNormalizedBreakoutPeriod = 14;
		public double TNKNBOBorder = TNKNormalizedBreakoutBorder;
		public int TNKNBOPeriod = TNKNormalizedBreakoutPeriod;
		public double TNKBOUpLevel, TNKBOLowLevel, TNKBOMidLevel; //Update in UpdatePreamble()
		//
		static public double TNKVolatilityChannelBreakoutBorder = 1.4;
		static public int    TNKVolatilityChannelPeriod = 14;		
		//
		// Maket Condition "UNCLEAR" or "TRNED" or "BOX"
		public string TNKTrendPresence = "UNCLEAR";
		// Market Cycle "ROOF","FLOOR","UNCLEAR"
		public string TNKMarketCycle = "UNCLEAR";
		//
		// Filters
		public bool   TNKRejectAllSignals = false;
		public string TNKRejectReason = "";
		public string TNKTerminateReason = "";
		//
		// Trade Specifications
		//   Position Sizing, Initial Stop Loss, Inisital Target Price, Initial Trade Term
		public bool   TNKDoCapInitialRisk = false;
		public int    TNKPositionMinimum = 0;  //This prameter should be set on Main Stragery source code.
		public int    TNKPositionMaximum = 0;  //This prameter should be set on Main Stragery source code.
//		public int    TNKPositionSize = 0; 		 // DefaultQuantity;　//Obsolete Var. //TBDel
		public int    TNKTradeUnit = 50;		 // ISSUE: Assume TradeUnit as 50
		public double TNKTradeUnitRatio; 		// 1/TNKTradeUnit Init in TNKInitUtil()
		private double TNKUnitRisk = 0.0; 		 // Risk ammount of each trade unit. calc and set in TNKSetUnitRisk();
		public double TNKBackTestAccountSize = 1000000.0; //100 Man yen for backtest.
		// 
		public double TNKVolatilityRiskFactor = 2.0;  //InitialRisk = ATR(n)*TNKVolatilityRiskFactor. override in Initialize().
		public double TNKInitialRisk = 0.0;
		public double TNKRawInitialRisk =0.0;
		public double TNKInitialStopLossDelta = 0.0; //Initial Stop Loss delta. Set in CalculateInitialStopLossDelta()
		public double TNKInitialProfitTargetDelta = 0.0; //Initial Profit Target delta. set in CalculateInitialProfitTargetDelta()
		public double TNKInitialRiskParContract = 0.0;      //Initial Risk Money par Contract. Set in TNKDetermineInitialRisk()
		//
		public int    TNKInitialTradeTerm = 20;//Inital Trade Term. Assume 20 bars for 10 min period.
		public int    TNKTradeTerm;
		//
		// Position Management
//TBDel	public int    TNKBarsInSession = 18; //DEPEND in N225M and OSE
		private TradeSpec TNKWorkingTradeSpec = null;
		private TradeSpec TNKPreviousTradeSpec = null;
		public double TNKStopLossPrice = 0.0;
		public double TNKTargetPrice = 0.0;
		public double TNKInitialProfitTargetFactor = 1.0; // Profit Target = Close +(-) StopLossDeleta * TNKTargetPriceFactor. 0.0:omit PT
		public double TNKRealStopLossPrice = 0.0; //Update in only OnOrderUpdate(IOrder order)
		public double TNKRealEntryPrice = 0.0;    //Update in only OnOrderUpdate(IOrder order)
		private Order TNKLatestOrder = null;
		public double TNKProfitTargetDelta = 0.0;
		public double TNKStopLossDelta = 0.0;
		public void SetWorkingTradeSpec(TradeSpec ts){
			if(TNKWorkingTradeSpec !=null){TNKPreviousTradeSpec = TNKWorkingTradeSpec;}
			TNKWorkingTradeSpec = ts;
		}

		//
		//Chart Marker
		public double TNKChartMarkPrevioudBL=0.0;
		public int    TNKChartMarkPositionCount=0;
		public bool   TNKFlagMarkMCMarkers = false;
		//
		//Trade Evalutation
		public int TNKLatestTradeCount = 0; // for check if new trade was added.
		public string TNKLatestConditionName = ""; //for embed condition name as siganl name. 
		
		//Signal Management
		//public Dictionary<int,Dictionary<string,string>> TNKSignalPackets = new Dictionary<int,Dictionary<string,string>>();
		private List<Dictionary<string,string>> TNKSignalList = new List<Dictionary<string,string>>();

		//Condition Name Definition
		//UP/DOWN side name
		private readonly List<string> TNKSideNameList = new List<string>(){
			"UPSIDE","DOWNSIDE","NOSIDE"
		};
		//Event Category name
		private readonly List<string> TNKEventCategoryNameList = new List<string>(){
			"UPDATE","BREAK","NOEVENT"
		};
		//Trend Definition
		private readonly List<string> TNKStateNameList = new List<string>(){
			"UP","UPRET","DOWN","DOWNRET","RANGE","UNDFINED","NOTSPECIFIED"
		};
		private readonly Dictionary<TrendType, string> TNKTrendName= new Dictionary<TrendType, string>()
		{
			{TrendType.Up,        			"UP"},
			{TrendType.UpRetracement,       "UPRET"},
			{TrendType.Down,      			"DOWN"},
			{TrendType.DownRetracement,     "DOWNRET"},
			{TrendType.Range,     			"RANGE"},
			{TrendType.Undefined, 			"UNDFINED"}
		};

		//Event Definition
		public List<string> TNKEventNameList;
		private readonly List<string> TNKConditionEventNameList = new List<string>(){
			"PEAKUP","PEAKDOWN","BOTTOMUP","BOTTOMDOWN","PEAKBREAKUP","PEAKBREAKDOWN",
			"BOTTOMBREAKUP","BOTTOMBREAKDOWN","PEAKREVERSAL","BOTTOMREVERSAL","NOEVENT"
		};
		private readonly List<string> TNKGeneralEventNameList = new List<string>(){
			"EN_VERIF_SESSION_BIGINNING",
			"EN_VCBO_UPPERBREAKUP","EN_VCBO_LOWERBREAKDOWN",
			"EN_IRBOWOBB_UPPER_BREAKUP","EN_IRBOWOBB_LOWER_BREAKDOWN",
			"EN_IRB_UPPER_BREAKUP","EN_IRB_LOWER_BREAKDOWN","EN_IR_UP_MOMENTUM","EN_IR_DOWN_MOMENTUM",
			"EN_VAC_HIGH_CROSS_UP","EN_VAC_LOW_CROSS_DOWN","EN_VAC_UP_MOMENTUM","EN_VAC_DOWN_MOMENTUM",
			"EN_NB_UPPERBREAKUP", "EN_NB_LOWERBREAKDOWN", "EN_NB_UPMOMENTUM", "EN_NB_DOWNMOMENTUM",
			"EN_BB_BIGPOSITIVEBAR","EN_BB_BIGNEGATIVEBAR",
			"EN_CD_CONTINUOUS_POSITIVE","EN_CD_CONTINUOUS_NEGATIVE",
			"EN_DC_UP","EN_DC_DOWN"
		};
		public List<string> InitializeEventNameList(){
			CC_All_Conditions = KeyListOfDictionary(TNKStateTransitionDict);
			TNKEventNameList = CC_All_Conditions.Concat(TNKGeneralEventNameList).ToList();
			return TNKEventNameList;
		}

		// Event Category Dictionary
		private readonly Dictionary<string,string> TNKEventCategory= new Dictionary<string,string>()
		{
			{"PEAKUP",    "UPDATE"},
			{"PEAKDOWN",  "UPDATE"},
			{"BOTTOMUP",  "UPDATE"},
			{"BOTTOMDOWN","UPDATE"},
			{"PEAKBREAKUP",    "BREAK"},
			{"PEAKBREAKDOWN",   "BREAK"},
			{"BOTTOMBREAKUP",   "BREAK"},
			{"BOTTOMBREAKDOWN", "BREAK"},
			{"PEAKREVERSAL",    "BREAK"},
			{"BOTTOMREVERSAL",  "UPDATE"},
			{"PEAKCORERELATION","UPDATE"},
			{"NOEVENT","NOEVENT"}		
		};
		
		// UPSIDE/DOWNSIDE Dictionary		
		private readonly Dictionary<string, string> TNKStateUpDownSide= new Dictionary<string, string>()
		{
			{"UP"     ,"UPSIDE"},
			{"UPRET"  ,"UPSIDE"},
			{"DOWN"   ,"DOWNSIDE"},
			{"DOWNRET","DOWNSIDE"},
			{"RANGE"  ,"NOSIDE"}
		};
		
		// UPSIDE/DOWNSIDE Dictionary
		private readonly Dictionary<string, string> TNKEventUpDownSide= new Dictionary<string, string>()
		{
			{"PEAKUP",    "UPSIDE"},
			{"PEAKDOWN",  "UPSIDE"},
			{"BOTTOMUP",  "DOWNSIDE"},
			{"BOTTOMDOWN","DOWNSIDE"},
			{"PEAKBREAKUP",    "UPSIDE"},
			{"PEAKBREAKDOWN",   "UPSIDE"},
			{"BOTTOMBREAKUP",   "DOWNSIDE"},
			{"BOTTOMBREAKDOWN", "DOWNSIDE"},
			{"PEAKREVERSAL",    "UPSIDE"},
			{"BOTTOMREVERSAL",  "DOWNSIDE"},
			{"PEAKCORERELATION","UPSIDE"},
			{"NOEVENT","NOSIDE"}		
		};		
		//Trend Color for marking on chart.
//NT7		Dictionary<string,Color> _NT7_TNKTrendColorDict = new Dictionary<string,Color> {
//			{"UP",Color.LemonChiffon},{"UPRET",Color.Ivory},
//			{"DOWN",Color.PaleTurquoise},{"DOWNRET",Color.LightCyan},
//			{"RANGE",Color.Empty}
//NT7	};
		Dictionary<string,Brush> TNKTrendColorDict = new Dictionary<string,Brush> {
			{"UP",Brushes.LemonChiffon},{"UPRET",Brushes.Ivory},
			{"DOWN",Brushes.PaleTurquoise},{"DOWNRET",Brushes.LightCyan},
			{"RANGE",null}
		};

		// General Counter. Use for get uniq number in single execution.
		public int TNKUniqNumberCounter = 0;
		// General stats for updateStats();
		public Stats TNKStats = new Stats();

		public string ConditionName(string trendName, string eventName){
			return string.Format("{0:s}:{1:s}",trendName,eventName);
		}
		public static string [] SplitConditionName(string conditionName){return conditionName.Split(new Char[]{':'});}
//		public static string StateNameOf(string conditionName){return SplitConditionName(conditionName)[0];}
//		public static string EventNameOf(string conditionName){return SplitConditionName(conditionName)[1];}		
		public static string StateNameOf(string conditionName){
			string[] comp = SplitConditionName(conditionName);
			string result = "";
			if (comp.Length == 1){
				//conditonName may be simple Event Name
				result = "";
			}else{
				//condtionName has state name and evnt name.
				result = comp[0];
			}
			return  result;
		}
		public static string EventNameOf(string conditionName){
			string[] comp = SplitConditionName(conditionName);
			string result = "";
			if (comp.Length == 1){
				//conditonName may be simple Event Name
				result = comp[0];
			}else{
				//2nd comp has event name
				result = comp[1];
			}
			return  result;			
			return SplitConditionName(conditionName)[1];
		}
		public string DisplayEventName(string eventName){
			string result = eventName.Replace("EN_","");
			return result;
		}

		//
		//Utility Methods
		//
	    static void Swap<T>(ref T a, ref T b) {
	        T t = a;
	        a = b;
	        b = t;
	    }
		//Convert BarNumber to Ago to the bar
		int NumToAgo(int barNumber){ return CurrentBar - barNumber;}
		
		//
		// Method for time range check
		//
		// Check if given time is in time range. All paremeter is minuntes expression.  IE. 12:30 = 12 * 60 + 30 = 750
		public bool IsInTimeRange(int theTime, int startTime, int endTime){
			return (theTime >= startTime && theTime <= endTime);
		}
		public int TimeByMinutes(int hour, int minute){
			return (hour * 60 + minute);
		}
		public int CurrentTimeByMinutes(){
			DateTime currentTime = Bars.GetTime(CurrentBar);
			return TimeByMinutes(currentTime.Hour,currentTime.Minute);
		}
			
		//
		// Method Invocation with String of method name
		//
		public object InvokeMethod(string name, object[] para){
			Type t = this.GetType();     
			MethodInfo mi = t.GetMethod(name);
			object result = mi.Invoke(this, para);
			return result;
		}

		public object InvokeWithNameListWhileReturnIsFalse(List<string> names, Object[] args){
			bool lastResult = false;
			foreach(string name in names){
				LogDebug("InvokeWithNameListWhileReturnIsFalse()",MemoNS("Going to call",name));  //DEBUG
				lastResult = (bool)InvokeWithName(name,args);
				LogDebug("InvokeWithNameListWhileReturnIsFalse()",MemoNS("Retuned from",name),MemoNB("lastResult",lastResult));  //DEBUG
				if (lastResult == true){
					break;
				}else{
					continue;
				}
			}
			return lastResult;
		}
		public Dictionary<string,object> InvokeWithNameList(List<string> names, Object[] args){
			Dictionary<string,object> results = new Dictionary<string,object>();
			object temp;
			foreach(string name in names){
//				LogDebug("InvokeWithNameList()",MemoNS("Going to call",name));  //DEBUG
				temp = (object)InvokeWithName(name,args);
//				LogDebug("InvokeWithNameList()",MemoNS("Retuned from",name));  //DEBUG
				results[name] = temp;
			}
			return results;
		}
		public object InvokeWithName(string name, object[] args){
			Type t = this.GetType();
			MethodInfo mi = t.GetMethod(name);
			object result = (object)mi.Invoke(this, args);
			return result;
		}
		
		// Return if current var is in real or historical
		//NT7 public bool IsInReal(){return !Historical;}
		//NT7 public bool IsInHistorical(){return Historical;}
		public bool IsInReal(){return State == State.Realtime;}
		public bool IsInHistorical(){return State == State.Historical;}
		//
		//
		// Retrun range of move. move define as Close - Open.
		//
		public double Move(int lookback){
			double move = Close[lookback] - Open[lookback];
			return move;
		}
		// Return Rage define as High - Low
		public double Range(int lookback){
			double range = High[lookback] - Low[lookback];
			return range;
		}
//TBDel
//		// Return Remain bars in session
//		public int _RemainBarsInSession(){
//			int remainBarsInSession = TNKBarsInSession - Bars.BarsSinceNewTradingDay;
//			return remainBarsInSession;
//		}
//TBDel

		// Return DateTime array of Session start time and session end time
		public List<DateTime> SessionStartTimeAndEndTime(){
			SessionIterator sessionIterator = new SessionIterator(Bars);
			sessionIterator.GetNextSession(Time[0], true);
 			DateTime beginTime = sessionIterator.ActualSessionBegin;
			DateTime endTime   = sessionIterator.ActualSessionEnd;
			return new List<DateTime>{beginTime, endTime};
		}
		
		// Return Remain bars in session (not be includeed current bar)
		public int RemainBarsInSession(){
			SessionIterator sessionIterator = new SessionIterator(Bars);
			sessionIterator.GetNextSession(Time[0], true);
 			DateTime beginTime = sessionIterator.ActualSessionBegin;
			DateTime endTime   = sessionIterator.ActualSessionEnd;
			int interval = BarsPeriods[0].Value;;
			if(BarsPeriod.BarsPeriodType == BarsPeriodType.Minute){
				interval = interval;
			}else if(BarsPeriod.BarsPeriodType == BarsPeriodType.Day){
				interval = interval * (60 * 24);
			}else{
				LogError("RemainBarsInSession()",TAGMsg("BarsPeriodType is nether minute nor day."));
			}
			TimeSpan ts = endTime - Time[0];
			int remainMinutes = (int)ts.TotalMinutes;
			int remainCount = remainMinutes / interval;
			LogDebug("RemainBarsInSession()",MemoNT("Time[0]",Time[0]),MemoNT("endTime",endTime),MemoNI("remainMinutes",remainMinutes),MemoNI("interval",interval));
			return remainCount;
		}

		// Return if the value is within the band
		//  "UPSIDE", "DOWNSIDE","INSIDE"
		// 3 parameter version
		public string PositionAgainstRange(double val, double upper, double lower){
			return PositionAgainstRange(val, upper, lower, 0.0);
		}
		// 4 parameter version. Margin expands/narrows the range wider as offset. positive value exapnd, negative value narrows.
		public string PositionAgainstRange(double val, double upper, double lower, double margin){
			//swap border if band is upside down.
			if(upper < lower){
				double temp = upper;
				upper = lower;
				lower = temp;
			}
			if (val > upper + margin){
				return "UPSIDE";
			} else if (val < lower - margin){
				return "DOWNSIDE";
			} else {
				return "INSIDE"; //On border is judged as INSIDE
			}
		}
		
		// Judge value is in Profit zone or Loss zone or on Even.
		// If called in flat position, return "UNCLEAR"
		//  outside means Profit zone, Inside means Loss zone and on border means Even. 
		// Return one of "PROFIT", "LOSS", "EVEN", "UNCLEAR"
		public string ProfitLossEven(double val, double border){
			string result = "UNCLRAR";
			if(haveLongPosition()){
			//In case of Long
				if(val > border){
					result = "PROFIT";
				}else if(val < border){
					result = "LOSS";
				}else{
					result = "EVEN";
				}
			}else if(haveShortPosition()){
			//in case of Short
				if(val < border){
					result = "PROFIT";
				}else if(val > border){
					result = "LOSS";
				}else{
					result = "EVEN";
				}
			}else{
			//in case of Flat
				result = "UNCLEAR";
			}
			return result;
		}
		
		// Return 1.0 when position is long, -1.0 when short, 0.0 when flat.
		public double PositionSign(){
			double sign=0.0;
			if(haveLongPosition()){
				sign = 1.0;
			}else if(haveShortPosition()){
				sign = -1.0;
			}else{
				sign = 0.0;
			}
			return sign;
		}
		// Return 1.0 when signalName is "BUY", -1.0 when signal name is "SELL", otherwise 0.0
		public static double SignalSign(string signalName){
			double sign = 0.0;
			switch(signalName){
				case "BUY":
					sign = 1.0;
					break;
				case "SELL":
					sign = -1.0;
					break;
				default:
					sign = 0.0;
					break;
			}
			return sign;
		}
		
		public double SigmaThisBar(int period){
			return StdDev(Close, period)[0];
		}
		
		public double Sigma(){
			int strandardPeriod = TNKNBOPeriod;
			return SigmaThisBar(strandardPeriod);
		}
		
		public double GetProfitAndLoss(){ //Old name TNKGetProfitAndLoss()
			//NT7 return Position.GetProfitLoss(Close[0], PerformanceUnit.Currency);
			return Position.GetUnrealizedProfitLoss(PerformanceUnit.Currency, Close[0]);
		}
		public double GetProfit(){
			return GetProfitAndLoss();
		}
		public double GetProfitParContract(){
			int positionSize = Position.Quantity;
			return GetProfit()/positionSize;
		}
		public double GetProiftDelta(){
			double ProfitDeltaParContract = DivideByPointValue(GetProfitParContract());
			return 	ProfitDeltaParContract;
		}
		
		public double MulPointValue(double val){return val * Instrument.MasterInstrument.PointValue;}
		public double DivideByPointValue(double val){return val / Instrument.MasterInstrument.PointValue;}
		public double moveToday(){return move(0);}
		public double move(int n){return Close[n]-Open[n];} //Minus when down
		//
		public int    TakeOneIfZero(int val){ return val == 0 ? 1:val;}
		public double TakeOneIfZero(double val){return val == 0.0 ? 1.0:val;}
		public int    TakeBigger(int val1, int val2) {return val1>=val2 ? val1:val2;}
		public double TakeBigger(double val1, double val2) {return val1>=val2 ? val1:val2;}
		public int    TakeSmaller(int val1, int val2) {return val1<=val2 ? val1:val2;}
		public double TakeSmaller(double val1, double val2) {return val1<=val2 ? val1:val2;}
		//
		// Gudge two value goes "UP", "DOWN" or "FLAT"
		//  if the difference bigger than margin. margin is always positive.
		public string IsUpOrDown(double f, double s){
			return IsUpOrDown(f, s, 0.0);
		}
		public string IsUpOrDown(double f, double s, double margin){
			double m = Math.Abs(margin);
			string result = "ERROR";
			if (s - f > m ) {
				result = "UP";
			}else if( f - s > m){
				result = "DOWN";
			}else{
				result = "FLAT";
			}
			return result;
		}
		//		
		public bool DoesContain(string s1, string s2){return s1.IndexOf(s2)==-1 ? false:true;}
		//
		public int TNKGetUniqNumber(){return TNKUniqNumberCounter++;}
		//
		public bool DoesPassWBList(string guest, List<string> white,List<string> black){
			// Checek if execution is allowd.
			bool isInWhite = white == null || white.Count==0 || white.Contains(guest);
			bool isInBlack = black != null && black.Count!=0 && black.Contains(guest);
			// Disable list override Enable list
			bool isOK = isInWhite && !isInBlack;
			return isOK;
		}
		//
		public string ConsultDictionary(string key, Dictionary<string,string>dict){
			string value = "";
			if (dict.TryGetValue(key, out value)){
				return value;
			}else{
				//LogError("ConsultDictionary()",MemoNS("Miss hit in dictionary",key),MemoNS("dict",StringOfDictionary(dict)));
				return null;
			}
		}
		// Take List from Dictionary
		public List<string> KeyListOfDictionary(Dictionary<string,string>dict){
			List<string> keyList = dict.Keys.ToList();
			return keyList;
		}
		public List<string> ValueListOfDictionary(Dictionary<string,string>dict){
			List<string> valList = dict.Values.ToList();
			return valList;
		}
		
		//
		//  List manipuration methods
		//
		public string StringOfList(List<string> list){
			if(list.Count==0) return "__empty__";
			string str="";
			foreach(string ent in list){str += (ent+" ");}
			return str.Remove(str.Length-1);
		}
		public string StringOfDictionary(Dictionary<string,string> items){
			string itemString = "";
			foreach(var item in items){
				itemString = itemString + String.Format("'{0}' => ",item.Key);
				itemString = itemString + String.Format("{0} ",item.Value);
			}
			return itemString;
		}
		public string StringOfDictionary(Dictionary<string,List<string>> items){
			string itemString = "";
			foreach(var item in items){
				itemString = itemString + String.Format("'{0}' => ",item.Key);
				itemString = itemString + String.Format("[{0}] ",StringOfList(item.Value));
			}
			return itemString;
		}
		//
		public int indexOfSeriesPosition(List<string>target,List<string>series){
			if(target.Count<series.Count) return -1;
			int cnt=target.Count-series.Count+1;
			for(int i=target.Count-series.Count;i>target.Count-cnt-2;i--){
				if(DoesContainSeries(target,series,i)){return i;}
			}
			return -1;
		}
		public bool DoesContainSeries(List<string>target,List<string>series,int position){
			if(target.Count < position + series.Count) return false;
			List<string> sub = target.GetRange(position,series.Count);
			//TNKLog(StringOfList(series)+"|"+StringOfList(sub));
			return DoesContainSameValue(series,sub);
		}
		//
		public bool DoesContainSameValue(List<string>a,List<string>b){
			if(a.Count != b.Count) return false;
			bool res=true;
			for(int i=0;i<a.Count;i++){
				if(a[i]!=b[i]){res=false;break;}
			}
			return res;
		}
		
		public string SearchValueInDict(string val, Dictionary<string,List<string>> dict){		
			//TNKLog(MemoNS("SearchValueInDict()val",val));
			//TNKLog(MemoNI("SearchValueInDict()dict.count",dict.Count));
			foreach (KeyValuePair<string, List<string>> kvp in dict) {
				//TNKLog(MemoNS("SearchValueInDict()kvp.Value",StringOfList(kvp.Value)));
				if(kvp.Value.Contains(val)) return kvp.Key;
			}
			return null;
		}
    }
}
