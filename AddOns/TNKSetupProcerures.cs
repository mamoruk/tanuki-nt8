//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion


// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>

    partial class Strategy
    {
		//Stab
//TBDel	public TrendType TNKJudgeTrend(){
//			return TNKJudgeTrendDMI();
//		}
//		// DBI Based trend mesurement
//		public TrendType TNKJudgeTrendDMI(){
//			double adx = ADX(14)[0];
//			if (adx<20){
//				return TrendType.Range;
//			}
//			if (DMI(14)[0]>=0){
//				return TrendType.Up;
//			}else{ 
//				return TrendType.Down;
//			}
//		}
		
		public class ObservedEvent{
			public string eventName{get; set;}
			public string eventType{get; set;} //Condition:"MC"  General Event:"GE"
			public int barNumber{get; set;}
			public double breakoutLevel{get; set;}
			public object addtionalData{get; set;}
			private Strategy owner;
			public ObservedEvent(string name,string type, int bar){
				eventName=name;
				eventType = type;
				barNumber=bar;
				addtionalData = null;
			}
			public string ConditionState(){
				if(eventType == "MC"){
					return Strategy.StateNameOf(eventName);
				}else{
					//owner.LogError("ObservedEvent.ConditionState()",Strategy.MemoNS("eventType",eventType));
					return "INVALID";
				}
			}
			public string ConditionEvent(){
				if(eventType == "MC"){
					return Strategy.EventNameOf(eventName);
				}else{
					//owner.LogError("ObservedEvent.CondtionEvent()",Strategy.MemoNS("eventType",eventType));
					return "INVALID";
				}
			}
			public string Description(){
				string result = "";
				result += "barNumber:"+barNumber.ToString();
				result += " eventType:"+eventType;
				result += " eventName:"+eventName;
				return result;
			}
		}
		
		//
		// Genarete Siganal based on Obseved Event
		//
		public List<TradeSpec> GenerateSignals(List<ObservedEvent> eventList ){
			LogDebug("GenerateSignals()",
				MemoNI("Input eventList.Count",eventList.Count)
			);

			List<TradeSpec> TradeSpecList =  GenerateTradeSpecList(eventList);
			return TradeSpecList;
		}
		
		//
		// Genarete Signals based on Swing idicator.
		//		Process observed events in TNKRecentObservedEventList
		// 		Condtion Hander(CH) calls Signal Generator(SG)
		// 		SG add signals to TNKSignalPackets
		//		These signals are fetched via TNKFetchSignal()
		//
		public List<TradeSpec> GenerateTradeSpecList(List<ObservedEvent> eventList){
			// new PositioSpecList to collect from event handlers.
			TradeSpec tradeSpec;
			List<TradeSpec> tsList = new List<TradeSpec>();
			//
			foreach(ObservedEvent oe in eventList){
				//Dispach Condition Handlers based on condition name
				tradeSpec = DispatchEventToHandlers(oe); //Change type of input and return values
				if(tradeSpec != null){
					LogDebug("GenerateTradeSpecList()",MemoNS("tradeSpec.Description()",tradeSpec.Description()));
					tsList.Add(tradeSpec);
				}
			};
			return tsList;
		}
		
		//Remve redudant noevent from list
		// (NOEVENT,NOEVENT)  => (NOEVENT)  
		// (anyevent,NOEVENT) => (anyevent), (NOEVENT,anyevent) => (anyevent) 
		// (anyevent,anyevent)=> (anyevent,anyevent)

		public List<string> RemoveRedundantNOEVENT(List<string> eventList){
			if(eventList.Count<=0) {
				LogError("RemoveRedundantNOEVENT",MemoNI("Empty eventNamelist",eventList.Count));
				return new List<string>(){"NOEVENT"};
			} else if (eventList.Count==1){
				return new List<string>(){"NOEVENT"};
			} else {
				string e0 = eventList[0];
				string e1 = eventList[1];
				if (e0=="NOEVENT" && e1=="NOEVENT"){ 
					//(NOEVENT,NOEVENT)  => (NOEVENT)
					return new List<string>(){"NOEVENT"};
				} else if(e0!="NOEVENT" && e1!="NOEVENT"){
					//(anyevent,anyevent)=> (anyevent,anyevent)
					return new List<string>(eventList);
				} else {
					//(anyEvent,NOEVENT) => (anyEvent),(NOEVENT,anyEvent) => (anyEvent) 
					string anyEvent = e0=="NOEVENT"? e1:e0;
					return new List<string>(){anyEvent};
				}
			}
		}		
		//
		// Observe Events based on Swing
		//
		public void SetupEventObserveMargins(){
			double halfOfRange = Range()[1]*0.5;
			LogDebug("SetupEventObserveMargins()",MemoND("halfOfRange",halfOfRange));
			// Event Ovserve magins
//			TNKMarginPeakBreakUp = 0.0;
//			TNKMarginPeakBreakDown = 0.0;
//			TNKMarginBottomBreakUp = 0.0;
//			TNKMarginBottomBreakDown = 0.0;
//			TNKMarginPeakUp = 0.0;
//			TNKMarginPeakDown = 0.0;
//			TNKMarginBottomUp = 0.0;
//			TNKMarginBottomDown = 0.0;					
		}

		//		
		// Detect chage of event. 
		//  If event did not chage, replace it as EventType.NoEvent
		//
		//REMARK:EVENTLIST
		public List<string> ReplaceNoChangeEventWithNOEVENT(List<string> prevList, List<string>currList){
			List<string> resList = new List<string>();
			for(int i=0; i<prevList.Count;i++){
				string theEvent = currList[i];
				if(theEvent==prevList[i]){
					resList.Insert(i,"NOEVENT"); //Did not Chage.
				}else{
					resList.Insert(i,theEvent); //Chaged.
				}
			}
			return resList;
		}

		public double PeakReversalThreshold(double ratio){
			if(TNKRecentPeakBreakUpValue == 0.0 || TNKRecentPeakValue == 0.0)
				return 0.0;
			else if(TNKRecentPeakBreakUpValue > TNKRecentPeakValue){
				return 0.0;
			} else {
				return TNKRecentPeakValue - (TNKRecentPeakValue - TNKRecentPeakBreakUpValue)*ratio;
			}
		}

		public double BottomReversalThreshold(double ratio){
			if(TNKRecentBottomBreakDownValue == 0.0 || TNKRecentBottomValue == 0.0)
				return 0.0;
			else if(TNKRecentBottomBreakDownValue < TNKRecentBottomValue){
				return 0.0;
			} else {
				return TNKRecentBottomValue + (TNKRecentBottomBreakDownValue - TNKRecentBottomValue)*ratio;
			}
		}

		public double TNKSetPBBreakMargin(){
			return ATR(3)[0]; //ISSUE  Temproal Grandless
		}
		
		//
		// Execute Event Handler and drive state transition
		//  
		public Strategy.TradeSpec DispatchEventToHandlers(ObservedEvent oe){
			bool isPresent=false, isEnabled=false, isDisabled=false, isExecutable=false;
			string eventName = oe.eventName;

			// Update Latest condition Name
			//TNKLatestConditionName = eventName;
			// Update Market Trend
			TNKTrendPresence = TrendPresence();
			// Update Market Cycle
			TNKMarketCycle = MarketCycle();
			//
			TradeSpec result = null;
			
			// Check if Event name is correct.
			isPresent = TNKEventNameList.Contains(eventName);
			if (isPresent) {
				// Checek if execution is allowd.
				isExecutable = DoesPassWBList(eventName,TNKEnabledEvents,TNKDisabledEvents);
				if(isExecutable){
					//string handlerName = TNKConditionHandlerName(conditionName);
					string handlerName = ConditionHandlerNameWithDict(eventName);
					LogDebug("DispatchEventToHandlers()",MemoNS("eventName",eventName),MemoNS(" handlerName",handlerName));
					ObservedEvent [] args = {oe};
					result = (TradeSpec)InvokeMethod(handlerName,args);
				}
				//Log ConditionChange
				LogMessage("DispatchEventToHandlers()",
					TAGMsg("New Event"),MemoNI("Bar",CurrentBar),MemoNS("eventName",eventName),MemoND("Price",Close[0])
				);
			}else{
				// Wrong Condtion Name
				LogError("DipatchEventHandlers()",MemoNS("Illegal_eventName",eventName));
			}
			return result;
		}
		
		//
		//  Transit State
		//
		public string TransitState(string stateName,string conditionName){
			string nextStateName;
			// Determin next state
			if(stateName=="NOTSPECIFIED"){
				nextStateName = TNKStateTransitionDict[conditionName];
			}else{
				nextStateName = stateName;
			}
			TNKStateNamePrivious = TNKStateNameCurrent;
			TNKStateNameCurrent = nextStateName;
			TNKLatestConditionName = conditionName;
			LogDebug("TransitState()",
				MemoNS("TNKTrendPrivious",TNKStateNamePrivious),
				MemoNS("TNKTrendCurrent",TNKStateNameCurrent),
				MemoNS("TNKLatestConditionName",TNKLatestConditionName)
			);
			return TNKStateNameCurrent;
		}

		//
		// Execute Condition Handler and drive state transition
		//
		
		// Make Condition Handler Name from Condition Name
		string TNKConditionHandlerName(string conditionName){
			string[] stateEvent = conditionName.Split(':');
			string handlerName = string.Format("CH_{0:s}_{1:s}",stateEvent[0],stateEvent[1]);
			return handlerName;
		}
		// Get Handler Name from TNKConditionHandlerNameDict
		string ConditionHandlerNameWithDict(string conditionName){
			string handlerName=SearchValueInDict(conditionName,TNKEventHandlerNameDict);
			if(handlerName!=null){
				return handlerName;
			}else{
				return "EHG_DEFAULT";
			}
		}
		
		//Collect specified events form TNKEventHistory
		//  Input: eventType "MC", "GE", lookBarCount  
		//  Out:List of ovserved event collection
		public List<ObservedEvent> CollectEvents(string type, int lookBackBarCount){
			int limitBarNumber = TakeBigger(0,CurrentBar - lookBackBarCount);
			List<ObservedEvent> result = new List<ObservedEvent>();
			int index=TNKEventHistory.Count-1;
			bool inRange = true;
			do{
				ObservedEvent oe = TNKEventHistory[index];
				inRange = (oe.barNumber >= limitBarNumber);
				if(oe.eventType==type && inRange){
					result.Add(oe);
					//LogDebug("CollectEvents()",MemoNS("Added OvservedEvent",oe.Description()));
				}
				index--;
			}while( inRange || index <= 0 );
			return result;
		}
	
		//Read Condition History
		// lookback 1 means privious condition. 
		// lookback should be larger equal 1.
		public ObservedEvent LookbackEventHistory(int lookback){
			int count = TNKEventHistory.Count;
			if (lookback < 1 || lookback - 1 > count ) {return null;}
			int index = count - lookback;
			LogDebug("LookbackConditionHistory()",MemoNI("lookback",lookback),MemoNI("count",count),MemoNI("index",index));
			return TNKEventHistory[index];
		}
    }
}
//==========================================
//  Obsolete code

//Obsolete Code	