//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//
		//  Reject Reason logging
		// 
		string AddRejectReason(string reason){
			TNKRejectReason = AddReason(reason,TNKRejectReason);
			return TNKRejectReason;
		}
		//
		//  Attempt Signal Filters. Call from ExecuteTradeSpec()
		//

		public bool AttemptSignalFilters(TradeSpec tradeSpec){

			//Filter name list
			List<string> signalFilters = new List<string>(){
				//
				// Mandatory filters
				//
				"SFIL_Reject_DuplicateSignalToSameDirection",   //Revised 2019/06/13
				"SFIL_RejectReverseSignalOnProfitedPosition",
				"SFIL_RejectZeroSizeTrade",
				"SFIL_Reject_JustBeforSessionEnd",
				//
				// Test parpose filters
				//
				//"SFIL_RejectAnySignal",       //2018/04/12
				//"SFIL_RejectAnyShortSignal",  //2018/04/12
				//"SFIL_RejectAnyLongSignal",   //2018/04/12			
				// 
				// Optional fiters
				//
				"SFIL_Reject_TooManyDotenSigal", //2019/06/15 under testing 
				"SFIL_Reject_InInitialRange",    //2019/06/13 Need for ver3.9.19 //Omit again in v3.9.23 because in case of Night->Day->Exit patern, ommit is quite better
				//"SFIL_Reject_SameDirectionWithLargePrivousDay", //2019/06/06 Need for ver 3.9.19
				"SFIL_Reject_PeakAnalysis",			//2019/01/15   Under construction
				"SFIL_VOC_Rejct_NarrowAndFlat",		//2019/02/28   Need for Ver 3.9.19
				//"SFIL_Pass_CounterDirection",      //2018/12/23
				//"SFIL_Reject_TooLowVolatility",    //2018/12/22
				//"SFIL_CounterDirection",         //2018/12/11  
				//"SFIL_LowBOMomentum",            //2018/11/17 //TEMP PENDING
				//"SFIL_BreakoutLowValueBOLevel",  //2018/09/24 //TEMP PENDING
				//"SFIL_NeutralBar",               //2018/09/08  add to prevent entry in Newtral bar of IRBOWOBB
				//"SFIL_EntryAfterStopLoss",       //2018/03/20 //PENDING
				//"SFIL_LargeBody_SingleBar",      //2018/05/05 //PENDING
				//"SFIL_LargeBody_MergedBar",	   //2018/05/26 //PENDING
				//"SFIL_EntryInPoorDirectionaity", //2018/07/14 //PENDING
				//"SFIL_EntryInBarHasLongHige"     //2018/04/05 //PENDING
				//"SFIL_NB_PeakBottom_Clipping",
				//"SFIL_NB_PeakBottom_Clipping",   //2016/08/08 //PENDING
				//"SFIL_NB_PeakBottom_Clipping_Counter", //2016/09/20 //PENDING
				//"SFIL_NarrowRage",
				//"SFIL_FromDirectionlessBar",
				//
				// Filters not adopted.
				//
				//"SFIL_Reject_LowVOCGradient",       //2018/12/29			
				//
				// Filters for Market Condition signal (Obseleted)
				//
				//"SFIL_ReverseDuringUpdateDetectionLag",
				//"SFIL_MomentumChekForNOEVENTEntry"
				//
				// Pending 
				//
				//"SFIL_RejectSellSignalFromExhaustedMomentum",
				//"SFIL_RejectSellSignalInUppserOfSMA50",
				//"SFIL_NB_MomentumChek",
				//"SFIL_NB_BreakoutAfterLongFlat",
				//"SFIL_NB_RejectSemiBigBarInLowStdDev", //2015/08/19
				//"SFIL_RejectWorstPerformanceEvent",
				//"SFIL_NB_Check_BOBandCondition", //move from ObserveNormalizedBreakout() 2016/11/03 TESTING <= DEGRADE result. do not use.
				//"SFIL_NB_TooLowStdDev",
				//"SFIL_BB_NegativeBigBar", //2017/03/18 //TESTING 2017/06/22 OFF
				//"SFIL_StallBar", //2017/04/11//TESTING
				//"SFIL_TestRejectEvent",
				//"SFIL_PreventToTakePositionInFirstBarInSession",
				//"SFIL_BadBigBarInFirstBarOfSession",	
				//"SFIL_OpenWindow",
				//"SFIL_BigBarOfBadCondition", // Screen Type 3 :NBO && PBBand Breakout
			};
			TNKRejectReason = "";

			//Call observer with name list.
			//List<object> args = new List<object>(){(object)tradeSpec};
			object[] args = new object[]{tradeSpec};
			bool rejected = (bool)InvokeWithNameListWhileReturnIsFalse(signalFilters, args);
			
			if(rejected) {
				//Mark Reject on Chart with black diamond
				MarkEventNameOnChart(tradeSpec.eventName, TNKRejectReason, Brushes.Black);
				LogDebug("AttemptSignalFilters()",MemoNS("Rejected signal",tradeSpec.signal));
			}				
			return rejected;
		}

		//====================
		//   Utility method
		//    Is this Doten trade?
		//====================
		public bool IsDoten(TradeSpec ts){
			bool result = false;
			// Is this Doten trade?
			int signal = ts.SignalSign;
			int position = (int)PositionSign();
			if (position == 0){  //No Position
				result = false;
			}else{
				bool isNotMatch = (signal != position);
				if (isNotMatch){
					result = true;
					LogDebug("IsThisDoten()",MemoNI("signal",signal),MemoNI("position",position));
				} else {
					result = false;
				}
			}
			return result;
		}
		
		//=========================
		//  Signal Filter Template
		//   v1.0  2109/mm/dd
		//=========================
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Temaplate(TradeSpec tradeSpec){
			//Parameters
			double dummyPram = 0.0;
			//
			bool willReject=false;
			if(tradeSpec.eventType != "SOME_TYPE") return false;
			//Any signal type
			//if(tradeSpec.signal != null){
			if(tradeSpec.signal=="SOME_SIGNAL"){
				bool rejectCondition = true;
				if(rejectCondition){
					willReject = true;
					string reason = "SOME_REASON";
					AddRejectReason(reason);
					LogFilter("SFIL_Temaplate()",MemoNS("reason",reason));
				}else{
					willReject = false;
				}
			}
			return willReject;
		}
		
		//=========================
		//  Too Many DOTEN signal
		//   v1.0  2109/06/15
		//=========================
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_TooManyDotenSigal(TradeSpec tradeSpec){
			bool willReject;
			//Parameters
			int dotenLimit = 4;// 1;//3;//2; //Count
			//Check Conditions
			bool isDoten = IsDoten(tradeSpec);
			bool isOverLimit = TNKInSessionDOTENSignalCount > dotenLimit;
			bool rejectCondition = isDoten && isOverLimit;
			if (rejectCondition){
				willReject = true;
				string reason = String.Format("TooManyDOTENSigal {0}>{1}",TNKInSessionDOTENSignalCount,dotenLimit);
				AddRejectReason(reason);
				LogFilter("SFIL_Reject_TooManyDotenSigal",TAGMsg(reason));
			} else {
				willReject = false;
			}
			return willReject; 
		}
		//============
		//  Reject signal in Initial Range
		//    Using IsInInitial() @ TNKObserver.cs
		//   v1.0 2019/06/13
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_InInitialRange(TradeSpec tradeSpec){
			//Parameters
			//  None
			//
			bool willReject=false;
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//Any signal type
			if(tradeSpec.signal != null){
			//if(tradeSpec.signal=="SOME_SIGNAL"){
				bool rejectCondition = IsInInitial();
				if(rejectCondition){
					willReject = true;
					string reason = "In InitialRange";
					AddRejectReason(reason);
					LogFilter("SFIL_Reject_InInitialRange()",MemoNS("reason",reason));
				}else{
					willReject = false;
				}
			}
			return willReject;
		}
		
		//============
		//  Filter signal for general TWD trading method. 
		//  　TWD based filter
		//    Using muti time frame. Assume Time[1][0] hold Day's bar
		//    v1.0 2019/08/25
		//============
		public bool SFIL_Accept_TWD_Direction(TradeSpec ts){
			//Initialize TWD table
			//  [int dayOfWeek  int acceptDirection, int ago]  acceptDirection:　-2:same direction of reference day  -1:reverse of refeernce day 0:Both 1:buy 2:sell  ago : agor for reference day 
			int[,] twdTable = new int[7,2]{
				{0,0},  //SUN actually Sunday is dummy.
				{0,0},	//MON
				{0,0},  //TUE
				{0,0},  //WED
				{0,0},  //THR
				{0,0},  //FRI
				{0,0}   //SAT
			};
			int moveLimit = 50;//Tics 250yen　
			//Check if Day's bar is not empty
			if(CurrentBars[1] < 0) return false;
			//
			// Check conditions
			//
			bool willReject = false;
			bool willAccept = false;
			int dow = (int)Time[0].DayOfWeek;
			int acceptDirection = twdTable[dow,0];
			int ago = twdTable[dow,1];
			// Move for reference day
			double o = Opens[1][ago];
			double c = Closes[1][ago];
			double moveRef = c -o;
			int directionRef = Math.Sign(moveRef);
			// Signal direction 
			int signalDirection = ts.SignalSign;
			//
			switch(acceptDirection) {
				case 0: //Accept both direction.  
					willAccept =  true;
					break;
				case 1:  //Accept only Buy signal
					willAccept = (signalDirection == 1);
					break;
				case 2:  //Accept only Sell signal
					willAccept = (signalDirection == -1);				
					break;
				case -1:  //Accept if reverse direction with refenece day
					willAccept = (signalDirection != directionRef);
					break;
				case -2: //Accetp if same direction with reference day
					willAccept = (signalDirection == directionRef);
					break;
				default:
					LogError("SFIL_Reject_TWD_Direction",MemoNI("Illegal acceptDirection",acceptDirection));
					willAccept = true;
					break;
			}
			if(!willAccept){
				willReject = true;
				string reason = String.Format("SFIL_Accept_TWD_Direction@{0},{1},{2},{3}",dow,acceptDirection,signalDirection,directionRef);
				AddRejectReason(reason);
				//LogFilter("SFIL_Accept_TWD_Direction",MemoNS("reason",reason),MemoNB("isDoten",isDoten));
				LogFilter("SFIL_Accept_TWD_Direction",MemoNS("reason",reason));
			}else{
				willReject = false;
			}
			return willReject;
	
		}
		
		//============
		//  Filter signal that has same direction with privous day which move long range. 
		//    Using muti time frame. Assume Time[1][0] hold Day's bar
		//  TWD based filter
		//    v1.0 2019/06/06
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_SameDirectionWithLargePrivousDay(TradeSpec tradeSpec){
			LogDebug("SFIL_Reject_SameDIrectionWithLargePrivousDay",TAGMsg("Enterd SFIL_Reject_SameDIrectionWithLargePrivousDay"));
			//Check if Day's bar is not empty
			//if(CurrentBars[1] < 3) return false; 
			if(CurrentBars[1] < 0) return false; 
			//
			// Parameters
			//
			//List<int> applicableDOW = new List<int>(){1}; //MON
			//List<int> applicableDOW = new List<int>(){2}; //TUE <= WORST in sigle day for 17Q3_19Q1
			//List<int> applicableDOW = new List<int>(){3}; //WED
			//List<int> applicableDOW = new List<int>(){4}; //THR 
			//List<int> applicableDOW = new List<int>(){5}; //FRI <= BEST in sigle day for 17Q3_19Q1
			//List<int> applicableDOW = new List<int>(){6}; //SAT
			//List<int> applicableDOW = new List<int>(){1,2,3,4,6};  //ALL TWD
			List<int> applicableDOW = new List<int>(){1,3,4,6};  //Exclude TUE <= BEST for 17Q3_19Q1
			//List<int> applicableDOW = new List<int>(){3,4,6};  //Exclude MON and TUE
			//int moveLimit = 25; //Tic 125yen
			int moveLimit = 50;//Tics 250yen　<= BEST perfornamce v3.9.19
			//int moveLimit = 100;//Tics 500yen
			//
			// Chek conditions
			//
			bool willReject=false;
			int dow = (int)Time[0].DayOfWeek;
			bool isApplicableDOW = applicableDOW.Contains(dow);
			//Price Range and direction of privous day
			// Mean value for last 2 day  <= VERY BAD PERFOMANCR
			//double o = Opens[1][0]+Opens[1][1];
			//double c = Closes[1][0]+Closes[1][1];
			//double m = (c-o)/2.0;
			// Mean value for last 3 day <= BAD PERFOMANCR
			//double o = Opens[1][0]+Opens[1][1]+Opens[1][2];
			//double c = Closes[1][0]+Closes[1][1]+Closes[1][2];
			//double m = (c-o)/3.0;
			// Single day Range
			//double h = Highs[1][0];
			//double l = Lows[1][0];
			//double r = h - l;
			// Single day Move
			double o = Opens[1][0];
			double c = Closes[1][0];
			double m = c -o;
			int directionOfPrivousDay = Math.Sign(m);
			//double move = Math.Abs(r);  //TEST replace Move with Range <= BAD PERFORMANCE
			double move = Math.Abs(m);
			LogDebug("SFIL_Reject_SameDIrectionWithLargePrivousDay",MemoND("o",o),MemoND("c",c),MemoND("move",move),MemoNI("directionOfPrivousDay",directionOfPrivousDay));
			//
			bool isPrevDay_LargeRange = (move >= moveLimit);
			bool isSameDirection = (directionOfPrivousDay == tradeSpec.SignalSign);
			LogDebug("SFIL_Reject_SameDIrectionWithLargePrivousDay",MemoNB("isApplicableDOW",isApplicableDOW),MemoNB("isPrevDay_LargeRange",isPrevDay_LargeRange),MemoNB("isSameDirection",isSameDirection));
			bool isDoten = IsDoten(tradeSpec);
			//
			bool rejectCondition = isApplicableDOW && isPrevDay_LargeRange && isSameDirection;
			//BAD Perfomance// bool rejectCondition = isApplicableDOW && isPrevDay_LargeRange && isSameDirection && !isDoten; //Accept signal if DOTEN. result was BAD PERFORMANCE
			//
			if(rejectCondition){
				willReject = true;
				string reason = String.Format("NexDayOfLargeRange@{0},{1}",dow,directionOfPrivousDay);
				AddRejectReason(reason);
				LogFilter("SFIL_Reject_SameDIrectionWithLargePrivousDay",MemoNS("reason",reason),MemoNB("isDoten",isDoten));
			}else{
				willReject = false;
			}
			return willReject;
		}

		//============
		//  Reject PeakCount test
		//    v1.0 2019/01/14
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_PeakAnalysis(TradeSpec tradeSpec){
			//Parameters
			int length = 30; //38 Search range
			// Return false if CurrentBar does not reach length
			if(CurrentBar < 30){return false;}
			//
			bool willReject=false;
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//Any signal type
			if(tradeSpec.signal != null){
			//if(tradeSpec.signal=="SOME_SIGNAL"){
				bool rejectCondition = false;
				//
				// Search peaks within range
				//
				CandleList cl = new CandleList(this, CurrentBar-length, CurrentBar);
				// In case Close
				//string valName = "CLOSE";
				string posValName = "CLOSE";//"HIGH";
				string negValName = "CLOSE";//"LOW";
				CandleList[] peaks;
				if(posValName == negValName){
					peaks = cl.ExtractPeaks(posValName);
				}else{
					peaks = new CandleList[]{cl.ExtractPeaks(posValName)[0], cl.ExtractPeaks(negValName)[1]};
				}
				//Extract all peaks   [[pPeak1,nPeak1], [pPeak2,nPeak2], [pPeak3,nPeak3]....]
				List<CandleList[]> allPeaks = cl.ExtractAllPeak(posValName,negValName);
				LogFilter("SFIL_Reject_PeakAnalysis",MemoNI("allPeaks.Count()",allPeaks.Count()));
				if(allPeaks.Count()==0) return false; //If no peak found, exit filter;
				string memo;
				//
				//  Search every oder peaks
				//
				if(allPeaks.Count()>0){
					CandleList upPeaks,downPeaks;
					List<SolidColorBrush> colors = new List<SolidColorBrush> {Brushes.Yellow,Brushes.Orange,Brushes.Red};
					foreach(int order in Enumerable.Range(0,allPeaks.Count())){
						upPeaks   = allPeaks[order][0];
						downPeaks = allPeaks[order][1];
						memo = string.Format("upPeaks[{0}][0]:{1} downPeaks[{2}][1][:{3}",order,upPeaks.Length(),order,downPeaks.Length());
						tradeSpec.Log.AddLog(memo);
						LogFilter("SFIL_Reject_PeakCounter",memo);
						string label = string.Format("Order{0}",order+1);
						SolidColorBrush color = order < 3 ? colors[order]:Brushes.Black;
						MarkAllowWithCandles("UP"  ,label+"PosPeak",TickSize*(order+1), color, upPeaks,  posValName);
						MarkAllowWithCandles("DOWN",label+"NegPeak",TickSize*(order+1), color, downPeaks,negValName);
					}
					
				}else{
				}
				//
				// RegressionLine of 1st peaks
				//
				//RegressionLine test
				//
				double[] tx=new double[]{1.1,2.3,2.8,4.2,5.1};
        		double[] ty=new double[]{0.7,1.9,3.1,4.2,5.6};
				double[] result = DecideRegressionLine(tx,ty);
				LogFilter("SFIL_Reject_PeakCounter",TAGMsg("RegressionLineTest"),MemoNAR("result",result));
				//
				//LogFilter("SFIL_Reject_PeakCounter",TAGMsg("will call 1st LinearInterpolation"),MemoNI("peaks[0].Length()",peaks[0].Length()));
				peaks = allPeaks[0];
				int[] peakCounts = new int[]{peaks[0].Length(), peaks[1].Length()};
				double[] upCoe, downCoe;
				RegressionLine upRegLine, downRegLine;
				//string valName = "CLOSE";
				if(peakCounts[0]>1){
					upRegLine= peaks[0].RegressionLine(posValName);
				}else{
					//No regression line for 1st peaks. Calc with all candles. 
					upRegLine= cl.RegressionLine(posValName);
				}
				//double upCoeRS = RSquared(upCoe[0], upCoe[1], double[] x, double[] y);
				if(peakCounts[1]>1){
					downRegLine = peaks[1].RegressionLine(negValName);
				}else{
					//No regression line for 1st peaks. Calc with all candles. 
					downRegLine = cl.RegressionLine(negValName);
				}
				memo = MemoNO("upRegLine",upRegLine)+" "+MemoNO("downRegLine",downRegLine);
				LogFilter("SFIL_Reject_PeakCounter",memo);
				tradeSpec.Log.AddLog(memo);
				// Mark Regresson lines
				//  Draw.Line(NinjaScriptBase owner, string tag, bool isAutoScale, int startBarsAgo, double startY, int endBarsAgo, double endY, Brush brush, DashStyleHelper dashStyle, int width)
				bool swtDrawRegLine = true;
				if(peakCounts[0]>1 || peakCounts[1]>1){
					Candle startCandle = cl.Candles.First();
					Candle lastCandle  = cl.Candles.Last();
					int startBar = startCandle.BarNumber;
					int endBar = lastCandle.BarNumber;
					int origX;
					// Up side Regression line
					if(peakCounts[0]>1){
						origX = peaks[0].Candles.First().BarNumber;
//TBDel					memo = MemoNAR("upCoe",upCoe)+" "+MemoNI("origX",origX) + " " + MemoNI("startBar",startBar) +" "+ MemoNI("endBar",endBar);
//TBDel					LogFilter("SFIL_Reject_PeakCounter",memo);
//TBDel					//if(swtDrawRegLine) MarkReglessionLine(upCoe, "Pos", origX, startBar, endBar);
						if(swtDrawRegLine) MarkReglessionLine2(upRegLine, "Pos", startBar, endBar);
					}
					// Down side Regression line
					if(peakCounts[1]>1){
						origX = peaks[1].Candles.First().BarNumber;
//TBDel					memo = MemoNAR("downCoe",downCoe)+" "+MemoNI("origX",origX) + " " + MemoNI("startBar",startBar) +" "+ MemoNI("endBar",endBar);
//TBDel					LogFilter("SFIL_Reject_PeakCounter",memo);
//TBDel					//if(swtDrawRegLine) MarkReglessionLine(downCoe,"Neg", origX, startBar, endBar);
						if(swtDrawRegLine) MarkReglessionLine2(downRegLine,"Neg", startBar, endBar);
					}
				}
				//
				// Reject opposit direction to Recent peak
				//
				//return  false; //!!!Skip this filter //DEBUG!!!
				willReject = false;
				string reason = "THIS_IS_BUG";
				int peakBarNum;
				double peakClose;
				Candle thePeak;
				bool inTrend;
				//Exit filter if 2nd order peak is not present
				if(allPeaks.Count()<2) return false;
				CandleList[] pop = allPeaks[1]; //pickup 2nd order peaks
				//get recent peak of peak as index of pop (array of CandleList)
				int recentPOP = SelectCandleListWhichHasRecent(pop);
				//LogFilter("SFIL_Reject_PeakAnalysis",MemoNI("recentPOP",recentPOP));
				switch(recentPOP){
					case 0: //Positive peak reject BUY
						thePeak = pop[0].Candles.Last();
						peakBarNum = thePeak.BarNumber;
						peakClose = thePeak.Close;
						inTrend = peakClose > Close[0];
						//LogFilter("SFIL_Reject_PeakAnalysis",MemoNI("recentPOP",recentPOP),MemoNS("Signal",tradeSpec.signal),MemoNB("inTrend",inTrend));
						if(tradeSpec.signal=="BUY" && inTrend){
							LogFilter("SFIL_Reject_PeakAnalysis",MemoNS("MARK","POITIVE"));
							willReject = true;
							reason = String.Format("OppositEntryToRecentPeak-Positive-{0}",peakBarNum);
						}
						break;
					case 1: //Negative peak reject SELL
						thePeak = pop[1].Candles.Last();
						peakBarNum = thePeak.BarNumber;
						peakClose = thePeak.Close;
						inTrend = peakClose < Close[0];
						//LogFilter("SFIL_Reject_PeakAnalysis",MemoNI("recentPOP",recentPOP),MemoNS("Signal",tradeSpec.signal),MemoNB("inTrend",inTrend));
						if(tradeSpec.signal=="SELL" && inTrend){
							willReject = true;
							reason = String.Format("OppositEntryToRecentPeak-Negative-{0}",peakBarNum);
						}
						break;
					default:
						willReject = false;
						break;
				}
				if(willReject){
					AddRejectReason(reason);
					LogFilter("SFIL_Reject_PeakAnalysis",MemoNS("reason",reason));
				}
			}	
			return willReject;
		}

		//============
		//  SFIL_VOC_Rejct_NarrowAndFlat
		//   v1.0 2018/02/29
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_VOC_Rejct_NarrowAndFlat(TradeSpec tradeSpec){
			//Parameters
			int volatilityLimitByTic = 7; //5; //7; //10; //15; //20; //Tic
			double volatilityLimit = TickSize * (float)volatilityLimitByTic;
			int dx = 5; //2; //14;//10;//5;//Gradient dx by bar count
			double gradBorder = 2.0; //1.0//1.5//2.0//2.5//3.0  par bar count
			//
			bool willReject=false;
			//
			// Only handle "VCBO" event
			if(tradeSpec.eventType != "VCBO") return false;
				//Any signal type
				if(tradeSpec.signal != null){
					//VolatilityChannel vc = new VolatilityChannel(this, TNKVolatilityChannelPeriod,TNKVolatilityChannelBreakoutBorder);
					double vocWidth = StdDev(TNKVolatilityChannelPeriod)[0] * TNKVolatilityChannelBreakoutBorder * 2.0;
					double vocGradient = ChannelGradient(TNKVolatilityChannelPeriod, dx); //Period 14bar, dx:5bar
					//
					bool isflat =Math.Abs(vocGradient) < gradBorder; //5 Bar
					//bool isFlat = true; //OFF gradient check
					bool isNarrow = vocWidth < volatilityLimit;
					//
					bool rejectCondition = false;
					if(isNarrow && isflat ){
						rejectCondition = true;
					}else{
						rejectCondition = false;
					}
					if(rejectCondition){
						willReject = true;
						string reason = MemoND("VOChannelIsTooNarrow:",vocWidth)+" "+MemoND("vocGradient",vocGradient);
						AddRejectReason(reason);
						LogFilter("SFIL_VOC_Rejct_NarrowAndFlat",MemoNS("reason",reason));
					}else{
						willReject = false;
					}
				}
			return willReject;
		}
		
		//============
		//  Reject Low VOChannel gradient
		//    v1.0  2018/12/29
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_LowVOCGradient(TradeSpec tradeSpec){
			//Parameters
			int length = 1;
			double bordarGradient = 1.1;
			//
			bool willReject=false;
			double gradient = INDVOCannelGradient(length)[0];
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//Any signal type
			//if(tradeSpec.signal=="SOME_SIGNAL"){
			if(tradeSpec.signal != null){
				bool rejectCondition = Math.Abs(gradient) <= bordarGradient;
				if(rejectCondition){
					willReject = true;
					string reason = string.Format("LowVOChannelGradient:{0}/{1}",gradient,bordarGradient);
					AddRejectReason(reason);
					LogFilter("SFIL_Reject_LowVOCGradient()",MemoNS("reason",reason));
				}else{
					willReject = false;
				}
			}
			return willReject;
		}

		
		//============
		//  Reject signal when volatility is too low
		//     Create 2018/12/22
		//
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_TooLowVolatility(TradeSpec tradeSpec){
			bool willReject=false;
			//if(tradeSpec.eventType != "NB") return false;
			//Parameters
			double sigmaFactor = 1.0;
			int borderByTick = 2; //Ticks
			double border = borderByTick * TickSize;
			int period = TNKVolatilityChannelPeriod;
			//
			double volatirity = SigmaThisBar(period) * sigmaFactor;
			//
			bool isVolatirityIsTooLow = volatirity < border;
			//
			if(isVolatirityIsTooLow){
				willReject = true;
				string reason = string.Format("VolatilitIsTooLow:{0}",volatirity);
				AddRejectReason(reason);
				LogFilter("SFIL_Reject_TooLowVolatirity()",TAGMsg(reason),MemoND("volatirity",volatirity),MemoND("border",border));
			}
			return willReject;
		}
		//============
		//  Pass CounterDirection
		//    Ver 1.0 2018/12/23
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Pass_CounterDirection(TradeSpec tradeSpec){
			//Parameters
			//
			bool willPass = false;
			//Any Event type
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//if(tradeSpec.signal=="SOME_SIGNAL"){
			if(tradeSpec.signal != null){  //Any signal type
				int barDirection = DirectionSign(0);
				int signalDirection = tradeSpec.SignalSign;
				bool isTombo = barDirection == 0;
				bool unmatchDirection = barDirection != signalDirection; // newtral bar is not included because signalDirection should be 1 or -1
				//Set Condtion 
				bool passCondition = unmatchDirection && !isTombo; //Reject match and Tombo
				if(passCondition){
					willPass = true;
				}else{
					willPass = false;
					string reason =string.Format("DirectionNotCounter barDirection:{0} signalDirection:{1} )",barDirection,signalDirection);
					AddRejectReason(reason);
					LogFilter("SFIL_Pass_CounterDirection()",MemoNS("reason",reason));
				}
			}
			return !willPass;
		}

		//============
		//  Reject CounterDirection
		//    Ver 1.0 2018/12/11
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_CounterDirection(TradeSpec tradeSpec){
			//Parameters
			//
			bool willReject=false;
			//Any Event type
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//if(tradeSpec.signal=="SOME_SIGNAL"){
			if(tradeSpec.signal != null){  //Any signal type
				int barDirection = DirectionSign(0);
				int signalDirection = tradeSpec.SignalSign;
				bool isTombo = barDirection == 0;
				bool matchDirection = barDirection == signalDirection; // newtral bar is not included because signalDirection should be 1 or -1
				//Set Condtion 
				//bool rejectCondition = matchDirection;  //Reject if macth
				//bool rejectCondition = !isTombo;  //Reject no Tombo
				//bool rejectCondition = !matchDirection; //Reject if not mach
				//bool rejectCondition = !matchDirection || isTombo; //Reject not match and Tombo
				bool rejectCondition = matchDirection || isTombo; //Reject match and Tombo
				if(rejectCondition){
					willReject = true;
					string reason =string.Format("DirectionMatch barDirection:{0} signalDirection:{1} )",barDirection,signalDirection);
					AddRejectReason(reason);
					LogFilter("SFIL_CounterDirection()",MemoNS("reason",reason));
				}else{
					willReject = false;
				}
			}
			return willReject;
		}		
		//============
		//  Reject Low BOMomentum.
		//    Ver 1.0 2018/11/11
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_LowBOMomentum(TradeSpec tradeSpec){
			//Parameters
			int momentumBoarderByTic = 2; //Tick
			double momentumBoarder = momentumBoarderByTic * TickSize;
			//
			bool willReject=false;
			//Any Event type
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//if(tradeSpec.signal=="SOME_SIGNAL"){
			if(tradeSpec.signal != null){  //Any signal type
				double boMomentum = Math.Abs(Close[0] - tradeSpec.triggerEvent.breakoutLevel);
				//Set Condtion 
				bool rejectCondition = boMomentum < momentumBoarder;
				if(rejectCondition){
					willReject = true;
					string reason =string.Format("LowBOMomentum({0})",boMomentum);
					AddRejectReason(reason);
					LogFilter("SFIL_LowBOMomentum()",MemoNS("reason",reason));
				}else{
					willReject = false;
				}
			}
			return willReject;
		}
				
		//============
		//  Reject Brekout signal of low value Breakout level.
		//   Use CharScanner to decide the BO level is clean or not.
		//   Ver 1.0 2018/09/24
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_BreakoutLowValueBOLevel(TradeSpec tradeSpec){
			//Parameters
			double dummyPram = 0.0;
			//
			bool willReject=false;
			//if(tradeSpec.eventType != "IRBOWOBB") return false; //Only for IRBOWOBB
			// Get Breakout lavel
			double boLevel = tradeSpec.triggerEvent.breakoutLevel;
			// Check if BO level is clean or not.
			// Get Bar number for session start
			//Params
			int guardZone = 2;  //Omit guard range privent to hit self BOWOBB pattern
			int irZone = 6;     //exclude Initial Range
			//
			//int privousBarNum = CurrentBar -1;
			int scanStartNum = CurrentBar - guardZone;
			//int sessionStartBarNum = CurrentBar - Bars.BarsSinceNewTradingDay;
			int sessionStartBarNum = TNKFirstBarIndexOfCurrentSession;
			int scanEndNum = sessionStartBarNum + irZone;
			ChartScanner cs = null;
			// Check Scan Range
			LogDebug("SFIL_BreakoutLowValueBOLevel()",MemoND("boLevel",boLevel),MemoNI("CurrentBar",CurrentBar),MemoNI("scanStartNum",scanStartNum),MemoNI("scanEndNum",scanEndNum));
			if(CurrentBar < sessionStartBarNum + irZone + guardZone){
				LogDebug("SFIL_BreakoutLowValueBOLevel()",MemoNI("CurrentBar",CurrentBar),MemoNI("sessionStartBarNum + irZone + guardZone",sessionStartBarNum + irZone + guardZone));
				willReject = false;
			}else{
				// Check BO leve with CharScanner
				cs = new ChartScanner(this,boLevel,scanStartNum, scanEndNum);
				if(cs.ScanedCandles.Length()>0){
					//The level is not clean
					int distance = CurrentBar - cs.ScanedCandles.Recent().BarNumber;
					LogDebug("SFIL_BreakoutLowValueBOLevel()",MemoNO("cs.ScanedCandles.Recent()",cs.ScanedCandles.Recent()),MemoNI("CurrentBar",CurrentBar),MemoNI("distance",distance));
					willReject = true;
				}
			}
			if(willReject == true){
				string reason = string.Format("BOLevelIsNotClean({0})",cs.ScanedCandles.Length());
				AddRejectReason(reason);
				LogFilter("SFIL_BreakoutLowValueBOLevel()",MemoNS("reason",reason));
			}
			return willReject;
		}
		
		//============
		//  Reject signal on Neutral bar
		//   Check if simple tonbo pattern
		//   Ver 1.0 2018/09/08  create for priventing entry in nodirection bar for IRBOWOBB.
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NeutralBar(TradeSpec tradeSpec){
			//Parameters
			// No parameter
			//
			bool willReject=false;
			// Check if adopt event
			if(tradeSpec.eventType != "IRBOWOBB") return false;  //Only for IRBOWOBB
			// Check if is neutral bar
			if(IsNeutralBar(0)){
				//It's Newtral (Tonbo) bar
				willReject = true;
				string reason = "Neutrral Bar (Tonbo)";
				AddRejectReason(reason);
				LogFilter("SFIL_NeutralBar()",MemoNS("reason",reason));
			}else{
				willReject = false;
			}
			return willReject;
		}
		
		//============
		//  Reject Signal on "Higenuma". A market with poor directionality.
		//    ver1.0 2018/07/07
		//   Ver 1.1 2018/08/11 replace exceptional event name to event type "IRBOWOBB"
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_EntryInPoorDirectionaity(TradeSpec tradeSpec){
			//Parameters
			double higeRatioBorder = 0.5;  //Propotion of amount of hige. 
			//
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//if (tradeSpec.eventName == "EN_IRB_UPPER_BREAKUP" || tradeSpec.eventName == "EN_IRB_LOWER_BREAKDOWN"){return false;}
			if (tradeSpec.eventType == "IRBOWOBB"){
				LogDebug("SFIL_EntryInPoorDirectionaity",MemoNS("tradeSpec.eventType",tradeSpec.eventType));
				return false;
			}
			bool willReject=false;
			//Calculate hige_amount / body_amount
			double higeRatio = HigeRatio();
			//Judge criteria
			if(higeRatio > higeRatioBorder){
				willReject = true;
				string reason = "Higenuma souba"+MemoND("higeRatio",higeRatio);
				AddRejectReason(reason);
				LogFilter("SFIL_EntryInPoorDirectionaity()",MemoNS("reason",reason));
			}
			return willReject;
		}
		public double HigeRatio(){
			int range = 5; //Check for 10 baras
			CandleSeries cs = new CandleSeries(this,0,range);
			double[] props = cs.AccumlatedComponentPropotions();
			return props[0]+props[2]; //Head + Leg
		}

		//============
		//  Rejet Signal on Just before session end
		//   ver 1.1 2018/06/14
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_Reject_JustBeforSessionEnd(TradeSpec tradeSpec){
			//Attemp to only minutes.
			if(BarsPeriod.BarsPeriodType != BarsPeriodType.Minute){
				LogError("SFIL_JustBeforSessionEnd()",TAGMsg("This filter is only of minute periodType."));
				return false;
			}
			//Parameters
			int margin = 20; //minutes Default value:20 minutes
			//
			SessionIterator sessionIterator = new SessionIterator(Bars);
			sessionIterator.GetNextSession(Time[0], true);
 			//DateTime beginTime = sessionIterator.ActualSessionBegin;
			DateTime endTime   = sessionIterator.ActualSessionEnd;
			TimeSpan toGo = endTime - Time[0];
			int toGoMinutes = (int)toGo.TotalMinutes;
			//
			bool willReject=false;
			if(toGoMinutes < margin){
				willReject = true;
				string reason = "Just before session end";
				AddRejectReason(reason);
				LogFilter("SFIL_JustBeforSessionEnd()",MemoNS("reason",reason));
			}
			return willReject;
		}
		
		//============
		//  Reject Signal on Merged long body bar
		//   Ver 1.0 2018/05/26
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_LargeBody_MergedBar(TradeSpec tradeSpec){
			// Exclude by event tyep
			if (tradeSpec.eventType == "IRBOWOBB"){
				LogDebug("SFIL_LargeBody_MergedBar()",MemoNS("Excluded tradeSpec.eventType",tradeSpec.eventType));
				return false;
			}
			//
			bool willReject=false;
			//
			//double[] compo = BarDecompose(0);
			int mergeCount = 3;
			//Check if all are same direction.
			//if( CheckMatchBarDirections(0, mergeCount) == 0) return false;
			
			//Count save direction bar
			int matchCount = CountMatchBarDirections(0, mergeCount);
			double[] compo = MergedBarDecompose(0, matchCount);
			double body = compo[1];		
			//
			//double factor = 2.8;
			double factor = 3.0;			
			double limit = StdDev(14)[0] * factor;
			bool isLargeBody = body > limit;
			if(isLargeBody){
				willReject = true;
				//string reason = "SignalOnLargeMoveMargedBar";
				string reason = "SignalOnLargeMoveMargedBar" + MemoND(" body",body) + MemoND(" limit",limit);
				AddRejectReason(reason);
				LogFilter("SFIL_LargeBody_MergedBar()",MemoNS("reason",reason));
			}
			return willReject;
		}		
		
		
		//============
		//  Reject Signal on long body bar
		//   Ver 1.0 2018/05/05
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_LargeBody_SingleBar(TradeSpec tradeSpec){
			//
			bool willReject=false;
			double[] compo = BarDecompose(0);
			double body = compo[1];
			double factor = 2.8;
			double limit = StdDev(14)[0] * factor;
			bool isLargeBody = body > limit;
			if(isLargeBody){
				willReject = true;
				//string reason = "SignalOnLargeMoveSingleBar";
				string reason = "SignalOnLargeMoveSingleBar" + MemoND(" body",body) + MemoND(" limit",limit);
				AddRejectReason(reason);
				LogFilter("SFIL_LargeBody_SingleBar()",MemoNS("reason",reason));
			}
			return willReject;
		}
		
		//============
		//  Rejecg Any long signal
		//   Ver 1.0 2018/04/12
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_RejectAnyLongSignal(TradeSpec tradeSpec){
			//
			bool willReject=false;
			if(tradeSpec.IsLong()){
				willReject = true;
				string reason = "RejactAnyLongSignal";
				AddRejectReason(reason);
				LogFilter("SFIL_RejectAnyLongSignal()",MemoNS("reason",reason));
			}
			return willReject;
		}
		//============
		//  Rejecg Any short signal
		//   Ver 1.0 2018/04/12
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_RejectAnyShortSignal(TradeSpec tradeSpec){
			//
			bool willReject=false;
			if(tradeSpec.IsShort()){
				willReject = true;
				string reason = "RejactAnyShortSignal";
				AddRejectReason(reason);
				LogFilter("SFIL_RejectAnyShortSignal()",MemoNS("reason",reason));
			}
			return willReject;
		}
		//============
		//  Refect Any signal for test perpose.
		//   Ver 1.0 2018/04/12
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_RejectAnySignal(TradeSpec tradeSpec){
			//Just return True
			bool willReject = true;
			string reason = "RejectAnySignalForTesting";
			AddRejectReason(reason);
			LogFilter("SFIL_RejectAnySignal()",MemoNS("reason",reason));
			return willReject;

		}

		//============
		//  Reject signal on var which has long head ort long leg (Ue-Hige, Shita-Hige)
		//   Ver 1.0 2018/04/05  
		//   Ver 1.1 2018/07/15 Use Candle class instead of BarProportion()/BarPropotion()
		//   Ver 1.2 2018/08/03 add exceptinal EVENT "EN_IRB_UPPER_BREAKUP" and "EN_IRB_LOWER_BREAKDOWN"
		//   Ver 1.3 2018/08/11 replace exceptional event name to event type "IRBOWOBB"
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_EntryInBarHasLongHige(TradeSpec tradeSpec){
			//Parameters
			double lengthLimit = 0.2; // Hige check condition 20%
			double headLimit = 0.2; //0.2
			double legLimit = 0.3;  //0.3
			double bodyLimit = 0.7; //0.8
			//
			bool willReject=false;
			// Apply to all event except "EN_IRB_UPPER_BREAKUP" and "EN_IRB_LOWER_BREAKDOWN"
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			//if (tradeSpec.eventName == "EN_IRB_UPPER_BREAKUP" || tradeSpec.eventName == "EN_IRB_LOWER_BREAKDOWN"){return false;}
			if (tradeSpec.eventType == "IRBOWOBB"){
				LogDebug("SFIL_EntryInBarHasLongHige",MemoNS("tradeSpec.eventType",tradeSpec.eventType));
				return false;
			}

			//Get current bar as Candle
			Candle candle = GetCandle(0);
			
			// Check Hige(head and leg) of current bar
			double[] compo = candle.Component; //Decompose current bar. Return:{head, body, leg, direction};
			double[] prop = candle.Propotion; //Return: {head/range, body/range, leg/range, direction};
			double head = compo[0];
			double body = compo[1];
			double leg = compo[2];
			double direction = candle.DirectionSign;
			double head_p = prop[0];
			double body_p = prop[1];
			double leg_p = prop[2];
			double length = candle.Range;
			
			
			//Hige check
			bool hasLongHead = head_p > headLimit;
			bool hasLongLeg  = leg_p  > legLimit; 
			bool hasShortBody = body_p < bodyLimit;
			//Direction check
			bool upDirection = direction == 1.0;
			bool downDirection = direction == -1.0;
			bool doesMatchLongDirection  = tradeSpec.IsLong()  && direction == 1.0;
			bool doesMatchShortDirection = tradeSpec.IsShort() && direction == -1.0;
			bool doesMatchDirection = doesMatchLongDirection || doesMatchShortDirection;
			
			bool matchLongDirectionAndLongHead = doesMatchLongDirection && hasLongHead; // && !hasLongLeg;
			bool matchShortDirectionAndLongLeg  = doesMatchShortDirection && hasLongLeg; // && !hasLongHead;
			bool matchLongDirectionAndLongLeg = doesMatchLongDirection && hasLongLeg;
			bool matchShortDirectionAndLongHead = doesMatchShortDirection && hasLongHead;

			//bool judge = matchShortDirectionAndLongHead || matchLongDirectionAndLongLeg;
			//bool judge = doesMatchLongDirection && (hasLongHead || hasLongLeg);
			bool judge = doesMatchDirection && (hasLongHead || hasLongLeg);
			//bool judge = doesMatchShortDirection && (hasLongHead || hasLongLeg); //<= GOOD in 2017Q4
			//bool judge = downDirection && hasShortBody;
			
			LogDebug("SFIL_EntryInBarHasLongHige()",MemoND("head_p",head_p),MemoND("body_p",body_p),MemoND("leg_p",leg_p),MemoND("direction",direction),MemoND("length",length));
			if(judge){
				willReject = true;
				string reason = "EntryOnLongHigeBar";
				AddRejectReason(reason);
				LogFilter("SFIL_EntryInBarHasLongHige()",MemoNS("reason",reason));
			}
			return willReject;
		}


		
		//============
		//  Reject same directin entry near after stoploss in loss
		//   Ver 1.0 2018/03/20
		//============
		public bool SFIL_EntryAfterStopLoss(TradeSpec tradeSpec){
			if (tradeSpec.eventType == "IRBOWOBB"){
				LogDebug("SFIL_EntryAfterStopLoss()",MemoNS("Excluded tradeSpec.eventType",tradeSpec.eventType));
				return false;
			}
			//Parameters
			int range = 3; //Check range in bar count
			//
			bool willReject=false;
			//if(tradeSpec.eventType != "SOME_TYPE") return false;
			// Check all signals
			// Get last terminated trade
			TradeSpec ltts = GetTerminatedTradeSpec(0);
			if(ltts != null){				
				bool isStopLoss = ltts.IsStopLoss();
				bool isSameDirection = tradeSpec.IsSignalEqual(ltts);
				int interval = CurrentBar - ltts.exitBarNo;
				bool isInRange = (interval < range);
				bool isLoss = (ltts.profitDelta < 0);
				LogDebug("SFIL_EntryAfterStopLoss()",MemoNI("CurrentBar",CurrentBar),MemoNI("ltts.exitBarNo",ltts.exitBarNo),MemoNI("interval",interval));
				LogDebug("SFIL_EntryAfterStopLoss()",MemoNB("isStopLoss",isStopLoss),MemoNB("isSameDirection",isSameDirection),MemoNB("isInRange",isInRange),MemoNB("isLoss",isLoss));
				if(isStopLoss && isSameDirection && isInRange && isLoss){
					willReject = true;
					string reason = "NearOfStopLossExit";
					AddRejectReason(reason);
					LogFilter("SFIL_EntryAfterStopLoss()",MemoNS("reason",reason));
				}
			}else{
				//No provious trade.
				willReject = false;
			}
			return willReject;
		}
	
		//==================
		// Reject Counter Conter Signal whtich goes away form SMA75 
		//  Ver 1.0 2017/04/22
		//  
		//==================
		public bool SFIL_CounterSignaGoesAwayFromSMA(TradeSpec tradeSpec){
			bool willReject = false;
			if(tradeSpec.eventType != "BB") return false;
			string eventName = tradeSpec.eventName;
			switch(eventName){
				case "EN_BB_BIGNEGATIVEBAR":
					willReject = true;
					string reason = "OmitNegativeBigBarSignal";
					AddRejectReason(reason);
					LogFilter("SFIL_BB_NegativeBigBar()",TAGMsg(reason));
					break;
				case "EN_BB_BIGPOSITIVEBAR":
					bool doesCloseInUpsideOfSMA75 = Close[0] > SMA(75)[0];
					if (doesCloseInUpsideOfSMA75){
						willReject = false;
					}else{
						willReject = true;						
					}
					break;
				default:
					LogError("SFIL_BB_NegativeBigBar()",MemoNS("Illegal eventName",eventName));
					break;
			}
			return willReject;
		}
		
		//============
		//  Reject signal if the bar is stalled(StallBar)
		//   Create 2017/04/11
		//   
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_StallBar(TradeSpec tradeSpec){
			//
			string reason ="";
			// if(tradeSpec.eventType != "SOME_TYPE") return false;
			// Check all signals
			string stallStatus = CheckStallBar(0);
			switch(stallStatus){
				case "NOSTALL":
					return false; //Don't reject.
					break;
				case "UPSIDE_STALL":
					break;
				case "DOWNSIDE_STALL":
					break;
				default:
					LogDebug("SFIL_StallBar()",MemoNS("Illegal_Stall_Status",stallStatus));
					break;
			}
			//"EN_NB_UPPERBREAKUP","EN_NB_LOWERBREAKDOWN","EN_NB_UPMOMENTUM", "EN_NB_DOWNMOMENTUM"
			string eventName = tradeSpec.eventName;
			switch(eventName){
				//Filter if NB momentum
				case "EN_NB_UPMOMENTUM":
					if(stallStatus=="UPSIDE_STALL"){
						reason = "UPSIDE_STALL@EN_NB_UPMOMENTUM";
						AddRejectReason(reason);
					}else{
						return false; //Don't reject.
					}
					break;
				case "EN_NB_DOWNMOMENTUM":
					if(stallStatus=="DOWNSIDE_STALL"){
						reason = "DOWNSIDE_STALL@EN_NB_DOWNMOMENTUM";
						AddRejectReason(reason);
					}else{
						return false; //Don't reject.
					}
					break;
				case "EN_NB_UPPERBREAKUP":
					if(stallStatus=="UPSIDE_STALL"){
						reason = "UPSIDE_STALL@EN_NB_UPPERBREAKUP";
						AddRejectReason(reason);
					}else{
						return false; //Don't reject.
					}
					break;
				case "EN_NB_LOWERBREAKDOWN":
					if(stallStatus=="DOWNSIDE_STALL"){
						reason = "DOWNSIDE_STALL@EN_NB_LOWERBREAKDOWN";
						AddRejectReason(reason);
					}else{
						return false; //Don't reject.
					}
					break;
				default:
					return false; //Don't reject.
					break;
			}
			LogFilter("SFIL_StallBar()",MemoNS("reason",reason));
			return true; //Reject the signal.
		}
		
		//============
		//  Reject Signal if BO band is shrinking/is not botle neck.
		//   Create 2016/11/03
		//  DEGRADED RESULTS 2016/11/05 Do not use.
		//============
		//Return true if signal will be rejected. else siginal accepted
		//NT7
//		public bool SFIL_NB_Check_BOBandCondition(TradeSpec tradeSpec){
//			//Parameters
//			int period = 14;
//			// if BOBand is shrinking 
//			bool bottleNeck = INDSlopeOfStdDev(period)[1] < 0.0 && INDSlopeOfStdDev(period)[2] < 0.0; //More strict
//			//bool bottleNeck = INDSlopeOfStdDev(period)[1] < 0.0 ; //Bad performance compare to above
//			// If BOBand is expanding in this bar. 
//			bool bandExpanding　= INDSlopeOfStdDev(period)[0] > 0.0 && INDSlopeOfStdDev(period)[1] > 0.0 ;
//			//
//			bool willReject=false;
//			if(tradeSpec.eventType != "NB") return false;
//			//
//			string eventName = tradeSpec.eventName;
//			switch (eventName) {
//				case "EN_NB_UPPERBREAKUP":
//				case "EN_NB_LOWERBREAKDOWN":
//					if(!bottleNeck){
//						willReject = true;
//						string reason = "BOBandIsNotBottleNeck";
//						AddRejectReason(reason);
//						LogFilter("SFIL_NB_Check_BOBandCondition()",MemoNS("reason",reason));
//					}
//					break;
//				case "EN_NB_UPMOMENTUM":
//				case "EN_NB_DOWNMOMENTUM":
//					if(!bandExpanding){
//						willReject = true;
//						string reason = "BOBandIsNotExpanding";
//						AddRejectReason(reason);
//						LogFilter("SFIL_NB_Check_BOBandCondition()",MemoNS("reason",reason));
//					}
//					break;
//				default:
//					LogError("SFIL_NB_Check_BOBandCondition()",MemoNS("Unknown eventName",eventName));
//					break;
//			}
//			return willReject;
//		}
		//NT7
		
		//============
		//  Reject signal when NB ouside Peak/Bottom Range under COUNTER tactics
		//    Create 2016/09/20
		//    V0.0  2016/09/20
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NB_PeakBottom_Clipping_Counter(TradeSpec tradeSpec){
			// Screening
			if(tradeSpec.eventType != "NB") return false;
			if(tradeSpec.tactics  != "COUNTER") return false;
			bool doesCheckTrend = false;　//true;
			// Parameter
			double gapMargin = 0.0; //Marginf for cheking if bottom/peak was changed.
			//
			bool willReject=false;
			string eventName = tradeSpec.eventName;
			//Check BO if it is withing Beak-Bottom band.
			double peak    = TNKRecentPeakValue;
			double bottom  = TNKRecentBottomValue;

			double breakPrice = tradeSpec.initialPrice; //shoudl be identical to Close[0];
			LogDebug("SFIL_NB_PeakBottom_Clipping_Counter",MemoND("breakPrice",breakPrice),MemoND("tradeSpec.initialPrice",tradeSpec.initialPrice));	
			
			switch (eventName) {
				case "EN_NB_UPPERBREAKUP":
				case "EN_NB_UPMOMENTUM":
				case "EN_NB_LOWERBREAKDOWN":
				case "EN_NB_DOWNMOMENTUM":
					string pos = PositionAgainstRange(breakPrice,peak,bottom);
					string logMessage = MemoNS("eventName",eventName)+MemoND("breakPrice",breakPrice)+MemoND("peak",peak)+MemoND("bottom",bottom);
						switch(pos){
						case "INSIDE":
							willReject = false;
							break;
						case "UPSIDE":
						case "DOWNSIDE":
							willReject = true;
							string reason = "NB_Counter_outside_PeakBottomBand";
							AddRejectReason(reason);
							LogFilter("SFIL_NB_PeakBottom_Clipping_Counter()",MemoNS("reason",reason),logMessage);
							break;
						default:
							LogError("SFIL_NB_PeakBottom_Clipping_Counter()",MemoNS("Unknown Poistion",pos));
							break;
						}
					break;
				default:
					LogError("SFIL_NB_PeakBottom_Clipping_Counter()",MemoNS("Unknown eventName",eventName));
					break;
			}
			return willReject;
		}
		//============
		//  Reject signal when NB within Peak/Bottom Range under FOLLOW strategy
		//    Create 2016/08/09
		//    V1.2  2016/09/06
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NB_PeakBottom_Clipping(TradeSpec tradeSpec){
			// Screening
			if(tradeSpec.eventType != "NB") return false;
			if(tradeSpec.tactics  != "FOLLOW") return false;
			//if(tradeSpec.signalName != "SN_NB_Follow") return false;
			// Parameters
			bool doesCheckTrend = false;　//true;
			double gapMargin = 0.0; //Marginf for cheking if bottom/peak was changed.
			//
			bool willReject=false;
			string eventName = tradeSpec.eventName;

			double breakPrice = tradeSpec.initialPrice; //shoudl be identical to Close[0];
			LogDebug("SFIL_NB_PeakBottom_Clipping",MemoND("breakPrice",breakPrice),MemoND("tradeSpec.initialPrice",tradeSpec.initialPrice));
			//
			switch (eventName) {
				case "EN_NB_UPPERBREAKUP":
				case "EN_NB_UPMOMENTUM":
				case "EN_NB_LOWERBREAKDOWN":
				case "EN_NB_DOWNMOMENTUM":		
					//Check BO if it is withing Beak-Bottom band.
					string pos = BreakPositionForPBBand(breakPrice); //@TNKGauge.cs
					string logMessage = MemoNS("eventName",eventName)+MemoND("breakPrice",breakPrice)+MemoND("TNKRecentPeakValue",TNKRecentPeakValue)+MemoND("TNKRecentBottomValue",TNKRecentBottomValue);
					switch(pos){
						case "INSIDE":
							willReject = true;
							string reason = "NB_follow_inside_PeakBottomBand";
							AddRejectReason(reason);
							LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);							
							break;
						case "UPSIDE":
						case "DOWNSIDE":
							if (doesCheckTrend){
								string trend = TrendCheckWithPeakBottom(gapMargin);
								logMessage = logMessage + MemoNS("trend",trend);
								switch (trend){
									case "UP":
										switch(eventName){
											case "EN_NB_UPPERBREAKUP":
											case "EN_NB_UPMOMENTUM":
												willReject = false;
												break;
											default:
												willReject = true;
												reason = "NB_P/B_BadEventInUPTrand";
												AddRejectReason(reason);
												LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);
												break;
										}
										break;
									case "DOWN":
										switch(eventName){
											case "EN_NB_LOWERBREAKDOWN":
											case "EN_NB_DOWNMOMENTUM":
												willReject = false;
												break;
											default:
												willReject = true;
												reason = "NB_P/B_BadEventInDOWNTrand";
												AddRejectReason(reason);
												LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);
												break;
										}
										break;
									default:
										willReject = true;
										reason = "NB_P/B_InappropriateTrend";
										AddRejectReason(reason);
										LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);
										break;
								}
							}else{
								willReject = false;
							}
							break;
						default:
							LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown Poistion",pos));
							break;
					}
					break;
				default:
					LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown eventName",eventName));
					break;
			}
			return willReject;
		}
		

		//============
		//  Reject signal when NB within Peak/Bottom Range
		//    Create 2016/08/09
		//    V1.1  2016/08/30
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NB_PeakBottom_ClippingV11(TradeSpec tradeSpec){
			// Parameters
			//bool checkBothSide = true; //Check Up/Down in both peak side and bottom side. if false, check only Breakout side.
			string pbTrendCheckOption = "NONE"; //"SINGLE_SIDE","BOTH_SIDE","NONE"　SIGNIFICANT_PARAMETER:
			double gapMargin = 0.0; //Marginf for cheking if bottom/peak was changed.
			//
			bool willReject=false;
			if(tradeSpec.eventType != "NB") return false;
			string eventName = tradeSpec.eventName;
			//Check BO if it is withing Beak-Bottom band.
			double peak    = TNKRecentPeakValue;
			double bottom  = TNKRecentBottomValue;
			double pPeak   = TNKPreviousPeakValue;
			double pBottom = TNKPreviousBottomValue;
			double breakPrice = Close[0];
			LogDebug("SFIL_NB_PeakBottom_Clipping",MemoND("breakPrice",breakPrice),MemoND("tradeSpec.initialPrice",tradeSpec.initialPrice));
			//
			switch (eventName) {
				case "EN_NB_UPPERBREAKUP":
				case "EN_NB_UPMOMENTUM":
				case "EN_NB_LOWERBREAKDOWN":
				case "EN_NB_DOWNMOMENTUM":
					string pos = PositionAgainstRange(breakPrice,peak,bottom);
					string logMessage = MemoND("breakPrice",breakPrice)+MemoND("peak",peak)+MemoND("bottom",bottom);
					switch(pos){
						case "INSIDE":
							willReject = true;
							string reason = "NB_within_PeakBottomBand";
							AddRejectReason(reason);
							LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);							
							break;
						case "UPSIDE":
							switch(eventName){
								case "EN_NB_UPPERBREAKUP":
								case "EN_NB_UPMOMENTUM":
									//Check if P/B goes up "UP"/"DOWN"/"FLAT"/"ERROR"
									string peakCheck   = IsUpOrDown(pPeak,   peak,   gapMargin);
									string bottomCheck = IsUpOrDown(pBottom, bottom, gapMargin);
									bool okPeak = (peakCheck == "UP");
									bool okBottom = (bottomCheck == "UP");
									bool ok = false;
									switch(pbTrendCheckOption){
										case "NONE":
											ok = true;
											break;
										case "SINGLE_SIDE":
											ok = okPeak;
											break;
										case "BOTH_SIDE":
											ok = okPeak && okBottom;
											break;
										default:
											LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown pbTrendCheckOption",pbTrendCheckOption));
											break;
									}
									if (ok) {
										willReject = false;
									}else{
										willReject = true;
										reason = "NB_P/B_IsNotUpTrand";
										AddRejectReason(reason);
										LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);
									}
									break;
								default:
									//Error wrong side
									LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("InconsistentUpSideCheck",eventName));
									break;
							}
							LogDebug("SFIL_NB_PeakBottom_Clipping()",logMessage);
							break;
						case "DOWNSIDE":
							switch(eventName){
								case "EN_NB_LOWERBREAKDOWN":
								case "EN_NB_DOWNMOMENTUM":
									//Check if P/B goes Down
									string peakCheck   = IsUpOrDown(pPeak,   peak,   gapMargin);
									string bottomCheck = IsUpOrDown(pBottom, bottom, gapMargin);
									bool okPeak = (peakCheck == "DOWN");
									bool okBottom = (bottomCheck == "DOWN");
									bool ok = false;
									switch(pbTrendCheckOption){
										case "NONE":
											ok = true;
											break;
										case "SINGLE_SIDE":
											ok = okBottom;
											break;
										case "BOTH_SIDE":
											ok = okPeak && okBottom;
											break;
										default:
											LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown pbTrendCheckOption",pbTrendCheckOption));
											break;
									}
									if (ok) {
										willReject = false;
									}else{
										willReject = true;
										reason = "NB_P/B_IsNotDownTrand";
										AddRejectReason(reason);
										LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);
									}
									break;
								default:
									//Error wrong side
									LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("InconsistentDownSideCheck",eventName));
									break;
							}
							LogDebug("SFIL_NB_PeakBottom_Clipping()",logMessage);
							break;
						default:
							LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown Poistion",pos));
							break;
					}
					break;
				default:
					LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown eventName",eventName));
					break;
			}
			return willReject;
		}
		
		// Old version
		public bool SFIL_NB_PeakBottom_ClippingV10(TradeSpec tradeSpec){
			bool willReject=false;
			if(tradeSpec.eventType != "NB") return false;
			string eventName = tradeSpec.eventName;
			//Check BO if it is withing Beak-Bottom band.
			double peak = TNKRecentPeakValue;
			double bottom = TNKRecentBottomValue;
			double breakPrice = Close[0];
			switch (eventName) {
				case "EN_NB_UPPERBREAKUP":
				case "EN_NB_UPMOMENTUM":
				case "EN_NB_LOWERBREAKDOWN":
				case "EN_NB_DOWNMOMENTUM":
					string pos = PositionAgainstRange(breakPrice,peak,bottom);
					string logMessage = MemoND("breakPrice",breakPrice)+MemoND("peak",peak)+MemoND("bottom",bottom);
					switch(pos){
						case "INSIDE":
							willReject = true;
							string reason = "NB_within_PeakBottomBand";
							AddRejectReason(reason);
							LogFilter("SFIL_NB_PeakBottom_Clipping()",MemoNS("reason",reason),logMessage);							
							break;
						case "UPSIDE":
						case "DOWNSIDE":
							LogDebug("SFIL_NB_PeakBottom_Clipping()",logMessage);
							break;
						default:
							LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown Poistion",pos));
							break;
					}
					break;
				default:
					LogError("SFIL_NB_PeakBottom_Clipping",MemoNS("Unknown eventName",eventName));
					break;
			}
			return willReject;
		}
		
		//============
		//  Reject signal when StdDev is too low
		//     Create 2016/08/23
		//
		//============
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NB_TooLowStdDev(TradeSpec tradeSpec){
			bool willReject=false;
			if(tradeSpec.eventType != "NB") return false;
			double sigmaFactor = 1.0;
			double stdDev = SigmaThisBar(14) * sigmaFactor;
			//SIGNIFICANT PARAMETER: DEPANDANT: Will not reject when stdDev equal to this border.
			double stdDevBorder = 10.0; // Yen
			//
			bool isTooLowStdDev = stdDev < stdDevBorder;
			//
			if(isTooLowStdDev){
				willReject = true;
				string reason = "StdDevIsTooLow";
				AddRejectReason(reason);
				LogFilter("SFIL_NB_TooLowStdDev()",TAGMsg(reason),MemoND("stdDev",stdDev),MemoND("stdDevBorder",stdDevBorder));
			}
			return willReject;
		}

		//==================================
		//  Reject signal from Open Window Test Version
		//   Create 2015/09/27
		//   v1.0 Revise 2015/09/27
		//==================================
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_OpenWindow(TradeSpec tradeSpec){
			bool willReject=false;
			//NT7 bool isFirstBarOfSession = Bars.FirstBarOfSession;		
			bool isFirstBarOfSession = Bars.IsFirstBarOfSession;
			// reject Only first bar in session.
			if(!isFirstBarOfSession){return false;}
			//
			string kind = OpenWindowName(0);
			switch(kind){
				case "UPWINDOW":
				case "DOWNWINDOW":
					double[] ohlc = MergeWithPreviousBar(0);
					double move = Math.Abs(MoveOfOHLC(ohlc));
					//
					int stdDevPeriod = 14;
					double sizeFactor = 2.0; // 2.0 * StdDev.  Need to ajust.
					double breakoutFactor = TNKNormalizedBreakoutBorder;
					double stdDev = StdDev(Close,stdDevPeriod)[0];
					bool isBigWindow = move > stdDev * sizeFactor;
					//
					if(isBigWindow){
						willReject = true;
						string reason = "OPENWINDOW";
						AddRejectReason(reason);
						LogFilter("SFIL_Temaplate()",TAGMsg(reason),MemoNS("kind",kind),MemoND("move",move));
					}else{
						//Do Nothing
						willReject = false;
					}	
					break;
				case "NOWINDOW":
					willReject=false;
					break;
				default:
					LogError("SFIL_OpenWindow()",MemoNS("Illegal OpenWindowName",kind));
					break;
			}
			return willReject;
		}
		
		//==================
		// Reject Negative Big Bar 
		//  Ver 1.0 2017/03/18
		//  BigBar Couner signal Performace was improved when omit negative Bigbar in backtest.
		//==================
		public bool SFIL_BB_NegativeBigBar(TradeSpec tradeSpec){
			bool willReject = false;
			if(tradeSpec.eventType != "BB") return false;
			string eventName = tradeSpec.eventName;
			switch(eventName){
				case "EN_BB_BIGNEGATIVEBAR":
					willReject = true;
					string reason = "OmitNegativeBigBarSignal";
					AddRejectReason(reason);
					LogFilter("SFIL_BB_NegativeBigBar()",TAGMsg(reason));
					break;
				case "EN_BB_BIGPOSITIVEBAR":
					willReject = false;
					break;
				default:
					LogError("SFIL_BB_NegativeBigBar()",MemoNS("Illegal eventName",eventName));
					break;
			}
			return willReject;
		}
		
		//==================
		// Reject Signal from Bad Big Bar in 1st bar of session
		//==================
		// 2015/10/03
		public bool SFIL_BadBigBarInFirstBarOfSession(TradeSpec tradeSpec){
			bool willReject = false;
			int type = 3; //Break both SwingRange and Normarized border.
			bool isBadBigBar = IsBadBigBar(type);
			//NT7 bool isFirstBarOfSession = Bars.FirstBarOfSession;
			bool isFirstBarOfSession = Bars.IsFirstBarOfSession;
			if (isFirstBarOfSession && isBadBigBar){
				willReject = true;
				string reason = "BBBInFirstBarInSession";
				AddRejectReason(reason);
				LogFilter("SFIL_BadBigBarInFirstBarOfSession()",TAGMsg(reason));
			}else{
				willReject = false;
			}
			return willReject;
		}
		
		//==================
		// Reject Big Bar that matches some bad conditions
		//==================
		// 2015/10/03
		public bool SFIL_BigBarOfBadCondition(TradeSpec tradeSpec){
			bool willReject = false;
			int type = 3; //Break both SwingRange and Normarized border.
			bool isBadBigBar = IsBadBigBar(type,0);
			//
			bool isPriviousBadBigBar = IsBadBigBar(type,1);
			bool isSameDirection = (BarDirectionName(1)==BarDirectionName(0));
				
			if (isBadBigBar){
				willReject = true;
				string reason = "BadBigBar";
				AddRejectReason(reason);
				LogFilter("SFIL_BigBarOfBadCondition()",TAGMsg(reason),MemoNI("type",type));
			}else if(isPriviousBadBigBar && isSameDirection){
				willReject = true;
				string reason = "BadBigBarConcatination";
				AddRejectReason(reason);
				LogFilter("SFIL_BigBarOfBadCondition()",TAGMsg(reason),MemoNI("type",type));				
			}else{
				willReject = false;
			}
			return willReject;
		}
	
		//==================
		// Reject Signal from 1st bar of session
		//==================
		// 2015/09/17
		public bool SFIL_PreventToTakePositionInFirstBarInSession(TradeSpec tradeSpec){
			bool willReject = false;
			
			//NT7 if (Bars.FirstBarOfSession){
			if (Bars.IsFirstBarOfSession){
				willReject = true;
				string reason = "FirstBarInSession";
				AddRejectReason(reason);
				LogFilter("SFIL_PreventToTakePositionInFirstBarInSession()",TAGMsg(reason));
			}else{
				willReject = false;
			}
			return willReject;
		}
		
		//============
		//  Reject Signal from TONBO or Long Bearded Bar
		//============
		// 2015/09/02
		public bool SFIL_FromDirectionlessBar(TradeSpec tradeSpec){
			bool willReject=false;
			// For All Signals //if(tradeSpec.eventType != "SOME_TYPE") return false;
			bool directionless = IsTonbo(0) || IsBearded(0);
			if(directionless){
				willReject = true;
				string reason = "DirectionlessBar";
				AddRejectReason(reason);
				LogFilter("SFIL_FromDirectionlessBar()",TAGMsg(reason));
			}
			return willReject;
		}
		
		//============
		//  Semi Bigbar filtering under low StdDev
		//    Counter for Devils's Money Collection
		//============
		// 2015/08/19
		public bool SFIL_NB_RejectSemiBigBarInLowStdDev(TradeSpec tradeSpec){
			bool willReject=false;
			if(tradeSpec.eventType != "NB") return false;
			double move = Move(0);
			double sigma = SigmaThisBar(14);
			double sigmaFactor = 1.0;
			double stdDevBorder = 30.0; //Yen
			//
			bool isSemiBigBar = Math.Abs(move)> (sigma*sigmaFactor); //>OneSigma
			bool isLowStdDev = sigma <= stdDevBorder;
			//
			if(isLowStdDev && isSemiBigBar){
				willReject = true;
				string reason = "SemiBigBarUnderLowStdDev";
				AddRejectReason(reason);
				LogFilter("SFIL_NB_RejectSemiBigBarInLowStdDev()",TAGMsg(reason));
			}
			return willReject;
		}
		
		//============
		//  Simple Event Filtering for test porpose.
		//============
		// 2015/09/15
		public bool SFIL_TestRejectEvent(TradeSpec tradeSpec){
			bool willReject=false;
			if(tradeSpec.eventType != "BB") return false;
			string evt = tradeSpec.eventName;
			if(evt=="EN_BB_BIGNEGATIVEBAR" || evt == "EN_BB_BIGPOSITIVEBAR"){
				willReject = true;
				string reason = MemoNS("RejectForTest",evt);
				AddRejectReason(reason);
				LogFilter("SFIL_TestRejectEvent()",TAGMsg(reason));
			}
			return willReject;
		}
		
		//============
		//  Worst Performance Event Filtering
		//   Under testing
		//============
		// 2015/08/07
		public bool SFIL_RejectWorstPerformanceEvent(TradeSpec tradeSpec){
			int startBorder = 200;
			string defaultRejectEvent = "EN_NB_LOWERBREAKDOWN";
			string rejectEventName = defaultRejectEvent;
			bool willReject=false;
			//Wait to pass start border var no.
			bool enable = CurrentBar > startBorder;
			
			if(enable){
				//string worstEvent = TNKWorstPerformanceEventContinuous;
				rejectEventName = TNKWorstPerformanceEventHistorical;
				// Use default event name when valid data is not there.
				if (rejectEventName=="") {rejectEventName = defaultRejectEvent;}
				//LogDebug("SFIL_RejectWorstPerformanceEvent() TEST",MemoNS("TNKWorstPerformanceEventContinuous",TNKWorstPerformanceEventContinuous));
				//LogDebug("SFIL_RejectWorstPerformanceEvent() TEST",MemoNS("TNKWorstPerformanceEventHistorical",TNKWorstPerformanceEventHistorical));
			}else{
				rejectEventName = defaultRejectEvent;
			}
			// Judge the event name.
			LogDebug("SFIL_RejectWorstPerformanceEvent()",
				MemoNB("enable",enable),
				MemoNB("IsInReal()",IsInReal()),
				MemoNS("rejectEventName",rejectEventName),
				MemoNS("WorstPerformanceEventHistorical",TNKWorstPerformanceEventHistorical),
				MemoNS("Continuous",TNKWorstPerformanceEventContinuous)
			);
			string evt = tradeSpec.eventName;
			if(evt == rejectEventName){
				willReject = true;
				string reason = "WorstPerformaceTriggerEvent";
				AddRejectReason(reason);
				LogFilter("SFIL_RejectWorstPerformanceEvent()",TAGMsg(reason));
			}
			//return willReject;
			return false; //TESTING
		}
		
		//================================================
		//  Remove first breakout after long flat market.
		//   Because Likely reverse in next bar.
		//================================================
		//NT7
//		public bool SFIL_NB_BreakoutAfterLongFlat(TradeSpec tradeSpec){
//			bool willReject=false;
//			int limit = -7;
//			if(tradeSpec.eventType != "NB") return false;
//			string evt = tradeSpec.eventName;
//			if(evt=="EN_NB_UPPERBREAKUP" || evt=="EN_NB_LOWERBREAKDOWN" ){
//				double boBorder = TNKNormalizedBreakoutBorder;
//				if(INDBarsFromNBreakout(boBorder,14)[1] < limit){
//					willReject = true;
//					string reason = "NBMomentumBreakoutAfterLongFlat";
//					AddRejectReason(reason);
//					LogFilter("SFIL_NB_BreakoutAfterLongFlat()",TAGMsg(reason));
//				}
//			}
//			return willReject;
//		}
		//NT7

		//==================
		//  Momentum Check
		//==================
		//Return true if signal will be rejected. else siginal accepted
		public bool SFIL_NB_MomentumChek(TradeSpec tradeSpec){
			bool willReject=false;
			if(tradeSpec.eventType != "NB") return false;
			string evt = tradeSpec.eventName;
			if(evt=="EN_NB_UPMOMENTUM" || evt=="EN_NB_DOWNMOMENTUM" ){
				//Check if Previus bar's direction is same as this bar's.
//				if(IsNeutralBar(0)){
//					//Neutral bar will be rejected - NO momentum
//					//Nretral bar never trigger this event
//					willReject = true;
//					string reason = "NBMomentumNEWTRALBAR";
//					AddRejectReason(reason);
//					LogFilter("SFIL_NB_MomentumChek()",TAGMsg(reason));				
//				}else if (BarDirectionName(0) != BarDirectionName(1)){
//					//Privious Direction is not same as this bar - NO momentum
//					willReject = true;
//					string reason = "NBMomentumPreviousBarReversed";
//					AddRejectReason(reason);
//					LogFilter("SFIL_NB_MomentumChek()",TAGMsg(reason));					
//				}else if(INDBarsFromNBreakout(1.0)[0]>5){
//					willReject = true;
//					string reason = "NBMomentumAfter4barsFromNB";
//					AddRejectReason(reason);
//					LogFilter("SFIL_NB_MomentumChek()",TAGMsg(reason));
//				}
			}
			return willReject;
		}
		
		
		//=========
		// Signal Filter to eliminate position likely to reverse soon.
		//=========
		public bool SFIL_RejectSellSignalInUppserOfSMA50(TradeSpec tradeSpec){
			bool willReject = false;
			double SMA50 = SMA(50)[0];
			if(tradeSpec.signal=="SELL"){
				if(Close[0]>SMA50){
					willReject = true;
					string reason = "SellInUpperOfSMA50";
					AddRejectReason(reason);
					LogFilter("SFIL_RejectSellSignalInUppserOfSMA50()",TAGMsg(reason));
				}else{
					willReject = false; //Lower than SMA50
				}
			}else{
				willReject = false; // Signal is not SELL
			}
			return willReject;
		}

		public bool SFIL_RejectSellSignalFromExhaustedMomentum(TradeSpec tradeSpec){
			bool willReject = false;
			double nc = INDNormalizedClose(14,TNKNormalizedBreakoutBorder)[0];
			nc = Math.Abs(nc);
			double exhaustedLimit = 2.0;
			if(tradeSpec.triggerEvent.eventName == "EN_NB_DOWNMOMENTUM"){
				if(nc > exhaustedLimit){ //Mometum may be exhausted in thi Bar aleady.
					willReject = true;
					string reason = "ExhaustedDownMometum";
					AddRejectReason(reason);
					LogFilter("SFIL_RejectSellSignalFromExhaustedMomentum()",TAGMsg(reason));
				}else{
					willReject = false; //nc < border
				}
			}else{
				willReject = false; // Even is not EN_NB_DOWNMOMENTUM
			}
			return willReject;
		}
		
//========================
		//=========
		// MANDATORY
		// Signal Filter for duplicate signal for same direction
		//   This filter is mandatory. Do NOT revmove this filter.
		//=========
		public bool SFIL_RejectAnySignalIfPositionAleadyTaken(TradeSpec tradeSpec){
			bool willReject = false;
			if (havePosition()){
				willReject = true;
				string reason = "PositionAlreadyTaken";
				AddRejectReason(reason);
				LogFilter("SFIL_RejectDuplicateSignalToSameDirection()",TAGMsg(reason));
			}else{
				willReject = false;	
			}
			return willReject; 
		}

		//=========
		// MANDATORY
		// Signal Filter for duplicate signal for same direction
		//   This filter is mandatory. Do NOT revmove this filter.
		//   Revised using IsDOTEN()   2019/06/13
		//  TNKInSessionDOTENSignalCount
		//=========
		public bool SFIL_Reject_DuplicateSignalToSameDirection(TradeSpec tradeSpec){
			bool willReject;
			bool isDoten = IsDoten(tradeSpec);
			if(isDoten) {
				TNKInSessionDOTENSignalCount += 1; //Count this is DOTEN signal
				LogDebug("SFIL_Reject_DuplicateSignalToSameDirection",MemoNI("TNKInSessionDOTENSignalCount",TNKInSessionDOTENSignalCount));
			}
			bool isMatch = (PositionSign()!=0.0) && !isDoten;
			if (isMatch){
				willReject = true;
				string reason = "DulicateSameDirectPosition";
				AddRejectReason(reason);
				LogFilter("SFIL_Reject_DuplicateSignalToSameDirection()",TAGMsg(reason));
			} else {
				willReject = false;
			}
			return willReject; 
		}
//TBDel
//		//=========
//		// MANDATORY
//		// Signal Filter for duplicate signal for same direction
//		//   This filter is mandatory. Do NOT revmove this filter.
//		//=========
//		public bool SFIL_RejectDuplicateSignalToSameDirection(TradeSpec tradeSpec){
//			bool willReject;
//			string signal = tradeSpec.signal;
//			string direction = PositionDirection();
//			if (direction == "NOPOSITION"){
//				willReject = false;
//			}else{
//				bool isMatch = DoesMatchDirection(signal,direction);
//				if (isMatch){
//					willReject = true;
//					string reason = "DulicateSameDirectPosition";
//					AddRejectReason(reason);
//					LogFilter("SFIL_RejectDuplicateSignalToSameDirection()",TAGMsg(reason));
//				} else {
//					willReject = false;
//				}
//			}
//			return willReject; 
//		}
//TBDel
		
		//=========
		// MANDATORY
		// Signal Filter for reject reverse direction signal on profited position.
		//=========
		public bool SFIL_RejectReverseSignalOnProfitedPosition(TradeSpec tradeSpec){
			bool willReject;
			string signal = tradeSpec.signal;
			string direction = PositionDirection();
			if (direction=="NOPOSITION"){
				willReject=false;
			}else{
				bool match = DoesMatchDirection(signal,direction);
				if(match) {
					willReject = true; //Direction is same to exist position
					string reason = "SameDiretionToExistPosition";
					AddRejectReason(reason);
					LogFilter("SFIL_RejectReverseSignalOnProfitedPosition()",TAGMsg(reason));
				} else {
					double profit = GetProfit();
					if (profit > 0) {
						willReject = true; //Position hold profit.
						string reason = "PositionHoldProfit";
						AddRejectReason(reason);
						LogFilter("SFIL_RejectReverseSignalOnProfitedPosition()",TAGMsg(reason));
					}else{
						willReject = false; //Reverse sginal on no profit position
					}
				}
			}
			return willReject; 
		}
		
		//=========
		// MANDATORY
		// Signal Filter for rejection 0 position size trade.
		//=========		
		public bool SFIL_RejectZeroSizeTrade(TradeSpec tradeSpec){
			int size = tradeSpec.size;
			bool willReject = false;
			if (size <= 0){
				willReject = true;
				string reason = "ZeroSizeTrade";
				AddRejectReason(reason);
				LogFilter("SFIL_RejectZeroSizeTrade()",TAGMsg(reason));
			}else{
				willReject = false;
			}
			return willReject;			
		}

		//=========
		// MANDATORY
		// Signal Filter for reject in last bar in session.(dedicate to OSE NIKKEI225M)
		// Modified last bar time. old 2:00am - 3:10am new 4:30am - 5:40am  (2017/06/03)
		//=========
		public bool SFIL_PreventToTakePositionInLastBarInSession(TradeSpec tradeSpec){
			bool willReject = false;
			int now = ToTime(Time[0]);
			int lastBarStart = 43000;  // 4:30am
			int lastBarEnd   = 54000;  // 5:40am  +10 min is margin time.
			if (now >= lastBarStart && now < lastBarEnd){
				willReject = true;
				string reason = "LastBarInSession";
				AddRejectReason(reason);
				LogFilter("SFIL_PreventToTakePositionInLastBarInSession()",TAGMsg(reason));
			}else{
				willReject = false;
			}
			return willReject;
		}
	
		//=========
		// Signal Filter for UP:NOEVNET and DOWN:NOEVENT
		//   judge if move monentam cotinue to the direction.
		//   check if past 3 bar(include current bar) direction mach to signal.
		//=========
		public bool SFIL_MomentumChekForNOEVENTEntry(TradeSpec tradeSpec){
			//Check if the trigger event's type is "MC"
			if(tradeSpec.eventType != "MC") return false;
			
			bool willReject = false; //Initial value must be false.
			string signal = tradeSpec.signal;
			string conditionName = tradeSpec.eventName;
			switch (conditionName) {
				case "UP:NOEVENT":
				case "DOWN:NOEVENT":
					switch(signal){
						case "BUY":
							if(!WasPositivePastIn(3,0)){ //Positive 3bar didn't contain neutral bar
								LogDebug("SFIL_MomentumChekForNOEVENTEntry()",MemoNB("@BUY WasPositivePastIn(3,0)",WasPositivePastIn(3,0)));
								willReject = true;
							}
							break;
						case "SELL":
							if(!WasNegativePastIn(3,0)){ //Positive 3bar didn't contain neutral bar
								LogDebug("SFIL_MomentumChekForNOEVENTEntry()",MemoNB("@SELL WasNegativePastIn(3,0)",WasNegativePastIn(3,0)));
								willReject = true;
							}
							break;
						default:
							//Illegal signal name
							LogError("SFIL_MomentumChekForNOEVENTEntry",MemoNS("Illegal signal",signal));
							break;
					}
					if(willReject) {
						string reason = conditionName +" "+ signal + " bfr3BarsNotAlong.";
						AddRejectReason(reason);
						LogFilter("SFIL_MoveMomentumChekForNOEVENTEntry()",TAGMsg(reason));
					}
					break;
				default:
					//NO Target condition
					break;
			}
			return willReject;
		}


		//=========
		// Signal Filter for update events. Chek if reverse during detection lag period.
		//  Detection lag means period between real peak occourence and detetection by Swing(n) Indicator
		//=========
		public bool SFIL_ReverseDuringUpdateDetectionLag(TradeSpec tradeSpec){
			//Check if the trigger event's type is "MC"
			if(tradeSpec.eventType != "MC") return false;
			
			int lookBack = TNKSwingPeriod; //Parameter of Swing(n) indicator.
			string eventName = EventNameOf(tradeSpec.triggerEvent.eventName);
			if (EventCategory(eventName)!= "UPDATE") return false; //No rejcet if event category is not update
			//
			bool willReject = false;
			//
			//int[] barCounts = CountUpBarsParDirection(lookBack);
			//int positiveBarCount = barCounts[0];
			//int negativeBarCount = barCounts[1];
			//int neutralBarCount = barCounts[2];
			//
			// Check price direction
			double spikePrice = Close[lookBack];
			double delta = Close[0] - spikePrice;
			string direction;
			if (delta>0){
				direction = "BUY";
			}else if (delta<0){
				direction = "SELL";
			}else{
				direction = "NODIRECTION"; //Any signal will be rejected.
			}
			// Cheeck if match signal and real direction
			string signal = tradeSpec.signal;
			bool match = DoesMatchDirection(signal,direction);

			if(match){
				willReject=false;
			}else{
				willReject=true;
				string reason = "DirectionInLagNotMatchSignal";
				AddRejectReason(reason);
				LogFilter("SFIL_ReverseDuringUpdateDetectionLag()",TAGMsg(reason),
					MemoNS("signal",signal),
					MemoNS("direction",direction)
				);
			}
			return willReject;
		}

		
		//=======
		// Segnal Filter entry signal in RANGE state when TNKSwingRange is narrow
		//=======
		public bool SFIL_NarrowRage(TradeSpec tradeSpec){
			LogDebug("SFIL_NarrowRage()",MemoNS("tradeSpec.eventType",tradeSpec.eventType)); //TEST!TEST!
			//Check if the trigger event's type is "MC"
			if(tradeSpec.eventType != "MC") return false;
			
			bool willReject = false;
			double widthLimitMinimum = TickSize * 10.0; //PARAM
			double widthLimit = ATR(3)[0]*3.0; //PARAM 
			widthLimit = widthLimit < widthLimitMinimum ? widthLimitMinimum : widthLimit;
			string stateName = StateNameOf(tradeSpec.eventName);
			string eventName = EventNameOf(tradeSpec.eventName);
			//bool isNarrowRange = (TNKStateNameCurrent == "RANGE") && (TNKSwingRange < widthLimit);
			bool isNarrowRange = (stateName == "RANGE") && (TNKSwingRange < widthLimit);
			//bool isBreakEvent = DoesContain(TNKLatestConditionName,"BREAK"); //ISSUE
			bool isBreakEvent = IsBreakEvent(eventName);
			if (isNarrowRange && !isBreakEvent) { //Braks event will not be rejected
				willReject = true;
				string reason = "Narrow Range";
				AddRejectReason(reason);
				LogFilter("SFIL_NarrowRage()",TAGMsg(reason));
			}
			if (willReject){
				LogFilter("SFIL_NarrowRage()",
					MemoND("TNKSwingRange",TNKSwingRange),MemoND("widthLimit",widthLimit),
					MemoNB("isNarrowRange",isNarrowRange),MemoNB("isBreakEvent",isBreakEvent),MemoNB("willReject",willReject)
				);
			}

			return willReject;
		}
		
		//======
		// Sginal Filter entry signal when the entry direction does not much trend in RANGE.
		//  Fileter Long(Short) entry when trend of Range goes down(up).
		//=======
		public bool SFIL_RangeTrendCheck(TradeSpec tradeSpec){
			//Check if the trigger event's type is "MC"
			if(tradeSpec.eventType != "MC") return false;
			
			string originalSignal = tradeSpec.signal;
			string signal = originalSignal;
			if (originalSignal=="REVERSE") {signal = GetActualDirectionOfREVERSE();}
			// no filter if trend is not range or signal is not "NONE"
			string stateName = StateNameOf(tradeSpec.eventName);
			//if(TNKStateNameCurrent != "RANGE" || signal == "NONE" ){ //TBDel
			if(stateName != "RANGE" || signal == "NONE" ){
				return false;
			}
			LogDebug("SFIL_RangeTrendCheck()",
				MemoNS("stateName",stateName),
				MemoNS("originalSignal",originalSignal),MemoNS("signal",signal)
			);
			//Check peak if current peak higher(lower) then 2 befor peak.
			double peakPrev = PeakValue(2); //PARAM
			bool isPeakUp = TNKRecentPeakValue > peakPrev;
			bool isPeakDown = TNKRecentPeakValue < peakPrev;
			//Check peak if current bottom higher(lower) then 2 befor peak.
			double bottomPrev = BottomValue(2); //PARAM
			bool isBottomUp = TNKRecentBottomValue > bottomPrev;
			bool isBottomDown = TNKRecentBottomValue < bottomPrev;
			//Judge Trend in RANGE
			bool isUpRange = isPeakUp && isBottomUp;
			bool isDownRange = isPeakDown && isBottomDown;
			//Judge rejection based siganal and Trend in Range
			bool willRejected = true;	

			if(isUpRange   && signal=="BUY")  {willRejected = false;}
			if(isDownRange && signal=="SELL") {willRejected = false;}
			
			if(willRejected){
				string reason = "Against renge trend";
				AddRejectReason(reason);
				LogFilter("SFIL_RangeTrendCheck()",TAGMsg(reason));
				LogFilter("SFIL_RangeTrendCheck()",
					MemoNB("isPeakUp",isPeakUp),MemoNB("isPeakDown",isPeakDown),
					MemoNB("isBottomUp",isBottomUp),MemoNB("isBottomDown",isBottomDown),
					MemoNB("isUpRange",isUpRange),MemoNB("isDownRange",isDownRange),
					MemoNS("originalSignal",originalSignal),MemoNS("signal",signal),MemoNB("willRejected",willRejected)
				);
			}
			return willRejected;
		}
    }
}

//=====================================================================
//  Obsolete codes
//================================

