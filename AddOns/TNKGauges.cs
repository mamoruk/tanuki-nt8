//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//============================
		//  Line Definition
		//   Create: 2019/03/15
		//   Line(double a,double b)
		//============================
		public class Line:IFDescription {
			public double A {get; set;}
			public double B {get; set;}
			public Line(double intercept, double gradient){
				A = intercept;
				B = gradient;
			}
			public Line() : this(0.0,0.0) {
			}
			//
			// Utility methods
			//
			public double ValueForX(double x){
				return A + (B * x);
			}
			//Retrun "CROSS","PARALLEL","SEPARATE"
			public string Parallelism(Line line){
				double sepalate = 5/10; //1 tic par 10 bar
				double cross = -sepalate;
				string result="UNKNOWN";
				//Judge
				double diffB = this.B - line.B;
				if(diffB > sepalate) {
					result = "SEPARATE";
				}else if(diffB < cross){
					result = "CROSS";
				}else{
					result = "PARALLEL";
				}
				return result;
			}
			//Return "UPTREND", "FLAT", "DOWNTREND"
			public string Trend(){
				double up = 50/10;  //10 tic par 10 bar
				double down = -up;
				string trend = "UNKNOWN";
				//Judge
				double b = this.B;
				if(b > up) {
					trend = "UPTREND";
				}else if(b < down){
					trend = "DOWNTREND";
				}else{
					trend = "FLAT";
				}				
				return trend;
			}
			//Return midline 
			public double[] Midline(Line line){
				double newA = (this.A + line.A)/2.0;
				double newB = (this.B + line.B)/2.0;
				return new double[2]{newA, newB};
			}
			//Require by IFDescription
			public string Description(){
				return string.Format("A:{0:f2} B:{1:f2}",A,B);
			}
		}
		
		//============================
		//  Regression Line Definition
		//   Create: 2019/03/10
		//   RegressionLine(a,b,origBarnum,r2)
		//============================
		public class RegressionLine : Line,IFDescription {
			Strategy o;
//			public double A;
//			public double B;
			public int OrigBarnum;
			public double R2;
			//
			// Constructors
			//
			RegressionLine(Strategy owner, double a, double b, int origBarnum, double r2) :base(a,b){
				o = owner;
//				A = a;
//				B = b;
				OrigBarnum = origBarnum;
				R2 = r2;
			}
			//origBarnum is Bar number of x[0]
			public RegressionLine(Strategy owner, double[] x, double[] y, int origBarnum){
				o = owner;
				double[] coe = o.DecideRegressionLine(x,y);
				A = coe[0];
				B = coe[1];
				R2 =o.RSquared(coe[0],coe[1],x,y);
				OrigBarnum = origBarnum;
			}
			//barNum[] contain barNumber.
			public RegressionLine(Strategy owner, int[] barNum, double[] y){
				double[] x = new double[barNum.Length];
				int origBarnum = barNum[0];
				for(int xi = 0; xi < barNum.Length; xi++){
					x[xi]= (double)(barNum[xi] - OrigBarnum);
				}
				double[] coe = o.DecideRegressionLine(x,y);
				A = coe[0];
				B = coe[1];
				R2 =o.RSquared(coe[0],coe[1],x,y);
				OrigBarnum = origBarnum;
			}
			//
			// Utility methods
			//
			public string Description(){
				return string.Format("A:{0:f2} B:{1:f2} R2:{2:f2}",A,B,R2);
			}
		}
		
		//============================
		//  VolatilityChannel Definition
		//   Create: 2018/11/22
		//   VolatilityChannel(period, factor)
		//============================
		public class VolatilityChannel {
			public int Period {get; set;}
			public double Width {get; set;}
			private Strategy o;
			public VolatilityChannel(Strategy owner, int period, double width){
				o = owner;
				Period = period;
				Width = width;
			}
			public double UpperValue(int ago){ return o.SMA(Period)[ago] + o.StdDev(Period)[ago] * Width; }
			public double LowerValue(int ago){ return o.SMA(Period)[ago] - o.StdDev(Period)[ago] * Width; }
			public double MidValue(int ago)  { return o.SMA(Period)[ago]; }
			public double ValueByName(string valName, int ago){
				switch(valName){
					case "UPPER":
						return UpperValue(ago);
						break;
					case "MID":
						return MidValue(ago); 
						break;
					case "LOWER":
						return LowerValue(ago);
						break;
					default:
						//Error
						o.LogError("ValueByName",o.MemoNS("valName",valName));
						return  MidValue(ago); 
						break;
				}
			}
		}
		//===========================
		//  Bar pattern detection
		//==========================
		
		//
		// Check pattern of Breakout without break back at lookback 
		//   level: level for breakout   direction: 1:Find break up   -1:Find brak down  barsAgo: bar for check
		//   Return true if the pattern is found.
		//
		public bool IsBreakOutWithoutBreakBack(double level, int direction, int barsAgo){
			Candle prev = GetCandle(barsAgo + 1);
			Candle curr = GetCandle(barsAgo);
			bool result = false;
			bool isBOWOBB = false;
			bool okMatchPatternPrev, okMatchPatternCurrent;
			bool okBOWBBPreve, okBOWOBBCurrent;
			string privPosition = prev.RelativePositionWithBody(level);
			string currPosition = curr.RelativePositionWithBody(level);
			LogDebug("IsBreakOutWithoutBreakBack()",MemoNS("privPosition",privPosition),MemoNS("currPosition",currPosition));
			//
			// Check BOWBB reguler pattarn
			//
			if(direction == 1) {
				// Check BOWOBB UP
				okMatchPatternPrev = (privPosition ==  "CROSS_UP");
				okMatchPatternCurrent = (currPosition == "DOWNSIDE_BODY" || curr.Hip == level); //Touch_HIP is ok too.
				//okMatchPatternCurrent = (currPosition == "DOWNSIDE_BODY"); //Touch pattern was not coverd
				//okMatchPatternCurrent = okMatchPatternCurrent && curr.DirectionSign == direction; //check if junkou
				isBOWOBB = okMatchPatternPrev && okMatchPatternCurrent;	
			} else if (direction == -1){
				// Check BOWOBB DOWN
				okMatchPatternPrev = (privPosition ==  "CROSS_DOWN");
				okMatchPatternCurrent = (currPosition == "UPSIDE_BODY" || curr.Neck == level); //Touch_NECK is ok too.
				//okMatchPatternCurrent = (currPosition == "UPSIDE_BODY"); //Touch pattern was not coverd
				//okMatchPatternCurrent = okMatchPatternCurrent && curr.DirectionSign == direction; //check if junkou
				isBOWOBB = okMatchPatternPrev && okMatchPatternCurrent;	
			}else{
				//Prameter Error
				LogError("IsBreakOutWithoutBreakBack()",MemoNI("Illegal parameter direction",direction));
			}
			if(isBOWOBB){
				//BOWOBB was found
				LogDebug("IsBreakOutWithoutBreakBack()", TAGMsg("BOWOBB(Reguler) was found"));
				return true;
			}
			//
			//check open widow pattern
			//
			isBOWOBB = false;
			Candle windowAsCandle = prev.GetWindowAsCandle(curr);
			if(windowAsCandle == null){
				//Window was not found
				return false;
			}else{
				// Check if the window clossed border
				string winPosition = windowAsCandle.RelativePositionWithDirection(level);
				if(direction == 1){
					isBOWOBB = (winPosition == "CROSS_UP");
				}else if(direction == -1){
					isBOWOBB = (winPosition == "CROSS_DOWN");
				}else{
					//Prameter Error
					LogError("IsBreakOutWithoutBreakBack()",MemoNI("Illegal parameter direction",direction));					
				}
			}
			if(isBOWOBB){
				//BOWOBB was found
				LogDebug("IsBreakOutWithoutBreakBack()", TAGMsg("BOWOBB(Window) by window was found"));
				return true;
			}
			//BOWOBB was not found
			return false;
		}

		//===========================
		//  Misc utilities
		//===========================
		
		//
		// VOChannel gredient
		//  Input period Channel period, dx  delta x of gradient
		public double ChannelGradient(int period,int dx){
			//Add your custom indicator logic here.
			if(CurrentBar > period && dx != 0) {
				//DEBUG Print("STARTED");
				//double diff = Bollinger(1.4,14)[0] - Bollinger(1.4,14)[Period];
				double diff = SMA(period)[0] - SMA(period)[dx];
				double gladient = diff/dx;
				return gladient;
			}else{
				//DEBUG Print("PRE");
				return  0.0;
			}
		}
		//
		//
		//  Liner function
		//   Input a0, a1, x  return a0 + a1 * x
		//
		public double LinerFunc(double a0, double a1, double x){
			return a0 + a1 * x;
		}
		//
		// R-squared
		//  Input a0, a1, x[], y[]
		//  output R-squared (Kettei keisuu)
		//
		public double RSquared(double a0, double a1, double[] x, double[] y){
			double yMean = y.Average();
			double diffSum = 0.0, allSum = 0.0;
			foreach(int i in Enumerable.Range(0, x.Length)){
				allSum  += Math.Pow(y[i] - yMean, 2.0);
				diffSum += Math.Pow(y[i] - LinerFunc(a0,a1,x[i]), 2.0); 
			}
			if(allSum == 0.0 || (allSum==0.0)&&(diffSum==0.0)){ //All values in y[] are same 
				return 1.0;
			}else{
				double rs = 1.0 - diffSum / allSum;
				return rs;
			}
		}
		// Linear RegressionLine
		//  Least squares method
		//  Input x[], y[]  Output: Coefficient double[]
		//
		public double[] DecideRegressionLine(double[] x, double[] y){
	        double A00=0.0 ,A01=0.0, A02=0.0, A11=0.0, A12=0.0;
			int n = Math.Min(x.Length, y.Length);
	        for (int i=0; i<n; i++) {
				A00+=1.0;
				A01+=x[i];
				A02+=y[i];
				A11+=x[i]*x[i];
				A12+=x[i]*y[i];
	        }
	        double a0 = (A02*A11-A01*A12) / (A00*A11-A01*A01);
	        double a1 = (A00*A12-A01*A02) / (A00*A11-A01*A01);
			return new double[]{a0, a1};
		}

		//
		// Count Up continuous same direction bar
		//    Return int the count
		//     Create:2018/05/26
		public int CountMatchBarDirections(int barsAgo, int count){
			if(count <= 1) return 1;
			int direction = DirectionSign(barsAgo);
			int next;
			for(next = barsAgo + 1 ; next < barsAgo + count; next++){
				int nextDirection = DirectionSign(next);
				if(direction != nextDirection) break;
			}
			return next;
		}

		//
		// Check if match bar direction for sepcified rage
		//   Return int means   0:No match   1:Match all positive  -1:Match all negative
		//    Create:2018/05/26
		public int CheckMatchBarDirections(int barsAgo, int count){
			int curr = DirectionSign(barsAgo);
			if(count < 2) return curr;
			for(int i=1;i<count;i++){
				int j = barsAgo + i;
				int next = DirectionSign(j);
				if(curr != next) return 0;
			}
			return curr;
		}
		public int DirectionSign(int barsAgo){
			return System.Math.Sign(move(barsAgo)); // 1 or 0 or -1
		}
		
		//
		// Compute Composited bar 
		//  Return an array contains [Open, High, Low, Close] 
		//   2018/05/24
		public double[] MergedBar(int barsAgo, int count){
			//double o=0,h=0,l=0,c=0;
			double o = Open[0];
			double h = High[0];
			double l = Low[0];
			double c = Close[0];
			//If there is enough bar, do mearge.
		    if(CurrentBar >= count) { 
				int cnt = count > 0 ? count : 1; //count should be greater equal than 1
				for(int i=0; i<cnt; i++){
					int j = barsAgo + i;
					h = Math.Max(h,High[j]);
					l = Math.Min(l,Low[j]);
					if(i==0)       c = Close[j]; //Latest bar
					if(i== cnt -1) o = Open[j];  //Earlest bar
				}
			}
			return new double[]{o,h,l,c};
		}
		//
		// Compute Composited bar propotion with head,body,leg
		//  Return an array contains length of head, body, leg with index value 
		//   and direction (1.0:positive, -1.0:negative, 0.0:even)
		//   2018/05/24
		public double[] MergedBarDecompose(int barsAgo, int count){
			double[] ohlc = MergedBar(barsAgo, count);
			double o = ohlc[0];
			double h = ohlc[1];
			double l = ohlc[2];
			double c = ohlc[3];
			double range = h - l;
			double neck = Math.Max(o,c);
			double hip  = Math.Min(o,c);
			double body = Math.Abs(c-o);
			double head = h - neck;
			double leg = hip - l;
			double direction;
			if(o == c) {
				direction = 0.0;
			}else{
				direction = c - o > 0 ? 1.0 : -1.0;
			}
			//
			double[] compo = {head, body, leg, direction}; 
			LogDebug("MergedBarDecompose()",MemoND("head",head),MemoND("body",body),MemoND("leg",leg),MemoND("direction",direction));
			return compo;
		}
		//
		// Compute bar propotion with head,body,leg
		//  Return an array contains length of head, body, leg with index value 
		//   and direction (1.0:positive, -1.0:negative, 0.0:even)
		//   2017/04/05
		public double[] BarDecompose(int barsAgo){
			double o = Open[barsAgo];
			double c = Close[barsAgo];
			double h = High[barsAgo];
			double l = Low[barsAgo];
			double range = h - l;
			double neck = Math.Max(o,c);
			double hip  = Math.Min(o,c);
			double body = Math.Abs(c-o);
			double head = h - neck;
			double leg = hip - l;
			double direction;
			if(o == c) {
				direction = 0.0;
			}else{
				direction = c - o > 0 ? 1.0 : -1.0;
			}
			//
			double[] compo = {head, body, leg, direction}; 
			return compo;
		}
		
		//
		// Compute bar propotion with head,body,leg
		//  Return an array contains normarized ratio of each value.  head + body + leg = 1.0 and direction
		//   2017/04/09
		public double[] BarProportion(int barsAgo){
			double[] compo = BarDecompose(barsAgo);
			double head = compo[0], body = compo[1], leg = compo[2], direction = compo[3];
			double length = head + body + leg; // High[barsAgo] - Low[barsAgo];
			double[] proportion = {head/length, body/length, leg/length, direction, length}; 
			return proportion;			
		}
		
		//
		// Judge if StallBar
		//  longSide = Max(head,leg); ShortSide = Min(head,leg)
		//  longSide > body*1.0 and LongSide > shortSide*2.0
		//  Output: "UPSIDE_STALL","DOWNDIDE_STALL","NOSTALL"
		//  2017/04/05
		public string CheckStallBar(int barsAgo){
			///double cap = TickSize * 10.0; //PARAMETER 50yen in N225M.
			double cap = StdDev(14)[barsAgo];
			double[] proportion = BarProportion(barsAgo);
			double head = proportion[0];
			double body = proportion[1];
			double leg  = proportion[2];
			double direction = proportion[3];
			double longSide = Math.Max(head,leg);
			double shortSide = Math.Min(head,leg);
			double range = High[barsAgo] - Low[barsAgo];
			bool isNotShortRange = range > cap;
			bool isStallBar = (longSide > body * 1.0) && (longSide > shortSide * 2.0) && isNotShortRange;
			//
			if(isStallBar) {
				return head > leg ? "UPSIDE_STALL" : "DOWNSIDE_STALL";
			}else{
				return "NOSTALL";
			}
		}
			
		//
		// Judge if BigBar under given type.
		//  2015/10/03
		public bool IsBadBigBar(int type){
			return IsBadBigBar(type, 0);
		}
		public bool IsBadBigBar(int type, int barsAgo){
			bool isBadBigBar = false;
			string judge = JudgeBigBar(type,barsAgo);
			switch(judge){
				case "BIGBAR_POSITIVE":
				case "BIGBAR_NEGATIVE":
					isBadBigBar = true;
					break;
				case "NOBIGBAR":
					isBadBigBar = false;
					break;
				default:
					isBadBigBar = false;
					LogError("IsBadBigBar()",MemoNS("Illegal judge string",judge));
					break;
			}
			return isBadBigBar;
		}
		
		//
		// Judge if the bar is Big bar or mot
		//  Input: type : Judge type of big, 0:All big bar, 1:No breakout, 2:Normalized Breakout(NBO) within PBBand, 3:NBO && PBBand Breakout
		//         barsAgo: specify judge traget
		//  Return: ether "BIGBAR_POSITIVE", "BIGBAR_NEGATIVE", "NOBIGBAR"
		public string JudgeBigBar(int type){
			return JudgeBigBar(type,0);
		}
		public string JudgeBigBar(int type, int barsAgo){
			string bbp ="BIGBAR_POSITIVE";
			string bbn ="BIGBAR_NEGATIVE";
			string nobb = "NOBIGBAR";
			string judge = nobb;
			//
			int stdDevPeriod = 14;
			double sizeFactor = 2.0; // 2.0 * StdDev.  Need to ajust.
			double breakoutFactor = TNKNormalizedBreakoutBorder;
			double stdDev = StdDev(Close,stdDevPeriod)[barsAgo];
			//
			bool isNegative = IsNegativeBar(barsAgo);
			bool isPositive = IsPositiveBar(barsAgo);
			bool isBig = Math.Abs(Move(barsAgo)) > stdDev * sizeFactor;
			// Breakout
			double breakout = stdDev * breakoutFactor;
			//NT7 bool didCrossAbobe = CrossAbove(INDNormalizedClose(stdDevPeriod), breakoutFactor,barsAgo + 1);
			//NT7 bool didCrossBelow = CrossBelow(INDNormalizedClose(stdDevPeriod),-breakoutFactor,barsAgo + 1);
			bool didCrossAbobe = CrossAbove(INDNormalizedClose(stdDevPeriod, breakoutFactor), breakoutFactor,barsAgo + 1);
			bool didCrossBelow = CrossBelow(INDNormalizedClose(stdDevPeriod, breakoutFactor),-breakoutFactor,barsAgo + 1);
			// Momentum (Meomntum condition includes breakout condition )
			//NT7 double normalizedClose = INDNormalizedClose(stdDevPeriod)[0];
			double normalizedClose = INDNormalizedClose(stdDevPeriod, breakoutFactor)[0];
			bool upMomentum = (normalizedClose > breakoutFactor);    //&& IsPositiveBar(0);
			bool downMomentum = (normalizedClose < -breakoutFactor); //&& IsNegativeBar(0);			
			
			//
			double close = Close[barsAgo];
			bool isUpsideOfSwingRange   = close > TNKRecentPeakValue;
			bool isDownsideOfSwingRange = close < TNKRecentBottomValue;
			bool isDownsideOfSwingPeak  = close < TNKRecentPeakValue;
			bool isUpsideOfSwingBottom  = close > TNKRecentBottomValue;
			//
			//NT7 bool isFirstBarOfSession = Bars.FirstBarOfSession;
			bool isFirstBarOfSession = Bars.IsFirstBarOfSession;

			//
			// Judge conditions
			//
			switch(type){
				case 0: //All big bar
					if (isBig && isPositive){
						judge = bbp;
					}else if(isBig && isNegative){
						judge = bbn;
					}
					break;
				case 1: //No breakout
					if (isBig && isPositive && !upMomentum && isDownsideOfSwingPeak){
						judge = bbp;
					}else if (isBig && isNegative && !downMomentum && isUpsideOfSwingBottom){
						judge = bbn;
					}
					break;
				case 2: // :Normalized Breakout(NBO) or Momentum within PBBand
					if (isBig && isPositive && (didCrossAbobe||upMomentum) && isDownsideOfSwingPeak){
						judge = bbp;
					}else if (isBig && isNegative && (didCrossBelow||downMomentum) && isUpsideOfSwingBottom){
						judge = bbn;
					}
					break;
				case 3: // NBO or Momentum && PBBand Breakout
					if (isBig && isPositive && (didCrossAbobe||upMomentum) && isUpsideOfSwingRange){
						judge = bbp;
					}else if (isBig && isNegative && (didCrossBelow||downMomentum) && isDownsideOfSwingRange){
						judge = bbn;
					}
					break;
				default:
					LogError("IsBadBigBar()",MemoNI("Illegal type",type),MemoNI("barsAgo",barsAgo));
					break;
			}
			return judge;
		}

		//NT8
		// BarsFromNBO()  Port logic from INDBadsFromBO()[]
		//
		public int BardsFromRecentNBO(){
			double breakoutLimit = TNKNBOBorder;
			int period = TNKNBOPeriod;
			int result=0;
			int current = CurrentBar;
			int upperCrossAbove, upperCrossBelow;
			int lowerCrossAbove, lowerCrossBelow;
			double limit = breakoutLimit;
			//find recent cross events
			//Search Bar# of Recent CrossBelow and ClossAbove with Upper Limit
			//Search Bar# of Recent CrossBelow and ClossAbove with Lower Limit
			upperCrossAbove = recentCrossAbove(limit,period);
			upperCrossBelow = recentCrossBelow(limit,period);
			lowerCrossAbove = recentCrossAbove(-limit,period);
			lowerCrossBelow = recentCrossBelow(-limit,period);
			// Judge current status
			bool upBreakout = upperCrossAbove < upperCrossBelow; //CA is recent.
			bool downBreakout = lowerCrossAbove > lowerCrossBelow; //CB is recent.
			if(upBreakout && downBreakout){
				result = Math.Min(upperCrossAbove,lowerCrossBelow); //return most recent one
			}else if(upBreakout && !downBreakout){
				result = upperCrossAbove;  //upBreakout
			}else if(!upBreakout && downBreakout){
				result = lowerCrossBelow; //downBreakout
			}else{
				//No breakout took place.
				//result = 0; 
				result = - Math.Min(upperCrossBelow,lowerCrossAbove);
			}		
            return result;		
		}
		int recentCrossAbove(double limit, int period){
			int result = CurrentBar+1;
			double dummy = 1.0;
			if (CurrentBar < 1) return 0;
			for (int ind = 0; ind < CurrentBar; ind++){
				bool c = INDNormalizedClose(period, dummy)[ind]  > limit;
				bool p = INDNormalizedClose(period, dummy)[ind+1]<= limit;
				if( c && p ){
					result = ind;
					break;
				}
			}
			return result; //CurrentBar+1 not found.
		}
		int recentCrossBelow(double limit, int period){
			int result = CurrentBar+1;
			double dummy = 1.0;
			if (CurrentBar < 1) return 0;
			for (int ind = 0; ind < CurrentBar; ind++){
				bool c = INDNormalizedClose(period, dummy)[ind]  < limit;
				bool p = INDNormalizedClose(period, dummy)[ind+1]>= limit;
				if( c && p ){
					result = ind;
					break;
				}
			}
			return result; //CurrentBar+1 means not found.
		}
		//Check if the valu is inside of NBOBand
		public bool IsInsideOfNBOBand(){
			return IsInsideOfNBOBand(Close[0]);
		}
		public bool IsInsideOfNBOBand(double val){
			return (BreakPositionForNBOBand(val) == "INSIDE");
		}
		// Check Breakout position for Peak/Bottom Band
		// Rerun value is same PositionAgainstRange() in TNKUtils.cs
		public string BreakPositionForNBOBand(double boValue){
			double centerVal = SMA(TNKNBOPeriod)[0];
			double offset = StdDev(TNKNBOPeriod)[0] * TNKNBOBorder;
			double upperVal = centerVal + offset;
			double lowerVal = centerVal - offset;
			return PositionAgainstRange(boValue,upperVal,lowerVal);
		}

		// Check Breakout position for Peak/Bottom Band
		// Rerun value is same PositionAgainstRange() in TNKUtils.cs
		public string BreakPositionForPBBand(double boValue){
			return PositionAgainstRange(boValue, TNKRecentPeakValue, TNKRecentBottomValue);
		}
		
		// Check tred with Peak/Bottom change
		// Returns any of "UP","DOWN","RANGE","EXPAND","SHRINK"
		// margin: Margin for judgeing if bottom/peak was changed.
		//
		public string TrendCheckWithPeakBottom(double margin){
			//Initialize Judge Matrix
			// See "Trend Jdugement with Peak/Bottom" page in TanukiDesign.key
			Dictionary<string, string> upDict = new Dictionary<string, string>()
			{{"UP","UP"}, {"FLAT","UP"},{"DOWN","EXPAND"}};
			Dictionary<string, string> flatDict = new Dictionary<string, string>()
			{{"UP","RANGE"}, {"FLAT","RANGE"},{"DOWN","DOWN"}};
			Dictionary<string, string> downDict = new Dictionary<string, string>()
			{{"UP","SHRINK"}, {"FLAT","RANGE"},{"DOWN","DOWN"}};
			Dictionary<string, Dictionary<string, string>> judgeDict = new Dictionary<string, Dictionary<string, string>>()
			{{"UP",upDict},{"FLAT",flatDict},{"DOWN",downDict}};
				
			string result = "";
			//Check if P/B goes up "UP"/"DOWN"/"FLAT"/"ERROR"
			string peakStatus   = IsUpOrDown(TNKPreviousPeakValue,   TNKRecentPeakValue,   margin);
			string bottomStatus = IsUpOrDown(TNKPreviousBottomValue, TNKRecentBottomValue, margin);
			// Draw Judge Matrix
			result = judgeDict[peakStatus][bottomStatus];
			//
			return result;
		}
		// No parameter version
		public string TrendCheckWithPeakBottom(){
			return  TrendCheckWithPeakBottom(0.0);
		}

		// Peak and Bottom (swing) based ternd mesurement
		public TrendType TNKJudgeTrendSwing(){
			int checkCount = 3; //Groundless #
			double[] pv = new double[checkCount];
			double[] bv = new double[checkCount];
			for (int i=1; i <= checkCount; i++){
				pv[i]=PeakValue(i);
				bv[i]=BottomValue(i);
				if(pv[i]==0.0 || bv[i]==0.0){
					return TrendType.Undefined;
				}
			}
			//Count Up and down
			int puc=0,pdc=0,buc=0,bdc=0;
			double pval=0.0, bval=0.0;
			TrendType peakTrend, bottomTrend;
			//Check peakTend
			for (int i=checkCount;i>1;i++){
				if (pv[i] <= pv[i-1])
					puc += 1;
				else
					pdc +=1;
			}
			if (puc == checkCount-1)
				peakTrend = TrendType.Up;
			else if (pdc == checkCount-1)
				peakTrend = TrendType.Down;
			else 
				peakTrend = TrendType.Range;
			//Check Bottom Treand
			for (int i=checkCount;i>1;i++){
				if (bv[i] <= bv[i-1])
					buc += 1;
				else
					bdc +=1;
			}
			if (buc == checkCount-1)
				bottomTrend = TrendType.Up;
			else if (bdc == checkCount-1)
				bottomTrend = TrendType.Down;
			else 
				bottomTrend = TrendType.Range;
			//Judge Total tend;
			if (peakTrend == TrendType.Up 
				&& bottomTrend == TrendType.Up
				)return TrendType.Up;
			else if (peakTrend == TrendType.Down
				&& bottomTrend == TrendType.Down
				)return TrendType.Down;
			else
				return TrendType.Range;
		}

		public double PeakValue(int instance){
			double value=0.0;
			int lookBack = Swing(TNKSwingPeriod).SwingHighBar(0, instance, 200); //Groundless PARAMS
//			LogDebug("PeakValue()",
//				MemoNI("lookBack",lookBack),MemoNI("CurrentBar",CurrentBar),MemoNI("Bars.BarsSinceSession",Bars.BarsSinceSession)
//			);
			if(lookBack<=CurrentBar){
				value = High[Math.Max(0, lookBack)];
			}else{
				value = 0.0;
			}
			LogDebug("PeakValue()",
				MemoND("value",value),MemoNI("lookBack",lookBack),
				//NT7 MemoNI("CurrentBar",CurrentBar),MemoNI("Bars.BarsSinceSession",Bars.BarsSinceSession)
				MemoNI("CurrentBar",CurrentBar)
			);
			return value;
		}

		public double BottomValue(int instance){
			double value=0.0;
			int lookBack = Swing(TNKSwingPeriod).SwingLowBar(0, instance, 200); //Groundless PARAMS
			if(lookBack<=CurrentBar){
				value = Low[Math.Max(0, lookBack)];
			}else{
				value = 0.0;
			}
			LogDebug("BottomkValue()",
				MemoND("value",value),MemoNI("lookBack",lookBack),
				//NT7 MemoNI("CurrentBar",CurrentBar),MemoNI("Bars.BarsSinceSession",Bars.BarsSinceSession)
				MemoNI("CurrentBar",CurrentBar)
			);
			return value;
		}
		
		public double _PeakValue(int instance){
			double pv=0.0;
			int num = Swing(TNKSwingPeriod).SwingHighBar(0, instance, 200); //Groundless PARAMS
			if (num > CurrentBar) {
				pv = 0.0;
			}else{
//				pv =  num == -1 ? 0.0:Close[num];
				pv =  num == -1 ? 0.0:High[num];
			}
			//NT7 TNKDebugLog(MemoNS("Loc","PeakValue()")+MemoND(" return",pv)+MemoNI(" num",num)+MemoNI(" CurrentBar",CurrentBar)+MemoNI(" Bars.BarsSinceSession",Bars.BarsSinceSession));
			LogDebug(MemoNS("Loc","PeakValue()")+MemoND(" return",pv)+MemoNI(" num",num)+MemoNI(" CurrentBar",CurrentBar));
			return pv;
		}
		
		public double _BottomValue(int instance){
			double bv=0.0;
			int num = Swing(TNKSwingPeriod).SwingLowBar(0, instance, 200); //Groundless PARAMS
			if (num > CurrentBar){
				bv =  0.0;
			}else{
//				bv = num == -1 ? 0.0:Close[num];
				bv = num == -1 ? 0.0:Low[num];
			}
			TNKDebugLog(MemoNS("Loc","BottomValue()")+" "+MemoND("return",bv) +" "+MemoNI("num",num)+" "+MemoNI("CurrentBar",CurrentBar));
			return bv;
		}
    }
}
