//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//
		//  Reject Reason logging
		// 
		string AddTerminalteReason(string reason){
			TNKTerminateReason = AddReason(reason,TNKTerminateReason);
			return TNKTerminateReason;
		}
		//
		//  Attempt Position Filters
		//

		public bool AttemptPositionFilters(TradeSpec tradeSpec){
			//Filter name list
			List<string> positionFilters = new List<string>(){
				//Mandatory filters

				//Optional filters
				"PFIL_ExitOnNightSessionEnd", //Testing 2019/07/20
				//"PFIL_ExitOnFridayNightSessionEnd", //Testing 2019/07/27
				//"PFIL_ExitOnDaySessionEnd", //Testing 2019/07/20
				//"PFIL_ExitOnSessionEnd",	//Testting 2019/06/20
				//"PFIL_NoProfitAfterWhile", //Testing 2019/01/06
				//"PFIL_PassedProfitPeak", //Testing 2018/07/26
				//"PFIL_TonboInProfit"
				//Pendig
				//"PFIL_NB_ExitIfNoProfitInNextOfBNB", //BNB Big Negative Bar
				//"PFIL_Recent2BarGainsOverOborder", //Exit if the profit over the border
				//"PFIL_FixProfitOversBorder", //Exit if the profit over the border
				//"SFIL_ReverseDuringUpdateDetectionLag",
				//"SFIL_MomentumChekForNOEVENTEntry"
			};
			//
			if(!havePosition()) return false; 
			bool terminated = false;	
			TNKTerminateReason = "";

			//Call observer with name list.
			//List<object> args = new List<object>(){(object)tradeSpec};
			object[] args = new object[]{tradeSpec};
			terminated = (bool)InvokeWithNameListWhileReturnIsFalse(positionFilters, args);
			
			if(terminated) {
				//Mark Reject on Chart with black diamond
				//MarkDiamondOnChartWithTagAndColor(Close[0], TNKRejectReason, Color.Black);
				//MarkEventNameOnChart(tradeSpec.eventName, TNKTerminateReason, Color.Black);
				ExitPosition(TNKTerminateReason);
				MarkDotOnChartWithTagAndColor(Close[0],TNKTerminateReason, Brushes.Black);
				LogDebug("AttemptPositionFilters()",MemoNS("TNKTerminateReason",TNKTerminateReason));
			}				
			return terminated;
		}
		//===========
		// Optional Position Filters
		//===========
		
		//===========
		// Posistion Filter Template
		//   2014/10/04
		//===========
		public bool PFIL_Template(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			//
			bool someCondition = false;
			//
			if (ts.eventName=="EN_SOMEEVNET" && someCondition){
				terminate = true;
				string reason = "SomeReason";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_Template()",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}
		
		//=====
		//  Check if is in time range
		//====
		public bool IsInTimeRange(DateTime target, DateTime now, int marginInsec){
			TimeSpan rest = target - now;
			int restInSec = (int)rest.TotalSeconds;
			if(restInSec >=0 && restInSec <= marginInsec){
				return true;
			}else{
				return false;
			}
		}
		public bool IsInTimeRange(TimeSpan target, TimeSpan now, int marginInsec){
			TimeSpan rest = target - now;
			int restInSec = (int)rest.TotalSeconds;
			if(restInSec >=0 && restInSec <= marginInsec){
				return true;
			}else{
				return false;
			}
		}

		//===========
		// Exit in Nigh Session end
		//   Ver 1.0 2019/07/20
		//===========
		public bool PFIL_ExitOnFridayNightSessionEnd(TradeSpec ts){
			bool isTodaySataurday = (Time[0].DayOfWeek==DayOfWeek.Saturday);
			if (isTodaySataurday){
				return PFIL_ExitOnNightSessionEnd(ts);
			}else{
				return false;
			}
		}

		//===========
		// Exit in Nigh Session end
		//   Ver 1.0 2019/07/20
		//===========
		public bool PFIL_ExitOnNightSessionEnd(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			bool inTime = false;
			//Paremas
			TimeSpan sessionEnd = new TimeSpan(05,30,0); //15:15.0
			int marginInSec = 1200; //sec
			//
			// Check if is in session end.
			//
			TimeSpan now = Time[0].TimeOfDay;
			inTime = IsInTimeRange(sessionEnd, now, marginInSec);
			LogDebug("PFIL_ExitOnNightSessionEnd",MemoNS("now",now.ToString()),MemoNS("sessionEnd",sessionEnd.ToString()),MemoNB("inTime",inTime));
			// Check exit condition
			//
			if (inTime){
				terminate = true;
				string reason = "Time to exit for NighttSessionEnd";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_ExitOnNightSessionEnd",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}

		//===========
		// Exit in Day Session end
		//   Ver 1.0 2019/07/18
		//===========
		public bool PFIL_ExitOnDaySessionEnd(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			bool inTime = false;
			//Paremas
			TimeSpan sessionEnd = new TimeSpan(15,15,0); //15:15.0
			int marginInSec = 600; //sec
			//
			// Check if is in session end.
			//
			TimeSpan now = Time[0].TimeOfDay;
			inTime = IsInTimeRange(sessionEnd, now, marginInSec);
			LogDebug("PFIL_ExitOnDaySessionEnd",MemoNS("now",now.ToString()),MemoNS("sessionEnd",sessionEnd.ToString()),MemoNB("inTime",inTime));
			// Check exit condition
			//
			if (inTime){
				terminate = true;
				string reason = "Time to exit for DaySessionEnd";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_ExitOnDaySessionEnd",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}

		//===========
		// Exit in Session end
		//  Due to Exit on Session end does not work at 3.9.20 in Real
		//   Ver 1.0 2019/06/20
		//===========
		public bool PFIL_ExitOnSessionEnd(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			bool inTime = false;
			//Paremas
			int marginInSec = 600; //sec
			//
			// Check if is in session end.
			//
			DateTime now = Time[0];
			List<DateTime> times = SessionStartTimeAndEndTime();
			LogDebug("PFIL_ExitOnSessionEnd",MemoNS("SessionStartTime",times[0].ToString()),MemoNS("SessionEndTime",times[1].ToString()));
			inTime = IsInTimeRange(times[1], now, marginInSec);
			// Check exit condition
			//
			if (inTime){
				terminate = true;
				string reason = "Time to exit";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_ExitOnSessionEnd",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}

		
		//===========
		// Exit position if norofit after while.
		//   2019/01/06    v1.0
		//===========
		public bool PFIL_NoProfitAfterWhile(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			//PARAMS
			int judgementBorder = 3; //Bar count  (better than 5)
			///
			int elepsedBar = BarsSinceEntryExecution();
			double profit = ts.MaximumProfit;
			bool noProftAfterWhile  = (elepsedBar >= judgementBorder) && (profit <= 0.0);
			//
			//if (ts.eventName=="EN_SOMEEVNET" && noProftAfterWhile){
			if (noProftAfterWhile){
				terminate = true;
				string reason = "NoProfitAfterWhile";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_Template()",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}

		
		//==========
		//  Exit portion if passed profit peak.
		//   2018/07/26  v1.0
		//   2018/12/27  v2.0
		//==========
		public bool PFIL_PassedProfitPeak(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			//PARAMS
			int    JudgeCapByTic = 5; //tic  
			double judgeCap = JudgeCapByTic * TickSize; //Attempt this filer when MaxProfit exeeds this border.
			//int    downLimitByTic = 2; //tic
			double downLimitRate = 0.2; //Parameter 0.0~1.0 Exit if Maximum proft down downLimite
			//Add for v2
			double initialExitBorder = 0.8;// 0.5; //50/%
			double maxExitBorder = 0.8;    //0.95; //95%
			double stepPerBar = 0.0;       //0.03; //3%
			int elapsed = CurrentBar - ts.executedBarNo; //Bar count since entry. BarsSinceEntryExecution()
			double exitBorder = TakeSmaller(maxExitBorder, initialExitBorder + stepPerBar * elapsed);
			//LogDebug("PFIL_PassedProfitPeak()",MemoNI("elapsed",elapsed),MemoND("exitBorder",exitBorder));
			//
			//Check conditions
			bool inBOBand = (Close[0] < TNKBOUpLevel) && (Close[0] > TNKBOLowLevel);
			double MaxProfit = ts.MaximumProfit;
			int    MaxProfitBar = ts.MaximumProfitBar;
			double currentProfit = ts.Profit();
			//
			bool exitAfterPeak = false;
			if((MaxProfit > 0.0) && (MaxProfit >= judgeCap) && inBOBand) {
			//if((MaxProfit > 0.0) && (MaxProfit >= judgeCap)) {
				//v1.0 
				//Cap dounw limit by downLimitByTic
				//double dounlimit = TakeBigger(MaxProfit * downLimitRate, downLimitByTic * TickSize);
				//exitAfterPeak = currentProfit < MaxProfit - dounlimit;
				//v2.0
				exitAfterPeak = (currentProfit/MaxProfit) < exitBorder;
				LogDebug("PFIL_PassedProfitPeak()",MemoNB("exitAfterPeak",exitAfterPeak),MemoND("exitBorder",exitBorder),MemoND("currentProfit/MaxProfit",currentProfit/MaxProfit));
			}else{
				//insufficient profit or loss
				// Do nothing
				exitAfterPeak = false;
			}
			//
			if (exitAfterPeak){
				terminate = true;
				string reason = "ExiOnPassedProfitPeak";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_PassedProfitPeak()",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}

		//
		// Exit Position if current bar is TomboWithMargin
		//   2014/10/10
		//
		public bool PFIL_TonboInProfit(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			//
			bool isTonboWithMargin = IsTonboWithMargin(0);
			bool isTonbo = IsTonbo(0); //Better than IsTonboWithMargin(0)
			bool hasProfit = (GetProfit() > 0);
			double move = Move(0);
			bool doesMoveReverse = (move!=0 && !DoesMoveAlong(move));
			//
			if (hasProfit){
				if(isTonbo){
					terminate = true;
					string reason = "TonboInProfit";
					AddTerminalteReason(reason);
					LogPositionFilter("PFIL_TonboInProfit()",TAGMsg(reason),MemoND("Move",Move(0)),MemoND("TickSize",TickSize));
				}else if(isTonboWithMargin && doesMoveReverse){
					terminate = true;
					string reason = "TonboInProfitMargin";
					AddTerminalteReason(reason);
					LogPositionFilter("PFIL_TonboInProfit()",TAGMsg(reason),MemoND("Move",Move(0)),MemoND("TickSize",TickSize));					
				}else{
					//Do nothing
					terminate = false;
				}
			}else{
				terminate = false;
			}
			return terminate;
		}
		
		//
		// Exit Position if current bar is Tombo
		//   2014/10/04
		//
		public bool PFIL_TonboInProfitOLD(TradeSpec ts){
			if (ts==null) return true;
			bool terminate = false;
			//
			//bool isTonbo = IsTonboWithMargin(0);
			bool isTonbo = IsTonbo(0); //Better than IsTonboWithMargin(0)
			bool hasProfit = GetProfit() > 0;
			//
			if (hasProfit && isTonbo){
				terminate = true;
				string reason = "TonboInProfit";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_TonboInProfit()",TAGMsg(reason),MemoND("Move",Move(0)),MemoND("TickSize",TickSize));				
			}else{
				terminate = false;
			}
			return terminate;
		}
		
		//
		// Exit when Profit/Contract overs the border(2sigma)
		//  2015/10/02
		//
		public bool PFIL_FixProfitOversBorder(TradeSpec ts){
			bool terminate = false;
			if (ts==null) return true;
			//
			double ProfitDeltaParContract = GetProiftDelta();
			double sigma = SigmaThisBar(14);
			double borderFactor = 2.0; //v2.2.14
			double border = sigma * borderFactor;
			bool doesProfitDeltaParContractOverBorder = (ProfitDeltaParContract >= border);
			//
			if (doesProfitDeltaParContractOverBorder){
				terminate = true;
				string reason = "ProfitParContractOversOneSgima";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_FixProfitOversBorder()",TAGMsg(reason),MemoND("ProfitDeltaParContract",ProfitDeltaParContract),MemoND("border",border));				
			}else{
				terminate = false;
			}
			return terminate;
		}
		
		//
		// Terminate Position if loss in next var of Big Negative Bar
		//
		public bool PFIL_NB_ExitIfNoProfitInNextOfBNB(TradeSpec ts){
			bool terminate = false;
			if (ts==null) return true;
			double profit = GetProfit();
			if (ts.eventName=="EN_BB_BIGNEGATIVEBAR" && profit < 0.0){
				terminate = true;
				string reason = "NoProfitInNextOfBNB";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_NB_ExitIfNoProfitInNextOfBNB()",TAGMsg(reason));				
			}else{
				terminate = false;
			}
			return terminate;
		}
		
		//
		// Exit Position Filter if thru recent 2 bar gains profit over border.
		//   2014/10/04
		//
		public bool PFIL_Recent2BarGainsOverOborder(TradeSpec ts){
			if (ts==null || CurrentBar <1) return true;
			bool terminate = false;
			//
			int   sinceEntry = BarsSinceEntryExecution();
			double profitDelta = GetProiftDelta();
			bool hasProfit = (profitDelta > 0);
			double move = Close[0] - Open[1]; //Move thru 2 bars
			bool doesMoveAlong = DoesMoveAlong(move);
			double sigma = SigmaThisBar(14);
			double borderFactor = 1.4; //v2.?
			double border = sigma * borderFactor;
			bool doesOverBorder = Math.Abs(move)>border;
			//
			if (hasProfit && doesMoveAlong && doesOverBorder && sinceEntry >0){
				terminate = true;
				string reason = "Recent2barMoveOverBorderInProfit";
				AddTerminalteReason(reason);
				LogPositionFilter("PFIL_Recent2BarGainsOverOborder()",TAGMsg(reason),MemoND("move",move),MemoND("border",border));				
			}else{
				terminate = false;
			}
			return terminate;
		}
//=========================
// Old Exit checking code. are going to be obsolete.
		//
		// Chcek if last 3 bars go along or reverse
		//
		public bool EXIT_ReverseLastNBars(){
			bool willExit = false;
			string reason = "";
			int lookBack = 3; //PARA N bars. Check past 3 bar include this bar.
			int barsSinceEntry = BarsSinceEntryExecution();
			if (barsSinceEntry < lookBack){
				LogDebug("EXIT_ReverseLastNBars()",
					TAGMsg("NotEXIT barsSinceEntry < lookback"),
					MemoNI("barsSinceEntry",barsSinceEntry),
					MemoNI("lookBack",lookBack)
				);
				return false;
			}
			int[] barCounts = CountUpBarsParDirection(lookBack);
			int positiveBarCount = barCounts[0];
			int negativeBarCount = barCounts[1];
			int neutralBarCount = barCounts[2];
			LogDebug("EXIT_ReverseLastNBars()",
				MemoNI("positiveBarCount",positiveBarCount),
				MemoNI("negativeBarCount",negativeBarCount),
				MemoNI("neutralBarCount",neutralBarCount)
			);
			if (haveLongPosition()) {
				if (negativeBarCount + neutralBarCount>=lookBack && negativeBarCount>=lookBack-1){
					//exit position
					reason = "ReverseLast3Brs";
					AddExitReason(reason);
					LogPositionFilter("EXIT_ReverseLastNBars()",
						TAGMsg("Exit Long position cause 3 rev bars."),
						MemoNI("negativeBarCount",negativeBarCount),
						MemoNI("neutralBarCount",neutralBarCount)
					);
					//ExitPosition("3 Reverse bars for LONG position");
					willExit = true;
				}
				
			}else if (haveShortPosition()){
				if (positiveBarCount + neutralBarCount>=lookBack && positiveBarCount>=lookBack-1){
					//exit position
					reason= "ReverseLast3Bars";
					AddExitReason(reason);
					LogPositionFilter("EXIT_ReverseLastNBars()",
						TAGMsg("Exit Short position cause 3 rev bars."),
						MemoNI("positiveBarCount",positiveBarCount),
						MemoNI("neutralBarCount",neutralBarCount)
					);
					//ExitPosition("3 Reverse bars for SHORT position");
					willExit = true;
				}
			} else {
				// Wrong condition
				LogError("EXIT_ReverseLastNBars()",TAGMsg("Impassible condition."));
			}
			return willExit;			
		}
		
		public bool EXIT_ReverseOver2TimesVolatility(){
			//Volatility Exit : exit when reverse 2 times of typical volatility in single day
			bool willExit = false;
			string reason = "";
			double move = moveToday();
			double volaLimit = ATR(10)[0]*2.0;
			switch(Position.MarketPosition){
				case MarketPosition.Long:
					if (move<0 && Math.Abs(move)>volaLimit){
						reason = "Reverse2TimesVolatiliry";
						AddExitReason(reason);
						LogPositionFilter("EXIT_ReverseOver2TimesVolatility()","Volatility Exit, Long");
						willExit = true;
					}
				break;
				case MarketPosition.Short:
					if (move>0 && Math.Abs(move)>volaLimit){
						reason = "Reverse2TimesVolatiliry";
						AddExitReason(reason);
						LogPositionFilter("EXIT_ReverseOver2TimesVolatility()","Volatility Exit Short");
						willExit = true;
					}
				break;
			}
			return willExit;
		}
		
		string AddExitReason(string reason){
			TNKTerminateReason = AddReason(reason,TNKTerminateReason);
			return TNKTerminateReason;
		}


    }
}
//===================================
// Going to be Obsoleted
//		//
//		//  Check if position meet exit conditions.
//		//
//		public void CheckForceExitConditions(){
//			// Return if any does not exist.
//			if(!havePosition()) return; 
//			
//			TNKTerminateReason = "";			
//			TradeSpec tradeSpec = TNKWorkingTradeSpec;
//			bool willExit = false;
//			//
//			// Check if Loss exceed TNKUnitRisk
//			//
//			//willExit = willExit || EXIT_LossExeedUnitRisk(); //going to be Obseleted
//			//
//			// Exit if reverse n bars 
//			//
//			//willExit = willExit || EXIT_ReverseLastNBars(); //Stop loss manage could be better than this condition.
//
//			//Execute Exit
//			if (willExit) {
//				LogDebug("CheckForceExitConditions()",MemoNS("TNKTerminateReason",TNKTerminateReason));
//				ExitPosition(TNKTerminateReason);
//				MarkDotOnChartWithTagAndColor(Close[0],TNKTerminateReason, Color.Black);
//			}
//			return;	
//		}
//		
//		//
//		// Check if Loss exceed TNKUnitRisk
//		//
//		public bool EXIT_LossExeedUnitRisk(){
//			bool willExit = false;
//			double pl = GetProfitAndLoss(); //Position.GetProfitLoss(Close[0], PerformanceUnit.Currency);
//			//Exit if Loss exeed Trade Unit Risk
//			if(pl < -TNKUnitRisk) {
//				string reason = "LossExeedUnitRisk";
//				AddExitReason(reason);
//				LogPositionFilter("EXIT_LossExeedUnitRisk()",
//					TAGMsg("Exit because loss exceed Unit Risk"),
//					MemoND("pl",pl),MemoND("TNKUnitRisk",TNKUnitRisk)
//				);
//				willExit = true;
//			}
//			return willExit;
//		}