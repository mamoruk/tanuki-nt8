//#region Using declarations
//using System;
//using System.IO;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy //NT7
	//partial	class TANUKI : Strategy  //Not applicable.  Any merit is not found so far.
    {
		//
		//  Limit LogDebug writing to sepecified facility in this String Array.
		//
		string[] TNKlogDEBUGOnly = null;
		//string[] TNKlogDEBUGOnly = new string[]{"ExecuteSignalList()"};
		
		//
		// Log Store
		//   ver 1.0  2018/12/30
		//
		public class LogStore {
			//IVar
			private Strategy o;
			public string Log {get; set;}
			public string Tag {get; set;}
			public string Facility {get; set;}
			//Constructor
			public LogStore(Strategy owner, string tag, string fac){
				o = owner;
				Tag = tag;
				Facility = fac;
				Log = "";
			}
			public string MemoString {
				get {return string.Format("{0} {1}",o.MemoNS("Facility",Facility),Log);}
			}
			public string LogString {
				get {return o.TNKLogTST(Tag, this.MemoString);}
			}
			public void Write(){
				o.LogTagAndStrings(Tag, this.MemoString);
			}
			public void AddLog(string memo){
				if(Log == "") {
					Log = memo;
				}else{
					Log = string.Format("{0} {1}",Log, memo);
				}
			}
		}
		
		// Logging Functions
		//
		public void WriteToFile(string text,string path){
      		//string path = @"c:\temp\MyTest.txt";
			string appendText = text + Environment.NewLine;
			//Print("WriteToFile() path:"+path); //DEBUGPRINT
			File.AppendAllText(path, appendText);
		}
		public string NewLogFileName(){
			if(TNKLogFolderName==null || TNKLogFolderName==""){
				TNKLogFolderName = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
			}
			string iname = Instrument == null ? "NONAME":Instrument.FullName;
			string dateTime = DateTime.Now.ToString("s");
			string fname = TNKLogFolderName + @"\"+RepCollonUb(RepSpUb(iname+"_"+dateTime+".log"));
			//Print("NewLogFileName()"+MemoNS("iname",iname)+MemoNS("dateTime",dateTime)+MemoNS("text",text)); //DEBUGPRINT
			return fname;
		}
		public void SetLogFileName(string fileName){
			TNKLogFileName = fileName;
			Print(string.Format("SetLogFileName() TNKLogFileName:{0}",TNKLogFileName)); //DEBUGPRINT
			//NT7 LogDebug("SetLogFileName()",MemoNS("TNKLogFileName",TNKLogFileName));
		}
		public void LogToFile(string text){
			//Print("LogToFile() TNKLogFileName"+TNKLogFileName); //DEBUGPRINT
			WriteToFile(text,TNKLogFileName);
		}

		public string TNKLog(string memo){
			//Print(memo); //DEBUGPRINT
			if (TNKDoesLogFile){
				if(TNKLogFileName==null){
					Print("ERROR:TNKLogFileName is null."); //DEBUGPRINT
				}else{
					LogToFile(memo);
				}
			}
			return memo;
		}
		// Interface for Description
		public interface IFDescription{
			string Description(); 
		}
		//
		public string TNKLogTS(string memo){return TNKLog(MemoNSPS(TimeStamp(Time[0]),memo));}
		public string _TNKLogTST(string tag,string memo){return TNKLog(MemoNSPS(TimeStamp(Time[0]),MemoNSPS(tag,memo)));}
		public string TNKLogTST(string tag,string memo){return TNKLog(MemoNSPS(TimeStamp(Time[0])+String.Format("_{0:d}",CurrentBar),MemoNSPS(tag,memo)));}
		public string MemoNSPS(string name,string sval){return string.Format("{0:s} {1:s}",name,sval);}
		public string MemoND(string name,double dval){return string.Format("{0:s}:{1:f2}",name,dval);}
		public string MemoNI(string name,int ival){return string.Format("{0:s}:{1:d}",name,ival);}
		public string MemoNS(string name,string sval){return string.Format("{0:s}:{1:s}",name,sval);}
		public string MemoNT(string name, DateTime time){return MemoNS(name,time.ToString());}
		public string MemoSgnD(double dval){return string.Format("{0:s}{1:f2}",dval>=0.0?"+":"",dval);} //+3.0
		public string MemoPD(double price, double delta){return string.Format("{0:f2} {1:s}",price,MemoSgnD(delta));}
		public string MemoPPD(double price1,double price2, double delta){return string.Format("{0:f2} {1:f2} {2:s}",price1,price2,MemoSgnD(delta));}
		public string MemoPPR(double price1,double price2){return string.Format("{0:f2} {1:f2} {2:f2}%",price1,price2,price1/price2*100.0);}
		public string MemoNR(string name,double dval){return string.Format("{0:s}:{1:f2}%",name,dval*100.0);} //15.2%
		public string MemoNB(string name, bool flag){return MemoNS(name,flag==true?"true":"false");} //true or folse
		public string MemoNO(string name, IFDescription obj){return MemoNS(name, '{'+obj.Description()+'}');}
		public string TAGMsg(string message){return MemoNS("Msg",message);}
		public string TimeStamp(DateTime time){return time.ToString("yy/MM/dd_HH:mm");}
		public string TAGNow(){return MemoNS("CurrentTime",TimeStamp(DateTime.Now));}
		public string RepSpUb(string text){return text.Replace(" ","_");}
		public string RepCollonUb(string text){return text.Replace(":","_");}
		public void LogTagAndStrings(string tag, params string[] str){TNKLogTST(tag,concatStr(str));}
		public void LogMessage(string facility, params string[] str)  {LogTagAndStrings("Message", MemoNS("Facility",facility),concatStr(str));}
		public void LogError(string facility, params string[] str)    {LogTagAndStrings("Error", MemoNS("Facility",facility),concatStr(str));}
		public void LogFilter(string facility, params string[] str)   {LogTagAndStrings("SigFilter",MemoNS("Facility",facility),concatStr(str));}
		public void LogPositionFilter(string facility, params string[] str)     {LogTagAndStrings("PosFilter",  MemoNS("Facility",facility),concatStr(str));}
		public void LogAlart(string facility, params string[] str)    {LogTagAndStrings("Alart", MemoNS("Facility",facility),concatStr(str));}
		public void LogDebugSimple(string facility, params string[] str){LogTagAndStrings("DEBUG", MemoNS("Facility",facility),concatStr(str));}
		public void LogDebugChekArray(string facility, params string[] str){
			if(TNKlogDEBUGOnly == null || Array.IndexOf(TNKlogDEBUGOnly,facility)>=0){LogDebugSimple(facility,str);}
		}
		public void LogDebug(string facility, params string[] str){if(TNKDebug)LogDebugChekArray(facility,str);}
		public void TNKDebugLog(string facility, params string[] str){LogDebug(facility,str);}
		public string concatStrSep(string sep, params string[] str){
			if (str.Length==0) return "";
			string text = str[0];
			for (int i=1; i<str.Length; i++){
				text=text+sep+str[i];
			}
			return text;
		}
		public string concatStr(params string[] str) {return concatStrSep(" ",str);}
		// Array to String
		public string MemoNAR<T>(string name, T[] array){return string.Format("{0:s}:{1:s}",name,ArrayToStr(array));}
		public string ArrayToStr<T>(T[] array){
			string str = "[";
			foreach(T v in array){
				str += v.ToString()+" ";
			}
			str = str.Trim().Replace(" ",",");
			str+="]";
			return str;
		}
		//
		public void LogHeader(){
			string fromDate = "NA", toDate = "NA";
			//if(TNKDebug) TNKLogTST("TNKLogHeader()",MemoNS("Bars.BarsSinceSession",Bars.FirstBarOfSession?"T":"F"));
			if (Bars != null && Instrument !=null && !TNKDidLogHeader) {
				string text = concatStr(
					TAGNow(),
					MemoNS("Item",RepSpUb(Instrument.FullName)),
					MemoNS("From",fromDate),
					MemoNS("To",toDate)
				);
				TNKLogTST("Header",text);
				TNKDidLogHeader = true;
			}
		}
		// Log Version Number
		public void LogVersion(){
			LogTagAndStrings("Version",
				MemoNS("Number",TNKVersionNumber),MemoNS("Description",TNKVersionDescription)
			);
		}
		// Log Account Info
		// AccountItem.BuyingPower
		// AccountItem.CashValue
		// AccountItem.RealizedProfitLoss
		public void LogAccountInfo(){
			string text = concatStr(
				//NT7
				//MemoND("BuyingPower",GetAccountValue(AccountItem.BuyingPower)),
				//MemoND("CashValue",GetAccountValue(AccountItem.CashValue)),
				//MemoND("RealizedProfitLoss",GetAccountValue(AccountItem.RealizedProfitLoss))
				//NT7
				MemoNS("Account.Name",Account.Name),
				MemoND("BuyingPower",Account.Get(AccountItem.BuyingPower,Currency.JapaneseYen)),
				MemoND("CashValue",Account.Get(AccountItem.CashValue,Currency.JapaneseYen)),
				MemoND("RealizedProfitLoss",Account.Get(AccountItem.RealizedProfitLoss,Currency.JapaneseYen))
			);
			LogTagAndStrings("AccountInfo",text);
		}
		public double ToPercent(double price){return price/Close[0];}
		public double ToPrice(double percent){return Close[0]*percent;}
		//
		// 
		// Reason string for filter and exit procedures.
		string AddReason(string reason, string accumReason){
			accumReason = concatStrSep(" ",accumReason,reason);
			LogDebug("AddReason()",MemoNS("reason",reason),MemoNS("accumReason",accumReason));
			return accumReason;
		}			
    }
}
