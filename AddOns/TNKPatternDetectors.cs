//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		public class HysteresisComparator{
			private double baseLevel;
			private double upperOffset;
			private double lowerOffset;
			private int validTerm; //Bar count to expiration
			private Strategy owner;
			//
			private string lastPosition; //"HIGH","LOW","NOPOSISION"
			private int lastBar;
			private double threshold;
			public HysteresisComparator(Strategy owner, double bl, double uoffset, double loffset, int term){
				baseLevel = bl;
				upperOffset = uoffset;
				lowerOffset = loffset;
				validTerm = term;
				lastPosition = "NOPOSITION";
				lastBar = -1; // set neagtive in reset.
				owner.LogDebug("HysteresisLevel()",owner.MemoND("baseLevel",baseLevel));
			}
			public string Compare(double lv){
				string result;
				int currBar = owner.CurrentBar;
				bool isValid = (validTerm ==0) || (lastBar>=0) && (currBar - lastBar <= validTerm);
				if(!isValid) {
					Reset();
				}
				switch(lastPosition){
					case "HIGH":
						threshold = baseLevel - lowerOffset;
						break;
					case "LOW":
						threshold = baseLevel + upperOffset;
						break;
					case "NOPOSITION":
						threshold = baseLevel;
						break;
					default:
						owner.LogError("HysteresisLevel.Compare",owner.MemoNS("Illegal lastPosition",lastPosition));
					break;
				}
				if (lv >= threshold){
					result = "HIGH";
				} else {
					result = "LOW";
				}
				return result;
			}
			public void Reset(){
				lastPosition = "NOPOSITION";
				lastBar = -1;
			}
		}
		
		//Utility Methods
		// Patterns

		//
		//	Condition Patterns
		//
		// Event Type
		public bool IsBreakEvent(string eventName){
			string category = EventCategory(eventName);
			return (category == "BREAK");
		}

		public bool IsUpdateEvent(string eventName){
			string category = EventCategory(eventName);
			return (category == "UPDATE");
		}
		
		public string EventCategory(string eventName){
			string category = ConsultDictionary(eventName,TNKEventCategory);
			if (category !=null){
				return category;
			}else{
				//LogError("EventCategory()",MemoNS("Unknown eventName",eventName));
				return null;
			}
		}
		
		public bool DoesMatchSide(string side, string targetSide){
			if(TNKSideNameList.Contains(side)){
				if (side==targetSide){
					return true;
				} else {
					return false;
				}
			}else{
				LogError("DoesMatchSide()",MemoNS("Illegal side name",side));
				return false;
			}
		}
		public bool IsUpSide(string side){
			return DoesMatchSide(side,"UPSIDE");
		}
		public bool IsDownSide(string side){
			return DoesMatchSide(side,"DOWNSIDE");
		}
		public string EventSide(string eventName){
			string result = "";
			string side = ConsultDictionary(eventName,TNKEventUpDownSide);
			if(side!=null){
				result = side;
			}else{
				result = "NOSIDE";
			}
			return result;
		}
		public string StateSide(string stateName){
			string result = "";
			string side = ConsultDictionary(stateName,TNKStateUpDownSide);;
			if(side!=null){
				result = side;
			}else{
				result = "NOSIDE";
			}
			return result;
		}

		public bool IsUpSideEvent(string eventName){
			string side = EventSide(eventName);
			return IsUpSide(side);
		}
		//ISSUE: Recursive Call 
		public bool IsDownSideEvent(string eventName){
			string side = EventSide(eventName);
			return IsDownSide(side);
		}
		public bool IsUpSideState(string stateName){
			string side = StateSide(stateName);
			return IsUpSide(side);
		}
		public bool IsDownSideState(string stateName){
			string side = StateSide(stateName);
			return IsUpSide(side);		
		}
		//
		public bool IsUpSideCondition(string conditionName){
				string[] names = SplitConditionName(conditionName);
				string state = names[0], evt = names[1];
				return IsUpSideState(state) || IsUpSideEvent(evt);
		}
		public bool IsDownSideCondition(string conditionName){
				string[] names = SplitConditionName(conditionName);
				string state = names[0], evt = names[1];
				return IsDownSideState(state) || IsDownSideEvent(evt);
		}		
		//Check if the string arry contains target string
		// Issue:Fixed index<0 could be BUG. revise to (index > -1)
		public bool DoesMatchAnyString(string target, params string[] array){
			int index = Array.IndexOf(array,target);
			return (index > -1);
		}
		//Check if current condtion matuch to any condition in the array
		public bool DoesConditionMach(string conditionName, params string[] conditions){
			return DoesMatchAnyString(conditionName,conditions);
		}

		//Check if condition name applicable with condition,state,event string array
		public bool IsApplicableConditionName(string conditionName, string[] names){
			if(DistinctNameCategory(conditionName)!=1){
				LogError("IsApplicableConditionName",
						TAGMsg("Illegal conditionName"),
						MemoNS("conditionName",conditionName)
				);
				return false;
			}
			//return true if names is not specify(null or empty). this means global match.
			if(names==null || names.Length==0){
				return true;
			}
			bool match = false;
			foreach(string name in names){
				int nameType = DistinctNameCategory(name);
//				LogDebug("IsApplicableConditionName()",MemoNS("name",name),MemoNI("nameType",nameType),
//					MemoNS("StateNameOf(conditionName)",StateNameOf(conditionName)),
//					MemoNS("EventNameOf(conditionName)",EventNameOf(conditionName))
//				);
				switch (nameType){
					case 0: //none
						match = false;
						break;
					case 1: //codtion name
						match = conditionName == name;
						break;
					case 2: //State name
						match = StateNameOf(conditionName) == name.Replace(":","");
						break;
					case 3: //Event name
						match = EventNameOf(conditionName) == name.Replace(":","");
						break;
					default:
						LogError("IsApplicableConditionName",
							TAGMsg("Illegal Name Category"),
							MemoNS("conditionName",conditionName),MemoNS("name",name)
						);
						match = false;
						break;
				}
				if(match) break;
			}
			return match;
		}
		// 0:None 1:ConditionName 2:StateName 3:EventName
		public int DistinctNameCategory(string name){
			int sepPosition = name.IndexOf(":");
			if(sepPosition < 0){
				return 0; //None no separator found
			}else if(sepPosition==0){
				return 3; //Event name  :PEAKUP
			}else if(sepPosition==name.Length-1){
				return 2; //State name  RANGE:
			}else{
				return 1; //Condtion name RANGE:PEAKUP
			};
		}
		
		//
		//  Price Patterns
		//
		//Check market is in Trend.
		//Return trend presence as one of "TREND", "BOX", "UNCLEAR"
		public string TrendPresence(){
			//return TrendPresenceWithTrendDirection();
			//return MarketConditionWithADXAccel();
			//return MarketConditionWithADX();  //Good win rate.
			return MarketConditionWithADXAndADXSlope(); //almost eaual only ADX version.
			//return MarketConditionAlwayTREND(); //TEST
			//return MarketConditionAlwayBOX();   //TEST quite good.
			//return MarketConditionWithADXAndADXAccel(); //53/1.0
		}

		// Trend Presence with TrendDirection()
		// TODO:TrendDirection should judge "BOX" condition
		string TrendPresenceWithTrendDirection(){
		   string result = "UNCLEAR";
		   string trend = TrendDirection();
		   if(trend=="UP" || trend =="DOWN") {
		     result = "TREND";
		   }else if(trend=="BOX"){
		     result = "BOX";
		   }else{
		     result = "UNCLEAR";
		   }
		   return result;
		}

		// Check 
		// Return Trend Direction(TD) as one of "UP", "DOWN", "UNCLEAR"
		//
		public string TrendDirection(){
			//return TrendDirectionSMA75();
			return TrendDirectionBollinger1Sigma();
		}
		public string TrendDirectionSMA75(){
			string direction = "UNCLEAR";
			double sma = SMA(Close,75)[0];
			double close = Close[0];
			if (sma <= TNKRecentPeakValue && sma >= TNKRecentBottomValue){
				direction = "UNCLEAR";
			}else if (close > sma) {
				direction = "UP";
			}else if (close < sma) {
				direction = "DOWN";
			}else{
				LogError("LongRangeTrendWithSMA75()",MemoND("sma",sma));
				direction = "UNCLEAR";
			}
			return direction;
		}
		
		public string TrendDirectionBollinger1Sigma(){
			string direction = "UNCLEAE";
			int period = 14;
			double sigma = 1.0;
			double close = Close[0];
			double upper = Bollinger(sigma, period).Upper[0];
			double lower = Bollinger(sigma, period).Lower[0];
			if (close > upper) {
				direction = "UP";
			}else if (close < lower) {
				direction = "DOWN";
			}else{
				direction = "UNCLEAR";
			}
			return direction;
		}
	
		// Check market cycle.
		// Return market cycle as one of "FLOOR", "ROOF", "UNCLEAR"
		//
		public string MarketCycle(){
			return MarketCycleWithWilliamsR();
		}	
		public string MarketCycleWithWilliamsR(){
			int period = 14;
			double overBought = -20.0; // -30.0
			double overSold = -80.0; // -70.0
			double wr = WilliamsR(period)[0];
			string cycle = "UNCLEAR";
			if(wr > overBought){
				cycle = "ROOF";
			}else if (wr < overSold){
				cycle = "FLOOR";
			}else{
				cycle = "UNCLEAR";
			}
			return cycle;
		}
		
		//TEST Parpose
		public string MarketConditionAlwayTREND(){
			return "TREND";
		}
		public string MarketConditionAlwayBOX(){
			return "BOX";
		}

//NT7
//		public string MarketConditionWithADXAndADXAccel(){
//			string condition = "";
//			string condADX = MarketConditionWithADX();
//			string condADXAccel = MarketConditionWithADXAccel();
//			if(condADX == "TREND" || condADXAccel == "TREND"){
//				condition = "TREND";
//			}else if(condADX == "UNCLEAR" || condADXAccel == "UNCLEAR"){
//				condition = "UNCLEAR";
//			}else{
//				condition = "BOX";
//			}
//			return condition;
//		}
//NT7

//NT7
//		public string MarketConditionWithADXAccel(){
//			int adxPeriod = 7;
//			int dx = 2;
//			double border = 1.5;
//			string condition = "";
//			double adxAccel = ADXAccel(adxPeriod,dx)[0];
//			if (adxAccel > border){
//				condition = "TREND";
//			}else if (adxAccel < border){
//				condition = "BOX";
//			}else{
//				condition = "UNCLEAR";
//			}
//			return condition;
//		}
//NT7

		public string MarketConditionWithADX(){
			int adxPeriod = 7;
			double upperBorder = 80.0; //80.0
			double lowerBorder = 40.0;　//40.0
			string condition = "";
			double adx = ADX(7)[0];
			if (adx > lowerBorder && adx < upperBorder){
				condition = "TREND";
			}else if (adx <= lowerBorder){
				condition = "BOX";
			}else{
				condition = "UNCLEAR";
			}
			return condition;
		}
		public string MarketConditionWithADXAndADXSlope(){
			int adxPeriod = 7;
			int adxSlopeLB = 2;
			double upperBorder = 100.0; //80.0
			double lowerBorder = 50.0;　//40.0
			string condition = "";
			double adx = ADX(adxPeriod)[0];
			double slope = Slope(ADX(adxPeriod),adxSlopeLB,0);
			if ((adx > lowerBorder || adx+slope> lowerBorder) && adx < upperBorder){
				condition = "TREND";
			}else if (adx <= lowerBorder || adx+slope<= lowerBorder){
				condition = "BOX";
			}else{
				condition = "UNCLEAR";
			}
			return condition;
		}
		
		//Check past nBar (include current) bars are positive 
		public bool WasPositivePastIn(int nBar,int allowedNeutralCount){
			if (nBar<1) {return false;}
			bool result;
			int[] res = CountUpBarsParDirection(nBar);
			int pos = res[0];
			int neg = res[1];
			int neu = res[2];
			LogDebug("WasPositivePastIn()",MemoNI("pos",pos),MemoNI("neg",neg),MemoNI("neu",neu));
			if (pos+neu == nBar && neu <= allowedNeutralCount){
				result = true;
			}else{
				result = false;
			}
			return result;
		}
		//Check past nBar (include current) bars are negative 
		public bool WasNegativePastIn(int nBar,int allowedNeutralCount){
			if (nBar<1) {return false;}
			bool result;
			int[] res = CountUpBarsParDirection(nBar);
			int pos = res[0];
			int neg = res[1];
			int neu = res[2];
			LogDebug("WasPositivePastIn()",MemoNI("pos",pos),MemoNI("neg",neg),MemoNI("neu",neu));
			if (neg+neu == nBar && neu <= allowedNeutralCount){
				result = true;
			}else{
				result = false;
			}
			return result;
		}
		// Count up Positive Bar(Yousen), Negative Bar(Insen) and Neutral bar(Tombo) in look back period.
		// result count include current bar.
		// results[0]:Positive Bar Count, [1]:Negative bar count, [2]:Neutral bar count
		public int[] CountUpBarsParDirection(int lookBack){
			int positiveBarCount=0,negativeBarCount=0, neutralBarCount=0;
			for (int i=0;i<lookBack;i++){
				if (IsPositiveBar(i)){
					positiveBarCount++;
				}else if (IsNegativeBar(i)){
					negativeBarCount++;
				}else if (IsNeutralBar(i)){
					neutralBarCount++;
				}else{
					LogError("CountUpBarsParDirection()",TAGMsg("Impassible branch"));
				}
			}
			return new int[]{positiveBarCount,negativeBarCount,neutralBarCount};
		}
		
		//
		//  Bar Characteristic
		//
		
		// Merge with Previous bar. retun Double Array of OHLC values.
		public double[] MergeWithPreviousBar(int barsAgo){
			double open,close,high,low;
			if(CurrentBar > 0){ 
				open  = Open[1];
				close = Close[0];
				high  = Math.Max(High[1],High[0]);
				low   = Math.Min(Low[1],Low[0]);
			}else{
				open  = Open[0];
				close = Close[0];
				high  = High[0];
				low   = Low[0];
			}
			double[] ohlc = {open, high, low, close};
			return ohlc;
		}
		// Range of OHLC
		public double RangeOfOHLC(double[]ohlc){
			return ohlc[1] - ohlc[2];
		}
		// Move of OHLC
		public double MoveOfOHLC(double[] ohlc){
			return ohlc[3] - ohlc[0];
		}
		// InRange of not
		public bool InRange(double value, double upper, double lower){
			if (upper<lower) {double temp = upper; upper = lower; lower = temp;} // Up side down
			return (value <= upper && value >= lower); 
		}
		// Judge open window pattern
		// "UPWINDOW","DOWNWINDOW","NOWINDOW"
		public string OpenWindowName(int barsAgo){
			string result = "NOWINDOW";
			int prev = barsAgo + 1;
			double higher = IsPositiveBar(barsAgo) ? Close[barsAgo]:Open[barsAgo];
			double lower  = IsPositiveBar(barsAgo) ? Open[barsAgo]:Close[barsAgo];
			//
			bool isPrevUpside   = Open[prev] > higher &&  Close[prev] > higher;
			bool isPrevDownside = Open[prev] < lower  &&  Close[prev] < lower;
			//
			if(isPrevDownside){
				result = "UPWINDOW";
			}else if(isPrevUpside){
				result = "DOWNWINDOW";
			}else{
				result = "NOWINDOW";
			}
			return result;
		}
		
		//  Judge bar's direction.
		public string BarDirectionName(int n){
			string result = "";
			double move = Move(n);
			if (move>0.0) {
				result = "POSITIVE";
			} else if (move<0.0){
				result = "NEGATIVE";
			} else {
				result = "NEWTRAL";
			}
			return result;
		}
		
		public bool IsPositiveBar(int n){
			if(Close[n] > Open[n]) {
				return true;
			}else{
				return false;
			}		
		}
		
		public bool IsNegativeBar(int n){
			if(Close[n] < Open[n]) {
				return true;
			}else{
				return false;
			}
		}
		
		public bool IsNeutralBar(int n){
			if(Close[n] == Open[n]){
				return true;
			}else{
				return false;
			}
		}
		
		public bool IsHarami(){
			return IsHarami(0);
		}
		
		public bool IsHarami(int n){
			if (High[n] < High[n+1] && Low[n]>Low[n+1]){
				return true;
			} else {
				return false;
			}
		}
		
		public bool IsTonbo(int n){
			return IsNeutralBar(n);
		}
		public bool IsTonboWithMargin(int n){
			bool result = (Math.Abs(move(n))<= TickSize);
			return result;
		}
		
		public bool IsBearded(int n){
			double border = 0.3;
			double range = Range(n);
			if (range == 0.0){ 
				return true; //is actualy tonbo
			}
			double ratio = Math.Abs(Move(n))/range;
			if(ratio < border) {
				return true;
			}else{
				return false;
			}
		}

		//
		// Judge if current bar is Highest/Lowest
		// 
		public bool isHighestIn(int n){
			if(n<=0) return false; //0 is illigal parameter
			if (!(High[1]>High[0])) return false; 
			for(int i=1; i<=n; i++){
				if(High[i+1]<High[i]) {
					continue;
				}else{
					return false;
				}
			}
			return true;
		}
		
		public bool isLowestIn(int n){
			if(n<=0) return false; //0 is illigal parameter
			if (!(Low[1]<Low[0])) return false; 
			for(int i=1; i<=n; i++){
				if(Low[i+1]>Low[i]) {
					continue;
				}else{
					return false;
				}
			}
			return true;
		}
		
		//Position Status
		public bool DoesMoveAlong(double move){
			bool result = false;
			if(haveLongPosition()){
				result = move > 0;
			}else if(haveShortPosition()){
				result = move < 0;
			}else{
				result = false;
			}
			return result;
		}
		public bool isGoingAlong(){ //Based on entry price. return ture also when entry == close
			if (TNKRealEntryPrice==0.0){
				LogError("isGoingAlong()",MemoNS("call before the oder filled","Exiting"));
				return false; //exit();
			}
			double cv = Close[0];
			if(haveLongPosition()){
				return (cv >= TNKRealEntryPrice);	
			}else if(haveShortPosition()){
				return (cv <= TNKRealEntryPrice);
			}else{
				LogError("isGoingAlong()",MemoNS("call when maybe Flat position","Exiting"));
				return false; //exit();
			}
		}
		public bool isGoingAgainst(){return !isGoingAlong();}
		public bool havePosition(){return Position.MarketPosition != MarketPosition.Flat;}
		public bool haveLongPosition(){return Position.MarketPosition == MarketPosition.Long;}
		public bool haveShortPosition(){return Position.MarketPosition == MarketPosition.Short;}
		public string PositionDirection(){
			if(haveLongPosition()) {
				return "LONG";
			}else if(haveShortPosition()){
				return "SHORT";
			}else{
				return "NOPOSITION";
			}
		}
		// "LONG" == "BUY", "SHORT" == "SELL"
		public bool DoesMatchDirection(string ds1, string ds2){
			int di1 = DirectionIdentity(ds1);
			int di2 = DirectionIdentity(ds2);
			if (di1==2 || di2 ==2){
				return false; //d1 or d2 hold nodiretion
			}else if (di1 == di2) {
				return true; //d1 and d2 hold idetical direction
			}else{
				return false; //d1 and d2 holsd deferent direction
			}
		}
		// LONG or BUY : 0, SHORT or SELL : 1, other : 2
		public int DirectionIdentity(string direction){
			int identity;
			if (direction=="LONG" || direction == "BUY"){
				identity =  0;
			}else if (direction == "SHORT" || direction == "SELL"){
				identity =  1;
			}else{
				identity = 2;
			}
			return identity;
		}
    }
}
