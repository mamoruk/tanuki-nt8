//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using System.Linq;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
	
    partial class Strategy
    {
		public void SetupCommon(){
			//Some thing common before meke position
		}

		//
		// Execute each TradeSpec which are feched form passed TradeSpecList
		//
		public void ExecuteSignalList(List<TradeSpec> tradeSpecList ){
			// Execute signals in tradeSpec List
			//   Entries in List are completly independant each other.(trade spec does not consider other specs in list)
			//   Execution order is that high priority first, earliest entry first in same priority.
			//   "BUY" and "SELL" signals
			//      if position is aready taken, ignore the signal which take new position.
			//      if it's flat, execute only one signal which has highest priority.
			//      if there is multiple signals in same priority, execute only one most early entry.
			//   "FLAT" signal close all position (shoud have higer proisity than "BUY"/"SELL" signals in list)
			//   "BREAK" signal close all positon and stop to execute rest signals (should have highest priority in list)
			
			LogDebug("ExecuteSignalList()",MemoNI("tradeSpecList.Count",tradeSpecList.Count));			
			bool rejected = false;

			//Record executeBarNo if the treadeSpec is not executed
			foreach(TradeSpec ts in  tradeSpecList){
				ts.executedBarNo = CurrentBar;
			}
			//Record TradeSpecs to TNKTradeSpecHistory
			TNKTradeSpecHistory = TNKTradeSpecHistory.Concat(tradeSpecList).ToList();

			//Execution loop
			List<TradeSpec> copiedList = new List<TradeSpec>(tradeSpecList); //make copy because FetchTradeSpec() is destractive.
			TradeSpec tradeSpec = FetchTradeSpec(copiedList);
			while(tradeSpec != null){
				// Filter signal
				rejected = AttemptSignalFilters(tradeSpec);
				//Execute Siganl
				if(!rejected) {
					//ExecuteTradeSpec(tradeSpec); //TBObseleted:
					tradeSpec.Execute();
					LogDebug("ExecuteSignalList()",MemoNS("Executed signal",tradeSpec.signal));
				}
				tradeSpec = FetchTradeSpec(copiedList);
			}
		}
		
		public TradeSpec FetchTradeSpec(List<TradeSpec> tradeSpecList){
			// parts.Find(x => x.PartName.Contains("seat")));
			// int searchAge = 35;result = listTestFind.Find(delegate(TestFind testFind) { return testFind.Age == searchAge; });

			// Get Highest priority number in list
			int highestPriority = 0;
			foreach(TradeSpec ts in tradeSpecList){
				highestPriority = Math.Max(highestPriority, ts.priority);
			}
			if(tradeSpecList.Count>0){LogDebug("FetchTradeSpec",MemoNI("highestPriority",highestPriority));}
			// Take Highest priorty and earlies entry
			//   Remove the entry from list
			TradeSpec found = null;
			int index = tradeSpecList.FindIndex(ts => ts.priority==highestPriority);
			if(index > -1){ //Found
				found = tradeSpecList[index];
				////remove all entrys which have the priority from source list
				////tradeSpecList.RemoveAll(ts => ts.priority==highestPriority);
				//remove only found tradeSpec.
				//tradeSpecList.RemoveAt(index);
				//remove all etnry
				tradeSpecList.Clear();
			}else{ //Not found
				found = null;
			}
			// return the entry
			return found;
		}
		
		//
		// Set Stop loss
		//
		public double SetStopPrice(double stopPrice){
			double ajustedPrice = Instrument.MasterInstrument.RoundToTickSize(stopPrice);
			if(TNKStopLossPrice != ajustedPrice){ //DO nothing if nochange
				double oldPrice = TNKStopLossPrice;
				TNKStopLossPrice = ajustedPrice;
				SetStopLoss(CalculationMode.Price, TNKStopLossPrice);
				LogDebug("SetStopPrice()",MemoND("Old",oldPrice),MemoND("Input",stopPrice),MemoND("New",TNKStopLossPrice));
			}
			MarkDotOnChart(TNKStopLossPrice);
			return TNKStopLossPrice;
		}		
		//
		// Set Profit Target
		//
		public double SetTargetPrice(double targetPrice){
			double ajustedPrice = Instrument.MasterInstrument.RoundToTickSize(targetPrice);
			if(TNKTargetPrice != ajustedPrice){ //DO nothing if nochange
				double oldPrice = TNKTargetPrice;
				TNKTargetPrice = ajustedPrice;
				SetProfitTarget(CalculationMode.Price, TNKTargetPrice);
				LogDebug("SetTargetPrice()",MemoND("Old",oldPrice),MemoND("Input",targetPrice),MemoND("New",TNKTargetPrice));
			}
			//MarkDotOnChart(TNKStopLossPrice);
			MarkDotOnChartWithTagAndColor(TNKTargetPrice,"", Brushes.Blue);
			return TNKTargetPrice;
		}
		
		// get direction of REVRSE 
		public string GetActualDirectionOfREVERSE(){
			if (haveLongPosition()){
				return "SELL";
			}else if (haveShortPosition()){
				return "BUY";
			}else{
				return "NONE";
			}
		}
			
		public void KeepPosition(){}
//TBDel		
//		//Maket Series
//		public void SetupLong(int quantity, double stopLossDelta, double targetDelta, string name){
//			if (quantity != 0){
//				SetStopPriceDeltaLong(Close[0],stopLossDelta);
//				if(targetDelta != 0.0) {SetTargetPriceDeltaLong(Close[0],targetDelta);}
//				TNKLatestOrder = EnterLong(quantity,name);
//				LogMessage("SetupLong(4p)",MemoND("quantity",quantity),MemoND("stopLossDelta",stopLossDelta),MemoND("targetDelta",targetDelta));
//			}else{
//				LogMessage("SetupLong(4p)",TAGMsg("Didn't take position due to calculated position was 0"));
//			}
//		}
	
//		public void SetupShort(int quantity,double stopLossDelta, double targetDelta, string name){
//			if (quantity != 0){
//				SetStopPriceDeltaShort(Close[0],stopLossDelta);
//				if(targetDelta != 0.0) {SetTargetPriceDeltaShort(Close[0],targetDelta);}
//				TNKLatestOrder = EnterShort(quantity,name);
//				LogMessage("SetupShort(4p)",MemoND("quantity",quantity),MemoND("stopLossDelta",stopLossDelta),MemoND("targetDelta",targetDelta));
//			}else{
//				LogMessage("SetupShort(4p)",TAGMsg("Didn't take position due to calculated position was 0"));
//			}
//		}
//
//		public void StopAndReverse(int quantity,double stopLossDelta, double targetDelta, string name){
//			LogMessage("StopAndReverseSLD()",MemoND("quantity",quantity),MemoND("stopLossDelta",stopLossDelta),MemoND("targetDelta",targetDelta));
//			if(haveLongPosition()){
//				SetupShort(quantity,stopLossDelta,targetDelta,name);
//			}else if(haveShortPosition()){
//				SetupLong(quantity,stopLossDelta,targetDelta,name);
//			}else{
//				//No action in flat.
//				LogError("StopAndReverse()",TAGMsg("Call in flat position."));
//			}					
//		}
//TBDel
		
		//ISSUE check if exit execute in asyncronusly.
		public void ExitPosition(string name){
			if(haveLongPosition()) ExitLong(); //Exit all posiiton
			if(haveShortPosition()) ExitShort(); //Exit all posiiton
		}

//TBDel
//		// Set Stop Loss
//		public double SetStopPriceDeltaLong(double baseval, double delta){
//			double thePrice = baseval - delta;
//			return SetStopPrice(thePrice);
//		}
//		public double SetStopPriceDeltaShort(double baseval, double delta){
//			double thePrice = baseval + delta;
//			return SetStopPrice(thePrice);
//		}
//		// Set Profit Target
//		public double SetTargetPriceDeltaLong(double baseval, double delta){
//			double thePrice = baseval + delta;
//			return SetTargetPrice(thePrice);
//		}
//		public double SetTargetPriceDeltaShort(double baseval, double delta){
//			double thePrice = baseval - delta;
//			return SetTargetPrice(thePrice);
//		}	
//TBDel

	}
}