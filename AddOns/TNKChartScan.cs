#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

//This namespace holds Add ons in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Strategies
{
   partial class Strategy{
		//===========================
		// Chart Scanner definition
		//   Create 2018/09/18
		//===========================
		public class ChartScanner:IFDescription {
			// IVars
			private Strategy o;
			public double Level {get; set;}
			public CandleList ScanedCandles {get; set;}
			// Constructor
			public ChartScanner(Strategy owner, double level, int nearBarNumber, int farBarNumber){
				//fromBarIndex should be recent bar. Scan will execute recent to old.
				o = owner;
				Level = level;
				ScanedCandles = new CandleList(o);
				//Scan(nearBarNumber, farBarNumber);
				ScanAcrossBars(nearBarNumber, farBarNumber,0);
			}
			// Methods
			// Collect bars that coross or touch sepcified level.
			//  Returnd scanedList holds most recent bar in scanedCandles[0] as candle.
			//  Input: nearBrNumber, farBarNumber:Scan Range,  direction: 1 = Mach upward, 0 = Mach both direction, -1  = Mach downward
			public void ScanAcrossBars(int nearBarNumber, int farBarNumber, int direction){
				if(nearBarNumber < farBarNumber){
					o.LogError("ChartScanner.ScanAcrossBars()",o.TAGMsg("nearBarNumber < farBarNumber"),o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("farBarNumber",farBarNumber));
					Strategy.Swap(ref nearBarNumber, ref nearBarNumber);
					//int t = nearBarNumber;nearBarNumber=farBarNumber;farBarNumber=t; //Swap nearBarNumber and farBarNumber
				}
				//Make CandleList that contains all bar in range
				CandleList allCandlesInRange = new CandleList(o, farBarNumber,nearBarNumber);
				//int lastInd = allCandlesInRange.Length()-1; //JUST DEBUG
				//o.LogDebug("ChartScanner.Scan()",o.MemoNI("o.CurrentBar",o.CurrentBar),o.MemoNI("farBarNumber",farBarNumber),o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("allCandlesInRange.BarNumber(lastInd)",allCandlesInRange.BarNumber(lastInd)));
				//Pickup candles that match criteria.
				string position;
				//foreach(Candle candle in allCandlesInRange.Candles){
				foreach(int i in Enumerable.Range(0, allCandlesInRange.Candles.Count)){
					Candle candle = allCandlesInRange.Candles[i];
					//Check cross direction
					// RelativePositionWithDirection()
					//string pos = candle.RelativePositionWithDirection(Level);
					string pos = candle.RelativePosition(Level);
					//"CROSS" does not include Touch the level and Tonbo pattern.
					if(pos == "CROSS"){
						if(direction == 0){
							//Pickup both direction
							ScanedCandles.Add(candle);
						}else if(direction == candle.DirectionSign){
							//Pickup matched direction
							ScanedCandles.Add(candle);
						}else{
							//Error
							o.LogError("ScanAcrossBars()",o.MemoNI("Unexpected condition diretion",direction),o.MemoNS("pos",pos));
						}
					}else{
						//Check if accross by open window
						if(i>0){
							Candle prevCandle = allCandlesInRange.Candles[i-1];
							//o.LogDebug("ScanAcrossBars()",o.MemoNO("prevCandle",prevCandle),o.MemoNO("candle",candle));
							if (candle.IsPutTheLeveBetweenTheCandle(Level, prevCandle)){
								ScanedCandles.Add(candle);
							}else{
								//Unmach 
								//Do not pick up
							}
						}else{
							//No privious bar
							//Do nothing
						}
					}
				}
				//Revwrse ScanedCandles to make ScanedCandles[0] holds most recent candle.
				ScanedCandles.Candles.Reverse();
				o.LogDebug("ScanAcrossBars()",o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("farBarNumber",farBarNumber),o.MemoNO("allCandlesInRange",allCandlesInRange),o.MemoNO("ScanedCandles",ScanedCandles));
			}

			//
			// Collect bars that coross or touch sepcified level.
			//  Returnd scanedList holds most recent bar in scanedCandles[0] as candle.
			public void Scan(int nearBarNumber, int farBarNumber){
				if(nearBarNumber < farBarNumber){
					o.LogError("ChartScanner.Scan()",o.TAGMsg("nearBarNumber < farBarNumber"),o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("farBarNumber",farBarNumber));
					Strategy.Swap(ref nearBarNumber, ref nearBarNumber);
					//int t = nearBarNumber;nearBarNumber=farBarNumber;farBarNumber=t; //Swap nearBarNumber and farBarNumber
				}
				//Make CandleList that contains all bar in range
				CandleList allCandlesInRange = new CandleList(o, farBarNumber,nearBarNumber);
				//int lastInd = allCandlesInRange.Length()-1; //JUST DEBUG
				//o.LogDebug("ChartScanner.Scan()",o.MemoNI("o.CurrentBar",o.CurrentBar),o.MemoNI("farBarNumber",farBarNumber),o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("allCandlesInRange.BarNumber(lastInd)",allCandlesInRange.BarNumber(lastInd)));
				//Pickup candles that match criteria.
				string position;
				//foreach(Candle candle in allCandlesInRange.Candles){
				foreach(int i in Enumerable.Range(0, allCandlesInRange.Candles.Count)){
					Candle candle = allCandlesInRange.Candles[i];
					if(candle.IsInBody(Level)){ //Check if Body across the level
						ScanedCandles.Add(candle);
					}else if (i>0 && candle.IsPutTheLeveBetweenTheCandle(Level,allCandlesInRange.Candles[i-1])){ //Check if accross by open window
						ScanedCandles.Add(candle);
					}else{
						//Did not across the level
						//Do nothing
					}
				}
				//Revwrse ScanedCandles to make ScanedCandles[0] holds most recent candle.
				ScanedCandles.Candles.Reverse();
				o.LogDebug("ChartScanner.Scan()",o.MemoNI("nearBarNumber",nearBarNumber),o.MemoNI("farBarNumber",farBarNumber),o.MemoNO("allCandlesInRange",allCandlesInRange),o.MemoNO("ScanedCandles",ScanedCandles));
			}
			public string Description(){
				return string.Format("Level:{0} ScanedCandles:{1}",Level,ScanedCandles.Description());
			}
		}
		//============
		// End of Class Chart Scanner
		//============	

		//===========================
		// CancleList definition
		//   Create 2018/09/20
		//===========================
		public class CandleList:IFDescription{
			// IVars
			public List<Candle> Candles;
			public Strategy o;
			// Constructor
			public CandleList(Strategy owner){
				o = owner;
				Candles = new List<Candle>(){};
			}
			public CandleList(Strategy owner, int fromBarNum, int toBarNum):this(owner){
				//o = owner;
				if(fromBarNum > toBarNum){
					Strategy.Swap(ref fromBarNum,ref toBarNum);
				}
				int count = toBarNum - fromBarNum +1 ;
				o.LogDebug("CandleList()",o.MemoNI("fromBarNum",fromBarNum),o.MemoNI("toBarNum",toBarNum),o.MemoNI("count",count)); //JUST DEBUG
				foreach(int i in Enumerable.Range(fromBarNum, count)){
					Candle candle = o.TNKCandlePool.fetchWithBarIndex(i);
					if(candle != null){
						Add(candle);
					}else{
						o.LogError("CandleList()",o.TAGMsg("BarIndex out of range"),o.MemoNI("fromBarNum",fromBarNum),o.MemoNI("toBarNum",toBarNum),o.MemoNI("i",i));
					}	
				}
			}
			// Premitive Methods
			public void Add(Candle candle){Candles.Add(candle);}
			public int Length(){return Candles.Count;}			
			public Candle At(int order){return Candles[order];}
			public Candle Recent(){return Candles.Last();}
			public int BarNumber(int order){return Candles[order].BarNumber;}
			public string Description(){return string.Format("Length:{0}",this.Length());}
			//Get numbers
			//Get Close
			public double[] GetCloses(){
				double[] d = new double[Length()];
				int i = 0;
				foreach( Candle c in Candles){
					d[i]=c.Close;
					i++;
				}
				return d;
			}
			public int[] GetBarNumbers(){
				int[] v = new int[Length()];
				int i = 0;
				foreach( Candle c in Candles){
					v[i]=c.BarNumber;
					i++;
				}
				return v;
			}
			//
			// Extract Candles
			//
			public CandleList[] ExtractPeakOfPeak(string posValName, string negValName){
				CandleList pp,np;
				CandleList[] next = new CandleList[2];
				int pc,nc,max;
				//
				if(posValName == negValName){
					next = ExtractPeaks(posValName);
				}else{
					next[0] = ExtractPositivePeaks(posValName);
					next[1] = ExtractNegativePeaks(negValName);
				}
				//
				while(true){
					pp = next[0];
					np = next[1];
					pc = pp.Length();
					nc = np.Length();
					max = Math.Max(pc, nc);
					if(max > 2) {
						next = nextRank(posValName, negValName,pp,np);
						continue;
					}else{
						break;
					}
				}
				return next;			
			}
			
			//=====
			// Extract all peaks of all orders
			//  Input:Value name for positive peak, Value name for nevative peaks
			//  Output: list of list of positive peaks and negative peaks pair.  [[pPeak1,nPeak1], [pPeak2,nPeak2], [pPeak3,nPeak3]....]
			//====
			public List<CandleList[]> ExtractAllPeak(string posValName, string negValName){
				List<CandleList[]> resultList = new List<CandleList[]>();
				CandleList pp,np;
				CandleList[] next = new CandleList[2];
				int pc,nc,max;
				//
				if(posValName == negValName){
					next = ExtractPeaks(posValName);
				}else{
					next[0] = ExtractPositivePeaks(posValName);
					next[1] = ExtractNegativePeaks(negValName);
				}
				//
				while(true){
					pp = next[0];
					np = next[1];
					pc = pp.Length();
					nc = np.Length();
					max = Math.Max(pc, nc);
					if(max > 2) {
						resultList.Add(next);
						next = nextRank(posValName, negValName,pp,np);
						continue;
					}else{
						break;
					}
				}
				return resultList;			
			}
			private CandleList[] nextRank(string posValName, string negValName, CandleList pp, CandleList np){
				pp = pp.ExtractPositivePeaks(posValName);
				np = np.ExtractNegativePeaks(negValName);
				return new CandleList[]{pp,np};
			}
			public CandleList ExtractPositivePeaks(string valName){
				return ExtractPeaks(valName)[0]; //Positive Peaks
			}
			public CandleList ExtractNegativePeaks(string valName){
				return ExtractPeaks(valName)[1]; //Negative peaks
			}
			//
			// Extract peaks of both direction
			//   [0]:CandleList of PositivePeaks    [1]:CandleList of Negative Peaks
			public CandleList[] ExtractPeaks(string valName){
				CandleList positivePeaks = new CandleList(o);
				CandleList negativePeaks = new CandleList(o);
				CandleList[] results = new CandleList[]{positivePeaks,negativePeaks};
				int count = this.Length();
				if(count < 3){return results;}
				Candle a,b,c;
				foreach(int i in Enumerable.Range(1, count-2)){
					a = this.At(i-1);
					b = this.At(i);
					c = this.At(i+1);
					string type = checkIfPeak(a.ValueByName(valName),b.ValueByName(valName),c.ValueByName(valName));
					switch(type){
						case "POSITIVE":
							positivePeaks.Add(b);
							break;
						case "NEGATIVE":
							negativePeaks.Add(b);
							break;
						case "OTHER":
							//Do nothing
							break;
						default:
							o.LogError("extractPeaks()",o.MemoNS("Illegal Peak type",type));
							break;
					}
				}
				return results;
			}
			string checkIfPeak(double a,double b,double c){
				string result = "OTHER";
				if(a<b && b>c){
					result = "POSITIVE";
				}else if (a>b && b<c){
					result = "NEGATIVE";
				}else{
					result = "OTHER";
				}
				return result;
			}

			//
			//  Regression line
			//   Output: Coefficent a0,a1 and R-Squared as {a0,a1,rs} if Lingth < 2 return {0.0,0.0,0.0}
			//
			public double[] RegressionLine1(){
				if(Length()>1){
					double[] x = new double[Length()];
					double[] y = new double[Length()];
					int origin = Candles.First().BarNumber;
					int xi = 0;
					foreach( Candle c in Candles){
						x[xi]=c.BarNumber - origin;
						y[xi]=c.Close;
						xi++;
					}
					o.LogDebug("RegressionLine",o.MemoNAR("x",x),o.MemoNAR("y",y));
					//calc R-Squaierd
					double[] coe = o.DecideRegressionLine(x,y);
					double rs = o.RSquared(coe[0],coe[1],x,y);
					//
					return new double[]{coe[0],coe[1],rs};
				}else{
					return new double[]{0.0,0.0,0.0};
				}
			}
			public RegressionLine RegressionLine(string valueName){
				if(Length()>1){
					double[] x = new double[Length()];
					double[] y = new double[Length()];
					int origin = Candles.First().BarNumber;
					int xi = 0;
					foreach( Candle c in Candles){
						x[xi]=(double)(c.BarNumber - origin);
						//y[xi]=c.Close;
						y[xi]=c.ValueByName(valueName);
						xi++;
					}
					o.LogDebug("RegressionLine",o.MemoNS("valueName",valueName),o.MemoNAR("x",x),o.MemoNAR("y",y));
					//Make resgression line
					RegressionLine rline = new RegressionLine(o, x, y, origin);
					return rline;
				}else{
					return null;
				}
			}
		}
		//============
		// End of Class ChartList
		//============	
		//===========================
		// Candle List Utitlities
		//
		//===========================
		public int SelectCandleListWhichHasRecent(CandleList[] lists){
			if(lists[0].Length() == 0 && lists[1].Length() == 0) return -2;  //All empty
			if(lists[0].Length() > 0 && lists[1].Length() == 0) return 0;    //Pickup not empty list
			if(lists[0].Length() == 0 && lists[1].Length() > 0) return 1;
			Candle posi = lists[0].Candles.Last();  //Pickup valid values
			Candle nega = lists[1].Candles.Last();
			if(posi.BarNumber == nega.BarNumber) return -1;  //Select most recent
			if(posi.BarNumber > nega.BarNumber) return 0;
			if(posi.BarNumber < nega.BarNumber) return 1;
			return -2;
		}
		
		//===========================
		// CandlePool definition
	    //   This class is acutually singlton. Pointed by TNKCandlePool
		//   Create 2018/09/18
		//===========================
		public class CandlePool:IFDescription {
			// IVars
			//private static CandlePool instance = new CandlePool();
			//public  static CandlePool Intstance {get{return instance;}}
			//private static Dictionary<int,Candle> pool = new Dictionary<int,Candle>(){};
			private Dictionary<int,Candle> pool ;
			private Strategy owner;
			// Constructor
			public CandlePool(){}
			public CandlePool(Strategy o){
				owner = o;
				pool = new Dictionary<int,Candle>(){};
			}
			// Methods
			public Candle fetchWithBarIndex(int barIndex){
				bool exist = pool.ContainsKey(barIndex) && pool[barIndex] != null;
				if(exist){
					return pool[barIndex];
				}else{
					Candle newOne = owner.GetCandleWithBarIndex(barIndex);
					pool[barIndex] = newOne;
					return newOne;
				}
			}
			public Candle fetchWithBarsAgo(int barsAgo){
				return fetchWithBarIndex(owner.CurrentBar - barsAgo);
			}
			public string Description(){return "";}
		}
		//============
		// End of Class Candle pool
		//============	
		//===========================
		// Candle Class definition
		//   Create 2018/07/10
		//===========================
		public class Candle:IFDescription {
			public double Open {get; set;}
			public double High {get; set;}
			public double Low {get; set;}
			public double Close {get; set;}
			public int BarNumber {get; set;}
			public double[] Component;
			public double[] Propotion;
			public double Move; 
			public double Range;
			public double Neck;
			public double Hip;
			public double Body;
			public double Head;
			public double Leg;
			public double Volume;
			public string Direction;
			public double DirectionSign;
			public double[] _Component(){
				return new double[]{Head,Body,Leg};
			}
			public double[] _Propotion(){
				return new double[]{Head/Range,Body/Range,Leg/Range};
			}

			public Candle(double o, double h, double l, double c)
			{
				Open = o;
				High = h;
				Low = l;
				Close = c;
				BarNumber = 0;
				Move = Close - Open;
				Range = High  - Low;
				Neck = Math.Max(Open,Close);
				Hip = Math.Min(Open,Close);
				Body = Math.Abs(Close-Open);
				Head = High - Neck;
				Leg = Hip - Low;
				//Init Direction
				if(Open == Close) {
					Direction = "NONE";
					DirectionSign =  0.0;
				}else{
					if(Close > Open){
						Direction = "UP";
						DirectionSign = 1.0;
					}else{
						Direction = "DOWN";
						DirectionSign = -1.0;
					}
				}
				//Init Component
				Component = new double[]{Head,Body,Leg};
				//Init Propotion
				if(Range==0.0){
					Propotion = new double[]{0.0,0.0,0.0};
				}else{
					Propotion = new double[]{Head/Range,Body/Range,Leg/Range};
				}
			}
			//
			public Candle(double o, double h, double l, double c, int bn) : this(o,h,l,c)
			{
				BarNumber = bn;
			}

			//
			// Methods
			//
			
			//
			// Retrune value sepecifyed by name
			//   name:OPEN, HIGH,LOW,CLOSE
			public double ValueByName(string name){
				switch(name){
					case "OPEN":
						return Open;
						break;
					case "HIGH":
						return High;
						break;
					case "LOW":
						return Low;
						break;
					case "CLOSE":
						return Close;
					default:
						//o.LogError("valueByName",MemoNS("Illegal name",name));
						return 0.0;
						break;
				}
			}
			// Composit with another candle.
			//
			public Candle CompositCandle(Candle candle){
				double o,h,l,c;
				o = this.Open;
				c = candle.Close;
				h = Math.Max(this.High, candle.High);
				l = Math.Min(this.Low, candle.Low);
				return new Candle(o,h,l,c);
			}
			//
			//  Return position relation with Candle by position name string.
			//   "CROSS",UPSIDE_HIGH,"TOUCH_HIGH","DOWNSIDE_LOW","TOUCH_LOW","UPSIDE_BODY","DOWNSIDE_BODY","TOUCH_HIP"
			// NOT IN USE. MAY BE BAD IDEA>
			public string RelativePosition(double level){
				if (level >  this.High){return "UPSIDE_HIGH";}
				if (level == this.High){return "TOUCH_HIGH";}
				if (level <  this.Low) {return "DOWNSIDE_LOW";}
				if (level == this.Low) {return "TOUCH_LOW";}
				if (level >  this.Neck){return "UPSIDE_BODY";}
				if (level == this.Neck){return "TOUCH_NECK";}
				if (level <  this.Hip) {return "DOWNSIDE_BODY";}
				if (level == this.Hip) {return "TOUCH_HIP";}
				return "CROSS";
			}

			//
			//  Return position relation with Candle by position name string.
			//   "CROSS_UP","CROSS_DOWN,"UPSIDE_BODY","DOWNSIDE_BODY","UPSIDE_HIGH","DOWNSIDE_LOW", "EQUAL_NODIRECTION", "ERROR"
			public string RelativePositionWithDirection(double level){
				if (level > this.High) {return "UPSIDE_HIGH";}
				if (level < this.Low) {return "DOWNSIDE_LOW";}
				if (level > this.Neck) {return "UPSIDE_BODY";}
				if (level < this.Hip) {return "DOWNSIDE_BODY";}
				if (this.Direction == "UP") {return "CROSS_UP";}
				if (this.Direction == "DOWN") {return "CROSS_DOWN";}
				if (this.Direction == "NONE") {return "EQUAL_NODIRECTION";}
				return "ERROR";
			}

			//
			//  Return posiotn realation with Body by position name string.
			//    "UPSIDE_HIGH"and "DOWNSIDE_LOW" are replace by "UPSIDE_BODY" and "DOWNSIDE_BODY" each. "UPSIDE_HIGH"and "DOWNSIDE_LOW" will be never returned.
			//    "CROSS_UP","CROSS_DOWN,"UPSIDE_BODY","DOWNSIDE_BODY", "EQUAL_NODIRECTION", "ERROR"
			public string RelativePositionWithBody(double level){
				string result = "";
				string position = RelativePositionWithDirection(level);
				//string position = RelativePosition(level);
				switch(position){
					case "UPSIDE_HIGH":
						result = "UPSIDE_BODY";
						break;
					case "DOWNSIDE_LOW":
						result = "DOWNSIDE_BODY";
						break;
					default:
						result = position;
						break;
				}
				return result;
			}
				
			// Check if level is in body.
			public bool IsInBody(double level){
				return  (level <= Neck) && (level >= Hip);
			}
			//Check if overlapped
			public bool IsOverlapped(Candle another){
				//Check no overlapped first
				bool noOverlapped = (another.Hip > this.Neck) || (another.Neck < this.Hip);
				return !noOverlapped;
			}
			// Check if corss level between given bar
			//  Condition of this pattarn
			//   Two bar do not overlap and the level is between Hip of upper bar and Neck of lower bar.
			public bool IsPutTheLeveBetweenTheCandle(double level, Candle another){
				//Check if overlaped?
				//if (IsOverlapped(another)){
				//	return false;
				//}
				//Check if the level is between Hip of upper bar and Neck of lower bar.					
				double upperLimit = Math.Max(Hip, another.Hip);
				double lowerLimit = Math.Min(Neck,another.Neck);
				if(upperLimit < lowerLimit) {
					return false;
				}else{ 
					return (level <= upperLimit && level >= lowerLimit);
				}
			}
			//
			//  Check if window is open.  If open, return the window as candle else return nil.
			//
			public Candle GetWindowAsCandle(Candle candle){
				//Check overlap
				bool isOverlap = IsOverlapped(candle);
				if(isOverlap) return null;
				//Check formation
				string formation = "INVALID";
				if (Neck < candle.Hip){
					formation = "UP";
				} else if (Hip > candle.Neck){
					formation = "DOWN";
				} else {
					//Print("Error GetWindowAsCandle() unexpected bar formation.");
				}
				//Make candle
				double o,h,l,c;
				switch(formation){
					case "UP":
						o = Neck; h = candle.Neck; l= Hip; c= candle.Hip;
						break;
					case "DOWN":
						o = Hip; h = Neck; l = candle.Hip; c = candle.Neck;
						break;
					default:
						//LogError("GetWindowAsCandle()",MemoNS("Illegal formation",formation));
						return null;
				}
				return new Candle(o,h,l,c);
			}
			//
			// Return Description as string
			//
			public string Description(){  // public override string ToString(){}
				//return $"O:{Open} H:{High} L:{Low} C:{Close} Direction:{Direction} Compo:{Component} Prop:{Propotion}";
				//return string.Format("O:{0} H:{1} L:{2} C:{3} BN:{4} Direction:{5} Compo:{6} Prop:{7}",Open,High,Low,Close,BarNumber,Direction,Component,Propotion);
				return string.Format("O:{0} H:{1} L:{2} C:{3} BN:{4} Direction:{5}",Open,High,Low,Close,BarNumber,Direction);
			}
		}
		//============
		// End of Class Candle
		//============

		//========================
		//  Class Candle Series
		//    Create:2018/07/11
		//=========================
		public class CandleSeries {
			public List<Candle> Candles;
			// Constructor
			public CandleSeries(Strategy o, int barsAgo, int range){
				Candles = new List<Candle>(){};
				foreach(int i in Enumerable.Range(0, range-1)){
					Candles.Add(o.GetCandle(i));
				}
			}
			//
			public CandleSeries(List<Candle> cs){
				Candles = cs;
			}
			//Sub Series
			public CandleSeries SubSeriesFromTail(int fromTail, int count){
				int start = Candles.Count - fromTail - count;
				start = Math.Max(0,start); //Cap to 0
				int to = start + count -1;
				to = Math.Min(Candles.Count-1, to); //Cap to size of Candles
				List<Candle> sub = Candles.GetRange(start,to);
				return new CandleSeries(sub);
			}
			
			//Utlities
			public int Count {get{return Candles.Count;}}
			public void AddToTail(Candle c){Candles.Add(c);}
			public void InsertToHead(Candle c){Candles.Insert(0,c);}
			public Candle MergedCandle(){
				double open,high,low,close;
				open =  Candles[0].Open;
				high = Candles[0].High;
				low = Candles[0].Low;
				close = Candles[Candles.Count-1].Close;
				//Scan High and Low
				foreach(Candle c in Candles){
					high = Math.Max(high,c.High);
					low  = Math.Min(low,c.Low);
				}
				return new Candle(open,high,low,close);
			} 
			public double[] AccumlatedComponents(){
				double head=0,body=0,leg=0;
				foreach(Candle c in Candles){
					head += c.Head;
					body += c.Body;
					leg  += c.Leg;
				}
				return new double[]{head,body,leg};
			}
			public double[] AccumlatedComponentPropotions(){
				double[] components = AccumlatedComponents();
				double head=components[0], body=components[1], leg = components[2];
				double length = head + body + leg;
				if(length==0){
					return new double[]{0.0,0.0,0.0};
				}else{
					return new double[]{head/length, body/length, leg/length};
				}
			}
		}
		//============
		// End of Class CandleSeries
		//============
		
		//==========================
		// Candle related utilitis
		//==========================
		//
		// Make single candle
		//
		public Candle GetCandle(int barsAgo){
			return new Candle(Open[barsAgo],High[barsAgo],Low[barsAgo],Close[barsAgo],CurrentBar-barsAgo);
		}
		//
		public Candle GetCandleWithBarIndex(int barIndex){
			double o = Bars.GetOpen(barIndex);
			double h = Bars.GetHigh(barIndex);
			double l = Bars.GetLow(barIndex);
			double c = Bars.GetClose(barIndex);
			return new Candle(o,h,l,c,barIndex);
		}
		//
		// Make array of candle
		// 
		public Candle[] GetCandoleSeries(int barsAgo, int range){
			Candle[] series = new Candle[]{};
			foreach(int i in Enumerable.Range(0, range-1)){
				series[range-i-1]=GetCandle(barsAgo + i);
			}
			return series;
		}

   }
}
