//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Drawing;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//========================
		//  Observe Events 
		//    Output:TNKRecentObservedEventList <= ToBeModified
		//    Output:TNKObservedEventList
		//  Marge mcObservedEventNameList and bbObservedEventNameList　into TNKObservedEventNameList
		//  Revise Condition Look back methods. Chcek EventList if multiple events for one bar.
		//========================
		
		public List<ObservedEvent> ObserveEvent(){
			//Observer name list
			List<string> observers = new List<string>(){
				//"ObserveMarketCondition",
				//"ObserveSessionBegining"   //Just for verification. Make sure to remove in ordinal operation.
				"ObserveVolatilityChannelBreakout",   //Need for v3.9.16
				"ObserveInitialRangeBreakoutWithoutBreakBack",  //Need for v3.9.16
				//"ObserveInitialRangeBreakout",
				//"ObserveVBandMACross",
				//"ObserveNormalizedBreakout",
				//"ObserveBigBar"
			};

			//Call observer with name list.
			Dictionary<string,object>results = InvokeWithNameList(observers, null);

			//Consolidate observed events
			List<object> eventLists = new List<object>(results.Values);
			List<ObservedEvent> consolidated = new List<ObservedEvent>();
			foreach(List<ObservedEvent> eventList in eventLists){
				consolidated.AddRange(eventList);
			}
			//
			//Record ObservedList to EventHistory
			//
			TNKObservedEventList = consolidated;
			TNKEventHistory.AddRange(TNKObservedEventList);
			
			return TNKObservedEventList;
		}

		//========================
		//  Template of Observer
		//   Events:EN_TEMPLATE_POSITIVE,EN_TEMPLATE_NEGATIVE
		//   v1.0 2019/05/28
		//========================
		public List<ObservedEvent> ObserveTEMPLATE(){
			//PARAMS
			int neededBarCount = 3;
			//New event list
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			// if bar count unser neededBarCount then abort observeation
			//Check needed bar
			if(CurrentBar < neededBarCount) {return eventList;}
			//
			string eventType ="TEMPLATETYPE";
			string eventName = null;
			ObservedEvent oe = null;
			//Judge Some Condition
			bool isSomeConditionPositive = IsPositiveBar(0) && IsPositiveBar(1) &&IsPositiveBar(2);
			bool isSomeConditionNegative = IsNegativeBar(0) && IsNegativeBar(1) &&IsNegativeBar(2);
			//
			if(isSomeConditionPositive){
	   			eventName = "EN_TEMPLATETYPE_TEMPLATE_POSITIVE";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			} else if(isSomeConditionNegative){
   				eventName = "EN_TEMPLATETYPE_TEMPLATE_NEGATIVE";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			}else{
				//No event is observed.
			}
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			return eventList;
		}
		
		//========================
		//  Observer for Session Beginning
		//   Events:EN_VERIF_SESSION_BIGINNING
		//   v1.0 2019/05/28
		//========================
		public List<ObservedEvent> ObserveSessionBegining(){
			//PARAMS
			int neededBarCount = 0;
			//New event list
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			// if bar count unser neededBarCount then abort observeation
			//Check needed bar
			if(CurrentBar < neededBarCount) {return eventList;}
			//
			string eventType ="VERIF";
			string eventName = null;
			ObservedEvent oe = null;
			//Judge Some Condition
			bool isFirstBarOfSession = Bars.IsFirstBarOfSession;
			//
			if(isFirstBarOfSession){
	   			eventName = "EN_VERIF_SESSION_BIGINNING";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			}else{
				//No event is observed.
			}
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			return eventList;
		}
		
		//==============
		//  Observer for Volatiity Channel Breakout without Breakback  using BOWOBB pattern check
		//    Events:EN_VCBO_UPPERBREAKUP,EN_VCBO_LOWERBREAKDOWN
		//           This observeer will NOT observe momentum events like EN_VCBO_UPMOMENTUM, EN_VCBO_DOWNMOMENTUM.
		//    v1.0 2018/10/29 Based on ObserveNormalizedBreakout()
		//==============
		public List<ObservedEvent> ObserveVolatilityChannelBreakout(){
			LogDebug("ObserveVolatilityChannelBreakout()",MemoNI("CurrentBar",CurrentBar));
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			//check CurrentBar is not zoro
			if(CurrentBar < 1) {
				return eventList;
			}else{
				//Continue
			}
			ObservedEvent oe = null;
			string type = "VCBO";
			string eventName = "";
			//PARAMS
			int period = TNKVolatilityChannelPeriod;              //14
			double boFactor = TNKVolatilityChannelBreakoutBorder; //1.4
			//
//			double sigma = StdDev(period)[0];
//			double average = SMA(period)[0];
			//BO level takes from privious bar
			double sigma = StdDev(period)[1];
			double average = SMA(period)[1];
			double offset = boFactor * sigma;
			double upperVal = average + offset;  //Channel upper border 
			double lowerVal = average - offset;  //Channel lower border
			Candle candle = GetCandle(0);
			//
			// Observe first if Close value Break out(cross) sigma range 
			//
			bool crossAbove = IsBreakOutWithoutBreakBack(upperVal,  1 , 0);  //Against direction is OK
			bool crossBelow = IsBreakOutWithoutBreakBack(lowerVal, -1 , 0);  //Against direction is OK
			
//			bool crossAbove   = CrossAbove(INDNormalizedClose(period, boFactor), upperVal, 1);
//			bool breakUp = crossAbove && IsPositiveBar(0); // && bottleNeck
//			bool crossBelow = CrossBelow(INDNormalizedClose(period, boFactor), lowerVal, 1);
//			bool breakDown = crossBelow && IsNegativeBar(0); // && bottleNeck 
			
			//
			// TODO: set even priority
			//
			if (crossAbove){
				//Make event EN_VCBO_UPPERBREAKUP
				eventName = "EN_VCBO_UPPERBREAKUP";
				oe = new ObservedEvent(eventName,type,CurrentBar);
				oe.breakoutLevel = upperVal;
			}else if (crossBelow){
				//Make event EN_VCBO_LOWERBREAKDOWN
				eventName = "EN_VCBO_LOWERBREAKDOWN";
				oe = new ObservedEvent(eventName,type,CurrentBar);
				oe.breakoutLevel = lowerVal;
			} else {
				//Do nothing
			}
			if (oe != null){
				eventList.Add(oe);
			}			
			return eventList;	
		}
		
	
		//========================
		//  Observer for Breakout without brakback
		//   Events:EN_IRBOWOBB_UPPER_BREAKUP, EN_IRBOWOBB_LOWER_BREAKDOWN
		//   v1.0 2018/08/08
		//========================
		public List<ObservedEvent> ObserveInitialRangeBreakoutWithoutBreakBack(){
			LogDebug("ObserveInitialRangeBreakoutWithoutBreakBack()",MemoNI("CurrentBar",CurrentBar));
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			// Prameters and Vars
			string eventType ="IRBOWOBB";
			string eventName = null;
			ObservedEvent oe = null;
			// Reset IR values at start of session
			if(Bars.IsFirstBarOfSession) {
				TNKIRHigh = double.MinValue;
				TNKIRLow  = double.MaxValue;
				TNKIRValid = false;
				LogMessage("ObserveInitialRangeBreakoutWithoutBreakBack()",TAGMsg("InitialRange was reset."));
			}else{
				//Do nothing
			}
			
			//Judge Condition
			double value = Close[0];
			//double highValue = Math.Max(Open[0],Close[0]);
			//double lowValue  = Math.Min(Open[0],Close[0]);
			double highValue = High[0];
			double lowValue = Low[0];
			if (IsInInitial()){
				//Mark Initial Term to chart 
				MarkInitialTerm(true);
				//Record Initail Range
				TNKIRHigh = Math.Max(TNKIRHigh, highValue);
				TNKIRLow  = Math.Min(TNKIRLow , lowValue);
				if(!TNKIRValid && TNKIRHigh > double.MinValue && TNKIRLow < double.MaxValue){
					TNKIRValid = true;
				}else{
					//Do nothing
				}
			}else{
				//Unmark Initial Term to chart 
				MarkInitialTerm(false);
				// Observe Breakout from Initial Range
				if(TNKIRValid){
					//Mark Initial Range to chart
					MarkHolizontalLine(TNKIRHigh,Brushes.Red);
					MarkHolizontalLine(TNKIRLow, Brushes.Blue);
					//IsBreakOutWithoutBreakBack(double level, int direction, int barsAgo)
					//Check Breakout from Initial range with BOWOBB pattern
					//bool crossAbove = IsPositiveBar(0) && IsBreakOutWithoutBreakBack(TNKIRHigh,  1 , 0);
					//bool crossBelow = IsNegativeBar(0) && IsBreakOutWithoutBreakBack(TNKIRLow, -1 , 0);
					LogDebug("ObserveInitialRangeBreakoutWithoutBreakBack()",MemoNB("IsPositiveBar(0)",IsPositiveBar(0)),MemoNB("IsNegativeBar(0)",IsNegativeBar(0)),MemoNB("IsNeutralBar(0)",IsNeutralBar(0)));
					//bool crossAbove = (IsPositiveBar(0) || IsNeutralBar(0)) && IsBreakOutWithoutBreakBack(TNKIRHigh,  1 , 0);
					//bool crossBelow = (IsNegativeBar(0) || IsNeutralBar(0)) && IsBreakOutWithoutBreakBack(TNKIRLow, -1 , 0);
					bool crossAbove = IsBreakOutWithoutBreakBack(TNKIRHigh,  1 , 0);  //Against direction is OK
					bool crossBelow = IsBreakOutWithoutBreakBack(TNKIRLow, -1 , 0);  //Against direction is OK
					//
					if(crossAbove) {
						eventName = "EN_IRBOWOBB_UPPER_BREAKUP";
						oe = new ObservedEvent(eventName,eventType,CurrentBar);
						oe.breakoutLevel = TNKIRHigh;
					}else if(crossBelow){
						eventName = "EN_IRBOWOBB_LOWER_BREAKDOWN";
						oe = new ObservedEvent(eventName,eventType,CurrentBar);
						oe.breakoutLevel = TNKIRLow;
					}else{
						//No cross event was oberved
						// Do nothing
					}
				}else{
					//InitialRange setting was incomplete
					LogError("ObserveInitialRangeBreakoutWithoutBreakBack()",MemoND("Invalid Initial Range. TNKIRHigh",TNKIRHigh),MemoND("TNKIRLow",TNKIRLow));
				}
			}
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}			
			//
			return eventList;
		}

		//========================
		//  Observer for Initial Range Breakout
		//   Events:EN_IRB_UPPER_BREAKUP, EN_IRB_LOWER_BREAKDOWN
		//   v1.0 2017/11/30
		//========================
		public List<ObservedEvent> ObserveInitialRangeBreakout(){
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			// Execute condition

			// Prameters and Vars
			string eventType ="IRB";
			string eventName = null;
			ObservedEvent oe = null;
			// Reset IR values at start of session
			if(Bars.IsFirstBarOfSession) {
				TNKIRHigh = double.MinValue;
				TNKIRLow  = double.MaxValue;
				TNKIRValid = false;
				LogMessage("ObserveInitialRangeBreakout()",TAGMsg("InitialRange was reset."));
			}else{
				//Do nothing
			}
			
			//Judge Condition
			double value = Close[0];
			//double highValue = Math.Max(Open[0],Close[0]);
			//double lowValue  = Math.Min(Open[0],Close[0]);
			double highValue = High[0];
			double lowValue = Low[0];
			if (IsInInitial()){
				//Mark Initial Term to chart 
				MarkInitialTerm(true);
				//Record Initail Range
				TNKIRHigh = Math.Max(TNKIRHigh, highValue);
				TNKIRLow  = Math.Min(TNKIRLow , lowValue);
				if(!TNKIRValid && TNKIRHigh > double.MinValue && TNKIRLow < double.MaxValue){
					TNKIRValid = true;
				}else{
					//Do nothing
				}
			}else{
				//Unmark Initial Term to chart 
				MarkInitialTerm(false);
				// Observe Breakout from Initial Range
				if(TNKIRValid){
					//Mark Initial Range to chart
					MarkHolizontalLine(TNKIRHigh,Brushes.Red);
					MarkHolizontalLine(TNKIRLow, Brushes.Blue);
					//
					//Check Breakout from Initial range
					bool crossAbove = CrossAbove(Close,TNKIRHigh,1) && IsPositiveBar(0);
					bool crossBelow = CrossBelow(Close,TNKIRLow,1)  && IsNegativeBar(0);
					//
					if(crossAbove) {
						eventName = "EN_IRB_UPPER_BREAKUP";
						oe = new ObservedEvent(eventName,eventType,CurrentBar);
					}else if(crossBelow){
						eventName = "EN_IRB_LOWER_BREAKDOWN";
						oe = new ObservedEvent(eventName,eventType,CurrentBar);
					}else{
						//No cross event was oberved
						//----------------
						// Observe momentum event for IR
						// EventName: EN_IR_UPMOMENTUM, EN_IB_DOWNMOMENTUM
						// If No brakout take place, Observe 
						// And if 
						//   VB_High value placed upper side of SMA and Close placed upper side of VB-Up then event "EN_IR_UP_MOMENTUM"
						// Else if
						//   VB_Low value place downside side of SMA and Close placed lower side of VB-Down then event "EN_IR_DOWN_MOMENTUM"
						int vbPeriod = 14;
						int smaPeriod = 72;
						double factor = 1.4;
						if (eventList.Count == 0){
							double vb_high = Bollinger(factor, vbPeriod).Upper[0];
							double vb_low  = Bollinger(factor, vbPeriod).Lower[0];
							double sma = SMA(smaPeriod)[0];
							double close = Close[0];
							bool upMomentum = false;
							bool downMomentum = false;

							// Check VBand does not  include SMA
							// Check Up Momentum
							upMomentum = (close > TNKIRHigh) && (close > vb_high) && IsPositiveBar(0);
							
							// Check Down Memetum
							downMomentum = (close < TNKIRLow) && (close < vb_low) && IsNegativeBar(0);
							
							if (upMomentum){
								//Make event EN_IR_UP_MOMENTUM
								eventName = "EN_IR_UP_MOMENTUM";
								oe = new ObservedEvent(eventName,eventType,CurrentBar);
							}else if (downMomentum){
								//Make event EN_IR_DOWN_MOMENTUM
								eventName = "EN_IR_DOWN_MOMENTUM";
								oe = new ObservedEvent(eventName,eventType,CurrentBar);
							} else {
								//Do nothing
							}
						}
						//----------------
					}
				}else{
					//InitialRange setting was incomplete
					LogError("ObserveInitialRangeBreakout()",MemoND("Invalid Initial Range. TNKIRHigh",TNKIRHigh),MemoND("TNKIRLow",TNKIRLow));
				}
			}

			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			// Finish
			return eventList;
		}
		public bool IsInInitial(){
			//Day Session 8:45 - 15:10    Night Session 16:30 - 5:25
			int init1StartMin =  8 * 60 + 45;       //08:45 
			int init1EndMin   = init1StartMin + 60; //09:45
			int init2StartMin = 16 * 60 + 30;       //16:30 
			int init2EndMin   = init2StartMin + 60; //17:30

			DateTime currentTime = Bars.GetTime(CurrentBar);
			int currentMin = currentTime.Hour * 60 + currentTime.Minute;
			bool isIn1 = false;
			bool isIn2 = false;
			isIn1 = currentMin >= init1StartMin && currentMin <= init1EndMin;
			isIn2 = currentMin >= init2StartMin && currentMin <= init2EndMin;
			LogDebug("IsInInitial()",MemoND("currentMin",currentMin),MemoND("init1StartMin",init1StartMin),MemoND("init1EndMin",init1EndMin),MemoNB("isIn1",isIn1));
			
			//return isIn1;  //Check only day session.
			return isIn1 || isIn2;  //Check both day and night session.
		}
		
		//========================
		//  Observer for Crossing Volatility Band and SMA72
		//   Events:EN_VAC_HIGH_CROSS_UP, EN_VAC_LOW_CROSS_DOWN, 
		//          EN_VAC_UP_MOMENTUM, EN_VAC_DOWN_MOMENTUM
		//   v1.0 2017/08/17
		//========================
		public List<ObservedEvent> ObserveVBandMACross(){
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			//Parameters
			int vbPeriod = 14;
			int smaPeriod = 72;
			double highSideFactor = 1.4;
			double lowSideFactor = 1.4;
			//
			// if bar count under period then abort observeation
			if(CurrentBar < Math.Max(vbPeriod, smaPeriod)) {return eventList;}
			//
			string eventType ="VAC";
			string eventName = null;
			ObservedEvent oe = null;
			
			//Judge Condition
			//  Chek if VB High cross up
			bool crossAbove   = CrossAbove(Bollinger(highSideFactor, vbPeriod).Upper, SMA(smaPeriod), 1);
			bool isVBHighCrossUpMA = crossAbove && IsPositiveBar(0); // && bottleNeck
			//  Check if  VB Low cross down
			bool crossBelow = CrossBelow(Bollinger(lowSideFactor, vbPeriod).Lower, SMA(smaPeriod), 1);
			bool isVBLowCrossDownMA = crossBelow && IsNegativeBar(0); // && bottleNeck 
			
			//
			if(isVBHighCrossUpMA){
	   			eventName = "EN_VAC_HIGH_CROSS_UP";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			} else if(isVBLowCrossDownMA){
   				eventName = "EN_VAC_LOW_CROSS_DOWN";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			}else{
				//No event is observed.
			}
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			//----------------
			// Observe momentum event of VB and SMA
			// EventName: EN_NB_UPMOMENTUM, EN_NB_DOWNMOMENTUM
			// If No brakout take place, Observe 
			// And if 
			//   VB_High value placed upper side of SMA and Close placed upper side of VB-Up then event "EN_VAC_UP_MOMENTUM"
			// Else if
			//   VB_Low value place downside side of SMA and Close placed lower side of VB-Down then event "EN_VAC_DOWN_MOMENTUM"
			
			if (eventList.Count == 0){
				double vb_high = Bollinger(highSideFactor, vbPeriod).Upper[0];
				double vb_low  = Bollinger(lowSideFactor, vbPeriod).Lower[0];
				double sma = SMA(smaPeriod)[0];
				double close = Close[0];
				bool upMomentum = false;
				bool downMomentum = false;

				// Check VBand does not  include SMA
				// Check Up Momentum
				upMomentum = (vb_high > sma) && (vb_low > sma)&& (close > vb_high) && IsPositiveBar(0);
				
				// Check Down Memetum
				downMomentum = (vb_low < sma) && (vb_high < sma) && (close < vb_low) && IsNegativeBar(0);
				
				if (upMomentum){
					//Make event EN_VAC_UP_MOMENTUM
					eventName = "EN_VAC_UP_MOMENTUM";
					oe = new ObservedEvent(eventName,eventType,CurrentBar);
				}else if (downMomentum){
					//Make event EN_VAC_DOWN_MOMENTUM
					eventName = "EN_VAC_DOWN_MOMENTUM";
					oe = new ObservedEvent(eventName,eventType,CurrentBar);
				} else {
					//Do nothing
				}
				if (oe != null){
					eventList.Add(oe);
				}
			}
			//----------------
			return eventList;
		}
		
		//========================
		//  Observer for Continuous Positive/Negative Bar
		//   Events:EN_CONTINUOUSBARS_POSITIVE,EN_CONTINUOUSBARS_NEGATIVE
		//   v1.0 2017/02/17
		//========================
		public List<ObservedEvent> ObserveContinuousBar(){
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			// if bar count unser 3 then abort observeation
			if(CurrentBar < 3) {return eventList;}
			//
			string eventType ="CD";
			string eventName = null;
			ObservedEvent oe = null;
			//Judge Condition
			bool isContinuousPositive = IsPositiveBar(0) && IsPositiveBar(1) &&IsPositiveBar(2);
			bool isContinuousNegative = IsNegativeBar(0) && IsNegativeBar(1) &&IsNegativeBar(2);
			//
			if(isContinuousPositive){
	   			eventName = "EN_CD_CONTINUOUS_POSITIVE";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			} else if(isContinuousNegative){
   				eventName = "EN_CD_CONTINUOUS_NEGATIVE";
				oe = new ObservedEvent(eventName,eventType,CurrentBar);
			}else{
				//No event is observed.
			}
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			return eventList;
		}
		
		
		//========================
		//  Observer for Big Bar Event
		//   v1.0 2015/09/15
		//   v1.1 2016/?/?
		//========================
		public List<ObservedEvent> ObserveBigBar(){
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			ObservedEvent oe = null;
			string evtType ="BB";
			string eventName = "";
			//string evtBNB = "EN_BB_BIGNEGATIVEBAR";
			//string evtBPB = "EN_BB_BIGPOSITIVEBAR";

			//
			// Judge conditions
			//
			//int type = 0; //0:All big bar
			//int type = 1; //No breakout,
			//int type = 2; //Normalized Breakout(NBO) within PBBand
			int type = 3; //NBO && PBBand Breakout //Best perfomance in BB_CounterSell signal.
			string judge = JudgeBigBar(type);
			switch(judge){
				case "BIGBAR_POSITIVE":
					//Make event EN_BB_BIGPOSITIVEBAR
					eventName = "EN_BB_BIGPOSITIVEBAR";
					oe = new ObservedEvent(eventName,evtType,CurrentBar);
					break;
				case "BIGBAR_NEGATIVE":
					//Make event EN_BB_BIGNEGATIVEBAR
					eventName = "EN_BB_BIGNEGATIVEBAR";
					oe = new ObservedEvent(eventName,evtType,CurrentBar);
					break;
				case "NOBIGBAR":
					//No Big bar event
					//  Do nothing.
					break;
				default:
					LogError("ObserveBigBar()",MemoNS("Illecal judge string",judge));
					break;
			}
			
			// Add event if took place.
			if (oe != null){
				eventList.Add(oe);
			}
			return eventList;
		}

		//==============
		//  Observer for Normalized Brakout method
		//    Events:EN_NB_UPPERBREAKUP,EN_NB_LOWERBREAKDOWN,
		//           EN_NB_UPMOMENTUM, EN_NB_DOWNMOMENTUM
		//    v1.2 2015/09/10  Refactor 2016/11/03
		//    v1.3 2016/10/05 Remove Botttleneck/BandExpanding condition check. Due to degrade performance.
		//==============
		public List<ObservedEvent> ObserveNormalizedBreakout(){
			List<ObservedEvent> eventList = new List<ObservedEvent>();
			ObservedEvent oe = null;
			string type = "NB";
			string eventName = "";
			int period = 14;
			//double boFactor = 1.0; 
			double boFactor = TNKNormalizedBreakoutBorder; //v2.2.12
			double upperVal =  1.0 * boFactor;  //Sigma Range upper  1.0 means Using TNKNormalizedBreakoutBorder asis.
			double lowerVal = -1.0 * boFactor;  //Sigma Range Lower -1.0 means Using TNKNormalizedBreakoutBorder asis.
			
			// Don't adopt below values to avoid curve fitting.
			//double upperVal =  1.5; // Best value in v2.2.12
			//double lowerVal = -1.3; // Best value in v2.2.12
				
			//
			// Observe first if Close value Break out(cross) sigma range 
			//
			bool crossAbove   = CrossAbove(INDNormalizedClose(period, boFactor), upperVal, 1);
			bool breakUp = crossAbove && IsPositiveBar(0); // && bottleNeck
			
			bool crossBelow = CrossBelow(INDNormalizedClose(period, boFactor), lowerVal, 1);
			bool breakDown = crossBelow && IsNegativeBar(0); // && bottleNeck 
			
			//
			if (breakUp){
				//Make event EN_NB_UPPERBREAKUP
				eventName = "EN_NB_UPPERBREAKUP";
				oe = new ObservedEvent(eventName,type,CurrentBar);
				oe.breakoutLevel = upperVal;
			}else if (breakDown){
				//Make event EN_NB_LOWERBREAKDOWN
				eventName = "EN_NB_LOWERBREAKDOWN";
				oe = new ObservedEvent(eventName,type,CurrentBar);
				oe.breakoutLevel = lowerVal;
			} else {
				//Do nothing
			}
			if (oe != null){
				eventList.Add(oe);
			}

			//
			// If No brakout take place, Observe if 
			//   close value places outside of sigama range
			//   AND this bar is Positive (negative) in  upper(lower) than upperVal(lowerVal).
			// EventName: EN_NB_UPMOMENTUM, EN_NB_DOWNMOMENTUM
			if (eventList.Count == 0){
				double normalizedClose = INDNormalizedClose(period, boFactor)[0];
				bool upMomentum = false;
				bool downMomentum = false;
				
				// Check Up Momentum
				upMomentum = (normalizedClose > upperVal) && IsPositiveBar(0);
				
				// Check Down Memetum
				downMomentum = (normalizedClose <lowerVal) && IsNegativeBar(0);
				
				if (upMomentum){
					//Make event EN_NB_UPMOMENTUM
					eventName = "EN_NB_UPMOMENTUM";
					oe = new ObservedEvent(eventName,type,CurrentBar);
				}else if (downMomentum){
					//Make event EN_NB_DOWNMOMENTUM
					eventName = "EN_NB_DOWNMOMENTUM";
					oe = new ObservedEvent(eventName,type,CurrentBar);
				} else {
					//Do nothing
				}
				if (oe != null){
					eventList.Add(oe);
				}
			}
			
			return eventList;	
		}
		
		//==============
		//  Observer for MarketCondition base stategys (since Eary development)
		//    Events:<STATENAME>:<EVENTNAME>
		//    Moved from TNKSetupProcedures.cs at 2017/01/13
		//==============
		public List<ObservedEvent> ObserveMarketCondition(){
			LogDebug("ObserveMarketCondition()",MemoNS("I'm here",""));
			int lookBackPeriod = 1; //for detecting break events.
			double cl = Close[0];
			double pv = PeakValue(1);
			double bv = BottomValue(1);
			List<ObservedEvent> eventList;
			
			// Wait until both peak and bottom is set.
			if (TNKRecentPeakValue==0.0 || TNKRecentBottomValue==0.0) { //check initialization
				LogDebug("ObserveMarketCondition()","Peak or Bottom is 0.0");
				if(pv>0) TNKRecentPeakValue = pv;
				if(bv>0) TNKRecentBottomValue = bv;
				TNKMCObservedEventNameList = new List<string>{"NOEVENT","NOEVENT"};
				eventList = new List<ObservedEvent>();
				eventList.Add(new ObservedEvent(TNKMCObservedEventNameList[0],"MC",CurrentBar));
				eventList.Add(new ObservedEvent(TNKMCObservedEventNameList[1],"MC",CurrentBar));
				return eventList;
			}
			// Setup margins for event observe.
			SetupEventObserveMargins();
			
			string    observedEventName = "NOEVENT";
			List<string> observedEventNameList = new List<string>(); //Empty event list

			//
			// Check update event
			//
			// Mark PeakValue and Bottom Value on chart
			//  MarkHolizontalLine(string tag, double y, Color color )
			if(pv==TNKRecentPeakValue){
				//NT7 MarkHolizontalLine(TNKRecentPeakValue,   Color.Red );
				MarkHolizontalLine(TNKRecentPeakValue,   Brushes.Red );
			}
			if (bv==TNKRecentBottomValue){
				//NT7 MarkHolizontalLine(TNKRecentBottomValue, Color.Blue);
				MarkHolizontalLine(TNKRecentBottomValue, Brushes.Blue);
			}
			//
			// Check Peak/Bottom update patterns
			//
			if (pv > TNKRecentPeakValue + TNKMarginPeakUp){ //Peak Up
				TNKPreviousPeakValue = TNKRecentPeakValue;
				TNKRecentPeakValue = pv;
				observedEventName = "PEAKUP";
			} else if (bv < TNKRecentBottomValue - TNKMarginBottomDown){ // Bottom Down
				TNKPreviousBottomValue = TNKRecentBottomValue;
				TNKRecentBottomValue = bv;
				observedEventName = "BOTTOMDOWN";
			} else if (pv < TNKRecentPeakValue - TNKMarginPeakDown){ 	// Peak Down
				TNKPreviousPeakValue = TNKRecentPeakValue;
				TNKRecentPeakValue = pv;
				observedEventName = "PEAKDOWN";
			} else if (bv > TNKRecentBottomValue + TNKMarginBottomUp){ 	// Bottom Up
				TNKPreviousBottomValue = TNKRecentBottomValue;
				TNKRecentBottomValue = bv;
				observedEventName = "BOTTOMUP";
			}
			TNKSwingRange = TNKRecentPeakValue - TNKRecentBottomValue; // Record Range Width
		
			//Compensate event order in UP/DOWN state. Remove redundant "UPDATE" event.
			// If breakout PEAKBREAKUP or BOTTOMBREAKDOWN event is found in detection lag period,
			//  consider observed update event convert to NOEVENT to keep current UP/DOWN state.
			//  This code does not assume that eventName is "PEAKBREAKUP". Use event category
			if (observedEventName != "NOEVENT" &&(TNKStateNameCurrent=="UP" || TNKStateNameCurrent=="DOWN")){
				bool cancelUpdeateEvent = false;
				string eventName="";
				// Collect event list within TNKSwingPeriod
				List<ObservedEvent> elist = CollectEvents("MC",TNKSwingPeriod);
				foreach(ObservedEvent oe in elist){
					eventName = oe.ConditionEvent();
					//
					//LogDebug("ObserveMarketCondition() RemoveLateDetectedEvent",MemoNS("oe",oe.Description()));
					if(IsBreakEvent(eventName)){
						if(TNKStateNameCurrent=="UP" && IsUpSideEvent(eventName)){
							cancelUpdeateEvent=true;
							break;
						}else if (TNKStateNameCurrent=="DOWN" && IsDownSideEvent(eventName)){
							cancelUpdeateEvent=true;
							break;
						}else{
							//No along brakout
							cancelUpdeateEvent = false;
						}
					}else{
						//No Breakout Event
						cancelUpdeateEvent = false;
					}
				}
				if(cancelUpdeateEvent){
					LogMessage("ObserveMarketCondition()",
						TAGMsg("RemoveLateDetectedEvent"),
						MemoNI("CurrentBar",CurrentBar),
						MemoNI("TNKSwingPeriod",TNKSwingPeriod),
						MemoNS("Found break eventName",eventName),
						MemoNS("Canceled update eventName",observedEventName)
					);
					observedEventName = "NOEVENT";
				}else{
					//Update event is valid. Do not override event.
				}
			}		
			// Lower index in EventList has higher priority event.
			// Update events are higher priority then Break events.
			//REMARK:EVENTLIST
			observedEventNameList.Insert(0,observedEventName); //Record Update Event to list first.

			//
			// Check update event
			//
			LogDebug("ObserveMarketCondition()",
				MemoND("TNKRecentPeakValue",TNKRecentPeakValue),
				MemoND("TNKRecentBottomValue",TNKRecentBottomValue),
				MemoND("TNKSwingRange",TNKSwingRange)
			); 
			
			// Check Peak/Bottom Break and Reversal patterns	
			observedEventName = "NOEVENT";
			//Break Events
			if (CrossAbove(Close, TNKRecentPeakValue + TNKMarginPeakBreakUp,lookBackPeriod)){ //Peak Break Up
				TNKRecentPeakBreakUpValue = TNKRecentPeakValue; //Receord Recent Paeak break out value. Prev Resist Lline.
				observedEventName = "PEAKBREAKUP";
			} else if (CrossBelow(Close,TNKRecentPeakValue - TNKMarginPeakBreakDown,lookBackPeriod)){ // Peak Break Down
				observedEventName = "PEAKBREAKDOWN";
			} else if (CrossAbove(Close,TNKRecentBottomValue + TNKMarginBottomBreakUp,lookBackPeriod)){ // Bottom Break Up
				observedEventName = "BOTTOMBREAKUP";
			} else if (CrossBelow(Close,TNKRecentBottomValue - TNKMarginBottomBreakDown,lookBackPeriod)){ // Bottom Break Down
				TNKRecentBottomBreakDownValue = TNKRecentBottomValue; //Receord Recent Bottom break out value. Prev Support Line.
				observedEventName = "BOTTOMBREAKDOWN";
			} else { 
				//Reversal Events
				double reversalThreshold = PeakReversalThreshold(TNKThresholdReversalRatio);
				if (reversalThreshold !=0.0 && CrossBelow(Close,reversalThreshold,lookBackPeriod)){
					//PeakReversal
					LogDebug("ObserveMarketCondition()",TAGMsg("PeakReversal"),MemoND("reversalThreshold",reversalThreshold));
					observedEventName = "PEAKREVERSAL";
				} else {
					reversalThreshold = BottomReversalThreshold(TNKThresholdReversalRatio);
					if (reversalThreshold !=0.0 && CrossAbove(Close,reversalThreshold,lookBackPeriod)){
						//BottomRevesal
						LogDebug("ObserveMarketCondition()",TAGMsg("BottomkReversal"),MemoND("reversalThreshold",reversalThreshold));
						observedEventName = "BOTTOMREVERSAL";
					}
				}
			}

			// Lower index in EventList has higher priority event.
			// Break events are Lower priority then Update events.
			//REMARK:EVENTLIST
			observedEventNameList.Insert(1,observedEventName);
			
			// Prevent to repeat reporting same event occation.
			//  Update event observation use static comparation rather than across.
			//  So, it need to supress repeation of same update event.
			//REMARK:EVENTLIST
			if (TNKMCObservedEventNameList == null){
				TNKMCObservedEventNameList = observedEventNameList;
			} else {
				//Detect event name change.
				TNKMCObservedEventNameList = ReplaceNoChangeEventWithNOEVENT(TNKMCObservedEventNameList,observedEventNameList);
			}

			LogDebug("ObserveMarketCondition()",
				MemoNS("TNKMCObservedEventNameList[0]",TNKMCObservedEventNameList[0]),
				MemoNS("TNKMCObservedEventNameList[1]",TNKMCObservedEventNameList[1])
			);
			
			
			// Make Event List from Event Name List.
			List<string> eventNameList = RemoveRedundantNOEVENT(TNKMCObservedEventNameList);
			LogDebug("ObserveMarketCondition()",MemoNI("eventNameList.Count",eventNameList.Count));
			eventList = new List<ObservedEvent>();

			//TODO: observedEventName contain just eventname. statename does not includeid.
			//TODO: concatinate statename and event name here
			foreach(string name in eventNameList){
				LogDebug("ObserveMarketCondition()",MemoNS("name",name));
				eventList.Add(new ObservedEvent(name,"MC",CurrentBar));
			}
			//TODO: Process state transition here
			foreach(ObservedEvent oe in eventList){
				//TODO: move to ovserver this part. vvvvvvv
				// Event is obsered by Market Condition Observer
				string eventName = oe.eventName;
				TNKPreviousEvenName = TNKCurrentEventName;
				TNKCurrentEventName = eventName;
				string conditionName = ConditionName(TNKStateNameCurrent,eventName);　
				oe.eventName = conditionName; //HOT: Override enentName with conditionName
				LogDebug("GenerateTradeSpecList()",
					MemoNS("overrided oe.event with conditionName",oe.eventName),MemoND("Close",Close[0])
				);
				
				//Dispach Condition Handlers based on condition name
				//posSpec = DispatchEventToHandlers(oe); //Change type of input and return values
				//Mark event on chart
				if (TNKFlagMarkMCMarkers){
					MarkEventNameOnChart(conditionName);　//Ommit MC Event on chart if TNKFlagMarkMCMarkers==false 
				}
				//Update trend
				//TransitState(nextState,conditionName);　//TBDel
				TransitState("NOTSPECIFIED",conditionName); //State Transision does not refer return value from Condition Handlers no longer.
				
				//Mark Ternd on Chart
				MarkTrendAsColor(TNKStateNameCurrent);
				//TODO: move to observer this part ^^^^^^^^
			}
			return eventList;
		}
		
	}
}
//============================================
// Going to be Obsolete
		//==============
		//  Observer for Devil's Money Collection
		//   Pending in NT8 porting. This needs porting indicator INDBarsFromNBreakout()
		//==============
//		public List<ObservedEvent> ObserveDevilsMoneyCollection(){
//			List<ObservedEvent> eventList = new List<ObservedEvent>();
//			ObservedEvent oe = null;
//			string eventName = "";
//			string type ="DC";
//			//string evtBNB = "EN_DC_UP";
//			//string evtBPB = "EN_DC_DOWN";
//			string dmcExitName = "Stop loss";

//			//See Recent closed trade.
			
//			int tradeCount = SystemPerformance.AllTrades.Count;
//			if(tradeCount <= 0){
//				//No trade is there yet.
//				return eventList;
//			}
//			Trade lastTrade = SystemPerformance.AllTrades[tradeCount - 1];
//			string entryName = lastTrade.Entry.Name;
//			string entryTime = TimeStamp(lastTrade.Entry.Time);
//			string positionType = lastTrade.Entry.MarketPosition.ToString();
//			string exitName = lastTrade.Exit.Name;
//			string exitTime = TimeStamp(lastTrade.Exit.Time);
//			double profit = lastTrade.ProfitCurrency * lastTrade.Quantity;
//			int barsSinceExit = BarsSinceExitExecution(dmcExitName);
			
//			//check if the trade was lose
//			bool isLose = profit < 0;
			
//			//check if the trade's entry event is UPPERBREAKOUT or LOWERBREAKOUT or UPMOMENTUM or DOWNMOMENTUM
//			//List<string> dmcEnryNames = new List<string>{"EN_NB_UPPERBREAKUP","EN_NB_LOWERBREAKDOWN","EN_NB_UPMOMENTUM","EN_NB_DOWNMOMENTUM"};
//			bool isUpside = entryName == "EN_NB_UPPERBREAKUP" || entryName == "EN_NB_UPMOMENTUM";
//			bool isDownside = entryName == "EN_NB_LOWERBREAKDOWN" || entryName == "EN_NB_DOWNMOMENTUM";
				
//			//check if the trade's exit bar is within 2 bars
//			bool isNearBar = barsSinceExit <= 0;　//HOT
			
//			//check if BarsFromBreakout lower than (? 3,4,5) //
//			double boBorder = TNKNormalizedBreakoutBorder;
//			bool isNotInTrend = INDBarsFromNBreakout(boBorder,14)[0] <= 0 ; //HOT
			
//			//if all abobe conditions are true, 
//			bool isDMC = isLose && (isUpside || isDownside) && isNearBar && isNotInTrend;
//			//  if both the entry bar and this bar are positive, found event is "EN_DC_UP"
//		    //  if both the etnry bar and this bar are negative, found event is "EN_DC_DOWN"
//			if (isDMC) {
//				if(isUpside){
//					eventName = "EN_DC_UP";
//					oe = new ObservedEvent(eventName,type,CurrentBar);
//					oe.addtionalData = lastTrade.Entry.Price;
//				} else if (isDownside){
//					eventName = "EN_DC_DOWN";
//					oe = new ObservedEvent(eventName,type,CurrentBar);
//					oe.addtionalData = lastTrade.Entry.Price;
//				}else{
//					//Error
//					LogError("ObserveDevilsMoneyCollection()",MemoNB("Inconsistecy condition, isDMC",isDMC),
//					　　　　　　　　MemoNB("isUpside",isUpside),MemoNB("isDownside",isDownside));
//				}
//			}
//			if(oe != null){
//				eventList.Add(oe);
//			}
//			return eventList;
//		}
