//#region Using declarations
//using System;
//using System.ComponentModel;
//using System.Collections.Generic;
//using System.Drawing;
//using NinjaTrader.Cbi;
//using NinjaTrader.Data;
//using NinjaTrader.Indicator;
//using NinjaTrader.Strategy;
//#endregion
#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.Gui.Tools;
#endregion

// This namespace holds all strategies and is required. Do not change it.
//namespace NinjaTrader.Strategy
namespace NinjaTrader.NinjaScript.Strategies
{
    /// <summary>
    /// This file holds all user defined strategy methods.
    /// </summary>
    partial class Strategy
    {
		//===
		//  General Event Handlers dispathed with TNKEventHandlerNameWithDict
		//     Described in TNKConditionHandlerNameDict
		//===
		
		// 
		// Template for Event Handler
		//  2019/05/30 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_EVENTCATEGORY_ACTIONNAME(ObservedEvent oe){
			LogDebug("EHG_EVENTCATEGORY_ACTIONNAME",MemoNS("Enter EHG_EVENTCATEGORY_ACTIONNAME with event",oe.eventName));
			//Initialize TradeSpec as return value.
			TradeSpec ts = null;
			// Correct enviromental vaues
			double tpd = Move(0); // Target Price DELTA is move of this bar. DELTA is positive value usual.
			
			//Dispach with event name to decide action
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_EVENTCATRGORY_EVENTNAME1":
					ts = TSG_TakeDefaultLong(oe); //Follow or Counter position
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_EVENTCATRGORY_SIGNALNAME1";
					ts.priority = 4;  //This trade has higer priority than default.
					break;
				case "EN_EVENTCATRGORY_EVENTNAME2":
					ts = TSG_TakeDefaultShort(oe); //Follow or Counter position
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_EVENTCATRGORY_SIGNALNAME2";
					ts.priority = 4; //This trade has higer priority than default.
					break;
				default:
					LogError("EHG_EVENTCATRGORY_ACTIONNAME()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			return ts;
		}

		// 
		// Event Handler for TWD(Trade Day of Week) Verification 
		//  2019/05/30 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_VERIF_TWD(ObservedEvent oe){
			LogDebug("EHG_VERIF_TWD",MemoNS("Enter EHG_VERIF_TWD with event",oe.eventName));
			//Initialize TradeSpec as return value.
			TradeSpec ts = null;
			// Correct enviromental vaues
			double tpd = Move(0); // Target Price DELTA is move of this bar. DELTA is positive value usual.
			
			//Dispach with event name to decide action
			string buySell = "_BUY";
			//string buySell = "_SELL";
			string eventName = oe.eventName + buySell;
			switch (eventName){
				case "EN_VERIF_SESSION_BIGINNING_BUY":
					ts = TSG_TakeDefaultLong(oe); //Follow or Counter position
					LogDebug("EHG_VERIF_TWD()",MemoNS("TSG_TakeDefaultLong(oe)","Finished"));
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_VERIF_TWD_BUY";
					ts.priority = 4;  //This trade has higer priority than default.
					ts.size = 1;
					ts.StopTrail = null;
					ts.TargetTrail = null;
					break;
				case "EN_VERIF_SESSION_BIGINNING_SELL":
					ts = TSG_TakeDefaultShort(oe); //Follow or Counter position
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_VERIF_TWD_SELL";
					ts.priority = 4; //This trade has higer priority than default.
					ts.size = 1;
					ts.StopTrail = null;
					ts.TargetTrail = null;
					break;
				default:
					LogError("EHG_VERIF_TWD()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			LogDebug("EHG_VERIF_TWD()",MemoNS("Leaving this facility",""));
			return ts;
		}
		
		// 
		// Volatility Channel Breakout 
		//  2018/11/21 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_VCBO_SimpleBuyAndSell(ObservedEvent oe){
			//Initialize TradeSpec as return value.
			TradeSpec ts = null;
			//Switches
			bool swtUseTarget = false;
			//Calculate risk
			double riskFactor = 1.0;   //Ratio for how much risk will take this trade.
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double riskToTake = unitRisk * riskFactor;

			// Correct enviromental vaues
			//double tpd = Move(0); // Target Price DELTA is move of this bar. DELTA is positive value usual.
			//PARAMS
			int period = TNKVolatilityChannelPeriod;              //14
			double with = TNKVolatilityChannelBreakoutBorder; //1.4
			VolatilityChannel vc = new VolatilityChannel(this,period,with); 
			//
			double initialStopPrice, initialStopDelta=0;
			// Target price
			double sigma = StdDev(period)[0]; 
			double tdpFactor = 15.0; // 15 sigma
			double tpd = sigma * tdpFactor; 

			//Dispach with event name to decide action
			PositionRisk pr;
			double delta;
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_VCBO_UPPERBREAKUP":
					ts = TSG_TakeDefaultLong(oe); //Follow or Counter position
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_VCBO_BUY_FOLLOW";
					ts.priority = 4;  //This trade has higer priority than default.
					// Calculate initialStopDelte and Size based on BO level and Unit Risk
					delta = Close[0] -  vc.LowerValue(0);  //Decide stop price first. Lowwer side border of VC
					pr = CalculatePositionRiskWithStopDelta(riskToTake, delta); //New method for caluculate trade risk
					initialStopDelta = pr.StopDelta;
					ts.size = pr.Size;
					initialStopPrice = Close[0] - initialStopDelta;	
					ts.ResetInitialStopTrail(initialStopPrice,initialStopDelta);
					// Set StopTrail as TrailVolatilityChannel
					//ts.StopTrail = new TrailVolatilityChannel(this, ts.signal, initialStopPrice, initialStopDelta, vc);
					ts.StopTrail = new TrailConstantLevel(this,"STOP", ts.signal, initialStopPrice); //No trail trailer
					//ts.StopTrail = null; //Disable stop trail
					ts.TargetTrail = null; //Disable target
					if(swtUseTarget) ts.TargetTrail = new TrailConstantLevel(this, ts.signal, Close[0]+tpd);
					break;
				case "EN_VCBO_LOWERBREAKDOWN":
					ts = TSG_TakeDefaultShort(oe); //Follow or Counter position
					ts.tactics = "FOLLOW";
					ts.signalName = "SN_VCBO_SELL_FOLLOW";
					ts.priority = 4; //This trade has higer priority than default.
					// Calculate initialStopDelte and Size based on BO level and Unit Risk
					delta = vc.UpperValue(0) - Close[0];  //Decide stop price first. Lowwer side border of VC
					pr = CalculatePositionRiskWithStopDelta(riskToTake, delta); //New method for caluculate trade risk
					initialStopDelta = pr.StopDelta;
					ts.size = pr.Size;
					initialStopPrice = Close[0] + initialStopDelta;	
					ts.ResetInitialStopTrail(initialStopPrice,initialStopDelta);
					// Set StopTrail as TrailVolatilityChannel
					//ts.StopTrail = new TrailVolatilityChannel(this, ts.signal, initialStopPrice, initialStopDelta, vc);
					ts.StopTrail = new TrailConstantLevel(this,"STOP", ts.signal, initialStopPrice); //No trail trailer
					//ts.StopTrail = null; //Disable stop trail
					ts.TargetTrail = null; //Disable target
					if(swtUseTarget) ts.TargetTrail = new TrailConstantLevel(this, ts.signal, Close[0]-tpd);
					break;
				default:
					LogError("EHG_EVENTCATRGORY_ACTIONNAME()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			if(ts.StopTrail != null){
				LogDebug("EHG_VCBO_SimpleBuyAndSell()",MemoNS("ts.StopTrail.ClassName()",ts.StopTrail.ClassName()));
			}else{
				LogDebug("EHG_VCBO_SimpleBuyAndSell()",MemoNS("ts.StopTrail","null"));
			}
			return ts;
		}		
		
		// 
		// Initial Range Breakout
		//  2018/08/06 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_IRBOWOBB_SimpleBuyAndSell(ObservedEvent oe){
			//PARAMS
			int    boMarginByTic = 4;//2; Breakout Margin (tic)
			double riskFactor = 0.3;   //Ratio for how much risk will take this trade.
			
			// Correct enviromental vaue
			double sigma = StdDev(14)[0]; 
			double tdpFactor = 5.0;
			double tpd = sigma * tdpFactor; // Target Price DELTA is Sigma * 2.0 //TEST
			int sleepPeriod = 5; //Bars

			//Calculate risk
			double unitRisk = CalcUnitRisk(TNKTradeUnit);
			double riskToTake = unitRisk * riskFactor;

			//Initialize TradeSpec as return value.
			TradeSpec result = null;

			//LogDebug("EHG_IRBOWOBB_SimpleBuyAndSell()",MemoNS("lastExitName",lastExitName),
			//	MemoNB("IsProfitTarget()",IsProfitTarget(lastExitName)),MemoNB("IsStopLoss()",IsStopLoss(lastExitName)),MemoNB("IsSessionClose()",IsSessionClose(lastExitName))
			//);
			
			//Dispach with event name to decide action
			double initialStopPrice=0.0, initialStopDelta=0.0;
			PositionSize ps = null;
			PositionRisk pr = null;

			Trail newStopTrail;
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_VCBO_UPPERBREAKUP":
				case "EN_IRBOWOBB_UPPER_BREAKUP":
					result = TSG_TakeDefaultLong(oe); //Follow or Counter position
					result.tactics = "FOLLOW";
					//result.signalName = "SN_IRBOWOBB_BUY_FOLLOW";  //Does this need?
					result.priority = 3;  //This trade has higer priority than default.
					//Set Stop on breakout level, TNKIRHigh.
					// Calculate initialStopDelte and Size based on BO level nad Unit Risk
					initialStopPrice = TNKIRHigh - boMarginByTic * TickSize ;  //Decide stop price first.
					initialStopDelta = Close[0] - initialStopPrice;            //Then caliculate Delta.
					//ps = CalculatePositionSizeWithSLD(initialStopDelta);
					pr = CalculatePositionRiskWithStopDelta(riskToTake, initialStopDelta); //New method for caluculate trade risk
					initialStopDelta = pr.StopDelta;
					initialStopPrice = Close[0] - initialStopDelta;
					// Set new initialStopDelta and Size.
					result.ResetInitialStopTrail(initialStopPrice,initialStopDelta);
					//result.size = ps.Size;
					result.size = pr.Size;
					if(result.StopTrail!=null){result.StopTrail.Sleep(sleepPeriod);}
					//Set TargetPrice with targetDelta
					result.ResetInitialTargetTrail(Close[0]+tpd, tpd); 
					//result.TargetTrail = null; //Do not use target.
					break;
				case "EN_VCBO_LOWERBREAKDOWN":
				case "EN_IRBOWOBB_LOWER_BREAKDOWN":
					result = TSG_TakeDefaultShort(oe); //Follow or Counter position
					result.tactics = "FOLLOW";
					//result.signalName = "SN_IRBOWOBB_SELL_FOLLOW"; //Does this need?
					result.priority = 3; //This trade has higer priority than default.
					//Set Stop on breakout level, TNKIRLow.
					// Calculate initialStopDelte and Size based on BO level nad Unit Risk
					initialStopPrice = TNKIRLow + boMarginByTic * TickSize ;  //Decide stop price first.
					initialStopDelta = initialStopPrice - Close[0];           //Then caliculate Delta.
					//ps = CalculatePositionSizeWithSLD(initialStopDelta);
					pr = CalculatePositionRiskWithStopDelta(riskToTake, initialStopDelta); //New method for caluculate trade risk
					initialStopDelta = pr.StopDelta;
					initialStopPrice = Close[0] + initialStopDelta;
					// Set new initialStopDelta and Size.
					result.ResetInitialStopTrail(initialStopPrice,initialStopDelta);
					//result.size = ps.Size;
					result.size = pr.Size;
					if(result.StopTrail!=null){result.StopTrail.Sleep(sleepPeriod);}
					//Set TargetPrice with targetDelta  HOT!!!
					result.ResetInitialTargetTrail(Close[0]-tpd, tpd);
					//result.TargetTrail = null; //Do not use target.
					break;
				default:
					LogError("EHG_IRBOWOBB_SimpleBuyAndSell()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			LogDebug("EHG_IRBOWOBB_SimpleBuyAndSell()",MemoND("initialStopDelta",initialStopDelta),MemoNI("pr.size",pr.Size));
			return result;
		}
		
		// 
		// Initial Range Breakout
		//  2017/11/30 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_IRB_SimpleBuyAndSell(ObservedEvent oe){
			//Initialize TradeSpec as return value.
			TradeSpec result = null;
			// Correct enviromental vaues
			double TwoSigma = StdDev(14)[0] * 2.0; 
			double tpd = TwoSigma; // Target Price DELTA is Sigma * 2.0 //TEST
			//

			//LogDebug("EHG_IRB_SimpleBuyAndSell()",MemoNS("lastExitName",lastExitName),
			//	MemoNB("IsProfitTarget()",IsProfitTarget(lastExitName)),MemoNB("IsStopLoss()",IsStopLoss(lastExitName)),MemoNB("IsSessionClose()",IsSessionClose(lastExitName))
			//);
			//
			//Dispach with event name to decide action
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_IRB_UPPER_BREAKUP":
				case "EN_IR_UP_MOMENTUM":
					result = TSG_TakeDefaultLong(oe); //Follow or Counter position
					result.tactics = "FOLLOW";
					result.signalName = "SN_IRB_BUY_FOLLOW";
					result.priority = 4;  //This trade has higer priority than default.
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				case "EN_IRB_LOWER_BREAKDOWN":
				case "EN_IR_DOWN_MOMENTUM":
					result = TSG_TakeDefaultShort(oe); //Follow or Counter position
					result.tactics = "FOLLOW";
					result.signalName = "SN_IRB_SELL_FOLLOW";
					result.priority = 4; //This trade has higer priority than default.
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				default:
					LogError("EHG_IRB_SimpleBuyAndSell()",MemoNS("Illeagal eventName",eventName));
					break;
			}
//			//
//			// Conpensate Profit target along prior trades <= OBSOLETED
//			//
//			TradeSpec ltts = GetTerminatedTradeSpec(0);
//			if(ltts != null && ltts.IsProfitTarget() && result.IsSignalEqual(ltts)){
//				//Use same TargetDelta of privous success trade.
//				LogDebug("EHG_IRB_SimpleBuyAndSell()",MemoNB("IsProfitTarget()",ltts.IsProfitTarget()),MemoND("tpd",tpd),MemoND("ltts.initialTargetDelta",ltts.initialTargetDelta));
//				result.setTargetDeltaAndModTargetPrice(ltts.initialTargetDelta); //Set TargetPrice with targetDelta
//			}else{
//				result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
//			}
			return result;
		}
		
		// 
		// Cross Volatility Band (VB) and SMA Handler 
		//  2017/08/2 Created
		//  Input: OvservedEvent
		//  Output: TradeSpec
		public TradeSpec EHG_VAC_VBCrossSMA(ObservedEvent oe){
			//Initialize TradeSpec as return value.
			TradeSpec result = null;
			// Correct enviromental vaues
			double tpd = 0.0; //Initailize
			double TwoSigma = StdDev(14)[0] * 2.0; 
			
			//Dispach with event name to decide action
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_VAC_HIGH_CROSS_UP":	//Follow
				case "EN_VAC_UP_MOMENTUM":		//Follow
				//case "EN_VAC_LOW_CROSS_DOWN":	//Counter
					result = TSG_TakeDefaultLong(oe); //Take Long position
					result.tactics = "FOLLOW";
					result.signalName = "SN_VAC_BUY_FOLLOW";
					result.priority = 4;  //This trade has higer priority than default.
					tpd = TwoSigma;
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				//case "EN_VAC_HIGH_CROSS_UP":	//Counter
				case "EN_VAC_LOW_CROSS_DOWN":	//Follow
				case "EN_VAC_DOWN_MOMENTUM":	//Follow
					result = TSG_TakeDefaultShort(oe); //Take Short position
					result.tactics = "FOLLOW";
					result.signalName = "SN_VAC_SELL_FOLLOW";
					result.priority = 4; //This trade has higer priority than default.
					tpd = TwoSigma;
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				default:
					LogError("EHG_VAC_VBCrossSMA()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			LogDebug("EHG_VAC_VBCrossSMA()",MemoNS("eventName",eventName),MemoND("Close",Close[0]),MemoND("TwoSigma",TwoSigma));
			return result;
		}

		//
		//  Trend Sensitive Normarized Breakout Handler
		//    Create: 2016/09/11
		//    v1.0 2016/09/13
		// 
		public TradeSpec EHG_NB_TrendSensitiveBuyAndSell(ObservedEvent oe){
			// Define EVENT/TREND/SIGNAL TABLE
			//  ETS_TBL_xxxx  (Event-Trend-Signal Table)			
			// No Counter  Same behavier/performance as v2.4.1 
			Dictionary<string, Dictionary<string, string>> ETS_TBL_AllFollow = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","BUY"}, {"EXPAND","BUY"}, {"SHRINK","BUY"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","BUY"}, {"EXPAND","BUY"}, {"SHRINK","BUY"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","SELL"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","SELL"}}}
			};
			// All Counter (couner when All trend/event ) 
			Dictionary<string, Dictionary<string, string>> ETS_TBL_AllCounter = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","SELL_COUNTER"},{"DOWN","SELL_COUNTER"},{"EXPAND","SELL_COUNTER"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","SELL_COUNTER"},{"DOWN","SELL_COUNTER"},{"EXPAND","SELL_COUNTER"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","BUY_COUNTER"}, {"EXPAND","BUY_COUNTER"}, {"SHRINK","BUY_COUNTER"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","BUY_COUNTER"}, {"EXPAND","BUY_COUNTER"}, {"SHRINK","BUY_COUNTER"}}}
			};
			//  Against version (counter when UP or DOWN trend)
			Dictionary<string, Dictionary<string, string>> ETS_TBL_Against = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","SELL_COUNTER"},{"EXPAND","BUY"}, {"SHRINK","BUY"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","SELL_COUNTER"},{"EXPAND","BUY"}, {"SHRINK","BUY"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","SELL"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","SELL"}}}
			};
			// Shrink (couner when SHRINK)
			Dictionary<string, Dictionary<string, string>> ETS_TBL_Shrink = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","BUY"}, {"EXPAND","BUY"}, {"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","BUY"}, {"EXPAND","BUY"}, {"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","BUY_COUNTER"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","BUY_COUNTER"}}}
			};
			// Shrink with NOACTION (couner when SHRINK)
			Dictionary<string, Dictionary<string, string>> ETS_TBL_Shrink_NoAction = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","NOACTION"},{"EXPAND","NOACTION"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","NOACTION"},{"EXPAND","NOACTION"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","NOACTION"},{"EXPAND","NOACTION"},{"SHRINK","BUY_COUNTER"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","SELL"},{"DOWN","NOACTION"},{"EXPAND","NOACTION"},{"SHRINK","BUY_COUNTER"}}}
			};
			// Ideal (Follow in expand side and couner in shrink side)
			Dictionary<string, Dictionary<string, string>> ETS_TBL_Ideal = new Dictionary<string, Dictionary<string, string>>(){
				{"EN_NB_UPPERBREAKUP",  new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","SELL_COUNTER"},{"EXPAND","BUY"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_UPMOMENTUM",    new Dictionary<string, string>(){{"UP","BUY"}, {"DOWN","SELL_COUNTER"},{"EXPAND","BUY"},{"SHRINK","SELL_COUNTER"}}},
				{"EN_NB_LOWERBREAKDOWN",new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","BUY_COUNTER"}}},
				{"EN_NB_DOWNMOMENTUM",  new Dictionary<string, string>(){{"UP","BUY_COUNTER"}, {"DOWN","SELL"},{"EXPAND","SELL"},{"SHRINK","BUY_COUNTER"}}}
			};
			Dictionary<string, Dictionary<string, string>> ETC_TBL_OUTSIDE_PBBand = ETS_TBL_AllFollow; //ETS_TBL_Shrink; //ETS_TBL_AllFollow;
			Dictionary<string, Dictionary<string, string>> ETC_TBL_INSIDE_PBBand  = ETS_TBL_Shrink_NoAction; //ETS_TBL_Shrink; //ETS_TBL_Ideal; //ETS_TBL_Shrink_NoAction; //ETS_TBL_AllCounter;
			Dictionary<string, Dictionary<string, string>> ETC_TBL_BIGBAR = ETS_TBL_Shrink; //Tempral
			
			// default ETS Table
			Dictionary<string, Dictionary<string, string>> etsTable =　ETC_TBL_OUTSIDE_PBBand; 
			//
			TradeSpec result = null;
			double gapMargin = 0.0; //Marginf for cheking if bottom/peak was changed.
			string eventName = oe.eventName;
			string trend = TrendCheckWithPeakBottom(gapMargin); //@TNKGauge.cs
			//
			// Take break posision
			//  Option: Close or Breakout Band value
			double breakValue = Close[CurrentBar - oe.barNumber]; //should be identical to Close[0]
			//double breakValue = oe.boBandValue; //BOBand value of braek side. set in ObserveNormalizedBreakout
			string breakPosition = BreakPositionForPBBand(breakValue);    //@TNKGauge.cs
			//
			// Select ETS Table
			// Check Big Bar situatuion
			//TBI:Get Bigbar situation
			string bigBar = "BB_NOBB"; //"BB_POSITIVE", "BB_NEGATIVE"
			switch(bigBar){
				case "BB_POSITIVE":
				case "BB_NEGATIVE":
					etsTable =　ETC_TBL_BIGBAR;
					break;
				case "BB_NOBB":
					//Check Break Position for PBBand
					switch (breakPosition){
						case "INSIDE":
							etsTable =　ETC_TBL_INSIDE_PBBand;
							break;
						case "UPSIDE": 
						case "DOWNSIDE":
							etsTable =　ETC_TBL_OUTSIDE_PBBand;
							break;
						default:
							LogError("EHG_NB_TrendSensitiveBuyAndSell()",MemoNS("Illeagal breakPosition",breakPosition));
							break;
					}
					break;
				default:
					LogError("EHG_NB_TrendSensitiveBuyAndSell()",MemoNS("Illeagal bigBar",bigBar));
					break;
			}
			//Decide signal with ETS Table.
			string action = etsTable[eventName][trend];
			switch (action){
				case "NOACTION":
					result = TSG_TakeDefaultNoAction(oe);
					result.tactics = "NOACTION";
					result.signalName = "SN_NB_NOACTION";
					break;
				case "BUY":
					result = TSG_TakeDefaultLong(oe);
					result.tactics = "FOLLOW";
					result.signalName = "SN_NB_BUY_FOLLOW";
					break;
				case "BUY_COUNTER":
					result = TSG_TakeDefaultLong(oe);
					result.tactics = "COUNTER";
					result.signalName = "SN_NB_BUY_COUNTER";
					break;
				case "SELL":
					result = TSG_TakeDefaultShort(oe);
					result.tactics = "FOLLOW";
					result.signalName = "SN_NB_SELL_FOLLOW";
					break;
				case "SELL_COUNTER":
					result = TSG_TakeDefaultShort(oe);
					result.tactics = "COUNTER";
					result.signalName = "SN_NB_SELL_COUNTER";
					break;
				default:
					LogError("EHG_NB_TrendSensitiveBuyAndSell()",MemoNS("Illeagal action",action));
					break;
			}
			LogDebug("EHG_NB_TrendSensitiveBuyAndSell()",MemoNS("eventName",eventName),MemoNS("trend",trend),MemoNS("bigBar",bigBar),MemoNS("breakPosition",breakPosition),MemoNS("action",action));
			return result;
		}
		
		//
		// Event handler for Devil's Maney Collection (DMC)
		//
		public TradeSpec EHG_DC_Getback(ObservedEvent oe){
			TradeSpec ts = null;
			string eventName = oe.eventName;
			double targetPrice = (double)oe.addtionalData;
			switch (eventName){
				case "EN_DC_UP":
					ts = TSG_TakeQuickLong(oe); //Counter position
					ts.priority = 4; //This trade has higer priority than default.
					if(targetPrice>Close[0]){
						double tpd = targetPrice - Close[0];
//OBSOLETED				ts.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					}
					break;
				case "EN_DC_DOWN":
					ts = TSG_TakeQucikShort(oe); //Counter position
					ts.priority = 4; //This trade has higer priority than default.
					if(targetPrice<Close[0]){
						double tpd = Close[0] - targetPrice;
//OBSOLETED				ts.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					}
					break;
				default:
					//Do nothing
					LogError("EHG_DC_Getback()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			return ts;
		}



		//
		// Event handler for Continuous Bar events
		//  2017/03/18 Ver 1.0
		public TradeSpec EHG_CD_SimpleBuyAndSell(ObservedEvent oe){
			TradeSpec result = null;
			string eventName = oe.eventName;
			switch(eventName){
				case "EN_CD_CONTINUOUS_POSITIVE":
					result = TSG_TakeDefaultLong(oe);
					break;
				case "EN_CD_CONTINUOUS_NEGATIVE":
					result = TSG_TakeDefaultShort(oe);
					break;
				default:
					LogError("EHG_CD_SimpleBuyAndSell()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			return result;
		}
		
		//
		// Event handler for BigBar events
		//  2017/02/16 Rename from EHG_BB_SimpleBuyAndSell()
		public TradeSpec EHG_BB_CounterBuyAndSell(ObservedEvent oe){
			TradeSpec result = null;
			double tpd = Move(0); // Target Price is open price of this bar.
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_BB_BIGNEGATIVEBAR":
					result = TSG_TakeDefaultLong(oe); //Counter position
					result.tactics = "COUNTER";
					result.signalName = "SN_BB_BUY_COUNTER";
					result.priority = 4;  //This trade has higer priority than default.
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				case "EN_BB_BIGPOSITIVEBAR":
					result = TSG_TakeDefaultShort(oe); //Counter position
					result.tactics = "COUNTER";
					result.signalName = "SN_BB_SELL_COUNTER";
					result.priority = 4; //This trade has higer priority than default.
//OBSOLETED			result.setTargetDeltaAndModTargetPrice(tpd); //Set TargetPrice with targetDelta
					break;
				default:
					LogError("EHG_BB_BigBarReversalTrade()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			return result;
		}
		
		//
		//  Simple Buy and Sell
		//    Use in v2.4.1
		public TradeSpec EHG_NB_SimpleBuyAndSell(ObservedEvent oe){
			TradeSpec result = null;
			string eventName = oe.eventName;
			switch (eventName){
				case "EN_NB_UPPERBREAKUP":
					result = TSG_TakeDefaultLong(oe);
					break;
				case "EN_NB_LOWERBREAKDOWN":
					result = TSG_TakeDefaultShort(oe);
					//result = TSG_TakeQucikShort(oe);  //bad perfomance
					break;
				case "EN_NB_UPMOMENTUM":
					result = TSG_TakeDefaultLong(oe);
					break;
				case "EN_NB_DOWNMOMENTUM":
					result = TSG_TakeDefaultShort(oe);
					//result = TSG_TakeQucikShort(oe); //bad performance
					break;
				default:
					LogError("EHG_NB_SimpleBuyAndSell()",MemoNS("Illeagal eventName",eventName));
					break;
			}
			return result;
		}

		//!!!!!!!
		//Below *_TD thins are traditional. These are going to be obsoleted.
		//!!!!!!!
		
		//===
		// Trend Direction sensitive handlers(TD)
		//  Hnandler genarate siganals based on Trend "UP", "DOWN", "UNCLEAR"
		//===
		public TradeSpec EHG_TD_BuyOnUpSellOnUnclear(ObservedEvent oe){
			TradeSpec result = null;
			string trendDirection = TrendDirection();
			LogDebug("EHG_TD_BuyOnUpSellOnUnclear()",MemoNS("trendDirection",trendDirection));
			switch (trendDirection) {
				case "UP":
					result = TSG_TakeDefaultLong(oe);
					break;
				case "UNCLEAR":
					result = TSG_TakeDefaultShort(oe);
					break;
				default:
					LogError("EHG_TD_BuyOnUpSellOnUnclear()",MemoNS("Illeagal trendDirection",trendDirection));
					break;
			}
			return result;
		}

		public TradeSpec EHG_TD_SellOnDownBuyOnUnclear(ObservedEvent oe){
			TradeSpec result = null;
			string trendDirection = TrendDirection();
			LogDebug("EHG_TD_SellOnDownBuyOnUnclear()",MemoNS("trendDirection",trendDirection));
			switch (trendDirection) {
				case "DOWN":
					result = TSG_TakeDefaultShort(oe);
					break;
				case "UNCLEAR":
					result = TSG_TakeDefaultLong(oe);
					break;
				default:
					LogError("CHG_TD_SellOnDownBuyOnUnclear()",MemoNS("Illeagal trendDirection",trendDirection));
					break;
			}
			return result;
		}
		
		public TradeSpec EHG_TD_BuyOnUp(ObservedEvent oe){
			TradeSpec result = null;
			string trendDirection = TrendDirection();
			LogDebug("EHG_SellOnLongRangeUp()",MemoNS("trendDirection",trendDirection));
			if(trendDirection=="UP") {
				result = TSG_TakeDefaultLong(oe);
			}
			return result;
		}

		public TradeSpec EHG_TD_SellOnDown(ObservedEvent oe){
			TradeSpec result = null;
			string trendDirection = TrendDirection();
			LogDebug("EHG_BuyOnLongRangeDown()",MemoNS("trendDirection",trendDirection));
			if(trendDirection=="DOWN") {
				result = TSG_TakeDefaultShort(oe);
			}
			return result;
		}
		
		//===
		//　　Mandatory Handlers
		//    Do NOT remove fallowing EHG_DEFAULT(),EHG_BAD_CONDITION() handlers. 
		//===
		// Default Condition Handler
		public TradeSpec EHG_DEFAULT(ObservedEvent oe){
			//LogDebug("EHG_DEFAULT",MemoNS("oe.eventName",oe.eventName));
			return null;
		}	
		//Bad Condition Handler
		public TradeSpec EHG_BAD_CONDITION(ObservedEvent oe){
			LogError("EHG_BAD_CONDITION()",MemoNS("oe.eventName",oe.eventName));
			return null;
		}
    }
}
