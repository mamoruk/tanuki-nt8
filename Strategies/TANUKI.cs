#region Using declarations
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.SuperDom;
using NinjaTrader.Gui.Tools;
using NinjaTrader.Data;
using NinjaTrader.NinjaScript;
using NinjaTrader.Core.FloatingPoint;
using NinjaTrader.NinjaScript.Indicators;
using NinjaTrader.NinjaScript.DrawingTools;
#endregion

//This namespace holds Strategies in this folder and is required. Do not change it. 
namespace NinjaTrader.NinjaScript.Strategies
{
	public partial class TANUKI : Strategy
	{
		protected override void OnStateChange()
		{
			if (State == State.SetDefaults)
			{
				Description									= @"TANUKI on NT8";
				Name										= "TANUKI";
				Calculate									= Calculate.OnBarClose;
				EntriesPerDirection							= 1;
				EntryHandling								= EntryHandling.AllEntries;
				IsExitOnSessionCloseStrategy				= true;
				ExitOnSessionCloseSeconds					= 30;
				IsFillLimitOnTouch							= false;
				MaximumBarsLookBack							= MaximumBarsLookBack.TwoHundredFiftySix;
				OrderFillResolution							= OrderFillResolution.Standard;
				Slippage									= 0;
				StartBehavior								= StartBehavior.WaitUntilFlat;
				TimeInForce									= TimeInForce.Gtc;
				TraceOrders									= false;
				RealtimeErrorHandling						= RealtimeErrorHandling.StopCancelClose;
				StopTargetHandling							= StopTargetHandling.PerEntryExecution;
				BarsRequiredToTrade							= 20;
				// Disable this property for performance gains in Strategy Analyzer optimizations
				// See the Help Guide for additional information
				IsInstantiatedOnEachOptimizationIteration	= true;
				DEBUG					= true;
				LogFolderPath					= @"Z:\tanuki\NinjaTrader\Logs";
				RiskByPrice					= 500;
				TradeUnit					= 50;
				PositionMinimum					= 0;
				PositionMaximum					= 5;
			}
			else if (State == State.Configure)
			{
				Log("Staring TANUKI Configure.",LogLevel.Information);
				TNKDoesLogFile = true;
				TNKLogFolderName = LogFolderPath;
				TNKDebug = DEBUG;
				TNKPositionMinimum = PositionMinimum;
				TNKPositionMaximum = PositionMaximum;
      			// Add a Day Bars object - BarsInProgress index = 1 
        		AddDataSeries(BarsPeriodType.Day, 1);
				//RiskByPrice					= 500;
				//TradeUnit					= 50;
				//PositionMinimum					= 0;
				//PositionMaximum					= 10;
				//TANUKI_Initialize();
			}
			else if (State == State.DataLoaded)
			{
				Log("Staring TANUKI DataLoaded.",LogLevel.Information);
				Print(Time[0]);
				//Tanuki own initialization
				TANUKI_Initialize();
				
				//Add indicator to chart
				add_indicators();
			}
			else if (State == State.Historical)
			{
				Log("Staring TANUKI Histrical.",LogLevel.Information);
				//Print(Time[0]);
			}
		}

		protected override void OnBarUpdate()
		{
		//Log("Enter OnBarUpdate().",LogLevel.Information);
		//Print(Time[0]);
		//
		// Skip all addtional time frame's bar update
		//
    	if (BarsInProgress > 0) {
			return;
		}
    	//if (CurrentBars[1] > 0) {
		//	LogDebug("OnBarUpdate",TAGMsg("Primary"),MemoNI("CurrentBars[1]",CurrentBars[1]),MemoND("Opens[0][0]",Opens[0][0]));
		//	LogDebug("OnBarUpdate",TAGMsg("2ndary"),MemoNS("Times[1][0]",Times[1][0].ToString()),MemoND("Opens[1][0]",Opens[1][0]));
		//	if(CurrentBar>0) LogDebug("OnBarUpdate",TAGMsg("2ndary"),MemoNS("Times[1][1]",Times[1][1].ToString()),MemoND("Opens[1][1]",Opens[1][1]));
		//}

		//===============
		//  Set Up
		//===============
		//LogTS("I'm in OnBarUpdate...");
			//Log Insturument name, FromDate(NA), ToDate(NA)
			LogHeader();
			// Log Account Info
			LogAccountInfo();
			
			//===================================
			//  Do update task for every start of bar update
			//		TNKUpdaters.cs
			//===================================
			UpdatePreamble();
			
			//==============================================
			// Observe Events
			//      TNKSetupProcedures.cs 
			//==============================================
			//  Event Category "MC" MarketCondition method, "ND" Normal Distibution method.
			List <ObservedEvent> eventList = ObserveEvent();
			
			//===============
			//  Make position when  current bar met BarsRequiredToTrade 
			//===============
			if(CurrentBar >= BarsRequiredToTrade){			
				//==============================================
				// Generate Signals
				//		TNKSetupProcedures.cs
				//		TNKSinalProcedures.cs
				//		TNKConditionHandlers.cs
				//==============================================
				//
				// Process observed events in TNKRecentObservedEventList
				// Condtion Hander(CH) calls Signal Generator(SG)
				// SG add signals to TNKSignalPackets
				//  These signals are fetched via TNKFetchSignal()
				List<TradeSpec>　tradeSpecList = GenerateSignals(eventList);
				
				//==============================================
				// GExectuiotn
				//		TNKExection.cs
				//==============================================
				//
				// Execute each Signals which are feched form TNKSignalList
				//
				ExecuteSignalList(tradeSpecList);
				
				//===================================
				//  Ajust Stop&Target
				//		TNKTradeManageProcedure.cs
				//===================================
				AjustTrade();	
			}
			//===================================
			//  Do update task for every end of bar update
			//		TNKUpdaters.cs
			//===================================
			UpdatePost();
			
		}
	protected void add_indicators()
		{
			Bollinger indBollinger = Bollinger(1.4, 14);
			SMA indSMA = SMA(72);
			// Set up color
			indSMA.Plots[0].Brush = Brushes.Blue;
			// Add to chart
			AddChartIndicator(indBollinger);
			AddChartIndicator(indSMA);
		}
		
	protected void TANUKI_Initialize()
        {
			Print("==========="); //DEBUGPRINT
			Print("INIT-LogFile"); //DEBUGPRINT
			//Set up New log file name
			TNKLogFolderName = LogFolderPath;
			string theFilename = NewLogFileName();
			Print(string.Format("theFilename:{0}",theFilename));//DEBUGPRINT
			SetLogFileName(theFilename);
			Print("SetLogFileName() was passed.");//DEBUGPRINT
			TNKLog("TestString");  //DEBUG
			TNKLogTS("Message of TNKLogTS()");
			//TNKLogTST("TAG","Message");
			Print("I'm here !!!"); //DEBUGPRINT
			//
			LogMessage("TANUKI_Initialize()",MemoNS("TNKLogFileName",TNKLogFileName));
			LogDebug("TANUKI_Initialize()","Start");
			//
			Print("INIT-Set Version"); //DEBUGPRINT
			//Version Number
			TNKVersionNumber = "3.9.26";
			TNKVersionDescription = "2019.09.08 Stop to use SFIL_Reject_SameDirectionWithLargePrivousDay ";
			TNKVersionBTPerformance = "41/1.72/971/1556k/-376k/1.6k";
			Print("INIT-Will Wirte Version to file."); //DEBUGPRINT
			LogVersion();
			//
			// Log Account Info
			LogMessage("TANUKI_Initialize()",MemoNS("Account.Name",Account.Name),
				MemoND("CashValue",Account.Get(AccountItem.CashValue, Currency.JapaneseYen)),
				MemoND("BuyingPower",Account.Get(AccountItem.BuyingPower, Currency.JapaneseYen))
			);
			
            //=========================
			// General Initailization
			//==========================
			TNKCandlePool = new CandlePool(this);
			
			//=====================================
			// Initalize method for each component
			//=====================================
			//SWING().Plots[0].Pen.Color = Color.Red;  //ISSUE:Draw Swing Chart 
			//EMA(EMASlow).Plots[0].Pen.Color = Color.Blue;
			//Add(EMA(EMASlow));
			//
			//Add(Swing(TNKSwingPeriod)); //DEBUG Swing() goes worng after execute this line.
			//
			//AccountSize = 0;
//			AccountSize = 10000000; //ISSUE tempral for debugging. Yen.
//			AccountSize = 1000000; //ISSUE tempral for debugging. Yen.
			//AccountSize = AccountSize/100; // Convert To $ from Yen when if AccountSize hold yen.
			//
			InitUtil(); //Inisitiarize some iVar for TNK frameowrk.
			TNKRejectAllSignals = false;
			TNKDoCapInitialRisk = false;
			//TNKPositionMinimum = 1; //For DEBUG Ordinary set to 0 //ISSUE:for signal debug.Ordinary should be 0
			//TNKPositionMaximum = 5; //ISSUE  this value is for PAPER ACCOUNT of InteractiveBrokers
			//
			TNKVolatilityRiskFactor = 2.0; //v1.31 set 2.0 Temprally
			//
			// Initialize Allowed and Disallowd Condtions 
			//	TNKCondtionControlLists.cs
			TNKSetupConditionControl();
			//
		}
		
		#region Properties
		[NinjaScriptProperty]
		[Display(ResourceType = typeof(Custom.Resource), Name="DEBUG", Description="Debug Log siwtch", Order=1, GroupName="NinjaScriptStrategyParameters")]
		public bool DEBUG
		{ get; set; }

		[NinjaScriptProperty]
		[Display(ResourceType = typeof(Custom.Resource), Name="LogFolderPath", Description="Log Folder path", Order=2, GroupName="NinjaScriptStrategyParameters")]
		public string LogFolderPath
		{ get; set; }

		[NinjaScriptProperty]
		[Range(500, double.MaxValue)]
		[Display(ResourceType = typeof(Custom.Resource), Name="RiskByPrice", Description="RiskByPrice", Order=3, GroupName="NinjaScriptStrategyParameters")]
		public double RiskByPrice
		{ get; set; }

		[NinjaScriptProperty]
		[Range(50, int.MaxValue)]
		[Display(ResourceType = typeof(Custom.Resource), Name="TradeUnit", Description="Risk by Trade Unit", Order=4, GroupName="NinjaScriptStrategyParameters")]
		public int TradeUnit
		{ get; set; }

		[NinjaScriptProperty]
		[Range(0, int.MaxValue)]
		[Display(ResourceType = typeof(Custom.Resource), Name="PositionMinimum", Description="PositionMinimum", Order=5, GroupName="NinjaScriptStrategyParameters")]
		public int PositionMinimum
		{ get; set; }

		[NinjaScriptProperty]
		[Range(1, int.MaxValue)]
		[Display(ResourceType = typeof(Custom.Resource), Name="PositionMaximum", Description="PositionMaximum", Order=6, GroupName="NinjaScriptStrategyParameters")]
		public int PositionMaximum
		{ get; set; }
		#endregion

	}
}